//
//  AppDelegate.h
//  LoneRanger
//
//  Created by Vitaliy Kolomoets on 5/14/18.
//  Copyright © 2018 None. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

