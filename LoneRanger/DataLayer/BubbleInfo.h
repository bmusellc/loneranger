//
//  Bubble.h
//  Unity-iPhone
//
//  Created by Artem Manzhura on 6/4/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>

@interface BubbleInfo : NSObject

@property (readonly)    NSMutableArray*         speeches;
@property (readonly)    NSString*               imagePath;
@property (assign)      CGPoint                 origin;

+ (BubbleInfo*)bubbleFromDictionary:(NSDictionary*)dict WithRootPath:(NSString*)path;

- (NSDictionary*)dictionaryRepresentation;

@end
