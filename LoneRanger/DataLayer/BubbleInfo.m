//
//  Bubble.m
//  Unity-iPhone
//
//  Created by Artem Manzhura on 6/4/13.
//
//

#import "BubbleInfo.h"
#import <UIKit/UIKit.h>

@interface BubbleInfo()
{
}
@end

@implementation BubbleInfo

- (void)dealloc
{
    [_speeches release];
    [_imagePath release];
    [super dealloc];
}

- (id)init
{
    self = [super init];
    if (self) {
        _speeches = [[NSMutableArray alloc] initWithCapacity:0];
    }
    return self;
}

+ (BubbleInfo*)bubbleFromDictionary:(NSDictionary*)dict WithRootPath:(NSString*)path
{
	BubbleInfo* bubbleInfo = [[[self alloc]init]autorelease];
    
    if([dict objectForKey: @"name"])
    {
        NSString* fullPath = [dict objectForKey: @"name"];
		
		if (fullPath.length > 0)
		{
            NSString* pathComponent = @"pages/bubbleImages";
			bubbleInfo->_imagePath = [[path stringByAppendingPathComponent:[pathComponent stringByAppendingPathComponent:fullPath]] retain];
		}
    }
    
    bubbleInfo->_origin = CGPointFromString([dict objectForKey: @"origin"]);
    
    if([dict objectForKey: @"speeches"])
    {
        NSArray* speeches = [dict objectForKey: @"speeches"];
        
        for (NSString* speech in speeches)
        {
            [bubbleInfo->_speeches addObject: [path stringByAppendingPathComponent:speech]];
        }
    }
	return bubbleInfo;
}

- (NSDictionary*)dictionaryRepresentation
{
//	NSString* imagePath = @"";
	
//	if (self.imagePath)
//	{
//		NSArray* pathComponents = [self.imagePath pathComponents];
//		pathComponents = [pathComponents subarrayWithRange:NSMakeRange(pathComponents.count - 3, 3)];
//		
//		imagePath = [NSString pathWithComponents:pathComponents];
//	}
	
	NSMutableArray* speeches = [NSMutableArray array];
	
	[self.speeches enumerateObjectsUsingBlock:^(NSString* path, NSUInteger idx, BOOL *stop) {
		
		NSArray* pathComponents = [path pathComponents];
		pathComponents = [pathComponents subarrayWithRange:NSMakeRange(pathComponents.count - 2, 2)];
		
		[speeches addObject:[NSString pathWithComponents:pathComponents]];
	}];
	
	return @{@"name" : self.imagePath, @"origin" : NSStringFromCGPoint(self.origin), @"speeches" : speeches};
}

@end
