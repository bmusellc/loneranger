//
//  DeviceModel.h
//  RussellJamesPhotobook
//
//  Created by Vitaliy Kolomoets on 7/30/13.
//  Copyright (c) 2013 bMuse Ukraine. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum
{
    DM_UNKNOWN,
    DM_IPHONE_1G,
    DM_IPHONE_3G,
    DM_IPHONE_3GS,
    DM_IPHONE_4,
    DM_IPHONE_4S,
    DM_IPOD_TOUCH_1G,
    DM_IPOD_TOUCH_2G,
    DM_IPOD_TOUCH_3G,
    DM_IPOD_TOUCH_4G,
    DM_IPAD,
    DM_IPAD_2,
    DM_IPAD_3,
    DM_IPAD_4,
    DM_SIMULATOR,
}DeviceModelType;

@interface DeviceModel : NSObject

+ (NSString*)deviceModel;//iPhone4S, iPad2, etc
+ (DeviceModelType)deviceModelType;

@end
