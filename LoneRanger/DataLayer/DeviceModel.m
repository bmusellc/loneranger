//
//  DeviceModel.m
//  RussellJamesPhotobook
//
//  Created by Vitaliy Kolomoets on 7/30/13.
//  Copyright (c) 2013 bMuse Ukraine. All rights reserved.
//

#import "DeviceModel.h"
#include <sys/types.h>
#include <sys/sysctl.h>

#define TREAT_SIMULATOR_AS_DEVICE_WITH_INDEX        DM_IPAD_4

static DeviceModel*     instanse = nil;

@interface DeviceModel()
{
    NSArray*        _models;
    DeviceModelType _modelType;
}
+ (DeviceModel*)sharedInstanse;
@end

@implementation DeviceModel

- (id)init
{
    self = [super init];
    if (self)
    {
        _models = @[@{@"originalName" : @"iPhone1,1", @"commonName": @"", @"" : @(DM_IPHONE_1G)},
                    @{@"originalName" : @"iPhone1,2", @"commonName": @"", @"" : @(DM_IPHONE_3G)},
                    @{@"originalName" : @"iPhone2,1", @"commonName": @"", @"" : @(DM_IPHONE_3GS)},
                    @{@"originalName" : @"iPhone3,1", @"commonName": @"", @"" : @(DM_IPHONE_4)},
                    @{@"originalName" : @"iPhone4,1", @"commonName": @"", @"" : @(DM_IPHONE_4S)},
                    @{@"originalName" : @"iPod1,1", @"commonName": @"", @"" : @(DM_IPOD_TOUCH_1G)},
                    @{@"originalName" : @"iPod2,1", @"commonName": @"", @"" : @(DM_IPOD_TOUCH_2G)},
                    @{@"originalName" : @"iPod3,1", @"commonName": @"", @"" : @(DM_IPOD_TOUCH_3G)},
                    @{@"originalName" : @"iPod4,1", @"commonName": @"", @"" : @(DM_IPOD_TOUCH_4G)},
                    @{@"originalName" : @"iPad1,1", @"commonName": @"", @"" : @(DM_IPAD)},
                    @{@"originalName" : @"iPad2,1", @"commonName": @"iPad2", @"value" : @(DM_IPAD_2)},
                    @{@"originalName" : @"iPad2,2", @"commonName": @"iPad2", @"value" : @(DM_IPAD_2)},
                    @{@"originalName" : @"iPad3,1", @"commonName": @"iPad3", @"value" : @(DM_IPAD_3)},
                    @{@"originalName" : @"iPad3,2", @"commonName": @"iPad3", @"value" : @(DM_IPAD_3)},
                    @{@"originalName" : @"iPad4,1", @"commonName": @"iPad4", @"value" : @(DM_IPAD_4)},
                    @{@"originalName" : @"iPad4,2", @"commonName": @"iPad4", @"value" : @(DM_IPAD_4)},
                    @{@"originalName" : @"i386", @"commonName": @"Simulator", @"value" : @(DM_SIMULATOR)},
                    @{@"originalName" : @"x86_64", @"commonName": @"Simulator", @"value" : @(DM_SIMULATOR)}];
        
        
        size_t size;
        sysctlbyname("hw.machine", NULL, &size, NULL, 0);
        char *machine = (char*)malloc(size);
        memset(machine, 0, size);
        sysctlbyname("hw.machine", machine, &size, NULL, 0);
        
        for( int i = 0; i < _models.count; i++)
        {
            if (strcmp(machine, [(NSString*)_models[i][@"originalName"] cStringUsingEncoding:NSUTF8StringEncoding]) == 0)
            {
                _modelType = i;
                break;
            }
        }
        
        free(machine);
    }
    return self;
}

+ (DeviceModel*)sharedInstanse
{
    if(!instanse)
    {
        instanse = [DeviceModel new];
    }
    return instanse;
}

+ (NSString*)deviceModel
{
    return [DeviceModel sharedInstanse]->_models[[DeviceModel sharedInstanse]->_modelType][@"commonName"];
}

+ (DeviceModelType)deviceModelType
{
    return [DeviceModel sharedInstanse]->_modelType;
}

@end
