//
//  Extentions.h
//  VideoMaps
//
//  Created by Vitaliy Kolomoets on 7/20/11.
//  Copyright 2011 iGeneration. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreGraphics/CoreGraphics.h>

@interface UIView(Extension)
@property (assign) CGPoint origin;
@property (assign) CGSize size;
@property (assign) CGFloat x;
@property (assign) CGFloat y;
@property (assign) CGFloat width;
@property (assign) CGFloat height;
@end

@interface UIButton(Extension)
- (void)addTargetForCommonEvent:(id)target action:(SEL)action;
@end

@interface UIBarButtonItem(Extension)
+ (UIBarButtonItem*)flexibleSpace;
+ (UIBarButtonItem*)fixedSpaceWithWidth:(CGFloat)width;
@end

@interface UIToolbar(Extension)
- (void)insertItem:(UIBarButtonItem*)item atIndex:(NSUInteger)index;
- (void)removeItemAtIndex:(NSUInteger)index;
- (void)replaceItemAtIndex:(NSUInteger)index withItem:(UIBarButtonItem*)item;
@end

//@interface NSArray(Extension)
//@property (readonly) NSUInteger count;
//@end

@interface NSTimer(Extension)
+ (NSTimer*)scheduledTimerWithTimeInterval:(NSTimeInterval)ti completion:(void (^)(NSTimer* timer))fireBlock repeats:(BOOL)yesOrNo;
@end

@interface UIColor(StringExtension)
+ (UIColor*)colorWithString:(NSString*)string;
@end

@interface UIViewController(Extension)
- (void)addSubview:(UIView*)subview;
@property (readonly) CGRect bounds;
@end

@interface NSMutableDictionary(Extension)
- (void)setUnsignedInteger:(NSUInteger)value forKey:(id)key;
- (void)setInteger:(NSInteger)value forKey:(id)key;
- (void)setFloat:(float)value forKey:(id)key;
- (void)setDouble:(double)value forKey:(id)key;
@end

@interface NSDictionary(Extension)
- (NSUInteger)unsignedIntegerForKey:(id)key;
- (NSInteger)integerForKey:(id)key;
- (float)floatForKey:(id)key;
- (double)doubleForKey:(id)key;
@end
