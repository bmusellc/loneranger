//
//  Extentions.m
//  VideoMaps
//
//  Created by Vitaliy Kolomoets on 7/20/11.
//  Copyright 2011 iGeneration. All rights reserved.
//

#import "Extentions.h"

#pragma mark -
#pragma mark UIView
#pragma mark -

@implementation UIView(Extension)

- (void)setOrigin:(CGPoint)origin
{
    CGRect frame = self.frame;
    frame.origin = origin;
    self.frame = frame;
}

- (void)setX:(CGFloat)x
{
    CGRect frame = self.frame;
    frame.origin.x = x;
    self.frame = frame;
}

- (void)setY:(CGFloat)y
{
    CGRect frame = self.frame;
    frame.origin.y = y;
    self.frame = frame;
}

- (void)setSize:(CGSize)size
{
    CGRect frame = self.frame;
    frame.size = size;
    self.frame = frame;
}

- (void)setWidth:(CGFloat)width
{
    CGRect frame = self.frame;
    frame.size.width = width;
    self.frame = frame;
}

- (void)setHeight:(CGFloat)height
{
    CGRect frame = self.frame;
    frame.size.height = height;
    self.frame = frame;
}

- (CGPoint)origin
{
	return self.frame.origin;
}

- (CGSize)size
{
	return self.frame.size;
}

- (CGFloat)x
{
    return self.frame.origin.x;
}

- (CGFloat)y
{
    return self.frame.origin.y;
}

- (CGFloat)width
{
    return self.frame.size.width;
}

- (CGFloat)height
{
    return self.frame.size.height;
}
@end

#pragma mark -
#pragma mark UIButton
#pragma mark -

@implementation UIButton(Extension)

- (void)addTargetForCommonEvent:(id)target action:(SEL)action
{
    [self addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
}
@end

#pragma mark -
#pragma mark UIBarButtonItem
#pragma mark -

@implementation UIBarButtonItem(Extension)

+ (UIBarButtonItem*)flexibleSpace
{
    return [[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil]autorelease];
}

+ (UIBarButtonItem*)fixedSpaceWithWidth:(CGFloat)width
{
    UIBarButtonItem* fixedSpace = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedSpace.width = width;
    return [fixedSpace autorelease];
}
@end

#pragma mark -
#pragma mark UIToolbar
#pragma mark -

@implementation UIToolbar(Extension)

- (void)insertItem:(UIBarButtonItem*)item atIndex:(NSUInteger)index
{
    NSArray* items = self.items;
    if ([items count] <= index) 
    {
        return;
    }
    NSMutableArray* newItems = [NSMutableArray arrayWithArray:items];
    [newItems insertObject:items atIndex:index];
    
    self.items = newItems;
}

- (void)removeItemAtIndex:(NSUInteger)index
{
    NSArray* items = self.items;
    if ([items count] <= index) 
    {
        return;
    }
    NSMutableArray* newItems = [NSMutableArray arrayWithArray:items];
    [newItems removeObjectAtIndex:index];
    
    self.items = newItems;
}

- (void)replaceItemAtIndex:(NSUInteger)index withItem:(UIBarButtonItem*)item
{
    NSArray* items = self.items;
    if ([items count] <= index || !item) 
    {
        return;
    }
    
    NSMutableArray* newItems = [NSMutableArray arrayWithArray:items];
    [newItems replaceObjectAtIndex:index withObject:item];
    
    self.items = newItems;
}
@end

#pragma mark -
#pragma mark NSArray
#pragma mark -

//@implementation NSArray(Extension)
//
//- (NSUInteger)count
//{
//    return [self count];
//}
//@end

#pragma mark -
#pragma mark NSTimer
#pragma mark -

typedef void (^TimerBlock)(NSTimer* timer);

@implementation NSTimer(Extension)

+ (void)executeBlockWithTimer:(NSTimer*)timer
{
    TimerBlock block = [timer userInfo];
    block(timer);
}

+ (NSTimer*)scheduledTimerWithTimeInterval:(NSTimeInterval)ti completion:(void (^)(NSTimer* timer))fireBlock repeats:(BOOL)yesOrNo
{
    return [self scheduledTimerWithTimeInterval:ti target:self selector:@selector(executeBlockWithTimer:) userInfo:[[fireBlock copy] autorelease] repeats:yesOrNo];
}

@end

#pragma mark -
#pragma mark UIColor
#pragma mark -

@implementation UIColor(StringExtension)

+ (UIColor*)colorWithString:(NSString*)string
{
	UIColor* res = [UIColor clearColor];
	
	if(string.length != 7)
	{
		return res;
	}
	
	if([string characterAtIndex: 0] != '#')
	{
		return res;
	}
	
	NSString* rgb[3] = {nil, nil, nil};
	
	rgb[0] = [string substringWithRange: (NSRange){1, 2}];
	rgb[1] = [string substringWithRange: (NSRange){3, 2}];
	rgb[2] = [string substringWithRange: (NSRange){5, 2}];
	
	if(!rgb[0] || !rgb[1] || !rgb[2])
	{
		return res;
	}
	
	int rgbf[3] = {0, 0, 0};
	
	sscanf([rgb[0] UTF8String], "%x", &rgbf[0]);
	sscanf([rgb[1] UTF8String], "%x", &rgbf[1]);
	sscanf([rgb[2] UTF8String], "%x", &rgbf[2]);
	
	res = [UIColor colorWithRed: (float)rgbf[0] / 255.0f
						  green: (float)rgbf[1] / 255.0f
						   blue: (float)rgbf[2] / 255.0f
						  alpha:1.0f];
	
	return res;
}
@end
#pragma mark -
#pragma mark UIViewController
#pragma mark -

@implementation UIViewController(Extension)

- (void)addSubview:(UIView*)subview
{
	[self.view addSubview:subview];
}

- (CGRect)bounds
{
	return self.view.bounds;
}
@end

@implementation NSMutableDictionary(Extension)

- (void)setUnsignedInteger:(NSUInteger)value forKey:(id)key
{
	[self setObject:[NSNumber numberWithUnsignedInteger:value] forKey:key];
}

- (void)setInteger:(NSInteger)value forKey:(id)key
{
	[self setObject:[NSNumber numberWithInteger:value] forKey:key];
}

- (void)setFloat:(float)value forKey:(id)key
{
	[self setObject:[NSNumber numberWithFloat:value] forKey:key];
}

- (void)setDouble:(double)value forKey:(id)key
{
	[self setObject:[NSNumber numberWithDouble:value] forKey:key];
}
@end

@implementation NSDictionary(Extension)
- (NSUInteger)unsignedIntegerForKey:(id)key
{
	return [[self objectForKey:key] unsignedIntegerValue];
}

- (NSInteger)integerForKey:(id)key
{
	return [[self objectForKey:key] integerValue];
}

- (float)floatForKey:(id)key
{
	return [[self objectForKey:key] floatValue];
}

- (double)doubleForKey:(id)key
{
	return [[self objectForKey:key] doubleValue];
}
@end