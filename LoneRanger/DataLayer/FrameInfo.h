//
//  FrameInfo.h
//  LoneRanger
//
//  Created by Artem Manzhura on 3/25/13.
//  Copyright (c) 2013 iGeneration. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "PageInfo.h"

typedef enum
{
    kFrameInfoTransitionSlideType = 0,
    kFrameInfoTransitionCrossfadeType,
}
FrameInfoTransitionType;

@interface FrameInfo : NSObject
{
    CGRect                      _rect;
    UIColor*                    _backgroundColor;
    FrameInfoTransitionType     _transitionType;
}

@property (readonly) FrameInfoTransitionType    transitionType;
@property (readonly) CGRect						rect;
@property (readonly) NSMutableArray*			ambientSounds;
@property (readonly) NSMutableArray*			bubbles;
@property (readonly) NSMutableArray*            sfx;

- (id)initWithParams:(NSDictionary*)params withRootPath:(NSString*)rootPath;

+ (FrameInfo*)pageInfoFromDictionary:(NSDictionary*)dict withRootPath:(NSString *)rootPath;

- (NSDictionary*)dictionaryRepresentation;

@end
