//
//  FrameInfo.m
//  LoneRanger
//
//  Created by Artem Manzhura on 3/25/13.
//  Copyright (c) 2013 iGeneration. All rights reserved.
//

#import "FrameInfo.h"
#import "Extentions.h"
#import "BubbleInfo.h"
#import "SfxInfo.h"

@implementation FrameInfo

- (void)dealloc
{
    [_ambientSounds release];
    [_bubbles release];
    [_sfx release];
	
    [super dealloc];
}

- (id)initWithParams:(NSDictionary*)params  withRootPath:(NSString*)rootPath
{
    if(self = [super init])
    {
        _ambientSounds = [[NSMutableArray alloc] initWithCapacity: 0];
        _bubbles = [[NSMutableArray alloc] initWithCapacity: 0];
        _sfx = [[NSMutableArray alloc] initWithCapacity: 0];
                
        if(![params objectForKey: @"Rect"])
		{
			NSLog(@"ERROR %s: Required parameter 'Rect' is missed for frame", __FUNCTION__);
			[self release];
			return nil;
		}
		else {
			_rect = CGRectFromString([params objectForKey: @"Rect"]);
		}
        
        if([params objectForKey: @"sfx"])
		{
            NSArray* sfx = [params objectForKey: @"sfx"];
			SfxInfo* sfxInfo = nil;
			for (NSDictionary* param in sfx)
			{
                sfxInfo = [SfxInfo sfxFromDictionary:param withRootPath:rootPath];
                [_sfx addObject:sfxInfo];
			}
		}
        
        if([params objectForKey: @"bubbles"])
		{
            NSArray* speeches = [params objectForKey: @"bubbles"];
			BubbleInfo* info = nil;
			for (NSDictionary* param in speeches)
			{
                info = [BubbleInfo bubbleFromDictionary:param WithRootPath:rootPath];
                [_bubbles addObject:info];
			}
		}
        
        if([params objectForKey: @"ambientSounds"])
		{
            NSArray* sounds = [params objectForKey: @"ambientSounds"];
			
			for (NSString* path in sounds)
			{
                [_ambientSounds addObject:path];
			}
		}
        
        if([params objectForKey: @"transitionType"])
		{
            NSString* type = [params objectForKey: @"transitionType"];
			_transitionType = [FrameInfo transitionTypeFromString:type];
		}
		else {
			_transitionType = kFrameInfoTransitionSlideType;
		}
    }
    
    return self;
}

+ (FrameInfo*)pageInfoFromDictionary:(NSDictionary*)dict withRootPath:(NSString *)rootPath
{
    FrameInfo* frame = [[[FrameInfo alloc] initWithParams:dict withRootPath:rootPath] autorelease];
	
    return frame;
}

+ (FrameInfoTransitionType)transitionTypeFromString:(NSString*)stringType
{
	FrameInfoTransitionType type = kFrameInfoTransitionSlideType;
	
	if ([stringType isEqualToString:@"crossfade"])
	{
		type = kFrameInfoTransitionCrossfadeType;
	}
	
	return type;
}

- (NSDictionary*)dictionaryRepresentation
{
	NSMutableArray* bubbles = [NSMutableArray array];
	NSMutableArray* sfxs = [NSMutableArray array];
    
    [self.sfx enumerateObjectsUsingBlock:^(SfxInfo* sfxInfo, NSUInteger idx, BOOL *stop) {
		
		[sfxs addObject:[sfxInfo sfxRepresentation]];
	}];
    
	[self.bubbles enumerateObjectsUsingBlock:^(BubbleInfo* bubbleInfo, NSUInteger idx, BOOL *stop) {
		
		[bubbles addObject:[bubbleInfo dictionaryRepresentation]];
	}];
	
	NSMutableDictionary* dict = [NSMutableDictionary dictionaryWithDictionary:@{@"bubbles" : bubbles, @"sfx" : sfxs, @"ambientSounds" : self.ambientSounds, @"Rect" : NSStringFromCGRect(self.rect)}];
	
	if (self.transitionType == kFrameInfoTransitionCrossfadeType)
	{
		dict[@"transitionType"] = @"crossfade";
	}
	
	return dict;
}

@end
