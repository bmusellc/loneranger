//
//  GameCenterManager.h
//  Unity-iPhone
//
//  Created by Vladislav Bakuta on 9/10/13.
//
//

#import <Foundation/Foundation.h>
#import <GameKit/GameKit.h>

@interface GameCenterManager : NSObject

+ (GameCenterManager *)sharedManager;

- (void)authenticateLocalPlayerWithViewController:(UIViewController *)viewController;

@end
