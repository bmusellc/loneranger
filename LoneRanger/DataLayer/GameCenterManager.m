//
//  GameCenterManager.m
//  Unity-iPhone
//
//  Created by Vladislav Bakuta on 9/10/13.
//
//

#import "GameCenterManager.h"

@implementation GameCenterManager

static GameCenterManager *gameCenterManager;

+ (GameCenterManager *)sharedManager
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        gameCenterManager = [[GameCenterManager alloc] init];
    });
    return gameCenterManager;
}

- (void)authenticateLocalPlayerWithViewController:(UIViewController *)viewController
{
    GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];
    localPlayer.authenticateHandler = ^(UIViewController *receivedViewController, NSError *error){
        if (receivedViewController != nil)
        {
            [viewController presentViewController:receivedViewController animated:YES completion:nil];
        }
        else if (localPlayer.isAuthenticated)
        {
            NSLog(@"Player is authenticated.");
        }
        else
        {
            NSLog(@"Game center not available");
        }
        NSLog(@"Error: %d", [error code]);
    };
}

@end
