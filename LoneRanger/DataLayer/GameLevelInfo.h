//
//  GameLevelInfo.h
//  Unity-iOS Simulator
//
//  Created by Yuriy Belozerov on 31.05.13.
//
//

#import <Foundation/Foundation.h>
typedef enum
{
	GameLevelInfoStateLocked,
	GameLevelInfoStateAvailable,
	GameLevelInfoStateOpened,
	GameLevelInfoStatePassed,
}
GameLevelInfoState;

@interface GameLevelInfo : NSObject

@property(readonly)	NSInteger				levelIndex;
@property(readonly)	GameLevelInfoState		state;
@property(readonly)	NSString*				thumbnailImageName;
@property(readonly)	BOOL					passed;

+ (GameLevelInfo*)gameLevelInfoWithDictionary:(NSDictionary*)dict;

- (NSDictionary*)dictionaryRepresentation;

@end
