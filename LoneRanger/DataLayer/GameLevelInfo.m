//
//  GameLevelInfo.m
//  Unity-iOS Simulator
//
//  Created by Yuriy Belozerov on 31.05.13.
//
//

#import "GameLevelInfo.h"

@implementation GameLevelInfo

- (BOOL)passed
{
	return self.state == GameLevelInfoStatePassed;
}

+ (GameLevelInfo*)gameLevelInfoWithDictionary:(NSDictionary*)dict
{
	GameLevelInfo* info = [[GameLevelInfo new] autorelease];
	
	info->_levelIndex = [dict[@"levelIndex"] integerValue];
	info->_state = [dict[@"state"] integerValue];
	info->_thumbnailImageName = [dict[@"thumbnail"] copy];
	
	return info;
}

- (void)dealloc
{
	[_thumbnailImageName release];
    
    [super dealloc];
}

- (NSDictionary*)dictionaryRepresentation
{
	return @{@"levelIndex" : @(self.levelIndex), @"state" : @(self.state), @"thumbnail" : self.thumbnailImageName};
}

@end
