//
//  Issue.h
//  LoneRanger
//
//  Created by Vitaliy Kolomoets on 3/25/13.
//  Copyright (c) 2013 iGeneration. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "PageInfo.h"

@interface Issue : NSObject
{
	
}

+ (Issue*)issueFromDictionary:(NSDictionary*)dict;

@property (readonly) NSString*                              rootPath;
@property (readonly) NSString*                              name;
@property (readonly) NSString*                              coverPath;
@property (readonly) NSString*                              backCoverPath;
@property (readonly) NSString*                              fullsizeCoverPath;
@property (readonly) NSMutableArray*                        pages;
@property (readonly, getter = isPrimary) BOOL               primary;

@end
