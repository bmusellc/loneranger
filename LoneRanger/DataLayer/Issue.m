//
//  Issue.m
//  LoneRanger
//
//  Created by Vitaliy Kolomoets on 3/25/13.
//  Copyright (c) 2013 iGeneration. All rights reserved.
//

#import "Issue.h"
#import <UIKit/UIKit.h>

@implementation Issue

- (void)dealloc
{
	[_name release];
	[_coverPath release];
    [_backCoverPath release];
	[_fullsizeCoverPath release];
	[_pages release];
    [_rootPath release];
	
    [super dealloc];
}

- (id)init
{
    self = [super init];
    if (self)
	{
		_pages = [[NSMutableArray alloc]init];
    }
    return self;
}

+ (Issue*)issueFromDictionary:(NSDictionary*)dict
{
	Issue* iss = [[[self alloc]init]autorelease];
    
	iss->_name = [dict[@"name"] copy];
	iss->_primary = [dict[@"primary"] boolValue];
    
    NSString* path = nil;
    
	if (iss.primary)
    {
        path = [[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:@"issues"];
        path = [path stringByAppendingPathComponent:iss.name];
    }
	else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        path = [paths objectAtIndex:0];
        path = [path stringByAppendingPathComponent:@"issues"];
        path = [path stringByAppendingPathComponent:iss.name];
    }
	
    iss->_rootPath = [path retain];
    
	NSString* coverPath = dict[@"coverPath"];
	NSString* backCoverPath = dict[@"backCoverPath"];
	NSString* fullsizeCoverPath = [coverPath stringByDeletingPathExtension];
	fullsizeCoverPath = [fullsizeCoverPath stringByAppendingString:@"-fullsize"];
	fullsizeCoverPath = [fullsizeCoverPath stringByAppendingPathExtension:coverPath.pathExtension];
	
	if ([UIScreen mainScreen].scale > 1) //retina display
	{
		NSString* ext = coverPath.pathExtension;
		coverPath = [coverPath stringByDeletingPathExtension];
		coverPath = [coverPath stringByAppendingString:@"@2x"];
		coverPath = [coverPath stringByAppendingPathExtension:ext];
		
		ext = backCoverPath.pathExtension;
		backCoverPath = [backCoverPath stringByDeletingPathExtension];
		backCoverPath = [backCoverPath stringByAppendingString:@"@2x"];
		backCoverPath = [backCoverPath stringByAppendingPathExtension:ext];
		
		ext = fullsizeCoverPath.pathExtension;
		fullsizeCoverPath = [fullsizeCoverPath stringByDeletingPathExtension];
		fullsizeCoverPath = [fullsizeCoverPath stringByAppendingString:@"@2x"];
		fullsizeCoverPath = [fullsizeCoverPath stringByAppendingPathExtension:ext];
	}
	
	iss->_coverPath = [[iss.rootPath stringByAppendingPathComponent:coverPath] copy];
    iss->_backCoverPath = [[iss.rootPath stringByAppendingPathComponent:backCoverPath] copy];
	iss->_fullsizeCoverPath = [[iss.rootPath stringByAppendingPathComponent:fullsizeCoverPath] copy];
	
    NSString* pagesFileName = [iss->_name stringByAppendingPathExtension:@"plist"];
    path = [path stringByAppendingPathComponent:pagesFileName];
    
    NSDictionary* pagesInfo = [NSDictionary dictionaryWithContentsOfFile:path];
    
	if (!pagesInfo)
    {
        NSLog(@"ERROR %s: Could not read Plist at %@", __FUNCTION__, path);
        return nil;
    }
	
    for (NSDictionary* pInfo in [pagesInfo objectForKey:@"pages"])
	{
		[iss->_pages addObject:[PageInfo pageInfoFromDictionary:pInfo withRootPath:iss->_rootPath]];
	}
	
	return iss;
}

@end
