//
//  IssuePlayStats.h
//  Unity-iOS Simulator
//
//  Created by Yuriy Belozerov on 01.06.13.
//
//

#import "LevelStats.h"
#import <CoreGraphics/CoreGraphics.h>

@interface IssuePlayStats : NSObject

@property(readonly)	NSArray*					levelsStats;
@property(readonly)	CGFloat						progress;

//compiled from all levels stats
@property(readonly)	NSInteger					totalShots;
@property(readonly)	NSInteger					hits;
@property(readonly)	NSInteger					coinsEarned;
@property(readonly)	NSInteger					performedChallenges;
@property(readonly)	NSInteger					totalChallenges;

+ (IssuePlayStats*)issuePlayStatsWithDictionary:(NSDictionary*)dict;

- (NSDictionary*)dictionaryRepresentation;

- (LevelStats*)levelStatsForLevelWithIndex:(NSInteger)levelIdx;

- (void)updateLevelStatistics:(LevelStats*)levelStats;

@end
