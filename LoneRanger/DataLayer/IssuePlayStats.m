//
//  IssuePlayStats.m
//  Unity-iOS Simulator
//
//  Created by Yuriy Belozerov on 01.06.13.
//
//

#import "IssuePlayStats.h"
#import "IssuesManager.h"
#import "LevelsManager.h"

@interface IssuePlayStats ()
{
	NSMutableArray*						_levelsStats;
}

@end

@implementation IssuePlayStats

- (CGFloat)progress
{
	NSArray* levels = [LevelsManager sharedManager].gameLevels;
	
	__block NSInteger passedLevelsCount = 0;
	
	[levels enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
		
		if ([obj passed])
		{
			passedLevelsCount++;
		}
	}];
	
	return passedLevelsCount / (CGFloat)levels.count;
}

- (NSArray*)levelsStats
{
	return [NSArray arrayWithArray:_levelsStats];
}

- (NSInteger)totalShots
{
	__block NSInteger totalShots = 0;
	
	[self.levelsStats enumerateObjectsUsingBlock:^(LevelStats* stats, NSUInteger idx, BOOL *stop) {
		
		totalShots += stats.totalShots;
	}];
	
	return totalShots;
}

- (NSInteger)hits
{
	__block NSInteger hits = 0;
	
	[self.levelsStats enumerateObjectsUsingBlock:^(LevelStats* stats, NSUInteger idx, BOOL *stop) {
		
		hits += stats.hits;
	}];
	
	return hits;
}

- (NSInteger)coinsEarned
{
	__block NSInteger coinsEarned = 0;
	
	[self.levelsStats enumerateObjectsUsingBlock:^(LevelStats* stats, NSUInteger idx, BOOL *stop) {
		
		coinsEarned += stats.coinsEarned;
	}];
	
	return coinsEarned;
}

- (NSInteger)performedChallenges
{
	__block NSInteger count = 0;
	
	[self.levelsStats enumerateObjectsUsingBlock:^(LevelStats* stats, NSUInteger idx, BOOL *stop) {
		
		[stats.challenges enumerateObjectsUsingBlock:^(NSNumber* num, NSUInteger idx, BOOL *stop) {
			
			if ([num boolValue])
			{
				count++;
			}
		}];
	}];
	
	return count;
}

- (NSInteger)totalChallenges
{
	NSArray* levels = [LevelsManager sharedManager].gameLevels;
	
	return (levels.count - 1) * 3;
}

+ (IssuePlayStats*)issuePlayStatsWithDictionary:(NSDictionary*)dict
{
	IssuePlayStats* stats = [[[IssuePlayStats alloc] initWithDictionary:dict] autorelease];
	return stats;
}

- (id)initWithDictionary:(NSDictionary*)dict
{
	self = [super init];
	
	if (self)
	{
		_levelsStats = [NSMutableArray new];
		
		NSArray* rawLevelsStats = dict[@"levelsStats"];
		
		[rawLevelsStats enumerateObjectsUsingBlock:^(NSDictionary* levelStatsDict, NSUInteger idx, BOOL *stop) {
			
			LevelStats* levelStats = [LevelStats levelStatsWithDictionary:levelStatsDict];
			[_levelsStats addObject:levelStats];
		}];
	}
	
	return self;
}

- (void)dealloc
{
	[_levelsStats release];
    
    [super dealloc];
}

- (NSDictionary*)dictionaryRepresentation
{
	NSMutableArray* levelsStats = [NSMutableArray array];
	
	[self.levelsStats enumerateObjectsUsingBlock:^(LevelStats* stats, NSUInteger idx, BOOL *stop) {
		
		[levelsStats addObject:[stats dictionaryRepresentation]];
	}];
	
	return @{@"levelsStats" : levelsStats};
}

- (LevelStats*)levelStatsForLevelWithIndex:(NSInteger)levelIdx
{
	__block LevelStats* targetStats = nil;
	
	[self.levelsStats enumerateObjectsUsingBlock:^(LevelStats* stats, NSUInteger idx, BOOL *stop) {
		
		if (stats.level == levelIdx)
		{
			targetStats = stats;
			*stop = YES;
		}
	}];
	
	return targetStats;
}

- (void)updateLevelStatistics:(LevelStats*)levelStats
{
	__block NSInteger targetStatsIdx = -1;
	
	[self.levelsStats enumerateObjectsUsingBlock:^(LevelStats* stats, NSUInteger idx, BOOL *stop) {
		
		if (stats.level == levelStats.level)
		{
			targetStatsIdx = idx;
			*stop = YES;
		}
	}];
	
	if (targetStatsIdx > -1)
	{
		LevelStats* currentStats = _levelsStats[targetStatsIdx];
		
		[levelStats setValue:@(currentStats.coinsEarned + levelStats.coinsEarned) forKey:@"coinsEarned"];
				
		_levelsStats[targetStatsIdx] = levelStats;
	}
	else
	{
		[_levelsStats addObject:levelStats];
	}
}

@end
