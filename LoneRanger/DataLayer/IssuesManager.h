//
//  IssuesManager.h
//  Unity-iPhone
//
//  Created by Artem Manzhura on 5/29/13.
//
//

#import "Issue.h"

typedef enum
{
	kIssue1,
	kIssue2,
	kIssue3,
	kIssue4,
	kIssue5
}
IssueType;

@interface IssuesManager : NSObject
{
    
}
@property (assign, readonly)    NSArray*        issues;
+ (IssuesManager*)sharedManager;
+ (NSURL*)packetURLForName:(NSString*)name;
+ (NSString*)localPacketDirectoryPathForName:(NSString*)name;
+ (BOOL)isPrimaryIssue:(NSString*)issue;

@end
