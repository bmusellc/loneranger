//
//  IssuesManager.m
//  Unity-iPhone
//
//  Created by Artem Manzhura on 5/29/13.
//
//

#import "NSFileManagerExtension.h"
#import "IssuesManager.h"
#import "PurchasesManager.h"
static  IssuesManager* manager = nil;

@interface IssuesManager()
{
    
}
- (void)_copyPlistToDocuments;
- (void)_loadData;

@end

@implementation IssuesManager

- (void)dealloc
{
    [_issues release];
    [super dealloc];
}

- (id)init
{
    self = [super init];
    if (self) {
        [self _copyPlistToDocuments];
        [self _loadData];
//        [[PurchasesManager sharedManager] purchaseItemOfType:@"Issue3"];
    }
    return self;
}

+ (IssuesManager*)sharedManager
{
    if (!manager)
    {
        manager = [[IssuesManager alloc] init];
    }
    return manager;
}

- (void)_loadData
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
     NSString *resourcePath = [documentsDirectory stringByAppendingPathComponent:@"issuesInfo.plist"];
    if(![[NSFileManager defaultManager] isReadableFileAtPath: resourcePath])
    {   
        NSLog(@"ERROR %s: Info.plist file is missing at %@", __FUNCTION__, resourcePath);
    }
    NSDictionary* params = [NSDictionary dictionaryWithContentsOfFile: resourcePath];
    if(!params)
    {
        NSLog(@"ERROR %s: Could not r   ead Info.plist at %@", __FUNCTION__, resourcePath);
    }
    if(![params objectForKey: @"issues"])
    {
        NSLog(@"ERROR %s: Required array 'issues' is missing at Info.plist", __FUNCTION__);
    }
    [_issues release];
    _issues = nil;
    NSArray* issues = [params objectForKey:@"issues"];
    NSMutableArray* deserializedIssues = [[NSMutableArray alloc] initWithCapacity:0];
    for (NSDictionary* issue in issues) {
        Issue* deserializedIssue = [Issue issueFromDictionary:issue];
        [deserializedIssues addObject:deserializedIssue];
    }
    _issues = [deserializedIssues retain];
    [deserializedIssues release];
}

- (void)_copyPlistToDocuments
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error = nil;

    NSString *txtPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0] stringByAppendingPathComponent:@"issuesInfo.plist"];

    if ([fileManager fileExistsAtPath:txtPath] == NO) {
        NSString *resourcePath = [[NSBundle mainBundle] pathForResource:@"issuesInfo" ofType:@"plist"];
        [fileManager copyItemAtPath:resourcePath toPath:txtPath error:&error];
    }
}

+ (BOOL)isPrimaryIssue:(NSString*)issue
{
    BOOL result = NO;
    if([issue isEqualToString:@"Issue1"])
	{
		result = YES;
	}
	else if ([issue isEqualToString:@"Issue2"])
	{
		result = NO;
	}
	else if ([issue isEqualToString:@"Issue3"])
	{
		result = NO;
	}
    return result;
}

+ (NSString*)localPacketDirectoryPathForName:(NSString*)name
{
	NSFileManager* fManager = [NSFileManager defaultManager];
	NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* assetsDirectoryPath = [[paths objectAtIndex:0]stringByAppendingPathComponent: @"issues"];
	
	NSString* result = [assetsDirectoryPath stringByAppendingPathComponent:name];
	
	if(![fManager fileExistsAtPath:result])
	{
		[fManager createDirectoryAtPath:result withIntermediateDirectories:YES attributes:nil error:nil];
		[[NSFileManager defaultManager] addSkipBackupAttributeToItemAtURL: [NSURL fileURLWithPath: assetsDirectoryPath]];
		[[NSFileManager defaultManager] addSkipBackupAttributeToItemAtURL: [NSURL fileURLWithPath: result]];
	}
	
	return result;
}

+ (NSURL*)packetURLForName:(NSString*)name
{
	NSString* _name = name;
	if ([_name isEqualToString:@"issue1"])
	{
		_name = @"issue1";
	}
	else if ([_name isEqualToString:@"issue2"])
	{
		_name = @"issue2";
	}
	else if ([_name isEqualToString:@"issue3"])
	{
		_name = @"issue3";
	}
//    return [NSURL URLWithString:[NSString stringWithFormat:@"http://content.oddcast.com/ccs6/customhost/1083/misc/%@_2.zip", _name]];
    
	return [NSURL URLWithString:[NSString stringWithFormat:@"http://uaserver.bmuse.com/loneRanger/%@.zip",_name]];
}

@end
