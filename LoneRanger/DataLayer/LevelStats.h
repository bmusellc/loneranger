//
//  LevelStats.h
//  Unity-iOS Simulator
//
//  Created by Yuriy Belozerov on 01.06.13.
//
//

#import <Foundation/Foundation.h>

@interface LevelStats : NSObject

@property(readonly)	NSInteger					level;

@property(readonly)	NSInteger					totalShots;
@property(readonly)	NSInteger					hits;
@property(readonly)	NSInteger					coinsEarned;
@property(readonly)	NSArray*					challenges;

+ (LevelStats*)levelStatsWithDictionary:(NSDictionary*)dict;

+ (LevelStats*)levelStatsWithString:(NSString*)string;

+ (LevelStats*)levelStatsWithLevel:(NSInteger)level
						totalShots:(NSInteger)totalShots
							  hits:(NSInteger)hits
					   coinsEarned:(NSInteger)coins
						challenges:(NSArray*)challenges;

- (NSDictionary*)dictionaryRepresentation;

@end
