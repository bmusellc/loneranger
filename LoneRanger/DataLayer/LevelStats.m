//
//  LevelStats.m
//  Unity-iOS Simulator
//
//  Created by Yuriy Belozerov on 01.06.13.
//
//

#import "LevelStats.h"

#import "LevelsManager.h"

@implementation LevelStats

+ (LevelStats*)levelStatsWithDictionary:(NSDictionary*)dict
{
	return [[[LevelStats alloc] initWithDictionary:dict] autorelease];
}

+ (LevelStats*)levelStatsWithLevel:(NSInteger)level totalShots:(NSInteger)totalShots hits:(NSInteger)hits coinsEarned:(NSInteger)coins challenges:(NSArray*)challenges
{
	return [LevelStats levelStatsWithDictionary:@{@"level" : @(level),
												  @"totalShots" : @(totalShots),
												  @"hits" : @(hits),
												  @"coinsEarned" : @(coins),
												  @"challenges" : challenges}];
}

+ (LevelStats*)levelStatsWithString:(NSString*)string
{
	NSMutableArray* components = [[string componentsSeparatedByString:@";"] mutableCopy];
	
	if ([[components lastObject] length] == 0)
	{
		[components removeLastObject];
	}
	
	NSMutableArray* challenges = [NSMutableArray array];
	
	if (components.count > 5)
	{
		for (int i = components.count - 3; i < components.count; i++)
		{
			NSString* component = components[i];
			[challenges addObject:@([component boolValue])];
		}
	}
	else
	{
		[challenges addObject:@(NO)];
		[challenges addObject:@(NO)];
		[challenges addObject:@(NO)];
	}
	
	return [LevelStats levelStatsWithDictionary:@{@"level" : @([LevelsManager sharedManager].currentGameLevelInfo.levelIndex),
												  @"totalShots" : @([components[1] integerValue]),
												  @"hits" : @([components[2] integerValue]),
												  @"coinsEarned" : @([components[3] integerValue]),
												  @"challenges" : challenges}];
}

- (id)initWithDictionary:(NSDictionary*)dict
{
	self = [super init];
	
	if (self)
	{
		_level = [dict[@"level"] integerValue];
		_totalShots = [dict[@"totalShots"] integerValue];
		_hits = [dict[@"hits"] integerValue];
		_coinsEarned = [dict[@"coinsEarned"] integerValue];
		_challenges = [dict[@"challenges"] copy];
	}
	
	return self;
}

- (void)dealloc
{
	[_challenges release];
    
    [super dealloc];
}

- (NSDictionary*)dictionaryRepresentation
{
	return @{@"level" : @(self.level), @"totalShots" : @(self.totalShots), @"hits" : @(self.hits), @"coinsEarned" : @(self.coinsEarned), @"challenges" : self.challenges};
}

@end
