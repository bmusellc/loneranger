//
//  WaypointsManager.h
//  Unity-iOS Simulator
//
//  Created by Yuriy Belozerov on 17.05.13.
//
//
#import <Foundation/Foundation.h>
#import "GameLevelInfo.h"

extern NSString* LevelsManagerChangedLevelsStateNotification;

@interface LevelsManager : NSObject

+ (LevelsManager*)sharedManager;

@property(retain)	GameLevelInfo*					currentGameLevelInfo;
@property(readonly)	NSArray*						gameLevels;

- (GameLevelInfo*)gameLevelInfoWithIndex:(NSInteger)levelIndex;
- (GameLevelInfo*)nextGameLevelInfo;

- (void)passLevel:(GameLevelInfo*)levelInfo;
- (void)switchToNextLevel;

@end
