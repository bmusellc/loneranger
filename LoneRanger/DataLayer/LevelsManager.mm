//
//  WaypointsManager.m
//  Unity-iOS Simulator
//
//  Created by Yuriy Belozerov on 17.05.13.
//
//

#import "LevelsManager.h"
#import "IssuesManager.h"

NSString* LevelsManagerChangedLevelsStateNotification = @"LevelsManagerChangedLevelsStateNotification";

static LevelsManager* sharedPtr = nil;

@interface LevelsManager ()
{
	NSMutableArray*							_gameLevels;
	NSMutableArray*							_waypoints;
}

@end

@implementation LevelsManager

+ (LevelsManager*)sharedManager
{
	if (!sharedPtr)
	{
		sharedPtr = [LevelsManager new];
	}
	
	return sharedPtr;
}

- (id)init
{
    self = [super init];
    
	if (self)
	{
		_gameLevels = [NSMutableArray new];
		
		NSString* plistPath = [self _gameLevelsPlistPath];
		NSString* bundlePath = [[NSBundle mainBundle] pathForResource:@"gameLevels" ofType:@"plist"];
		NSArray* rawPlistData = nil;
		
		if(![[NSFileManager defaultManager] fileExistsAtPath:plistPath])
		{
			rawPlistData = [NSArray arrayWithContentsOfFile:bundlePath];
			[rawPlistData writeToFile:plistPath atomically:YES];
			
			NSURL* plistURL = [NSURL fileURLWithPath:plistPath];
			[plistURL setResourceValue:[NSNumber numberWithBool:YES] forKey:NSURLIsExcludedFromBackupKey error:nil];
		}
		else
		{
			rawPlistData = [NSArray arrayWithContentsOfFile:plistPath];
		}
		
		[rawPlistData enumerateObjectsUsingBlock:^(NSDictionary* dict, NSUInteger idx, BOOL *stop) {
			
			[_gameLevels addObject:[GameLevelInfo gameLevelInfoWithDictionary:dict]];
		}];		
    }
    
	return self;
}

- (GameLevelInfo*)gameLevelInfoWithIndex:(NSInteger)levelIndex
{
	__block GameLevelInfo* targetInfo = nil;
	
	[self.gameLevels enumerateObjectsUsingBlock:^(GameLevelInfo* info, NSUInteger idx, BOOL *stop) {
		
		if (info.levelIndex == levelIndex)
		{
			targetInfo = info;
			*stop = YES;
		}
	}];
	
	return targetInfo;
}

- (GameLevelInfo*)nextGameLevelInfo
{
	NSInteger currentLevelInfoArrayIndex = [self.gameLevels indexOfObject:self.currentGameLevelInfo];
	
	GameLevelInfo* targetLevelInfo = nil;
	
	if (currentLevelInfoArrayIndex < self.gameLevels.count - 1)
	{
		targetLevelInfo = self.gameLevels[currentLevelInfoArrayIndex + 1];
	}
	
	return targetLevelInfo;
}

- (void)passLevel:(GameLevelInfo *)levelInfo
{
	[levelInfo setValue:@(GameLevelInfoStatePassed) forKey:@"state"];
	
	GameLevelInfo* nextLevelInfo = [self nextGameLevelInfo];
	
	if (nextLevelInfo.state == GameLevelInfoStateAvailable)
	{
		[nextLevelInfo setValue:@(GameLevelInfoStateOpened) forKey:@"state"];
	}
	
	[self _saveGameLevelsPlist];
	
	[[NSNotificationCenter defaultCenter] postNotificationName:LevelsManagerChangedLevelsStateNotification object:nil];
}

- (void)switchToNextLevel
{
	self.currentGameLevelInfo = [self nextGameLevelInfo];
}

- (NSString*)_gameLevelsPlistPath
{
	return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0] stringByAppendingPathComponent:@"gameLevels.plist"];
}

- (void)_saveGameLevelsPlist
{
	NSMutableArray* gameLevels = [NSMutableArray array];
	
	[_gameLevels enumerateObjectsUsingBlock:^(GameLevelInfo* info, NSUInteger idx, BOOL *stop) {
		
		[gameLevels addObject:[info dictionaryRepresentation]];
	}];
	
	[gameLevels writeToFile:[self _gameLevelsPlistPath] atomically:YES];
}

extern "C"
{
	void gameLevelDidFinishSuccessfully()
	{
		[[LevelsManager sharedManager] passLevel:[LevelsManager sharedManager].currentGameLevelInfo];
	}
	
	void switchToNextLevel()
	{
		[[LevelsManager sharedManager] switchToNextLevel];
	}
	
	const char* isNextLevelAvailable()
	{
		GameLevelInfo* nextLevelInfo = [[LevelsManager sharedManager] nextGameLevelInfo];
		
		const char* infoString = [[NSString stringWithFormat:@"%@", nextLevelInfo.state > GameLevelInfoStateLocked ? @"1" : @"0"] cStringUsingEncoding:NSUTF8StringEncoding];
		
		char* result = (char*)malloc(strlen(infoString) + 1);
		
		memset(result, '\0', sizeof(char) * strlen(infoString) + 1);
		strcpy(result, infoString);
		
		return result;
	}
}

@end
