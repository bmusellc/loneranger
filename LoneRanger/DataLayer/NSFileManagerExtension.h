//
//  NSFileManagerExtension.h
//  ElfYourself
//
//  Created by Dmitry Panin on 04.12.12.
//  Copyright (c) 2012 Dmitry Panin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSFileManager(BackupDisablingExtension)

- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL;

@end
