//
//  NSFileManagerExtension.m
//  ElfYourself
//
//  Created by Dmitry Panin on 04.12.12.
//  Copyright (c) 2012 Dmitry Panin. All rights reserved.
//

#import "NSFileManagerExtension.h"

@implementation NSFileManager(BackupDisablingExtension)

- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL
{
    assert([[NSFileManager defaultManager] fileExistsAtPath:URL.path]);
    
    NSError *error = nil;
    
    BOOL success = [URL setResourceValue:@(YES) forKey:NSURLIsExcludedFromBackupKey error:&error];
	
    if (!success)
	{
        NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
    }
	
    return success;
}

@end
