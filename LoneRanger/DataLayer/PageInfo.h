//
//  PageInfo.h
//  LoneRanger
//
//  Created by Vitaliy Kolomoets on 3/25/13.
//  Copyright (c) 2013 iGeneration. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>
typedef enum
{
    kPageInfoTypeUnknown = -1,
	kPageInfoTypeComic,
	kPageInfoTypeEnhancedPanel,
	kPageInfoTypeWaypoint,
}
PageInfoType;

@interface PageInfo : NSObject

@property (readonly) NSString*          rootPath;
@property (readonly) NSString*          previewBackgroundPath;
@property (readonly) NSString*			fullsizeBackgroundPath;
@property (readonly) PageInfoType		pageType;
@property (readonly) NSMutableArray*    frames;

@property(readonly)	NSString*			sceneName;
@property(readonly)	NSInteger			waypointIndex;

@property(assign)	CGRect				contentFrame;

+ (PageInfo*)pageInfoFromDictionary:(NSDictionary*)dict withRootPath:(NSString*)rootPath;

- (NSDictionary*)dictionaryRepresentation;

@end
