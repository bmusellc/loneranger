//
//  PageInfo.m
//  LoneRanger
//
//  Created by Vitaliy Kolomoets on 3/25/13.
//  Copyright (c) 2013 iGeneration. All rights reserved.
//

#import "PageInfo.h"
#import "FrameInfo.h"

#import "LevelsManager.h"

@interface PageInfo ()

+ (PageInfoType)_pageTypeFromString:(NSString*)stringType;

@end

@implementation PageInfo

- (void)dealloc
{
    [_previewBackgroundPath release];
   	[_fullsizeBackgroundPath release];
	[_sceneName release];
    [_rootPath release];
    [_frames release];
    
    [super dealloc];
}


- (id)initWithParams:(NSDictionary*)params withRootPath:(NSString *)rootPath
{
    self = [super init];
	
    if (self)
	{
		NSString* key = nil;
		NSString* value = nil;
		
		key = @"type";
		
		if (params[key])
		{
			value = params[key];
		}
		else
		{
			value = nil;
		}
		
		_pageType = [PageInfo _pageTypeFromString:value];
		
		
		key = @"Preview-Image";
		
        if (params[key])
		{
			value = params[key];
		}
		else
		{
			NSLog(@"WARNING %s: '%@' is missed for page", __FUNCTION__, key);
			value = nil;
		}
		
		_previewBackgroundPath = [value copy];
		
		
		key = @"Fullsize-Image";
		
        if (params[key])
		{
			value = params[key];
		}
		else
		{
			NSLog(@"WARNING %s: '%@' is missed for page", __FUNCTION__, key);
			value = nil;
		}
		
		_fullsizeBackgroundPath = [value copy];
		
		
        _frames = [NSMutableArray new];
		
        if (params[@"Frames"])
		{
			NSArray* frameRaw = params[@"Frames"];
			
			for (NSDictionary* dict in frameRaw)
			{
                FrameInfo* frame = [FrameInfo pageInfoFromDictionary:dict withRootPath:rootPath];
                if(frame)
                {
                    [_frames addObject:frame];
                }
			}
		}
		
		_rootPath = [rootPath copy];
		
		//exclusive for enhanced panels
		if (self.pageType == kPageInfoTypeEnhancedPanel && [params[@"sceneName"] length] > 0)
		{
			_sceneName = [params[@"sceneName"] copy];
		}
		
		//exclusive for waypoints
		if (self.pageType == kPageInfoTypeWaypoint)
		{
			_waypointIndex = [params[@"waypointIndex"] integerValue];
		}
    }
	
    return self;
}

+ (PageInfo*)pageInfoFromDictionary:(NSDictionary*)dict withRootPath:(NSString*)rootPath
{
	PageInfo* pInfo = [[[PageInfo alloc] initWithParams:dict withRootPath:rootPath] autorelease];

	return pInfo;
}

+ (PageInfoType)_pageTypeFromString:(NSString*)stringType
{
	PageInfoType type = kPageInfoTypeUnknown;
	
	if ([stringType isEqualToString:@"comicPage"])
	{
		type = kPageInfoTypeComic;
	}
	else if ([stringType isEqualToString:@"enhancedPanel"])
	{
		type = kPageInfoTypeEnhancedPanel;
	}
	else if ([stringType isEqualToString:@"waypoint"])
	{
		type = kPageInfoTypeWaypoint;
	}
	
	return type;
}

- (NSString*)pageTypeString
{
	switch (self.pageType)
	{
		case kPageInfoTypeComic:
			return @"comicPage";
			
		case kPageInfoTypeWaypoint:
			return @"waypoint";

		case kPageInfoTypeEnhancedPanel:
			return @"enhancedPanel";
			
		default:
			return @"unknown";
	}
}

- (NSDictionary*)dictionaryRepresentation
{
	NSMutableArray* frames = [NSMutableArray array];
	
	[self.frames enumerateObjectsUsingBlock:^(FrameInfo* frameInfo, NSUInteger idx, BOOL *stop) {
		
		[frames addObject:[frameInfo dictionaryRepresentation]];
	}];
	
	NSMutableDictionary* dict = [NSMutableDictionary dictionaryWithDictionary:@{@"type" : [self pageTypeString],
		@"Fullsize-Image" : self.fullsizeBackgroundPath,
		@"Preview-Image" : self.previewBackgroundPath,
		@"Frames" : frames}];
	
	if (self.pageType == kPageInfoTypeWaypoint)
	{
		dict[@"waypointIndex"] = @(self.waypointIndex);
	}
	else if (self.pageType == kPageInfoTypeEnhancedPanel)
	{
		dict[@"sceneName"] = self.sceneName;
	}
	
	return dict;
}

@end
