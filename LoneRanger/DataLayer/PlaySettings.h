//
//  PlaySettings.h
//  Unity-iPhone
//
//  Created by Vitaliy Kolomoets on 5/31/13.
//
//

#import <Foundation/Foundation.h>

#define 	kMaxSensitivity  8.0f
#define 	kMinSensitivity  1.0f

@interface PlaySettings : NSObject

@property (assign)	BOOL	isAdvancedShooting;
@property (assign)	BOOL	isAxisReversed;
@property (assign)	BOOL	isVoiceOverEnabled;
@property (nonatomic, assign)	float	sensitivity;
@property (assign)  float	volumeLevel;

+(PlaySettings*)sharedSettings;
- (void)save;
@end
