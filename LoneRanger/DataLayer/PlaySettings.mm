//
//  PlaySettings.h
//  Unity-iPhone
//
//  Created by Vitaliy Kolomoets on 5/31/13.
//
//

#import "PlaySettings.h"

static PlaySettings* _sharedSettings = nil;

@interface PlaySettings()
@end

@implementation PlaySettings

//- (void)setSensitivity:(float)sensitivity
//{
//	_sensitivity = sensitivity * (kMaxSensitivity - kMinSensitivity) + kMinSensitivity;
//}

- (id)init
{
    self = [super init];
    if (self)
	{
        if([[NSUserDefaults standardUserDefaults] floatForKey:@"sentitivity"] != 0.0f)
		{
			_isAdvancedShooting = [[NSUserDefaults standardUserDefaults] boolForKey:@"isAdvancedShooting"];
			_sensitivity = [[NSUserDefaults standardUserDefaults] floatForKey:@"sentitivity"];
			_isAxisReversed = [[NSUserDefaults standardUserDefaults] boolForKey:@"isAxisReversed"];
			_isVoiceOverEnabled = [[NSUserDefaults standardUserDefaults] boolForKey:@"isVoiceOverEnabled"];
			_volumeLevel = [[NSUserDefaults standardUserDefaults] floatForKey:@"volumeLevel"];
		}
		else
		{
			_isAdvancedShooting = NO;
			_sensitivity = 7.0f;
			_isAxisReversed = NO;
			_isVoiceOverEnabled = YES;
			_volumeLevel = 1.0f;
			
			[self save];
		}
    }
    return self;
}

- (void)save
{
	[[NSUserDefaults standardUserDefaults] setFloat:_sensitivity forKey:@"sentitivity"];
	[[NSUserDefaults standardUserDefaults] setFloat:_volumeLevel forKey:@"volumeLevel"];
	[[NSUserDefaults standardUserDefaults] setBool:_isAdvancedShooting forKey:@"isAdvancedShooting"];
	[[NSUserDefaults standardUserDefaults] setBool:_isAxisReversed forKey:@"isAxisReversed"];
	[[NSUserDefaults standardUserDefaults] setBool:_isVoiceOverEnabled forKey:@"isVoiceOverEnabled"];
}

+ (PlaySettings*)sharedSettings
{
	if(!_sharedSettings)
	{
		_sharedSettings = [[PlaySettings alloc]init];
	}
	
	return _sharedSettings;
}

@end

extern "C"
{
	const char* settingsString()
	{
		NSMutableString* settingsString = [[NSMutableString alloc]init];
		
		[settingsString appendFormat:@"%.1f;", [PlaySettings sharedSettings].sensitivity];
		[settingsString appendFormat:@"%.1f;", [PlaySettings sharedSettings].volumeLevel];
		[settingsString appendFormat:@"%d;", (int)[PlaySettings sharedSettings].isAdvancedShooting];
		[settingsString appendFormat:@"%d;", (int)[PlaySettings sharedSettings].isAxisReversed];
		[settingsString appendFormat:@"%d", (int)[PlaySettings sharedSettings].isVoiceOverEnabled];
		
		const char* str = [settingsString cStringUsingEncoding:NSUTF8StringEncoding];
		
		char* result = (char*)malloc(strlen(str) + 1);
		
		strcpy(result, str);
		
		return result;
	}
	
	void saveSettings(const char* settingsStr)
	{
		NSString* str = [NSString stringWithUTF8String:settingsStr];
        NSLog(@"settings ios %@", str);
		NSArray* values = [str componentsSeparatedByString:@";"];
		[PlaySettings sharedSettings].sensitivity = [values[0] floatValue];
		[PlaySettings sharedSettings].volumeLevel = [values[1] floatValue];
		[PlaySettings sharedSettings].isAdvancedShooting = [values[2] intValue];
		[PlaySettings sharedSettings].isAxisReversed = [values[3] intValue];
		[PlaySettings sharedSettings].isVoiceOverEnabled = [values[4] intValue];
		[[PlaySettings sharedSettings] save];
	}
}