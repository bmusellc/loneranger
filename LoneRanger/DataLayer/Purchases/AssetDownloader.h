//
//  AssetDownloader.h
//  Yourself
//
//  Created by Vitaliy Kolomoets on 11/16/12.
//  Copyright (c) 2012 Dmitry Panin. All rights reserved.
//

#import <Foundation/Foundation.h>

@class AssetDownloader;

@protocol AssetDownloaderDelegate <NSObject>
- (void)onDownloadStateDidChanged:(AssetDownloader*)sender;
@end

typedef enum
{
	ASSET_DOWNLOAD_NOT_STARTED,
	ASSET_DOWNLOADING,
	ASSET_DOWNLOADED_PARTIALLY,
	ASSET_DOWNLOADED_SUCCESSFULLY,
	ASSET_UNARCHIVING,
	ASSET_UNARCHIVED,
	ASSET_DOWNLOAD_FAILED
	
}AssetDownloadState;

@interface AssetDownloader : NSObject

@property (assign) NSObject<AssetDownloaderDelegate>*       delegate;
@property (readonly) AssetDownloadState						downloadState;
@property (readonly) NSString*								name;
@property (readonly) float									progress;

- (id)initWithName:(NSString*)name;

- (void)startDownloadAsset;
- (void)cancelAssetDownloading;

@end
