//
//  AssetDownloader.m
//  Yourself
//
//  Created by Vitaliy Kolomoets on 11/16/12.
//  Copyright (c) 2012 Dmitry Panin. All rights reserved.
//

#import "AssetDownloader.h"
//#import "CompositionBuilder.h"
#import "SSZipArchive.h"
#import "IssuesManager.h"

@interface AssetDownloader () <SSZipArchiveDelegate>
{
	NSString*													_name;
	NSOutputStream*												_fileOutput;
	AssetDownloadState											_downloadState;
	__unsafe_unretained NSObject<AssetDownloaderDelegate>*      _delegate;
	
	NSURLConnection*											_connection;
	long long													_expectedDataLength;
	long long													_receicedDataLength;
    
    float                                                       _unZipPercent;
    
    BOOL                                                        _downloadingCancelled;
}
- (void)_unarchiveFileAtPath:(NSString*)path;
@end

@implementation AssetDownloader
@synthesize delegate = _delegate, downloadState = _downloadState;//, downloadedAssetURL = _downloadedAssetURL;

- (float)progress
{
    float balance = 0.9f; // balance between downloading and unzipping progress
    
	if(_downloadState == ASSET_DOWNLOADING && _receicedDataLength != 0)
	{
		return ((float)_receicedDataLength / (float)_expectedDataLength) * balance;
	}
	else if (_downloadState == ASSET_UNARCHIVING)
	{
        NSLog(@"unZip progress = %f", balance + ((1.0f - balance) * _unZipPercent / 100.0f));   
		return balance + ((1.0f - balance) * _unZipPercent / 100.0f);
	}
	
	return 0.0f;    
}

- (id)initWithName:(NSString *)name
{
	self = [super init];
    if (self)
	{
		_downloadState = ASSET_DOWNLOAD_NOT_STARTED;
		_name = name;
		_expectedDataLength = 0;
        _unZipPercent = 0;
	}
	return self;
}

- (void)startDownloadAsset
{
	if(_connection)
	{
		return;
	}
	
	NSURLRequest *theRequest = [NSURLRequest requestWithURL:[IssuesManager packetURLForName:_name]
												cachePolicy:NSURLRequestUseProtocolCachePolicy
											timeoutInterval:10.0];
	
	_connection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
	if (_connection)
	{
        
		NSString* path = [[IssuesManager localPacketDirectoryPathForName:_name] stringByAppendingPathComponent:[[[IssuesManager packetURLForName:_name] path] lastPathComponent]];
        
        //cleaning directory
        [[NSFileManager defaultManager] removeItemAtPath:[IssuesManager localPacketDirectoryPathForName:_name]
                                                   error:nil];
        
        [[NSFileManager defaultManager] createDirectoryAtPath: [IssuesManager localPacketDirectoryPathForName:_name]
                                  withIntermediateDirectories: YES
                                                   attributes: nil
                                                        error: nil];
        
		_downloadState = ASSET_DOWNLOADING;
		_fileOutput = [NSOutputStream outputStreamToFileAtPath:path append:NO];
		if(!_fileOutput)
		{
			_downloadState = ASSET_DOWNLOAD_FAILED;
			[_delegate onDownloadStateDidChanged:self];
		}
		else
		{
			[_fileOutput open];
		}
	}
	else
	{
		_downloadState = ASSET_DOWNLOAD_FAILED;
		[_delegate onDownloadStateDidChanged:self];
	}
}

- (void)cancelAssetDownloading
{
	if(!_connection)
	{
		return;
	}
	
	[_connection cancel];
	_connection = nil;
	
	[_fileOutput close];
	_fileOutput = nil;
	
	_expectedDataLength = 0;
    
    _downloadingCancelled = YES;
    
    //cleaning directory
    [[NSFileManager defaultManager] removeItemAtPath:[IssuesManager localPacketDirectoryPathForName:_name]
                                               error:nil];
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse*)response
{
	_expectedDataLength = response.expectedContentLength;
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData*)data
{
    NSInteger       dataLength;
    const uint8_t * dataBytes;
    NSInteger       bytesWritten;
    NSInteger       bytesWrittenSoFar;
	
    dataLength = [data length];
    dataBytes  = [data bytes];
	
    bytesWrittenSoFar = 0;
	
    do
	{
        bytesWritten = [_fileOutput write:&dataBytes[bytesWrittenSoFar] maxLength:dataLength - bytesWrittenSoFar];
        assert(bytesWritten != 0);
        if (bytesWritten == -1)
		{
			_downloadState = ASSET_DOWNLOAD_FAILED;
			[_delegate onDownloadStateDidChanged:self];
            break;
        }
		else
		{
            bytesWrittenSoFar += bytesWritten;
        }
		
    } while (bytesWrittenSoFar != dataLength);
	
	_receicedDataLength += dataLength;
}

- (void)connection:(NSURLConnection *)connection  didFailWithError:(NSError*)error
{
    _connection = nil;
	[_fileOutput close];
	_fileOutput = nil;

    _expectedDataLength = 0;
    
    //cleaning directory
    [[NSFileManager defaultManager] removeItemAtPath:[IssuesManager localPacketDirectoryPathForName:_name]
                                               error:nil];
    
	_downloadState = ASSET_DOWNLOAD_FAILED;
	[_delegate onDownloadStateDidChanged:self];
	
    // inform the user
    NSLog(@"Connection failed! Error - %@ %@",
          [error localizedDescription],
          [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	if(_receicedDataLength < _expectedDataLength)
	{
		_downloadState = ASSET_DOWNLOADED_PARTIALLY;
	}
	else
	{
		_downloadState = ASSET_DOWNLOADED_SUCCESSFULLY;
	}
//	NSError* err = nil;
	
	[_delegate onDownloadStateDidChanged:self];
	_connection = nil;
	[_fileOutput close];
	_fileOutput = nil;
	_expectedDataLength = 0;
	_receicedDataLength = 0;
	
	NSString* path = [[IssuesManager localPacketDirectoryPathForName:_name] stringByAppendingPathComponent:[[[IssuesManager packetURLForName:_name] path] lastPathComponent]];
	
	[self performSelectorInBackground:@selector(_unarchiveFileAtPath:) withObject:path];
}

- (void)_unarchiveFileAtPath:(NSString*)path
{
	_downloadState = ASSET_UNARCHIVING;
	[_delegate performSelectorOnMainThread:@selector(onDownloadStateDidChanged:) withObject:self waitUntilDone:NO];
    BOOL result = [SSZipArchive unzipFileAtPath:path toDestination:[IssuesManager localPacketDirectoryPathForName:_name] delegate:self];
//	BOOL result = [SSZipArchive unzipFileAtPath:path toDestination:[ElfCompositionBuilder localVideoDirectoryPathForStyle:_style]];
    
    if(!result)
    {
        //cleaning directory
        [[NSFileManager defaultManager] removeItemAtPath:[IssuesManager localPacketDirectoryPathForName:_name]
                                                   error:nil];
        
        _downloadState = ASSET_DOWNLOAD_FAILED;
        [_delegate performSelectorOnMainThread:@selector(onDownloadStateDidChanged:) withObject:self waitUntilDone:NO];
        
        return;
    }
    
    if(_downloadingCancelled)
    {
        //cleaning directory
        [[NSFileManager defaultManager] removeItemAtPath:[IssuesManager localPacketDirectoryPathForName:_name]
                                                   error:nil];
        
        return;
    }

    _downloadState = ASSET_UNARCHIVED;
    [_delegate performSelectorOnMainThread:@selector(onDownloadStateDidChanged:) withObject:self waitUntilDone:NO];
    [[NSFileManager defaultManager] removeItemAtPath:path error:nil];
}

- (void)zipArchiveDidUnzipFileAtIndex:(NSInteger)fileIndex totalFiles:(NSInteger)totalFiles archivePath:(NSString *)archivePath fileInfo:(unz_file_info)fileInfo
{
    _unZipPercent = (float)fileIndex * 100.0 / (float)totalFiles;
}

@end
