//
//  PurchasableItem.h
//
//  Created by Vitaliy Kolomoets on 11/16/12.
//  Copyright (c) 2012 Dmitry Panin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>

typedef enum
{
	kIssuePurchaseStateNotPurchased,
	kIssuePurchaseStatePurchased,
}
IssuePurchaseState;

@interface PurchasableItem : NSObject

@property(assign)	IssuePurchaseState	purchaseState;
@property(readonly) NSString*			issueName;
@property(readonly) NSString*			appStoreId;
@property(readonly) NSDecimalNumber*	appStorePrice;
@property(readonly) NSString*			appStorePriceString;
@property(retain)	SKProduct*			product;

- (id)initWithDictionary:(NSDictionary*)dict;
- (id)initWithAppStoreID:(NSString*)appStoreID;
+ (PurchasableItem*)itemWithDictionary:(NSDictionary*)dict;
- (NSDictionary*)dictionaryRepresentation;
@end
