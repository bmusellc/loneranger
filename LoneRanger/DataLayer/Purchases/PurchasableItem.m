//
//  PurchasableItem.m
//
//  Created by Vitaliy Kolomoets on 11/16/12.
//  Copyright (c) 2012 Dmitry Panin. All rights reserved.
//

#import "PurchasableItem.h"

@implementation PurchasableItem

+ (PurchasableItem*)itemWithDictionary:(NSDictionary*)dict
{
	return [[self alloc] initWithDictionary:dict];
}

- (id)initWithDictionary:(NSDictionary*)dict
{
	self = [super init];
    
	if (self)
	{
		_appStoreId = [dict[@"appStoreId"] copy];
		_purchaseState = [dict[@"purchaseState"] integerValue];
		_issueName = [dict[@"issueName"] copy];
    }
	
    return self;
}

- (id)initWithAppStoreID:(NSString*)appStoreID
{
	self = [super init];
	
	if (self)
	{
		_appStoreId = appStoreID;
	}
	
	return self;
}

- (void)dealloc
{
	[_appStoreId dealloc];
	[_issueName dealloc];
	
	self.product = nil;
    
    [super dealloc];
}

- (NSDictionary*)dictionaryRepresentation
{
	NSMutableDictionary* dict = [NSMutableDictionary dictionary];
	
	[dict setObject:_appStoreId forKey:@"appStoreId"];
	[dict setObject:[NSNumber numberWithUnsignedInteger:_purchaseState] forKey:@"purchaseState"];
	
	return dict;
}

- (NSDecimalNumber*)appStorePrice
{
	NSDecimalNumber* result = [NSDecimalNumber notANumber];
	
	if (_product)
	{
		result = _product.price;
	}
	
	return result;
}

- (NSString*)appStorePriceString
{
	NSString* result = @"N/A";
	
	if (_product)
	{
		NSNumberFormatter* numberFormatter = [[NSNumberFormatter alloc] init];
		[numberFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
		[numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
		[numberFormatter setLocale:_product.priceLocale];
		result = [numberFormatter stringFromNumber:_product.price];
	}
	
	return result;
}

@end
