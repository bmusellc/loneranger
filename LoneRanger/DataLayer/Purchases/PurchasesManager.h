//
//  PurchasesManager.h
//  Yours
//
//  Created by Vitaliy Kolomoets on 11/13/12.
//  Copyright (c) 2012 Dmitry Panin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PurchasableItem.h"

@class PurchasesManager;

typedef enum
{
	kPurchaseErrorUnknown,
	kPurchaseErrorPaymentCancelled,
	kPurchaseErrorPaymentNotAllowed,
}
PurchaseErrorType;

typedef enum
{
    kPurchasesManagerStateNone,
	kPurchasesManagerStateInitializing,
	kPurchasesManagerStateInitialized,
	kPurchasesManagerStateInitializingFailed,
	kPurchasesManagerStatePurchasingItem,
	kPurchasesManagerStatePurchasingFailed,
	kPurchasesManagerStateRestoringPurchases,
}
PurchasesManagerState;

@protocol PurchasesManagerDelegate <NSObject>
@required
- (void)purchasesManager:(PurchasesManager*)manager didSwitchToState:(PurchasesManagerState)newState fromState:(PurchasesManagerState)oldState;
@end

#define PurchasesManagerNewContentAvailableNotification @"PurchasesManagerNewContentAvailableNotification"
extern NSString* PurchasesManagerReceivedProductsInformationNotification;

@interface PurchasesManager : NSObject

@property(assign)	id <PurchasesManagerDelegate>			delegate;
@property(readonly) NSError*								error;
@property(readonly) NSArray*								restoredItems;
@property(readonly) PurchasesManagerState					state;

//common
+ (PurchasesManager*)sharedManager;

- (void)loadPurchasableItems;

//issues
- (BOOL)purchaseIssueNamed:(NSString*)name;

- (NSArray*)availableIssues;
- (IssuePurchaseState)purchaseStateForIssueName:(NSString*)name;
- (NSString*)priceStringForName:(NSString*)name;

- (BOOL)restorePurchases;

@end
