//
//  PurchasesManager.m
//  Yours
//
//  Created by Vitaliy Kolomoets on 11/13/12.
//  Copyright (c) 2012 Dmitry Panin. All rights reserved.
//

#import "PurchasesManager.h"
#import "AssetDownloader.h"
#import "IssuesManager.h"
//#import "CompositionBuilder.h"
#import <StoreKit/StoreKit.h>
#import "PurchasableItem.h"
#import "Reachability.h"
#import "NSFileManagerExtension.h"
#import "WeaponManager.h"

static PurchasesManager* _sharedManager = nil;

NSString* PurchasesManagerReceivedProductsInformationNotification = @"PurchasesManagerReceivedProductsInformationNotification";

@interface PurchasesManager () <SKPaymentTransactionObserver, SKProductsRequestDelegate, SKRequestDelegate>
{
	PurchasesManagerState										_state;

	NSError*													_error;
	NSMutableDictionary*										_purchasableItems;
	
	Reachability*												_reachability;
}
@end

@implementation PurchasesManager

#pragma mark - Properties

+ (PurchasesManager*)sharedManager
{
	if (!_sharedManager)
	{
		_sharedManager = [PurchasesManager new];
	}
	
	return _sharedManager;
}

#pragma mark - Initialization

- (id)init
{
    self = [super init];
	
    if (self)
	{
        _state = kPurchasesManagerStateNone;
		
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(_reachabilityStateDidChanged:)
													 name:kReachabilityChangedNotification
												   object:nil];
		
		_reachability = [Reachability reachabilityForInternetConnection];
		[_reachability startNotifier];
    }
	
    return self;
}

- (void)loadPurchasableItems
{
	_purchasableItems = [NSMutableDictionary new];
	
	//issues
	NSDictionary* issues = [NSDictionary dictionaryWithContentsOfFile:[self _purchasesPlistPath]];
	
	[issues enumerateKeysAndObjectsUsingBlock:^(NSString* issueName, NSDictionary* dict, BOOL *stop) {
		
		NSMutableDictionary* issueDict = [dict mutableCopy];
		issueDict[@"issueName"] = issueName;
		[_purchasableItems setObject:[PurchasableItem itemWithDictionary:issueDict] forKey:issueName];
	}];
	
	if(![SKPaymentQueue canMakePayments])
	{
		[self _composeAnErrorWithCode:kPurchaseErrorPaymentNotAllowed descripition:@"Your device is unable or not allowed to make purchases."];
		[self _setState:kPurchasesManagerStateInitializingFailed];
	}
	else
	{
		[self _requestProductsDataFromAppStore];
		[self _setState:kPurchasesManagerStateInitializing];
	}
	
	[[SKPaymentQueue defaultQueue] addTransactionObserver:self];
}

- (void)_requestProductsDataFromAppStore
{
	NSMutableArray* arr = [NSMutableArray arrayWithCapacity:_purchasableItems.count];
	
	for (PurchasableItem* item in [_purchasableItems allValues])
	{
		[arr addObject:item.appStoreId];
	}
	
	NSMutableSet* identifiers = [NSMutableSet setWithArray:arr];
	
	SKProductsRequest* request = [[SKProductsRequest alloc] initWithProductIdentifiers:identifiers];
    request.delegate = self;
    [request start];
}

#pragma mark - Issues

- (BOOL)purchaseIssueNamed:(NSString*)name
{
    if(_reachability.currentReachabilityStatus == NotReachable)
    {
        [[[UIAlertView alloc] initWithTitle:@"Could not restore your purchases because Internet connection appears to be offline."
									message:nil
								   delegate:nil
						  cancelButtonTitle:@"OK"
						  otherButtonTitles:nil] show];
        return NO;
    }
    
	if ([self purchaseStateForIssueName:name] != kIssuePurchaseStateNotPurchased)
	{
		return NO;
	}
	
	PurchasableItem* item = [_purchasableItems objectForKey:name];
	
	if(!item)
	{
		NSAssert1(0, @"Trying to purchase wrong issue %@", name);
	}
    
#ifdef DEBUG_TEST_PURCHASES
    
    [self _setState:kPurchasesManagerStatePurchasingItem];
    for (PurchasableItem* item in [_purchasableItems allValues])
    {
        if ([item.issueName isEqualToString:name])
        {
            item.purchaseState = kIssuePurchaseStatePurchased;
            [self _saveItems];
            break;
        }
    }
    [self _setState:kPurchasesManagerStateInitialized];
    
#else
	
	SKPayment* payment = [SKPayment paymentWithProduct:item.product];
	[[SKPaymentQueue defaultQueue] addPayment:payment];
	[self _setState:kPurchasesManagerStatePurchasingItem];
    
#endif
    
    return YES;
}

#pragma mark - SKProductRequestDelegate

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response
{
	if (response.products.count > 0)
	{
		[response.products enumerateObjectsUsingBlock:^(SKProduct* product, NSUInteger idx, BOOL *stop) {
			
			for (PurchasableItem* item in [_purchasableItems allValues])
			{
				if ([item.appStoreId isEqualToString:product.productIdentifier])
				{
					item.product = product;
					break;
				}
			}
		}];
		
		[self _setState:kPurchasesManagerStateInitialized];
		
		[[NSNotificationCenter defaultCenter] postNotificationName:PurchasesManagerReceivedProductsInformationNotification object:nil];
	}
	else
	{
		[self _composeAnErrorWithCode:kPurchaseErrorUnknown descripition:@"Unable to connect to AppStore. Please check your Internet connection."];
		[self _setState:kPurchasesManagerStateInitializingFailed];
	}
	
	if (response.invalidProductIdentifiers.count > 0)
	{
		NSLog(@"Some product IDs are invalid:");
		[response.invalidProductIdentifiers enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
			
			NSLog(@"%@", obj);
		}];
	}
}

- (void)request:(SKRequest *)request didFailWithError:(NSError *)error
{
	[self _composeAnErrorWithCode:kPurchaseErrorUnknown descripition:@"Unable to connect to AppStore. Please check your Internet connection."];
	[self _setState:kPurchasesManagerStateInitializingFailed];
	NSLog(@"STATUS: request did fail with error: %@", error.localizedDescription);
}

#pragma mark - SKPaymentTransactionObserver

- (void)paymentQueue:(SKPaymentQueue*)queue updatedTransactions:(NSArray*)transactions
{
	
	[transactions enumerateObjectsUsingBlock:^(SKPaymentTransaction* transaction, NSUInteger idx, BOOL *stop) {
        
		switch (transaction.transactionState)
        {
			case SKPaymentTransactionStateFailed:
			{
				[[SKPaymentQueue defaultQueue] finishTransaction:transaction];
				PurchaseErrorType errorCode = transaction.error.code == SKErrorPaymentCancelled ? kPurchaseErrorPaymentCancelled : kPurchaseErrorUnknown;
				
				[self _composeAnErrorWithCode:errorCode descripition:@"Failed to finish transaction"];
				[self _setState:kPurchasesManagerStatePurchasingFailed];
                _state = kPurchasesManagerStateInitialized;
			}
				break;
			
            case SKPaymentTransactionStateRestored:
			case SKPaymentTransactionStatePurchased:
			{
				[[SKPaymentQueue defaultQueue] finishTransaction:transaction];
				
				for (PurchasableItem* item in [_purchasableItems allValues])
				{
					if ([item.appStoreId isEqualToString:transaction.payment.productIdentifier] && item.purchaseState == kIssuePurchaseStateNotPurchased)
					{
						item.purchaseState = kIssuePurchaseStatePurchased;
						[self _saveItems];
						break;
					}
				}
				
//				[[WeaponManager sharedManager].weapons enumerateObjectsUsingBlock:^(WeaponInfo* weaponInfo, NSUInteger idx, BOOL *stop) {
//					
//					[weaponInfo.upgrades enumerateObjectsUsingBlock:^(WeaponUpgradeInfo* upgradeInfo, NSUInteger idx, BOOL *stop) {
//						
//						if ([upgradeInfo.appStoreID isEqualToString:transaction.payment.productIdentifier])
//						{
//							[[WeaponManager sharedManager] upgradeWeapon:weaponInfo];
//						}
//					}];
//				}];
				
				[self _setState:kPurchasesManagerStateInitialized];
			}
				break;
				
			case SKPaymentTransactionStatePurchasing:
			{
				// Transaction is being processed by AppStore. Nothing to do.
			}
				break;
				
		}
	}];
	
	if (_state == kPurchasesManagerStateRestoringPurchases)
	{
		[self _saveItems];
		[self _setState:kPurchasesManagerStateInitialized];
	}
}

- (void)paymentQueue:(SKPaymentQueue *)queue removedTransactions:(NSArray *)transactions
{
	[transactions enumerateObjectsUsingBlock:^(SKPaymentTransaction* transaction, NSUInteger idx, BOOL *stop){
		
		NSLog(@"STATUS: Transaction %@ from %@ with product ID %@ is removed.",
			  transaction.transactionIdentifier,
			  transaction.transactionDate,
			  transaction.payment.productIdentifier);
	}];
}

- (void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error
{
	PurchaseErrorType errorCode = error.code == SKErrorPaymentCancelled ? kPurchaseErrorPaymentCancelled : kPurchaseErrorUnknown;
	
	[self _composeAnErrorWithCode:errorCode descripition:@"Failed to finish transaction"];
	[self _setState:kPurchasesManagerStatePurchasingFailed];
    _state = kPurchasesManagerStateInitialized;
}

#pragma mark - Downloading

- (BOOL)restorePurchases
{
    if(_reachability.currentReachabilityStatus == NotReachable)
    {
        [[[UIAlertView alloc] initWithTitle:@"Could not restore your purchases because Internet connection appears to be offline."
									message:nil
								   delegate:nil
						  cancelButtonTitle:@"OK"
						  otherButtonTitles:nil] show];
        return NO;
    }
    
	[self _setState:kPurchasesManagerStateRestoringPurchases];
	[[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
    
    return YES;
}

- (NSArray*)availableIssues
{
	NSMutableArray* arr = [NSMutableArray array];
	
	for (PurchasableItem* item in [_purchasableItems allValues])
	{
		if (item.purchaseState == kIssuePurchaseStatePurchased)
		{
			[arr addObject:item.issueName];
		}
	}
	
	return arr;
}

- (IssuePurchaseState)purchaseStateForIssueName:(NSString *)issueName
{
	PurchasableItem* item = _purchasableItems[issueName];
	
	return [item purchaseState];
}

- (NSString*)priceStringForName:(NSString *)name
{
	return [[_purchasableItems objectForKey:name] appStorePriceString];
}
  
- (void)_setState:(PurchasesManagerState)newState
{
	if (_state == newState)
	{
		return;
	}
	
	PurchasesManagerState oldState = _state;
	_state = newState;
	
	switch (newState)
	{
		case kPurchasesManagerStateInitialized:
			break;
			
		case kPurchasesManagerStateInitializing:
			break;
			
		case kPurchasesManagerStateInitializingFailed:
			break;
			
		case kPurchasesManagerStatePurchasingItem:
			break;
			
		case kPurchasesManagerStatePurchasingFailed:
			break;
			
		case kPurchasesManagerStateRestoringPurchases:
			break;
			
		default:
			NSAssert1(0, @"Unknown PurchasesManagerState %d", _state);
			break;
	}
	
	[_delegate purchasesManager:self didSwitchToState:_state fromState:oldState];
}

- (void)_composeAnErrorWithCode:(NSUInteger)code descripition:(NSString*)descr
{
	NSDictionary* userInfo = [NSDictionary dictionaryWithObjectsAndKeys: descr, NSLocalizedDescriptionKey, nil];
	_error = [NSError errorWithDomain:@"PurcahesManager" code:code userInfo:userInfo];
}

#pragma mark - Private Methods

- (void)_saveItems
{
	NSMutableDictionary* dictToSave = [NSMutableDictionary dictionaryWithCapacity:_purchasableItems.count];
	
	for (PurchasableItem* item in [_purchasableItems allValues])
	{
		if (item.issueName.length > 0)
		{
			[dictToSave setObject:[item dictionaryRepresentation] forKey:item.issueName];
		}
	}
	
	[dictToSave writeToFile:[self _purchasesPlistPath] atomically:YES];
}

- (NSString*)_purchasesPlistPath
{
	NSFileManager* fileManager = [NSFileManager defaultManager];
    NSString* documentsDirectory = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
	NSString* assetsDirectory = [documentsDirectory stringByAppendingPathComponent: @"Issues"];
    NSString* plistPath = [assetsDirectory stringByAppendingPathComponent:@"purchasableItemsData.plist"];
	
	if(![fileManager fileExistsAtPath:assetsDirectory isDirectory:nil])
	{
		[fileManager createDirectoryAtPath: assetsDirectory
			   withIntermediateDirectories: YES
								attributes: nil
									 error: nil];
		
		[fileManager addSkipBackupAttributeToItemAtURL: [NSURL fileURLWithPath: assetsDirectory]];
	}
	
	if (![fileManager fileExistsAtPath:plistPath])
	{
		[fileManager copyItemAtPath:[[NSBundle mainBundle] pathForResource:@"purchasableItemsData" ofType:@"plist"] toPath:plistPath error:nil];
	}
	
	return plistPath;
}

#pragma mark - Reachability

- (void)_reachabilityStateDidChanged:(NSNotification*)notification
{
	switch (_reachability.currentReachabilityStatus)
	{
		case NotReachable:
		{
			
		}
			break;
			
		case ReachableViaWiFi:
		case ReachableViaWWAN:
		{
			if(_state == kPurchasesManagerStateInitializingFailed)
			{
				if([SKPaymentQueue canMakePayments])
				{
					[self _requestProductsDataFromAppStore];
					[self _setState:kPurchasesManagerStateInitializing];
				}
			}
		}
			break;
			
		default:
			break;
	}
}

@end
