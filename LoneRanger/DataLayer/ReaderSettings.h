//
//  ReaderSettings.h
//  Unity-iPhone
//
//  Created by Artem Manzhura on 5/24/13.
//
//

#import <Foundation/Foundation.h>

@interface ReaderSettings : NSObject

@property (assign, nonatomic)   BOOL    voEnabled;
@property (assign, nonatomic)   BOOL    textBubblesEnabled;
@property (assign, nonatomic)   BOOL    audioEnabled;
@property (assign, nonatomic)   float   volume;
@property (assign, nonatomic)   BOOL    transitionAnimated;
@property (assign, nonatomic)   BOOL    autoplay;

+ (ReaderSettings*)sharedManager;
- (BOOL)isVOEnabled;
- (BOOL)isTextBubblesEnabled;
- (BOOL)isAudioEnabled;
- (BOOL)isTransitionAnimated;
- (BOOL)isAutoplay;

@end
