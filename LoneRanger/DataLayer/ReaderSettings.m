//
//  ReaderSettings.m
//  Unity-iPhone
//
//  Created by Artem Manzhura on 5/24/13.
//
//

#import "ReaderSettings.h"


static ReaderSettings*  manager = nil;

@implementation ReaderSettings

- (id)init
{
    self = [super init];
    if (self) {
        NSDictionary *appDefaults = [NSDictionary dictionaryWithObjects:@[@YES, @YES, @YES, @85, @YES, @NO]
                                                                forKeys:@[@"VO", @"TextBubbles", @"audio", @"volume", @"transitionAnimated", @"autoplay"]];
        [[NSUserDefaults standardUserDefaults] registerDefaults:appDefaults];
        
    }
    return self;
}

- (void)setVolume:(float)volume
{
    [[NSUserDefaults standardUserDefaults] setFloat:volume forKey:@"volume"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)setVoEnabled:(BOOL)enabled
{
    [[NSUserDefaults standardUserDefaults] setBool:enabled forKey:@"VO"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)setTextBubblesEnabled:(BOOL)enabled
{
    [[NSUserDefaults standardUserDefaults] setBool:enabled forKey:@"TextBubbles"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)setAutoplay:(BOOL)autoplay
{
    [[NSUserDefaults standardUserDefaults] setBool:autoplay forKey:@"autoplay"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)setTransitionAnimated:(BOOL)animated
{
    [[NSUserDefaults standardUserDefaults] setBool:animated forKey:@"transitionAnimated"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)setAudioEnabled:(BOOL)enabled
{
    [[NSUserDefaults standardUserDefaults] setBool:enabled forKey:@"audio"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (float)volume
{
    return [[NSUserDefaults standardUserDefaults] floatForKey:@"volume"];
}

- (BOOL)isAutoplay
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:@"autoplay"];
}

- (BOOL)isTransitionAnimated
{
    BOOL enabled = [[NSUserDefaults standardUserDefaults] boolForKey:@"transitionAnimated"];
    return enabled;
}

- (BOOL)isVOEnabled
{
    BOOL enabled = [[NSUserDefaults standardUserDefaults] boolForKey:@"VO"];
    return enabled;
}

- (BOOL)isTextBubblesEnabled
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:@"TextBubbles"];
}

- (BOOL)isAudioEnabled
{
    BOOL enabled = [[NSUserDefaults standardUserDefaults] boolForKey:@"audio"];
    return enabled;
}

+ (ReaderSettings*)sharedManager
{
    if(!manager)
    {
        manager = [[ReaderSettings alloc] init];
    }
    return manager;
}

@end
