//
//  SfxInfo.h
//  Unity-iOS Simulator
//
//  Created by Artem Manzhura on 6/26/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>

@interface SfxInfo : NSObject

@property (assign)      CGFloat     time;
@property (readonly)    NSString*   soundPath;

+ (SfxInfo*)sfxFromDictionary:(NSDictionary*)dict withRootPath:(NSString*)path;

- (NSDictionary*)sfxRepresentation;

@end
