//
//  SfxInfo.m
//  Unity-iOS Simulator
//
//  Created by Artem Manzhura on 6/26/13.
//
//

#import "SfxInfo.h"

@implementation SfxInfo

- (void)dealloc
{
    [_soundPath release];
    [super dealloc];
}

+ (SfxInfo*)sfxFromDictionary:(NSDictionary*)dict withRootPath:(NSString*)path
{
    SfxInfo* sfxInfo = [[[self alloc]init]autorelease];

    sfxInfo->_time  = [[dict objectForKey:@"time"] floatValue];
    
    if([dict objectForKey: @"sound"])
    {
        NSString* soundPath = [dict objectForKey: @"sound"];
		
		if (soundPath.length > 0)
		{
            NSString* pathComponent = @"speechBubbles";
			sfxInfo->_soundPath = [[path stringByAppendingPathComponent:[pathComponent stringByAppendingPathComponent:soundPath]] retain];
		}
    }
    
	return sfxInfo;
}

- (NSDictionary*)sfxRepresentation
{
    return @{@"time" : @(_time), @"sound" : _soundPath};
}

@end
