//
//  SocialManager.h
//  Unity-iOS Simulator
//
//  Created by Vladislav Bakuta on 9/10/13.
//
//

#import <Foundation/Foundation.h>
#import <Social/Social.h>
#import <Accounts/Accounts.h>

@interface SocialManager : NSObject

+ (SocialManager *)sharedManager;

- (void)postOnFacebook:(UIViewController *)sender;
- (void)postOnTwitter: (UIViewController *)sender;

@end
