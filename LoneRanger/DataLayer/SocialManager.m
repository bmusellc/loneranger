//
//  SocialManager.m
//  Unity-iOS Simulator
//
//  Created by Vladislav Bakuta on 9/10/13.
//
//

#import "SocialManager.h"
#import "AlertView.h"

@implementation SocialManager
{
    NSTimer                     *_timer;
    SLComposeViewController     *twitterPost;
}

static SocialManager *socialManager;

+ (SocialManager *)sharedManager
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        socialManager = [[SocialManager alloc] init];
    });
    return socialManager;
}

#pragma mark Facebook

-(void)postOnFacebook:(UIViewController *)sender
{
    SLComposeViewController *facebookPost = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
    [facebookPost setInitialText:@" Hey! I'm playing Lone Ranger!"];
    [facebookPost addURL:[NSURL URLWithString:@"https://www.facebook.com/pages/Loneranger_test/207266686115582?ref=hl"]];
    [sender presentViewController:facebookPost animated:YES completion:nil];
}

#pragma mark Twitter

- (void)postOnTwitter:(UIViewController *)sender
{
    twitterPost = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    [twitterPost setInitialText:@"Hey! I'm playing Lone Ranger!\n"];
    [twitterPost addURL:[NSURL URLWithString:@"https://www.facebook.com/pages/Loneranger_test/207266686115582?ref=hl"]];
//    SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
//        AlertView* alert;
//        switch (result) {
//            case SLComposeViewControllerResultCancelled:
//                break;
//            case SLComposeViewControllerResultDone:
//                 alert = [[AlertView alloc] initWithTitle:@"DONE!" fontSize:40];
//                [alert showInView:sender.view];
//            default:
//                break;
//        }
//        [alert release];
//        [twitterPost dismissViewControllerAnimated:YES completion:Nil];
//    };
//    twitterPost.completionHandler =myBlock;
    [sender presentViewController:twitterPost animated:YES completion:nil];
    twitterPost = nil;
}

@end
