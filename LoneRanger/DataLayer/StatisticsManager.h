//
//  StatisticsManager.h
//  Unity-iOS Simulator
//
//  Created by Yuriy Belozerov on 01.06.13.
//
//

#import "IssuePlayStats.h"

extern NSString* StatisticsManagerChangedLevelStatisticsNotification;

@interface StatisticsManager : NSObject

@property(readonly)	NSInteger					coins;
@property(readonly)	IssuePlayStats*				playStats;

+ (StatisticsManager*)sharedManager;

- (void)updateLevelStatistics:(LevelStats*)levelStats;

- (void)decreaseCoins:(NSInteger)decrement;
- (void)increaseCoins:(NSInteger)increment;

@end
