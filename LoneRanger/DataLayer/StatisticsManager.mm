//
//  StatisticsManager.m
//  Unity-iOS Simulator
//
//  Created by Yuriy Belozerov on 01.06.13.
//
//

#import "StatisticsManager.h"
#import "IssuesManager.h"
#import "LevelsManager.h"

NSString* StatisticsManagerChangedLevelStatisticsNotification = @"StatisticsManagerChangedLevelStatisticsNotification";

static StatisticsManager* sharedPtr = nil;

@interface StatisticsManager ()
{
	
}

@end

@implementation StatisticsManager

+ (StatisticsManager*)sharedManager
{
	if (!sharedPtr)
	{
		sharedPtr = [StatisticsManager new];
	}
	
	return sharedPtr;
}

- (id)init
{
    self = [super init];
    
	if (self)
	{
		NSString* plistPath = [self _statisticsPlistPath];
		
		if([[NSFileManager defaultManager] fileExistsAtPath:plistPath])
		{
			_playStats = [IssuePlayStats issuePlayStatsWithDictionary:[NSDictionary dictionaryWithContentsOfFile:plistPath]];
		}
		else
		{
			_playStats = [IssuePlayStats issuePlayStatsWithDictionary:nil];
		}
		
		[_playStats retain];
		
		if ([[NSUserDefaults standardUserDefaults] objectForKey:@"coins"])
		{
			_coins = [[NSUserDefaults standardUserDefaults] integerForKey:@"coins"];
		}
		else
		{
			_coins = 0;
			[[NSUserDefaults standardUserDefaults] setInteger:_coins forKey:@"coins"];
		}
    }
    
	return self;
}

- (void)updateLevelStatistics:(LevelStats*)levelStats
{
	[self increaseCoins:levelStats.coinsEarned];
	[self.playStats updateLevelStatistics:levelStats];
	[self _saveStatisticsPlist];
	
	[[NSNotificationCenter defaultCenter] postNotificationName:StatisticsManagerChangedLevelStatisticsNotification object:nil];
}

- (void)decreaseCoins:(NSInteger)decrement
{
	if (_coins - decrement >= 0)
	{
		_coins -= decrement;
		[[NSUserDefaults standardUserDefaults] setInteger:_coins forKey:@"coins"];
	}
}

- (void)increaseCoins:(NSInteger)increment
{
	_coins += increment;
	[[NSUserDefaults standardUserDefaults] setInteger:_coins forKey:@"coins"];
}

- (NSString*)_statisticsPlistPath
{
	return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0] stringByAppendingPathComponent:@"statistics.plist"];
}

- (void)_saveStatisticsPlist
{
	NSString* plistPath = [self _statisticsPlistPath];
	[[self.playStats dictionaryRepresentation] writeToFile:plistPath atomically:YES];
	
	NSURL* plistURL = [NSURL fileURLWithPath:plistPath];
	[plistURL setResourceValue:[NSNumber numberWithBool:YES] forKey:NSURLIsExcludedFromBackupKey error:nil];
}

@end

extern "C"
{
	void updateLevelStatistics(const char* statsString)
	{
		NSLog(@"stats = %s", statsString);
		
		LevelStats* levelStats = [LevelStats levelStatsWithString:[NSString stringWithCString:statsString encoding:NSUTF8StringEncoding]];
		[[StatisticsManager sharedManager] updateLevelStatistics:levelStats];
	}
	
	const char* totalCoins()
	{
		const char* infoString = [[NSString stringWithFormat:@"%d", [StatisticsManager sharedManager].coins] cStringUsingEncoding:NSUTF8StringEncoding];
		
		char* result = (char*)malloc(strlen(infoString) + 1);
		
		memset(result, '\0', sizeof(char) * strlen(infoString) + 1);
		strcpy(result, infoString);
		
		NSLog(@"coins = %s", result);
		
		return result;
	}
}
