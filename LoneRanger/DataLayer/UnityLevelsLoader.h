//
//  UnityLevelsLoader.h
//  Unity-iPhone
//
//  Created by Vitaliy Kolomoets on 3/29/13.
//
//

#import <Foundation/Foundation.h>
typedef enum
{
	kUnityLevelsLoaderStateIdle,
	kUnityLevelsLoaderStateLoadingLevel
}UnityLevelsLoaderState;

@class UnityLevelsLoader;

@protocol UnityLevelsLoaderDelegate <NSObject>

@optional
- (void)onLevelLoaded;
- (void)onLevelUnloaded;
- (void)showArmoryOverUnity;
@end

@interface UnityLevelsLoader : NSObject

@property (readonly) UnityLevelsLoaderState					state;
@property (assign)	 NSObject<UnityLevelsLoaderDelegate>*	delegate;

+ (UnityLevelsLoader*)sharedLoader;
- (void)loadLevelWithIndex:(NSUInteger)levelIndex forIssueWithIndex:(NSInteger)issueIndex forConsumer:(NSObject<UnityLevelsLoaderDelegate>*)consumer;
- (void)loadWaypointWithIndex:(NSUInteger)waypointIndex forConsumer:(NSObject<UnityLevelsLoaderDelegate>*)consumer;
- (void)loadLevelNamed:(NSString*)levelName forConsumer:(NSObject<UnityLevelsLoaderDelegate>*)consumer;
- (void)unloadLevel;
@end
