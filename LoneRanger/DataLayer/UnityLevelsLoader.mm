//
//  UnityLevelsLoader.m
//  Unity-iPhone
//
//  Created by Vitaliy Kolomoets on 3/29/13.
//
//

#import "UnityLevelsLoader.h"
//#import "iPhone_View.h"

extern void UnitySendMessage(const char * className, const char * methodName, const char * param);

static UnityLevelsLoader*	_sharedLoader = nil;

@interface UnityLevelsLoader()
- (void)_onLevelLoaded;
@end

@implementation UnityLevelsLoader

+ (UnityLevelsLoader*)sharedLoader
{
	if(!_sharedLoader)
	{
		_sharedLoader = [UnityLevelsLoader new];
	}
	return _sharedLoader;
}

- (void)loadLevelWithIndex:(NSUInteger)levelIndex forIssueWithIndex:(NSInteger)issueIndex forConsumer:(NSObject<UnityLevelsLoaderDelegate>*)consumer
{
	if(_state != kUnityLevelsLoaderStateIdle)
	{
		return;
	}
	_state = kUnityLevelsLoaderStateLoadingLevel;
	_delegate = consumer;
//    UnitySendMessage("Main Camera", "loadLevel", [[NSString stringWithFormat:@"level%d", levelIndex] cStringUsingEncoding:NSUTF8StringEncoding]);
}

- (void)loadWaypointWithIndex:(NSUInteger)waypointIndex forConsumer:(NSObject<UnityLevelsLoaderDelegate>*)consumer
{
	if(_state != kUnityLevelsLoaderStateIdle)
	{
		return;
	}
	_state = kUnityLevelsLoaderStateLoadingLevel;
	_delegate = consumer;
	NSString* levelName = [NSString stringWithFormat:@"%d", waypointIndex];
//    UnitySendMessage("Main Camera", "loadWaypointNamed", [levelName cStringUsingEncoding:NSUTF8StringEncoding]);
}

- (void)loadLevelNamed:(NSString*)levelName forConsumer:(NSObject<UnityLevelsLoaderDelegate>*)consumer
{
	if(_state != kUnityLevelsLoaderStateIdle)
	{
		return;
	}
	_state = kUnityLevelsLoaderStateLoadingLevel;
	_delegate = consumer;
//    UnitySendMessage("Main Camera", "loadLevelNamed", [levelName cStringUsingEncoding:NSUTF8StringEncoding]);
}

- (void)unloadLevel
{
//    UnitySendMessage("Main Camera", "loadLevel", "main");
	[_delegate onLevelUnloaded];
}

- (void)_onLevelLoaded
{
	[_delegate onLevelLoaded];
//	_delegate = nil;
	_state = kUnityLevelsLoaderStateIdle;
}

- (void)showArmoryOverUnity
{
	[_delegate showArmoryOverUnity];
}

@end
	
extern "C"
{
//    void onLevelLoaded()
//    {
//        [[UnityLevelsLoader sharedLoader] _onLevelLoaded];
//    }
//    
//    void onLevelFinished()
//    {
//        [[UnityLevelsLoader sharedLoader] unloadLevel];
//        hideUnity();
//    }
//    
//    void showArmoryOverUnity()
//    {
//        [[UnityLevelsLoader sharedLoader] showArmoryOverUnity];
//    }
}
