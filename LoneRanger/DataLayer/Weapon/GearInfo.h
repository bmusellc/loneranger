//
//  GearInfo.h
//  Unity-iOS Simulator
//
//  Created by Vladislav Bakuta on 8/29/13.
//
//

#import <Foundation/Foundation.h>
#import "GearUpgradeInfo.h"

typedef enum
{
	kGearTypeVest,
    kGearTypeBadge,
}
GearType;

typedef enum
{
    GearModelHeavyVest,
    GearModelInfantryVest,
	GearModelSteelVest,
    GearModelCopperBadge,
    GearModelSilverBadge,
    GearModelGoldBadge,
}
GearModel;

@interface GearInfo : NSObject

@property(readonly)	GearModel			model;
@property(readonly)	GearType			type;
@property(readonly)	NSString*			iconPath;
@property(readonly)	NSArray*			features;

//according to current upgrade level
@property(readonly) NSInteger           level;
@property(readonly) NSInteger           maxLevel;
@property(readonly)	NSInteger			reducesDamage;
@property(readonly)	NSString*			description;
@property(readonly) NSInteger			price;              //coins to unlock next upgrade
@property(readonly)	NSString*			appStoreID;
@property(readonly)	NSString*			appStorePrice;

//properties for serialization
@property(readonly)	NSString*			typeStr;
@property(readonly)	NSString*			modelStr;

+ (GearInfo*)gearInfoWithType:(GearType)type;
+ (GearInfo*)gearInfoWithDictionary:(NSDictionary*)dict;

- (NSDictionary*)   dictionaryRepresentation;
- (NSString*)       stringRepresentation;

@end
