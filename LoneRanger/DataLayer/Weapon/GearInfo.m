//
//  GearInfo.m
//  Unity-iOS Simulator
//
//  Created by Vladislav Bakuta on 8/29/13.
//
//

#import "GearInfo.h"
#define MAX_UPGRADE_LEVEL 3

@interface GearInfo ()
{
    GearUpgradeInfo*				_upgradeInfo;
}
@end

@implementation GearInfo

- (NSInteger)level
{
    return _upgradeInfo.level;
}

- (NSInteger)maxLevel
{
    return MAX_UPGRADE_LEVEL;
}

- (NSInteger)reducesDamage
{
	return _upgradeInfo.reducesDamage;
}

- (NSString*)description
{
	return _upgradeInfo.description;
}

- (NSString*)iconPath
{
	return [NSString stringWithFormat:@"menu/Armory/Gear/%@", _upgradeInfo.iconPath];
}

- (NSInteger)price
{
	return _upgradeInfo.price;
}

- (NSString*)appStoreID
{
	return _upgradeInfo.appStoreID;
}

- (NSString*)appStorePrice
{
	return _upgradeInfo.appStorePrice;
}


+ (GearInfo*)gearInfoWithType:(GearType)type
{
	GearInfo* info = [[[self alloc] init] autorelease];
	info->_type = type;
	return info;
}

+ (GearInfo*)gearInfoWithDictionary:(NSDictionary*)dict
{
	GearInfo* info = [[self new] autorelease];
	
	info->_type = [GearInfo _typeFromString:[dict objectForKey:@"type"]];
	info->_model = [GearInfo _modelFromString:dict[@"model"]];
    NSMutableArray* features = [NSMutableArray array];
    
    GearUpgradeInfo* upgradeInfo = [GearUpgradeInfo gearUpgradeInfoWithDictionary:dict[@"features"]];
    [features addObject:upgradeInfo];
    info->_upgradeInfo = upgradeInfo;
	info->_features = [features copy];
    
	return info;
}

- (id)init
{
    self = [super init];
    if (self)
	{
        
    }
    return self;
}

- (void)dealloc
{
    [super dealloc];
}

- (NSString*)typeStr
{
	switch (self.type)
	{
		case kGearTypeVest:
			return @"vest";
            
        case kGearTypeBadge:
            return @"badge";
			
		default:
			return nil;
	}
}

- (NSString*)modelStr
{
	switch (self.model)
	{
		case GearModelHeavyVest:
			return @"Texas Ranger Heavy Vest";
			
		case GearModelInfantryVest:
			return @"Infantry Vest";
			
		case GearModelSteelVest:
			return @"Steel Vest";
			
        case GearModelCopperBadge:
            return @"Copper Ranger Badge";
            
        case GearModelSilverBadge:
            return @"Bright Silver Badge";
            
        case GearModelGoldBadge:
            return @"Gold Ranger Badge";
            
		default:
			return nil;
	}
}

- (NSDictionary*)dictionaryRepresentation
{
	NSMutableArray* features = [NSMutableArray array];
	
	[self.features enumerateObjectsUsingBlock:^(GearUpgradeInfo* info, NSUInteger idx, BOOL *stop) {
		[features addObject:[info dictionaryRepresentation]];
	}];
	
	return @{@"type" : self.typeStr,
            @"model" : self.modelStr,
         @"features" : features};
}

- (NSString*)stringRepresentation
{
	NSString* formattedString = @"";
	
	formattedString = [formattedString stringByAppendingFormat:@"%d,", self.model];
	formattedString = [formattedString stringByAppendingFormat:@"%d,", self.type];
	formattedString = [formattedString stringByAppendingFormat:@"%d,", self.reducesDamage];
	
	return formattedString;
}

+ (GearModel)_modelFromString:(NSString*)modelString
{
	GearModel model = -1;
	
	if([modelString isEqualToString:@"Texas Ranger Heavy Vest"])
	{
		return GearModelHeavyVest;
	}
	else if([modelString isEqualToString:@"Infantry Vest"])
	{
		return GearModelInfantryVest;
	}
	else if([modelString isEqualToString:@"Steel Vest"])
	{
		return GearModelSteelVest;
	}
    else if ([modelString isEqualToString:@"Copper Ranger Badge"])
    {
        return GearModelCopperBadge;
    }
    else if ([modelString isEqualToString:@"Bright Silver Badge"])
    {
        return GearModelSilverBadge;
    }
    else if ([modelString isEqualToString:@"Gold Ranger Badge"])
    {
        return GearModelGoldBadge;
    }
	
	return model;
}

+ (GearModel)_typeFromString:(NSString*)typeString
{
	GearType type = -1;
	
	if ([typeString isEqualToString:@"vest"])
	{
		return kGearTypeVest;
	}
    else if ([typeString isEqualToString:@"badge"])
    {
        return kGearTypeBadge;
    }
	
	return type;
}

@end
