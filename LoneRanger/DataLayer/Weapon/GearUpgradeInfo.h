//
//  GearUpgradeInfo.h
//  Unity-iOS Simulator
//
//  Created by Vladislav Bakuta on 8/29/13.
//
//

#import <Foundation/Foundation.h>

@interface GearUpgradeInfo : NSObject

+ (GearUpgradeInfo*)gearUpgradeInfoWithDictionary:(NSDictionary*)dict;

@property(readonly)	NSInteger		level;
@property(readonly)	BOOL			purchased;

@property(readonly)	NSInteger		reducesDamage;
@property(readonly) NSInteger       heal;
@property(readonly)	NSString*		description;
@property(readonly)	NSString*		iconPath;
@property(readonly)	NSInteger		price;
@property(readonly)	NSString*		appStoreID;
@property(copy)		NSString*		appStorePrice;

- (NSDictionary*)dictionaryRepresentation;

@end
