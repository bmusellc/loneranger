//
//  GearUpgradeInfo.m
//  Unity-iOS Simulator
//
//  Created by Vladislav Bakuta on 8/29/13.
//
//

#import "GearUpgradeInfo.h"

@interface GearUpgradeInfo()
{
    NSString* _descr;
}
@end

@implementation GearUpgradeInfo

+ (GearUpgradeInfo*)gearUpgradeInfoWithDictionary:(NSDictionary*)dict
{
	GearUpgradeInfo* upgradeInfo = [[GearUpgradeInfo new] autorelease];
	
	upgradeInfo->_level         = [dict[@"level"]           integerValue];
	upgradeInfo->_purchased     = [dict[@"purchased"]       boolValue];
	upgradeInfo->_reducesDamage = [dict[@"reducesDamage"]   integerValue];
    upgradeInfo->_heal          = [dict[@"heal"]            integerValue];
	upgradeInfo->_descr   = [dict[@"description"]     copy];
	upgradeInfo->_iconPath      = [dict[@"icon"]            copy];
	upgradeInfo->_price         = [dict[@"price"]           integerValue];
	upgradeInfo->_appStoreID    = [dict[@"appStoreID"]      copy];
	
	return upgradeInfo;
}

- (void)dealloc
{
	[_descr       release];
	[_iconPath          release];
	[_appStoreID        release];
	[_appStorePrice     release];
    [super dealloc];
}

- (NSString *)description
{
    return _descr;
}

- (NSDictionary*)dictionaryRepresentation
{
	return @{@"level"       : @(self.level),
		  @"purchased"      : @(self.purchased),
		  @"reducesDamage"  : @(self.reducesDamage),
		  @"description"    :   self.description,
		  @"icon"           :   self.iconPath,
		  @"price"          : @(self.price),
		  @"appStoreID"     :   self.appStoreID};
}

@end
