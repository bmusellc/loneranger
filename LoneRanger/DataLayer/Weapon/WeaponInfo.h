//
//  WeaponInfo.h
//  Unity-iPhone
//
//  Created by Vitaliy Kolomoets on 4/5/13.
//
//

#import "WeaponUpgradeInfo.h"
#import <CoreGraphics/CoreGraphics.h>

typedef enum
{
	kWeaponTypePistol,
    kWeaponTypeShotgun,
}
WeaponType;

typedef enum
{
	WeaponModelPShooter,
	WeaponModelRanger,
	WeaponModelModArmy,
    WeaponModelDoubleBarrel,
}
WeaponModel;

typedef enum
{
	WeaponUpgradeLevelNotOwned = -1,
	WeaponUpgradeLevelStock,
	WeaponUpgradeLevel1,
	WeaponUpgradeLevel2,
	WeaponUpgradeLevel3,
}
WeaponUpgradeLevel;

@interface WeaponInfo : NSObject

@property(readonly)	WeaponModel			model;
@property(readonly)	WeaponType			type;
@property(readonly)	NSString*			iconPath;

@property(readonly)	NSArray*			upgrades;
@property(readonly)	WeaponUpgradeLevel	upgradeLevel;
@property(readonly)	WeaponUpgradeLevel	maxUpgradeLevel;

//according to current upgrade level
@property(readonly)	NSInteger			clipSize;
@property(readonly)	CGFloat				reloadTime;
@property(readonly)	CGFloat				recoil;
@property(readonly)	NSInteger			accuracy;
@property(readonly)	CGFloat				damage;
@property(readonly)	NSString*			description;
@property(readonly) NSInteger			price; //coins to unlock next upgrade
@property(readonly)	NSString*			appStoreID;
@property(readonly)	NSString*			appStorePrice;

//properties for serialization
@property(readonly)	NSString*			typeStr;
@property(readonly)	NSString*			modelStr;

+ (WeaponInfo*)weaponInfoWithType:(WeaponType)type;
+ (WeaponInfo*)weaponInfoWithDictionary:(NSDictionary*)dict;

- (NSDictionary*)dictionaryRepresentation;
- (NSString*)stringRepresentation;

- (void)increaseUpgradeLevel;

- (void)upgradeToMaxLevel;

@end
