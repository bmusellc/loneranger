//
//  WeaponInfo.m
//  Unity-iPhone
//
//  Created by Vitaliy Kolomoets on 4/5/13.
//
//

#import "WeaponInfo.h"

@interface WeaponInfo ()
{
	WeaponUpgradeInfo*						_upgradeInfo;
}

@end

@implementation WeaponInfo

- (NSInteger)clipSize
{
	return _upgradeInfo.clipSize;
}

- (CGFloat)reloadTime
{
	return _upgradeInfo.reloadTime;
}

- (CGFloat)recoil
{
	return _upgradeInfo.recoil;
}

- (NSInteger)accuracy
{
	return _upgradeInfo.accuracy;
}

- (CGFloat)damage
{
	return _upgradeInfo.damage;
}

- (NSString*)description
{
	return _upgradeInfo.description;
}

- (NSString*)iconPath
{
	return [NSString stringWithFormat:@"menu/Armory/Weapons/%@", _upgradeInfo.iconPath];
}

- (NSInteger)price
{
	switch (self.upgradeLevel)
	{
		case WeaponUpgradeLevelNotOwned:
		case WeaponUpgradeLevel3:
			return _upgradeInfo.price;
			
		case WeaponUpgradeLevelStock:
		case WeaponUpgradeLevel1:
		case WeaponUpgradeLevel2:
		{
			WeaponUpgradeInfo* nextUpgradeInfo = _upgrades[self.upgradeLevel + 1];
			return nextUpgradeInfo.price;
		}
				
		default:
			return 0;
	}
}

- (NSString*)appStoreID
{
	switch (self.upgradeLevel)
	{
		case WeaponUpgradeLevelNotOwned:
		case WeaponUpgradeLevel3:
			return _upgradeInfo.appStoreID;
			
		case WeaponUpgradeLevelStock:
		case WeaponUpgradeLevel1:
		case WeaponUpgradeLevel2:
		{
			WeaponUpgradeInfo* nextUpgradeInfo = _upgrades[self.upgradeLevel + 1];
			return nextUpgradeInfo.appStoreID;
		}
			
		default:
			return 0;
	}
}

- (NSString*)appStorePrice
{
	switch (self.upgradeLevel)
	{
		case WeaponUpgradeLevelNotOwned:
		case WeaponUpgradeLevel3:
			return _upgradeInfo.appStorePrice;
			
		case WeaponUpgradeLevelStock:
		case WeaponUpgradeLevel1:
		case WeaponUpgradeLevel2:
		{
			WeaponUpgradeInfo* nextUpgradeInfo = _upgrades[self.upgradeLevel + 1];
			return nextUpgradeInfo.appStorePrice;
		}
			
		default:
			return 0;
	}
}

- (WeaponUpgradeLevel)maxUpgradeLevel
{
	return self.upgrades.count - 1;
}

+ (WeaponInfo*)weaponInfoWithType:(WeaponType)type
{
	WeaponInfo* info = [[[self alloc]init]autorelease];
	info->_type = type;
	return info;
}

+ (WeaponInfo*)weaponInfoWithDictionary:(NSDictionary*)dict
{
	WeaponInfo* info = [[self new] autorelease];
	
	info->_type = [WeaponInfo _typeFromString:[dict objectForKey:@"type"]];
	info->_model = [WeaponInfo _modelFromString:dict[@"model"]];
	info->_upgradeLevel = WeaponUpgradeLevelNotOwned;
	
	NSMutableArray* upgrades = [NSMutableArray array];
	
	[dict[@"upgrades"] enumerateObjectsUsingBlock:^(NSDictionary* upgradeInfoDict, NSUInteger idx, BOOL *stop) {
		
		WeaponUpgradeInfo* upgradeInfo = [WeaponUpgradeInfo weaponUpgradeInfoWithDictionary:upgradeInfoDict];
		[upgrades addObject:upgradeInfo];
		
		if (upgradeInfo.purchased)
		{
			info->_upgradeLevel = upgradeInfo.level;
			info->_upgradeInfo = upgradeInfo;
		}
	}];
    
	info->_upgrades = [upgrades copy];
	
	if (info.upgradeLevel == WeaponUpgradeLevelNotOwned && info.upgrades.count > 0)
	{
		info->_upgradeInfo = info.upgrades[0];
	}
	return info;
}

- (id)init
{
    self = [super init];
    if (self)
	{
        
    }
    return self;
}

- (void)dealloc
{
	[_upgrades release];
    
    [super dealloc];
}

- (NSString*)typeStr
{
	switch (self.type)
	{
		case kWeaponTypePistol:
			return @"pistol";
			
        case kWeaponTypeShotgun:
            return @"shotgun";
		default:
			return nil;
	}
}

- (NSString*)modelStr
{
	switch (self.model)
	{
		case WeaponModelPShooter:
			return @"5-Shot P-Shooter";
			
		case WeaponModelRanger:
			return @"6-Shot Ranger";
			
		case WeaponModelModArmy:
			return @"7-Shot Mod-Army";
        
        case WeaponModelDoubleBarrel:
            return @"Double-Barrel";
			
		default:
			return nil;
	}
}

- (NSDictionary*)dictionaryRepresentation
{
	NSMutableArray* upgrades = [NSMutableArray array];
	
	[self.upgrades enumerateObjectsUsingBlock:^(WeaponUpgradeInfo* info, NSUInteger idx, BOOL *stop) {
		
		[upgrades addObject:[info dictionaryRepresentation]];
	}];
	
	return @{@"type" : self.typeStr,
		  @"model" : self.modelStr,
		  @"upgrades" : upgrades};
}

- (NSString*)stringRepresentation
{
	NSString* formattedString = @"";
	
	formattedString = [formattedString stringByAppendingFormat:@"%d,", self.model];
	formattedString = [formattedString stringByAppendingFormat:@"%d,", self.type];
	formattedString = [formattedString stringByAppendingFormat:@"%.2f,", self.damage];
	formattedString = [formattedString stringByAppendingFormat:@"%d,", self.clipSize];
	formattedString = [formattedString stringByAppendingFormat:@"%d,", self.clipSize * 8];
	formattedString = [formattedString stringByAppendingFormat:@"%d,", self.accuracy];
	formattedString = [formattedString stringByAppendingFormat:@"%.2f,", self.recoil];
	formattedString = [formattedString stringByAppendingFormat:@"%.2f,", self.reloadTime];
	
	return formattedString;
}

- (void)increaseUpgradeLevel
{
	if (self.upgradeLevel == self.maxUpgradeLevel)
	{
		return;
	}
	
	_upgradeLevel++;
	self->_upgradeInfo = self.upgrades[self.upgradeLevel];
	[self->_upgradeInfo setValue:@(YES) forKey:@"purchased"];
}

- (void)upgradeToMaxLevel
{
	while (self.upgradeLevel != self.maxUpgradeLevel)
	{
		[self increaseUpgradeLevel];
	}
}

+ (WeaponModel)_modelFromString:(NSString*)modelString
{
	WeaponModel model = -1;
	
	if([modelString isEqualToString:@"5-Shot P-Shooter"])
	{
		return WeaponModelPShooter;
	}
	else if([modelString isEqualToString:@"6-Shot Ranger"])
	{
		return WeaponModelRanger;
	}
	else if([modelString isEqualToString:@"7-Shot Mod-Army"])
	{
		return WeaponModelModArmy;
	}
    else if ([modelString isEqualToString:@"Double-Barrel"])
	{
        return WeaponModelDoubleBarrel;
    }
	return model;
}

+ (WeaponType)_typeFromString:(NSString*)typeString
{
	WeaponType type = -1;
	
	if([typeString isEqualToString:@"pistol"])
	{
		return kWeaponTypePistol;
	}
    if ([typeString isEqualToString:@"shotgun"]) {
        return kWeaponTypeShotgun;
    }
	
	return type;
}

@end
