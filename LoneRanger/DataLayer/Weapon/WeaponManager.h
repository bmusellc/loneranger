//
//  WeaponManager.h
//  Unity-iPhone
//
//  Created by Vitaliy Kolomoets on 4/5/13.
//
//

#import "WeaponInfo.h"
#import "GearInfo.h"

@interface WeaponManager : NSObject

+ (WeaponManager*)sharedManager;

@property(retain)	WeaponInfo*                         currentWeapon;
@property(retain)   GearInfo*                           currentGear;

@property(readonly)	NSArray*							weapons;
@property(readonly)	NSArray*							availableWeapons;

@property(readonly)	NSArray*							gear;
@property(readonly)	NSArray*							availableGear;

@property(readonly)	NSArray*							appStoreIDs;

- (void)upgradeWeapon:(WeaponInfo*)weaponInfo;

@end
