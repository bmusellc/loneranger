//
//  WeaponManager.m
//  Unity-iPhone
//
//  Created by Vitaliy Kolomoets on 4/5/13.
//
//

#import "WeaponManager.h"

static WeaponManager* _sharedManager = nil;

@interface WeaponManager ()
{
	NSMutableArray*				_weapons;
    NSMutableArray*             _gear;
}

@end

@implementation WeaponManager

- (NSArray*)weapons
{
	return _weapons;
}

- (NSArray*)gear
{
    return _gear;
}

- (NSArray*)availableWeapons
{
	NSMutableArray* weapons = [NSMutableArray array];
	
	[self.weapons enumerateObjectsUsingBlock:^(WeaponInfo* info, NSUInteger idx, BOOL *stop) {
		
		if (info.upgradeLevel > WeaponUpgradeLevelNotOwned)
		{
			[weapons addObject:info];
		}
	}];
	
	return weapons;
}

- (NSArray*)appStoreIDs
{
	NSMutableArray* IDs = [NSMutableArray array];
	
	[self.weapons enumerateObjectsUsingBlock:^(WeaponInfo* weaponInfo, NSUInteger idx, BOOL *stop) {
		
		[weaponInfo.upgrades enumerateObjectsUsingBlock:^(WeaponUpgradeInfo* upgradeInfo, NSUInteger idx, BOOL *stop) {
			
			if (upgradeInfo.appStoreID.length > 0)
			{
				[IDs addObject:upgradeInfo.appStoreID];
			}
		}];
	}];
	
	return IDs;
}

+ (WeaponManager*)sharedManager
{
	if(!_sharedManager)
	{
		_sharedManager = [[WeaponManager alloc] init];
	}
	
	return _sharedManager;
}

- (id)init
{
    self = [super init];
    
	if (self)
	{
		_weapons = [NSMutableArray new];
		NSString* plistPath = [self _weaponsPlistPath];
		NSString* bundlePath = [[NSBundle mainBundle] pathForResource:@"weapons" ofType:@"plist"];
		NSArray* rawPlistData = nil;
		
		if(![[NSFileManager defaultManager] fileExistsAtPath:plistPath])
		{
			rawPlistData = [NSArray arrayWithContentsOfFile:bundlePath];
			[rawPlistData writeToFile:plistPath atomically:YES];
			
			NSURL* plistURL = [NSURL fileURLWithPath:plistPath];
			[plistURL setResourceValue:[NSNumber numberWithBool:YES] forKey:NSURLIsExcludedFromBackupKey error:nil];
		}
		else
		{
			rawPlistData = [NSArray arrayWithContentsOfFile:plistPath];
		}
		
		[rawPlistData enumerateObjectsUsingBlock:^(NSDictionary* dict, NSUInteger idx, BOOL *stop) {
			[_weapons addObject:[WeaponInfo weaponInfoWithDictionary:dict]];
		}];
		
		self.currentWeapon = self.weapons[0];
        
		
#if DEBUG_OPEN_ALL_WEAPONS
		[self.weapons enumerateObjectsUsingBlock:^(WeaponInfo* weaponInfo, NSUInteger idx, BOOL *stop) {
			
			[weaponInfo upgradeToMaxLevel];
		}];
		
		self.currentWeapon = self.weapons[2];
#endif
        
        
        _gear = [NSMutableArray new];
		NSString* gearPlistPath = [self _gearPlistPath];
		NSString* gearBundlePath = [[NSBundle mainBundle] pathForResource:@"gear" ofType:@"plist"];
		NSArray* gearRawPlistData = nil;
		
		if(![[NSFileManager defaultManager] fileExistsAtPath:gearPlistPath])
		{
			gearRawPlistData = [NSArray arrayWithContentsOfFile:gearBundlePath];
			[gearRawPlistData writeToFile:gearPlistPath atomically:YES];
			
			NSURL* gearPlistURL = [NSURL fileURLWithPath:gearPlistPath];
			[gearPlistURL setResourceValue:[NSNumber numberWithBool:YES] forKey:NSURLIsExcludedFromBackupKey error:nil];
		}
		else
		{
			gearRawPlistData = [NSArray arrayWithContentsOfFile:gearPlistPath];
		}
		
		[gearRawPlistData enumerateObjectsUsingBlock:^(NSDictionary* dict, NSUInteger idx, BOOL *stop) {
            [_gear addObject:[GearInfo gearInfoWithDictionary:dict]];
		}];
        
		self.currentGear = self.gear[0];
    }
    
    return self;
}

- (void)upgradeWeapon:(WeaponInfo*)weaponInfo
{
	[weaponInfo increaseUpgradeLevel];
	[self _saveWeaponsPlist];
}

- (NSString*)_weaponsPlistPath
{
	return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0] stringByAppendingPathComponent:@"weapons.plist"];
}

- (void)_saveWeaponsPlist
{
	NSMutableArray* weapons = [NSMutableArray array];
	
	[self.weapons enumerateObjectsUsingBlock:^(WeaponInfo* info, NSUInteger idx, BOOL *stop) {
		
		[weapons addObject:[info dictionaryRepresentation]];
	}];
	
	[weapons writeToFile:[self _weaponsPlistPath] atomically:YES];
}

- (NSString*)_gearPlistPath
{
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0] stringByAppendingPathComponent:@"gear.plist"];
}

- (void)_saveGearPlist
{
    NSMutableArray* gear = [NSMutableArray array];
	
	[self.gear enumerateObjectsUsingBlock:^(GearInfo* info, NSUInteger idx, BOOL *stop)
    {		
		[gear addObject:[info dictionaryRepresentation]];
	}];
	
	[gear writeToFile:[self _gearPlistPath] atomically:YES];
}

@end

extern "C"
{
	const char* currentWeaponInfo()
	{
		const char* infoString = [[[[[WeaponManager sharedManager] currentWeapon] stringRepresentation] retain] cStringUsingEncoding:NSUTF8StringEncoding];
		
		char* result = (char*)malloc(strlen(infoString) + 1);
		
		strcpy(result, infoString);
		
		return result;
	}
}

extern "C"
{
	const char* currentGearInfo()
	{
		const char* infoString = [[[[[WeaponManager sharedManager] currentGear] stringRepresentation] retain] cStringUsingEncoding:NSUTF8StringEncoding];
		
		char* result = (char*)malloc(strlen(infoString) + 1);
		
		strcpy(result, infoString);
		
		return result;
	}
}
