//
//  WeaponUpgradeInfo.h
//  Unity-iOS Simulator
//
//  Created by Yuriy Belozerov on 22.05.13.
//
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>

@interface WeaponUpgradeInfo : NSObject

+ (WeaponUpgradeInfo*)weaponUpgradeInfoWithDictionary:(NSDictionary*)dict;

@property(readonly)	NSInteger		level;
@property(readonly)	BOOL			purchased;

@property(readonly)	NSInteger		clipSize;
@property(readonly)	CGFloat			reloadTime;
@property(readonly)	CGFloat			recoil;
@property(readonly)	NSInteger		accuracy;
@property(readonly)	CGFloat			damage;
@property(readonly)	NSString*		iconPath;
@property(readonly)	NSInteger		price;
@property(readonly)	NSString*		appStoreID;
@property(copy)		NSString*		appStorePrice;

- (NSDictionary*)dictionaryRepresentation;

@end
