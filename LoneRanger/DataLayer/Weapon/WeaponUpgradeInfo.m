//
//  WeaponUpgradeInfo.m
//  Unity-iOS Simulator
//
//  Created by Yuriy Belozerov on 22.05.13.
//
//

#import "WeaponUpgradeInfo.h"

@interface WeaponUpgradeInfo()
{
    NSString* _descr;
}
@end

@implementation WeaponUpgradeInfo

+ (WeaponUpgradeInfo*)weaponUpgradeInfoWithDictionary:(NSDictionary*)dict
{
	WeaponUpgradeInfo* upgradeInfo = [[WeaponUpgradeInfo new] autorelease];
	
	upgradeInfo->_level = [dict[@"level"] integerValue];
	upgradeInfo->_purchased = [dict[@"purchased"] boolValue];
	upgradeInfo->_clipSize = [dict[@"clipSize"] integerValue];
	upgradeInfo->_reloadTime = [dict[@"reloadTime"] floatValue];
	upgradeInfo->_recoil = [dict[@"recoil"] floatValue];
	upgradeInfo->_accuracy = [dict[@"accuracy"] integerValue];
	upgradeInfo->_damage = [dict[@"damage"] floatValue];
	upgradeInfo->_descr = [dict[@"description"] copy];
	upgradeInfo->_iconPath = [dict[@"icon"] copy];
	upgradeInfo->_price = [dict[@"price"] integerValue];
	upgradeInfo->_appStoreID = [dict[@"appStoreID"] copy];
	
	return upgradeInfo;
}

- (void)dealloc
{
	[_descr release];
	[_iconPath release];
	[_appStoreID release];
	[_appStorePrice release];
    
    [super dealloc];
}

-(NSString *)description
{
    return _descr;
}

- (NSDictionary*)dictionaryRepresentation
{
	return @{@"level": @(self.level),
		  @"purchased" : @(self.purchased),
		  @"clipSize": @(self.clipSize),
		  @"reloadTime" : @(self.reloadTime),
		  @"recoil" : @(self.recoil),
		  @"accuracy" : @(self.accuracy),
		  @"damage" : @(self.damage),
		  @"description" : self.description,
		  @"icon" : self.iconPath,
		  @"price" : @(self.price),
		  @"appStoreID" : self.appStoreID};
}

@end
