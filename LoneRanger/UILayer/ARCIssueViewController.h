//
//  ChaptersViewController.h
//  RussellJamesPhotobook
//
//  Created by Yuriy Bilozerov on 1/17/13.
//  Copyright (c) 2013 bMuse Ukraine. All rights reserved.
//

#import "AudioPlayer.h"
#import "Issue.h"
#import "HelpView.h"
#import "ReaderSettingsView.h"

typedef enum  {
    kARCIssueViewReviewStateFullPage = 0,
    kARCIssueViewReviewStateFrameByFrame = 1,
    kARCIssueViewReviewStateZooming = 2
    }ARCIssueViewReviewState;

@interface ARCIssueViewController : UIViewController <UIScrollViewDelegate>
{
	NSMutableArray*								_pageViews;
	UIScrollView*								_scrollView;

	UIView*                                     _indicatingView;
	UIView*										_visibleRectParentView;
	UIView*										_visibleRectPointerView;

    ARCIssueViewReviewState                     _reviewState;
    
    AudioPlayer*                                _speechBubblesPlayer;
    AudioPlayer*                                _ambientSoundPlayer;

    UIButton*                                   _backButton;
    UIButton*                                   _settingsButton;
    UIButton*                                   _helpButton;
    UIButton*                                   _switchScreenMode;
    HelpView*                                   _helpOverlay;
    ReaderSettingsView*                         _settingsView;
    UIView*                                     _leftCurtain;
	UIView*                                     _rightCurtain;
	UIView*                                     _topCurtain;
	UIView*                                     _bottomCurtain;
}

@property(assign)	NSInteger                               currentPageIndex;
@property(assign, readonly)   ARCIssueViewReviewState       reviewState;
@property(strong)	UIPopoverController*                    sharingOptionsPopover;

- (id)initWithIssue:(Issue*)issue;

@end
