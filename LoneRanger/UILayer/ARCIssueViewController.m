//
//  ChaptersViewController.m
//  RussellJamesPhotobook
//
//  Created by Yuriy Bilozerov on 1/17/13.
//  Copyright (c) 2013 bMuse Ukraine. All rights reserved.
//

#import "ARCIssueViewController.h"

#import "PhotoPageView.h"
#import "FrameInfo.h"
#import <QuartzCore/QuartzCore.h>

#import "ReaderSettings.h"
#import "MainMenuNavigationController.h"
#import "SpeechBubble.h"
#import "LevelsManager.h"
#import "WeaponManager.h"
#import "BubblesManager.h"
#import "WaypointAnimationView.h"

#define IDLE_ANIMATION_DURATION         2.0

#define BUTTON_OFFSET_X                 10.0
#define BUTTON_OFFSET_Y                 10.0

#define MAX_ZOOM_SCALE                  25.0

#define FADE_DURATION					0.75

#define PAGES_SHADOW_BOUNDARY_WIDTH		(320 * (self.view.bounds.size.width / self.view.bounds.size.height))
#define MAX_PAGE_SHADOW_OPACITY			0.8

#define MAX_IDLE_TIME_INTERVAL			5

@interface ARCIssueViewController () <AudioPlayerDelegate, PageViewDelegate, UIGestureRecognizerDelegate, BubbleManagerDelegate>
{
    Issue*                          _issue;
	NSOperationQueue*				_imagesLoadingQueue;
	NSMutableDictionary*			_activeOperations;
	BubblesManager*                 _bubblesManager;
	BOOL							_needToRecalculatePagesBounds;
    BOOL                            _isSettingsOpened;
    
	NSInteger						_currentPageIndex;
	
    CGRect                          _lockingRect;
    NSInteger                       _lockedPageIndex;
    NSInteger                       _lockedFrameIndex;
	NSArray*						_pageInfos;
	NSInteger                       _currentBubbleSpeechInFrame;
    NSInteger                       _currentFrameForBubbleSpeech;

	BOOL                            _autoplayModeEnabled;
    BOOL                            _speechBubbleCanPlay;
    BOOL                            _isAnimating;
	
	NSTimer*						_idleTimer;
	UITapGestureRecognizer*			_idleRecognizer;
	
	NSMutableArray*					_effectViews;
	
	WaypointAnimationView*			_waypointAnimationView;
	
	BOOL							_viewAlreadyAppear;
}

@end

@implementation ARCIssueViewController

@synthesize reviewState = _reviewState;

- (NSInteger)currentPageIndex
{
	return _currentPageIndex;
}

- (void)setCurrentPageIndex:(NSInteger)currentPageIndex
{
	if (self.currentPageIndex != currentPageIndex)
	{
		_currentPageIndex = currentPageIndex;
	
		_currentFrameForBubbleSpeech = 0;
        _currentBubbleSpeechInFrame = 0;
		
		[self _updateContentThumbnails];
		[self _loadContent];
		
        [self _playAmbientSoundAtPageIndex:_currentPageIndex atFrameIndex:_currentFrameForBubbleSpeech];
        if(_reviewState == kARCIssueViewReviewStateFullPage)
        {
            [self _playFirstSpeechBubbleAtPageIndex:_currentPageIndex atFrameIndex:_currentFrameForBubbleSpeech];
        }
	}
}

- (BOOL)prefersStatusBarHidden
{
	return YES;
}

- (id)initWithIssue:(Issue*)issue
{
    self = [super init];
    
	if (self)
	{
        _issue = issue;
        _pageViews = [NSMutableArray new];
        _pageInfos = issue.pages;
        
        _activeOperations = [NSMutableDictionary dictionary];
		_imagesLoadingQueue = [NSOperationQueue new];
		_imagesLoadingQueue.maxConcurrentOperationCount = 1;
        
        _currentPageIndex   = -1;
		_isAnimating = NO;
        _lockedPageIndex    = 0;
        _lockedFrameIndex   = 0;
		
        _currentBubbleSpeechInFrame  = -1;
        _currentFrameForBubbleSpeech = -1;
        _reviewState = kARCIssueViewReviewStateFrameByFrame;
		
        _bubblesManager = [[BubblesManager alloc] initWithPages:_pageViews withState:_reviewState];
        _bubblesManager.delegate = self;
		_effectViews = [NSMutableArray array];
    }
	
    return self;
}

- (void)dealloc
{
    NSLog(@"Reader is dealloced");
//	[_bottomNavigationPanel removeObserver:self forKeyPath:@"bottomNavigationPanelPositon"];
}

- (void)viewDidLoad
{
	NSLog(@"View did load");
	
    [super viewDidLoad];

//    _speechBubblesPlayer = [[AudioPlayer alloc] init];
    _ambientSoundPlayer  = [[AudioPlayer alloc] init];
    
    _ambientSoundPlayer.delegate = self;
//    _speechBubblesPlayer.delegate = self;
    
	self.view.backgroundColor = [UIColor blackColor];
    
	_scrollView = [UIScrollView new];
	[self.view addSubview:_scrollView];
	_scrollView.showsVerticalScrollIndicator = NO;
	_scrollView.showsHorizontalScrollIndicator = NO;
	_scrollView.pagingEnabled = YES;
    _scrollView.pinchGestureRecognizer.enabled = NO;
    _scrollView.panGestureRecognizer.enabled = NO;
	_scrollView.scrollEnabled = NO;
    
    _leftCurtain = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, self.view.bounds.size.height)];
	_leftCurtain.backgroundColor = [UIColor blackColor];
	_leftCurtain.alpha = 0.0;
	_leftCurtain.userInteractionEnabled = NO;
	[self.view addSubview:_leftCurtain];
	
	_rightCurtain = [[UIView alloc] initWithFrame:CGRectMake(self.view.bounds.size.width, 0, 0, self.view.bounds.size.height)];
	_rightCurtain.backgroundColor = [UIColor blackColor];
	_rightCurtain.alpha = 0.0;
	_rightCurtain.userInteractionEnabled = NO;
	[self.view addSubview:_rightCurtain];
	
	_topCurtain = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 0)];
	_topCurtain.backgroundColor = [UIColor blackColor];
	_topCurtain.alpha = 0.0;
	_topCurtain.userInteractionEnabled = NO;
	[self.view addSubview:_topCurtain];
	
	_bottomCurtain = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height, self.view.bounds.size.width, 0)];
	_bottomCurtain.backgroundColor = [UIColor blackColor];
	_bottomCurtain.alpha = 0.0;
	_bottomCurtain.userInteractionEnabled = NO;
	[self.view addSubview:_bottomCurtain];
    
    _backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_backButton addTarget:self action:@selector(_onBackButton:) forControlEvents:UIControlEventTouchUpInside];
    UIImage* normalStateImage = [UIImage imageNamed:@"reader/backToMenuButton.png"];
    UIImage* presedStateImage = [UIImage imageNamed:@"reader/backToMenuButtonPressed.png"];
    [_backButton setBackgroundImage:normalStateImage forState:UIControlStateNormal];
    [_backButton setBackgroundImage:presedStateImage forState:UIControlStateHighlighted];
    _backButton.frame = CGRectMake(10, 10, normalStateImage.size.width, normalStateImage.size.height);
    [self.view addSubview:_backButton];
    _backButton.hidden = YES;
    
    
    _settingsButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_settingsButton addTarget:self action:@selector(_onSettingsButton:) forControlEvents:UIControlEventTouchUpInside];
    normalStateImage = [UIImage imageNamed:@"reader/settingsButton.png"];
    presedStateImage = [UIImage imageNamed:@"reader/settingsButtonPressed.png"];
    [_settingsButton setBackgroundImage:normalStateImage forState:UIControlStateNormal];
    [_settingsButton setBackgroundImage:presedStateImage forState:UIControlStateHighlighted];
    _settingsButton.frame = CGRectMake(0, 0, normalStateImage.size.width, normalStateImage.size.height);
    [self.view addSubview:_settingsButton];
    _settingsButton.hidden = YES;
    
    _helpButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_helpButton addTarget:self action:@selector(_onHelpButton:) forControlEvents:UIControlEventTouchUpInside];
    normalStateImage = [UIImage imageNamed:@"reader/helpButton.png"];
    presedStateImage = [UIImage imageNamed:@"reader/helpButtonPressed.png"];
    [_helpButton setBackgroundImage:normalStateImage forState:UIControlStateNormal];
    [_helpButton setBackgroundImage:presedStateImage forState:UIControlStateHighlighted];
    _helpButton.frame = CGRectMake(0, 0, normalStateImage.size.width, normalStateImage.size.height);
    [self.view addSubview:_helpButton];
    _helpButton.hidden = YES;
    
    
    _switchScreenMode = [UIButton buttonWithType:UIButtonTypeCustom];
    [_switchScreenMode addTarget:self action:@selector(_onSwitchScreenModeButton:) forControlEvents:UIControlEventTouchUpInside];
    normalStateImage = [UIImage imageNamed:@"reader/switchScreenModeButton.png"];
    presedStateImage = [UIImage imageNamed:@"reader/switchScreenModeButtonPressed.png"];
    [_switchScreenMode setBackgroundImage:normalStateImage forState:UIControlStateNormal];
    [_switchScreenMode setBackgroundImage:presedStateImage forState:UIControlStateHighlighted];
    _switchScreenMode.frame = CGRectMake(0, 0, normalStateImage.size.width, normalStateImage.size.height);
    [self.view addSubview:_switchScreenMode];
    _switchScreenMode.hidden = YES;
    
    UISwipeGestureRecognizer* swipeGetsureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(_handleSwipe:)];
	swipeGetsureRecognizer.direction = UISwipeGestureRecognizerDirectionLeft;
	swipeGetsureRecognizer.numberOfTouchesRequired = 1;
	[self.view addGestureRecognizer:swipeGetsureRecognizer];
    
    swipeGetsureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(_handleSwipe:)];
	swipeGetsureRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
	swipeGetsureRecognizer.numberOfTouchesRequired = 1;
	[self.view addGestureRecognizer:swipeGetsureRecognizer];

/*	swipeGetsureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(_handleSwipe:)];
	swipeGetsureRecognizer.direction = UISwipeGestureRecognizerDirectionUp;
	swipeGetsureRecognizer.numberOfTouchesRequired = 1;
	[self.view addGestureRecognizer:swipeGetsureRecognizer];
	
	swipeGetsureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(_handleSwipe:)];
	swipeGetsureRecognizer.direction = UISwipeGestureRecognizerDirectionDown;
	swipeGetsureRecognizer.numberOfTouchesRequired = 1;
	[self.view addGestureRecognizer:swipeGetsureRecognizer]; */
	
    _indicatingView = [UIView new];
    _indicatingView.layer.borderWidth = 4.0;
    _indicatingView.layer.borderColor = [[UIColor colorWithRed:196.0f/255.0f green:252.0f/255.0f blue:253.0f/255.0f alpha:1.0] CGColor];
    _indicatingView.layer.shadowColor = [[UIColor colorWithRed:196.0f/255.0f green:252.0f/255.0f blue:253.0f/255.0f alpha:1.0] CGColor];
    _indicatingView.layer.shadowOffset = CGSizeMake(1, 1);
    _indicatingView.layer.shadowOpacity = 1.0;
    _indicatingView.layer.shadowRadius = 4;
//    [self.view addSubview:_indicatingView];
    _indicatingView.frame = CGRectMake(50, 50, 250, 250);
    _indicatingView.userInteractionEnabled = NO;
    
    _helpOverlay = [[HelpView alloc] initWithFrame:self.view.bounds];
    [self.view addSubview:_helpOverlay];
    _helpOverlay.hidden = YES;
    UITapGestureRecognizer* tapRecognizerForControlOverlay = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(_handleHelpOverlayTapGesture:)];
    tapRecognizerForControlOverlay.numberOfTapsRequired = 1;
    tapRecognizerForControlOverlay.numberOfTouchesRequired = 1;
    [_helpOverlay addGestureRecognizer:tapRecognizerForControlOverlay];
    
    _settingsView = [[ReaderSettingsView alloc] initWithFrame:self.view.bounds];
    [self.view addSubview:_settingsView];
	_settingsView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
	_idleRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:nil action:nil];
	_idleRecognizer.delegate = self;
//	[self.view addGestureRecognizer:_idleRecognizer];
    _isSettingsOpened = NO;
	
	[_pageInfos enumerateObjectsUsingBlock:^(PageInfo* pageInfo, NSUInteger idx, BOOL *stop) {
		
		[_pageViews addObject:[NSNull null]];
	}];
	
	[self _showControls];
	
	self.currentPageIndex = 0;
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [_ambientSoundPlayer stop];
    [_bubblesManager stopShowing];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
	self.sharingOptionsPopover = nil;
    _speechBubbleCanPlay = NO;
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(_hideControls) object:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	_viewAlreadyAppear = YES;
	
	_speechBubbleCanPlay = YES;

    _helpOverlay.frame = self.view.bounds;
    _helpButton.frame = CGRectMake(self.view.bounds.size.width - 10 - _helpButton.frame.size.width, self.view.bounds.size.height - 10 - _helpButton.frame.size.height, _helpButton.frame.size.width, _helpButton.frame.size.height);
    _settingsButton.frame = CGRectMake(self.view.bounds.size.width - 10 - _settingsButton.frame.size.width, 10, _settingsButton.frame.size.width, _settingsButton.frame.size.height);
    _switchScreenMode.frame = CGRectMake(10, self.view.bounds.size.height - 10 - _switchScreenMode.frame.size.height, _switchScreenMode.frame.size.width, _switchScreenMode.frame.size.height);
	
	[self _recalculatePagesBounds];
	
    if (_reviewState == kARCIssueViewReviewStateFrameByFrame)
    {
        [self _lockToPageAtIndex:_lockedPageIndex frameAtIndex:_lockedFrameIndex animated:NO];
        [self _setCurtainsPositionsAnimated:NO];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear: animated];
//	[_bottomNavigationPanel reload];
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
	_needToRecalculatePagesBounds = (UIInterfaceOrientationIsLandscape(toInterfaceOrientation) && UIInterfaceOrientationIsPortrait(self.interfaceOrientation)) ||
									(UIInterfaceOrientationIsPortrait(toInterfaceOrientation) && UIInterfaceOrientationIsLandscape(self.interfaceOrientation));
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
	if (!_needToRecalculatePagesBounds || !_viewAlreadyAppear)
	{
		return;
	}
	
	[self _recalculatePagesBounds];
	
	if(_reviewState == kARCIssueViewReviewStateFrameByFrame)
	{
		[self _lockToPageAtIndex:_lockedPageIndex frameAtIndex:_lockedFrameIndex animated:NO];
		[self _setCurtainsPositionsAnimated:NO];
	}
}

- (void)_recalculatePagesBounds
{
	[_pageInfos enumerateObjectsUsingBlock:^(PageInfo* pageInfo, NSUInteger idx, BOOL *stop) {
		
		pageInfo.contentFrame = CGRectMake(self.view.bounds.size.width * idx, 0, self.view.bounds.size.width, self.view.bounds.size.height);
		
		id obj = _pageViews[idx];
		
		if (![obj isKindOfClass:[NSNull class]])
		{
			PageView* pageView = (PageView*)obj;
			
			if (_reviewState == kARCIssueViewReviewStateFrameByFrame)
			{
				pageView.frame = pageInfo.contentFrame;
				
				pageView.contentScrollView.zoomScale = 1.0;
				pageView.contentScrollView.contentSize = pageView.imageFrame.size;
				pageView.contentScrollView.contentOffset = CGPointZero;
				pageView.contentView.frame = pageView.imageFrame;
			}
			else
			{
				CGPoint relativeContentOffset = CGPointMake(pageView.contentScrollView.contentOffset.x / pageView.contentScrollView.contentSize.width,
															pageView.contentScrollView.contentOffset.y / pageView.contentScrollView.contentSize.height);
				
				pageView.frame = pageInfo.contentFrame;
				
				pageView.contentScrollView.contentSize = CGSizeMake(pageView.imageFrame.size.width * pageView.contentScrollView.zoomScale,
																	pageView.imageFrame.size.height * pageView.contentScrollView.zoomScale);
				
				pageView.contentView.frame = CGRectMake(MAX((pageView.contentScrollView.frame.size.width - pageView.contentScrollView.contentSize.width) / 2, 0),
														0,
														pageView.contentScrollView.contentSize.width,
														pageView.contentScrollView.contentSize.height);
				
				pageView.contentScrollView.contentOffset = CGPointMake(pageView.contentScrollView.contentSize.width * relativeContentOffset.x,
																	   pageView.contentScrollView.contentSize.height * relativeContentOffset.y);
			}
            
            if(idx == 1)
            {
                NSLog(@"%d", idx);
            }
            [pageView recalcBubbleFrames];
		}
	}];
	
	_scrollView.frame = self.view.bounds;
	_scrollView.contentSize = CGSizeMake(self.view.bounds.size.width * _pageInfos.count, self.view.bounds.size.height);
	_scrollView.contentOffset = CGPointMake(self.view.bounds.size.width * self.currentPageIndex, 0);
	
    _helpButton.frame = CGRectMake(self.view.bounds.size.width - 10 - _helpButton.frame.size.width, self.view.bounds.size.height - 10 - _helpButton.frame.size.height, _helpButton.frame.size.width, _helpButton.frame.size.height);
    
	_settingsButton.frame = CGRectMake(self.view.bounds.size.width - 10 - _settingsButton.frame.size.width, 10, _settingsButton.frame.size.width, _settingsButton.frame.size.height);
	
    _switchScreenMode.frame = CGRectMake(10, self.view.bounds.size.height - 10 - _switchScreenMode.frame.size.height, _switchScreenMode.frame.size.width, _switchScreenMode.frame.size.height);
    
    _helpOverlay.frame = self.view.bounds;
}

#pragma mark - NavigationPanel Delegate

- (void)_switchToNextFrame
{
    if ((_reviewState != kARCIssueViewReviewStateFrameByFrame) || _isAnimating)
    {
        return;
    }
    if (_lockedFrameIndex == [((PageInfo*)[_issue.pages objectAtIndex:_lockedPageIndex]).frames count] - 1)
    {
        // next frame is on the next page. need to check if next page exists at all
        if (_lockedPageIndex == [_issue.pages count] - 1)
        {
            NSLog(@"STATUS (%s): the last panel in the book. Nowhere to focus!", __FUNCTION__);
            [self dismissModalViewControllerAnimated:YES];
        }
        else
        {
			if([[ReaderSettings sharedManager] isTransitionAnimated])
			{
				[self _lockToPageAtIndex:_lockedPageIndex + 1 frameAtIndex:0 animated:NO];
				
				CATransition *customCAAnimation = [CATransition animation];
				[customCAAnimation setType:kCATransitionFade];
				[customCAAnimation setDuration:0.7f];
				[customCAAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear]];
				[[_scrollView layer] addAnimation:customCAAnimation forKey: @"customViewControllerAnimation"];
				[self _setCurtainsPositionsAnimated:YES];
			}
			else
			{
				[self _lockToPageAtIndex:_lockedPageIndex + 1 frameAtIndex:0 animated:NO];
				[self _setCurtainsPositionsAnimated:NO];
			}
        }
    }
    else
	{
        if(![[ReaderSettings sharedManager] isTransitionAnimated])
        {
            [self _lockToPageAtIndex:_lockedPageIndex frameAtIndex:_lockedFrameIndex + 1 animated:NO];
            [self _setCurtainsPositionsAnimated:NO];
            return;
        }
        
        PageView* pageView = (PageView*)[_pageViews objectAtIndex:_lockedPageIndex];
        FrameInfo* frameInfo = (FrameInfo*)[pageView.pageInfo.frames objectAtIndex:_lockedFrameIndex + 1];
        if(frameInfo.transitionType == kFrameInfoTransitionCrossfadeType)
        {
            [self _lockToPageAtIndex:_lockedPageIndex frameAtIndex:_lockedFrameIndex + 1 animated:NO];
            CATransition *customCAAnimation = [CATransition animation];
            [customCAAnimation setType:kCATransitionFade];
            [customCAAnimation setDuration:0.7f];
            [customCAAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear]];
            [[_scrollView layer] addAnimation:customCAAnimation forKey: @"customViewControllerAnimation"];
            [self _setCurtainsPositionsAnimated:YES];
        }
        else{
            [self _lockToPageAtIndex:_lockedPageIndex frameAtIndex:_lockedFrameIndex + 1 animated:YES];
            [self _setCurtainsPositionsAnimated:YES];
        }
    }
}

- (void)_switchToPreviousFrame
{
	if (_lockedFrameIndex == 0)
	{
		if (_lockedPageIndex == 0){
			NSLog(@"STATUS (%s): the first panel in the book. Nowhere to focus!", __FUNCTION__);
		}
		else
		{

			NSInteger pageToLock =  _lockedPageIndex - 1;
			NSInteger frameToLock = [((PageInfo*)[_issue.pages objectAtIndex:pageToLock]).frames count] - 1;
            
            if(![[ReaderSettings sharedManager] isTransitionAnimated])
            {
                [self _lockToPageAtIndex:pageToLock frameAtIndex:frameToLock animated:NO];
                [self _setCurtainsPositionsAnimated:NO];
                return;
            }
			[self _lockToPageAtIndex:pageToLock frameAtIndex:frameToLock animated:NO];
            
            CATransition *customCAAnimation = [CATransition animation];
            [customCAAnimation setType:kCATransitionFade];
            [customCAAnimation setDuration:0.7f];
            [customCAAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear]];
            [[_scrollView layer] addAnimation:customCAAnimation forKey: @"customViewControllerAnimation"];
            [self _setCurtainsPositionsAnimated:YES];
		}
	}
	else
	{
        
        if(![[ReaderSettings sharedManager] isTransitionAnimated])
        {
            [self _lockToPageAtIndex:_lockedPageIndex frameAtIndex:_lockedFrameIndex - 1 animated:NO];
            [self _setCurtainsPositionsAnimated:NO];
            return;
        }
        PageView* pageView = (PageView*)[_pageViews objectAtIndex:_lockedPageIndex];
        FrameInfo* frameInfo = (FrameInfo*)[pageView.pageInfo.frames objectAtIndex:_lockedFrameIndex - 1];
        if(frameInfo.transitionType == kFrameInfoTransitionCrossfadeType)
        {
            [self _lockToPageAtIndex:_lockedPageIndex frameAtIndex:_lockedFrameIndex - 1 animated:NO];
            CATransition *customCAAnimation = [CATransition animation];
            [customCAAnimation setType:kCATransitionFade];
            [customCAAnimation setDuration:0.7f];
            [customCAAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear]];
            [[_scrollView layer] addAnimation:customCAAnimation forKey: @"customViewControllerAnimation"];
            [self _setCurtainsPositionsAnimated:YES];
        }
        else{
            [self _lockToPageAtIndex:_lockedPageIndex frameAtIndex:_lockedFrameIndex - 1 animated:YES];
            [self _setCurtainsPositionsAnimated:YES];
        }
	}			

}

- (void)pageViewDidBeginZooming:(PageView*)sender
{
    _reviewState = kARCIssueViewReviewStateZooming;
    [_ambientSoundPlayer stop];
    [_bubblesManager pause];
    [self curtainsHidden:YES];
}

- (void)pageViewDidBecomePrimaryZoom:(PageView*)sender
{
    _reviewState = kARCIssueViewReviewStateFullPage;
    _bubblesManager.currentState = _reviewState;
    _currentFrameForBubbleSpeech = 0;
    _currentBubbleSpeechInFrame = 0;
    [self curtainsHidden:NO];
    [self _playAmbientSoundAtPageIndex:_currentPageIndex atFrameIndex:_currentFrameForBubbleSpeech];
    [self _playFirstSpeechBubbleAtPageIndex:_currentPageIndex atFrameIndex:_currentFrameForBubbleSpeech];

}

- (void)pageViewDidRecognizeTap:(PageView *)sender withRecognizer:(UIGestureRecognizer *)recognizer
{
    if (_reviewState == kARCIssueViewReviewStateZooming)
    {
        return;
    }
    
    CGRect visibleBounds = self.view.bounds;
    
    // checking taps for traversing panels
    CGPoint touchPoint = [recognizer locationInView:self.view];
    
    if (touchPoint.x < (CGRectGetMinX(visibleBounds) + 180))
    {
		if (_reviewState == kARCIssueViewReviewStateFrameByFrame)
		{
			[self _switchToPreviousFrame];
		}
		else
		{
            if(![[ReaderSettings sharedManager] isTransitionAnimated])
            {
                [self _scrollContentToPageAtIndex:_currentPageIndex - 1 animated:NO];
            }
            else{
                [self _scrollContentToPageAtIndex:_currentPageIndex - 1 animated:YES];
            }
		}
    }
    else if (touchPoint.x > (CGRectGetMaxX(visibleBounds) - 180))
    {
        if (_reviewState == kARCIssueViewReviewStateFrameByFrame)
		{
			[self _switchToNextFrame];
		}
		else
		{
            if(![[ReaderSettings sharedManager] isTransitionAnimated])
            {
                [self _scrollContentToPageAtIndex:_currentPageIndex + 1 animated:NO];
            }
            else{
                [self _scrollContentToPageAtIndex:_currentPageIndex + 1 animated:YES];
            }
		}
    }
    else
    {
        if (![self _isControlsHidden]) {
            [self _hideControls];
            return;
        }
        [self _showControls];
//        [self.view bringSubviewToFront:_backButton];
//        [self.view bringSubviewToFront:_settingsButton];
//        [self.view bringSubviewToFront:_helpButton];
//        [UIView animateWithDuration:0.3 animations:^{
//            _backButton.hidden = !_backButton.hidden;
//            _settingsButton.hidden = !_settingsButton.hidden;
//            _helpButton.hidden = !_helpButton.hidden;
//        }];
    }
}

- (void)pageViewDidRecognizeDoubleTap:(PageView *)sender withRecognizer:(UIGestureRecognizer *)recognizer
{
//    NSLog(@"pageViewDidRecognizeDoubleTap");
    __block NSInteger	pageForLockIndex      = -1;
	__block NSInteger	frameForLockIndex     = -1;
    [self curtainsHidden:NO];
    if (_reviewState == kARCIssueViewReviewStateFrameByFrame)
    {
		_reviewState = kARCIssueViewReviewStateFullPage;
		_bubblesManager.currentState = _reviewState;
        [_bubblesManager revealBubbles];
        
        [self _setCurtainsPositionsAnimated:YES];
        
        __block PageView* pageView = nil;
        [_pageViews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            pageView = (PageView*)obj;
            if (self.currentPageIndex == idx)
            {
                [UIView animateWithDuration:0.7 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionBeginFromCurrentState animations:^{
                    
                    pageView.contentScrollView.zoomScale = 1.0;
                    pageView.contentScrollView.contentOffset = CGPointZero;
                    
                } completion:^(BOOL finished) {
                    
                    _lockedFrameIndex   = -1;
                    _lockedPageIndex    = -1;
                }];
            }
            else{
                if (obj != [NSNull null])
                {
                    pageView.contentScrollView.zoomScale = 1.0;
                    pageView.contentScrollView.contentOffset = CGPointZero;
                }
            }
        }];
		
        return;
    }
	
//    _reviewState = kARCIssueViewReviewStateFrameByFrame;
//    _bubblesManager.currentState = _reviewState;
    
    PageView *pageView = sender;
    __block CGRect rectToZoom = CGRectNull;
    
    CGPoint pointInPageView = [recognizer locationInView:recognizer.view];
		
    if (CGRectContainsPoint(pageView.contentView.bounds, pointInPageView))
    {
        [pageView.pageInfo.frames enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            FrameInfo* frame = (FrameInfo*)obj;
            CGRect rectToTest = frame.rect;
            
            rectToTest = CGRectApplyAffineTransform(rectToTest, CGAffineTransformMakeScale(pageView.imageFrame.size.width, pageView.imageFrame.size.height));
            if (CGRectContainsPoint(rectToTest, pointInPageView))
            {
                pageForLockIndex = [_pageViews indexOfObject:pageView];
                frameForLockIndex = idx;
                rectToZoom = [_scrollView convertRect:rectToTest fromView:pageView];
                
                _reviewState = kARCIssueViewReviewStateFrameByFrame;
                _bubblesManager.currentState = _reviewState;
                sender.contentScrollView.scrollEnabled = NO;
                
                *stop = YES;
            }

        }];
    }
    
    if (!CGRectIsNull(rectToZoom))	{
        [self _lockToPageAtIndex:pageForLockIndex frameAtIndex:frameForLockIndex animated:YES];
        [self _setCurtainsPositionsAnimated:YES];
//        [UIView animateWithDuration:0.7 delay:0.0 options: UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionBeginFromCurrentState animations:^{
////            [self _lockToRect:rectToZoom withPage:pageView];
//        } completion:^(BOOL finished) {
//            _manualZooming = NO;
//        }];
//        [_scrollView zoomToRect:rectToZoom animated:YES];
	}
    
}

- (void)pageViewDidRecognizeSwipeUp:(PageView*)sender
{

}

- (void)pageViewDidRecognizeSwipeDown:(PageView*)sender
{

}

- (void)pageView:(PageView*)sender didSelectPageWithLocalIndex:(NSUInteger)localIndex;
{
//	NSUInteger idx = localIndex;
//	NSUInteger chapterIdx = [[LocalContentManager sharedInstance].chapters indexOfObject:sender.pageInfo.parentChapterInfo];
//	idx += [[LocalContentManager sharedInstance] firstPageAbsoluteIndexInChapter:chapterIdx];
//
//	[self _scrollContentToPageIndex:idx centerPage:YES];
//	[self _loadContent];
}

- (void)pageView:(PageView*)sender didSelectPageWithAbsoluteIndex:(int)index
{
//	[self _scrollContentToPageIndex:index centerPage:YES];
//	[self _loadContent];
}

//- (void)mainMenuPageViewDidSelectChapter:(ChapterInfo*)chapterInfo
//{
//	NSInteger chapterIdx = [[LocalContentManager sharedInstance].chapters indexOfObject:chapterInfo];
//	NSInteger idx = [[LocalContentManager sharedInstance] firstPageAbsoluteIndexInChapter:chapterIdx];
//	
//	[self _scrollContentToPageIndex:idx centerPage:NO];
//	[self _loadContent];
//}
//
//- (void)mainMenuPageWouldLikeToGoFullscreen:(PageView*)sender
//{
//	_scrollView.scrollEnabled = NO;
//	_bottomNavigationPanel.hidden = YES;
//	_controlsOverlay.hidden = YES;
//	
//	[_scrollView setZoomScale: _scrollView.minimumZoomScale];
//
//	CGRect mainMenuRect = [_scrollView convertRect:sender.bounds fromView:sender];
//	[_scrollView scrollRectToVisible:mainMenuRect animated:YES];
//}
//
//- (void)mainMenuPageWouldLikeToLeaveFullscreen:(PageView*)sender
//{
//	_scrollView.scrollEnabled = YES;
//	_bottomNavigationPanel.hidden = NO;
//}

//#pragma mark - ControlsOverlay Delegate
//
//- (void)chapterControlsOverlayView:(ChapterControlsOverlayView *)controlsView buttonDidPressed:(UIButton *)button
//{
//	switch (button.tag)
//	{
//		case ChapterControlTagHome:
//			[self _scrollToHomePage];
//			break;
//			
//		case ChapterControlTagText:
//			[self _setTextPanelsVisible:controlsView.textPanelsVisible animated:YES];
//			break;
//			
//		case ChapterControlTagSharing:
//			[self _openSharingMenuOptionsPopover];
//			break;
//			
//		default:
//			break;
//	}
//}

//#pragma mark - UIGestureRecognizerDelegate
//
//- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
//{
//	[self _enablePanelAndStartIdleTimer];
//	
//	return NO;
//}
//
//- (void)_enablePanelAndStartIdleTimer
//{
//	[_idleTimer invalidate];
//	_idleTimer = [NSTimer scheduledTimerWithTimeInterval:MAX_IDLE_TIME_INTERVAL
//												  target:self
//												selector:@selector(_idleTimerFired)
//												userInfo:nil
//												 repeats:NO];
//	
//	_controlsOverlay.alpha = 1.0;
//}
//
//- (void)_idleTimerFired
//{	
//	[UIView animateWithDuration:0.5
//					 animations:^{
//						 
//						 _controlsOverlay.alpha = 0.0;
//					 }];
//}

#pragma mark - AudioPlayer Delegates

- (void)audioPlayerDidFinishPlaying:(AudioPlayer*)audioPlayer
{
    if (audioPlayer == _ambientSoundPlayer)
    {
        ReaderSettings* settings = [ReaderSettings sharedManager];
        if (settings.isAudioEnabled) {
            _ambientSoundPlayer.volume = [[ReaderSettings sharedManager] volume] / 100.0f;
            [_ambientSoundPlayer play];
        }
    }
}

- (void)_playButtonTapped
{
//	PageInfo* pageInfo = [_pageInfos objectAtIndex:_currentPageIndex];
	
//	_controlsOverlay.audioPlayer.delegate = self;
//	
//	if (!_controlsOverlay.audioPlayer.initialized)
//	{
//		[_controlsOverlay.audioPlayer loadMusicWithFilePath:[[NSBundle mainBundle] pathForResource:pageInfo.audioPath ofType:@"m4a" inDirectory:@"Sounds"] startTime:0 volume:1.0];
//	}
	
//	[_effectViews makeObjectsPerformSelector:@selector(removeFromSuperview)];
//	[_effectViews removeAllObjects];
//	
//	PageView* pageView = _pageViews[_currentPageIndex];
	
//	KenBurnsEffectView* kenBurnsView = [[KenBurnsEffectView alloc] init];
//	kenBurnsView.frame = pageView.bounds;
//	[pageView insertSubview:kenBurnsView belowSubview:pageView.descriptionTextView];
//	kenBurnsView.imagePath = pageInfo.contentPath;
//	[kenBurnsView animateKenBurnsEffectAtPoint:pageInfo.pointOfInterest duration:_controlsOverlay.audioPlayer.duration];
//	[_effectViews addObject:kenBurnsView];
//	kenBurnsView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
//	kenBurnsView.imageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
//	kenBurnsView.rotationZoom = 1.25;
//		
//	[_controlsOverlay.audioPlayer play];
}

- (void)_pauseButtonTapped
{
//    [_controlsOverlay.audioPlayer pauseByUser];
//	
//	[_effectViews makeObjectsPerformSelector:@selector(removeFromSuperview)];
//	[_effectViews removeAllObjects];
}

- (void)_switchAudioTracks
{
//	[_playerTimes setObject:@(_controlsOverlay.audioPlayer.currentTime + FADE_DURATION / 2) forKey:@(_playingIndex)];
//	
//	if (_playingIndex == _currentPageIndex)
//	{
//		return;
//	}
//	
//	_playingIndex = _currentPageIndex;
//	
//	if (_controlsOverlay.audioPlayer.shouldAutoplay)
//	{
//		PageInfo* pageInfo = [_pageInfos objectAtIndex:_playingIndex];
//		NSString* path = pageInfo.audioPath ? [[NSBundle mainBundle] pathForResource:pageInfo.audioPath ofType:@"m4a" inDirectory:@"Sounds"] : nil;
//		
//		if (path)
//		{
//			CGFloat startTime = _playerTimes[@(_playingIndex)] ? [_playerTimes[@(_playingIndex)] floatValue] : 0;
//			
//			if (_controlsOverlay.audioPlayer.playing)
//			{
//				[_controlsOverlay.audioPlayer fadeOutVolumeWithDuration:FADE_DURATION
//									  completionHandler:^{
//										  
//										  [_controlsOverlay.audioPlayer loadMusicWithFilePath:path startTime:startTime volume:0];
//										  [_controlsOverlay.audioPlayer fadeInVolumeWithDuration:FADE_DURATION
//															   completionHandler:^{
//																   
//															   }];
//									  }];
//			}
//			else
//			{
//				[_controlsOverlay.audioPlayer loadMusicWithFilePath:path startTime:startTime volume:0];
//				[_controlsOverlay.audioPlayer fadeInVolumeWithDuration:FADE_DURATION
//									 completionHandler:^{
//										 
//									 }];
//			}
//			
//			[_effectViews makeObjectsPerformSelector:@selector(removeFromSuperview)];
//			[_effectViews removeAllObjects];
//			
//			PageView* pageView = _pageViews[_currentPageIndex];
//			
//			KenBurnsEffectView* kenBurnsView = [[KenBurnsEffectView alloc] init];
//			kenBurnsView.frame = pageView.bounds;
//			[pageView insertSubview:kenBurnsView belowSubview:pageView.descriptionTextView];
//			kenBurnsView.imagePath = pageInfo.contentPath;
//			[kenBurnsView animateKenBurnsEffectAtPoint:pageInfo.pointOfInterest duration:_controlsOverlay.audioPlayer.duration];
//			[_effectViews addObject:kenBurnsView];
//			kenBurnsView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
//			kenBurnsView.imageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
//			kenBurnsView.rotationZoom = 1.25;
//			
//			[_controlsOverlay.audioPlayer play];
//		}
//		else
//		{
//			[_controlsOverlay.audioPlayer fadeOutVolumeWithDuration:FADE_DURATION
//								  completionHandler:^{
//									  
//									  [_controlsOverlay.audioPlayer pause];
//								  }];
//		}
//	}
//	else
//	{
//		PageInfo* pageInfo = [_pageInfos objectAtIndex:_playingIndex];
//		NSString* path = pageInfo.audioPath ? [[NSBundle mainBundle] pathForResource:pageInfo.audioPath ofType:@"m4a" inDirectory:@"Sounds"] : nil;
//		
//		if (path)
//		{
//			CGFloat startTime = _playerTimes[@(_playingIndex)] ? [_playerTimes[@(_playingIndex)] floatValue] : 0;
//			[_controlsOverlay.audioPlayer loadMusicWithFilePath:path startTime:startTime volume:1];
//		}
//	}
}

#pragma mark - Internal implementations

- (void)_selectFrameAtIndex:(NSInteger)index
{
    if (_reviewState == kARCIssueViewReviewStateFullPage)
    {
        PageView* pageView = (PageView*)[_pageViews objectAtIndex:_currentPageIndex];
        FrameInfo* frameInfo = (FrameInfo*)[pageView.pageInfo.frames objectAtIndex:index];

        CGFloat imageHeight = pageView.bounds.size.height;
        CGFloat imageWidth = imageHeight * (pageView.thumbnailImageView.image.size.width / pageView.thumbnailImageView.image.size.height);
        CGRect imageFrame = CGRectMake((pageView.bounds.size.width - imageWidth) / 2, 0, imageWidth, imageHeight);
        CGRect rectToTest = CGRectApplyAffineTransform(frameInfo.rect, CGAffineTransformMakeScale(imageFrame.size.width, imageFrame.size.height));
//        float xOffset = (imageWidth / pageView.bounds.size.width) * pageView.bounds.size.width;
        float xOffset = (pageView.bounds.size.width - imageWidth) / 2.0;
        float yOffset = (pageView.bounds.size.height - imageHeight) / 2.0;
        rectToTest = CGRectMake(rectToTest.origin.x + xOffset, rectToTest.origin.y + yOffset, rectToTest.size.width, rectToTest.size.height);
        
                    
//        NSLog(@"%@", NSStringFromCGRect(rectToTest));
        
                    
                    
                    
//        CGRect frame = CGRectMake(frameInfo.rect.origin.x * page.pageInfo.,
//                                  frameInfo.rect.origin.y * page.frame.size.height,
//                                  frameInfo.rect.size.width * page.frame.size.width * _scrollView.zoomScale,
//                                  frameInfo.rect.size.height * page.frame.size.height * _scrollView.zoomScale);
//        NSLog(@"%@",NSStringFromCGRect(page.frame));
//        rectToTest = [_scrollView convertRect:frame fromView:page];
        rectToTest = [self.view convertRect:rectToTest fromView:pageView];
        _indicatingView.frame = rectToTest;
    }
}


- (void)_switchToNextPage
{
    _currentFrameForBubbleSpeech = 0;
    _currentBubbleSpeechInFrame = 0;
    if (_currentPageIndex + 1 < [_pageViews count])
    {
        if(![[ReaderSettings sharedManager] isTransitionAnimated])
        {
            [self _scrollContentToPageAtIndex:_currentPageIndex + 1 animated:NO];
        }
        else{
            [self _scrollContentToPageAtIndex:_currentPageIndex + 1 animated:YES];
        }
    
    }
}

- (void)_playAmbientSoundAtPageIndex:(NSInteger)pageIndex atFrameIndex:(NSInteger)frameIndex
{
    if ((pageIndex < 0) || (frameIndex < 0)){
		return;
	}
	else
	{
        PageView* page = (PageView*)[_pageViews objectAtIndex:pageIndex];
        FrameInfo* frame = (FrameInfo*)[page.pageInfo.frames objectAtIndex:frameIndex];
        
		if (frame.ambientSounds.count >= 1)
		{
            ReaderSettings* settings = [ReaderSettings sharedManager];
            if (!settings.isAudioEnabled) {
                return;
            }
                
            NSString* fullPath = [page.pageInfo.rootPath stringByAppendingPathComponent:[frame.ambientSounds objectAtIndex:0]];
            if([fullPath isEqualToString:_ambientSoundPlayer.soundPath])
            {
                return;
            }
			[_ambientSoundPlayer loadMusicWithFilePath:fullPath startTime:0.0 volume:1.0];
            _ambientSoundPlayer.volume = [[ReaderSettings sharedManager] volume] / 100.0f;
            [_ambientSoundPlayer play];
        }
        else{
            [_ambientSoundPlayer stop];
        }
    }
}

- (void)_playFirstSpeechBubbleAtPageIndex:(NSInteger)pageIndex atFrameIndex:(NSInteger)frameIndex
{
    if (_isSettingsOpened)
    {
        [_ambientSoundPlayer stop];
        return;
    }
    if(_speechBubbleCanPlay)
        [_bubblesManager showBubbleAtPageIndex:pageIndex atFrameIndex:frameIndex];
}

- (BOOL)_isControlsHidden
{
    if(_backButton.hidden == NO &&
       _settingsButton.hidden == NO &&
       _helpButton.hidden == NO &&
       _switchScreenMode.hidden == NO)
    {
        return NO;
    }
    else
    {
        return YES;
    }
}

- (void)_showControls
{
    [self.view bringSubviewToFront:_backButton];
    [self.view bringSubviewToFront:_settingsButton];
    [self.view bringSubviewToFront:_helpButton];
    [self.view bringSubviewToFront:_switchScreenMode];
    
    _backButton.hidden = NO;
    _settingsButton.hidden = NO;
    _helpButton.hidden = NO;
    _switchScreenMode.hidden = NO;
    [UIView animateWithDuration:0.3 animations:^{
        _backButton.alpha = 1.0;
        _settingsButton.alpha = 1.0;
        _helpButton.alpha = 1.0;
        _switchScreenMode.alpha = 1.0;
    } completion:^(BOOL finished) {
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(_hideControls) object:nil];
        [self performSelector:@selector(_hideControls) withObject:nil afterDelay:3.0];
    }];
}

- (void)_hideControls
{
    [UIView animateWithDuration:0.3 animations:^{
        _backButton.alpha = 0.0;
        _settingsButton.alpha = 0.0;
        _helpButton.alpha = 0.0;
        _switchScreenMode.alpha = 0.0;
    } completion:^(BOOL finished) {
        _backButton.hidden = YES;
        _settingsButton.hidden = YES;
        _helpButton.hidden = YES;
        _switchScreenMode.hidden = YES;
    }];
}

- (void)_relayoutHelpImageViews
{
    
}

- (void)_handleSwipe:(UISwipeGestureRecognizer*)recognizer
{
    if (_isSettingsOpened || (_reviewState == kARCIssueViewReviewStateZooming))
        return;
    
	switch (recognizer.direction)
	{
		case UISwipeGestureRecognizerDirectionLeft:
		case UISwipeGestureRecognizerDirectionDown:
			
			if (_reviewState == kARCIssueViewReviewStateFrameByFrame)
			{
				[self _switchToNextFrame];
			}
			else
			{
                if(![[ReaderSettings sharedManager] isTransitionAnimated])
                {
                    [self _scrollContentToPageAtIndex:_currentPageIndex + 1 animated:NO];
                }
                else{
                    [self _scrollContentToPageAtIndex:_currentPageIndex + 1 animated:YES];
                }
			}
			
			break;
			
		case UISwipeGestureRecognizerDirectionRight:
		case UISwipeGestureRecognizerDirectionUp:
			
			if (_reviewState == kARCIssueViewReviewStateFrameByFrame)
			{
				[self _switchToPreviousFrame];
			}
			else
			{
                if(![[ReaderSettings sharedManager] isTransitionAnimated])
                {
                    [self _scrollContentToPageAtIndex:_currentPageIndex - 1 animated:NO];
                }
                else{
                    [self _scrollContentToPageAtIndex:_currentPageIndex - 1 animated:YES];
                }
			}
			
			break;
			
		default:
			break;
	}
}

- (void)_onSwitchScreenModeButton:(UIButton*)button
{
//    NSLog(@"_onSwitchScreenModeButton");
    if (_reviewState == kARCIssueViewReviewStateFrameByFrame && _lockedPageIndex != -1 && _lockedFrameIndex != -1)
    {
        _reviewState = kARCIssueViewReviewStateFullPage;
        _bubblesManager.currentState = _reviewState;
        
        [self _setCurtainsPositionsAnimated:YES];
		
        _switchScreenMode.userInteractionEnabled = NO;
        
        [UIView animateWithDuration:0.7 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionBeginFromCurrentState animations:^{
            			
			PageView* pageView = _pageViews[self.currentPageIndex];
			
			pageView.contentScrollView.zoomScale = 1.0;
			pageView.contentScrollView.contentOffset = CGPointZero;
			
        } completion:^(BOOL finished) {
			
            _lockedFrameIndex   = -1;
            _lockedPageIndex    = -1;
            _switchScreenMode.userInteractionEnabled = YES;
        }];
		
        return;
    }
    
    _reviewState = kARCIssueViewReviewStateFrameByFrame;
    _bubblesManager.currentState = _reviewState;
    
	[self _lockToPageAtIndex:_currentPageIndex frameAtIndex:0 animated:YES];
	[self _setCurtainsPositionsAnimated:YES];
}


- (void)_onSettingsButton:(UIButton*)button
{
    [_bubblesManager stopShowing];
    [_ambientSoundPlayer stop];
    _isSettingsOpened = YES;
    
    [self.view bringSubviewToFront:_settingsView];
    [self _hideControls];
	
	[_settingsView showAnimated:YES
				 appearanceType:ReaderSettingsViewAppearanceTypeReader
			  completionHandler:^{
				  
				  _isSettingsOpened = NO;
				  
				  ReaderSettings* settings = [ReaderSettings sharedManager];
				  
				  if (settings.isAudioEnabled)
				  {
					  if (_reviewState == kARCIssueViewReviewStateFullPage)
					  {
						  [self _playAmbientSoundAtPageIndex:_currentPageIndex atFrameIndex:_currentFrameForBubbleSpeech];
					  }
					  else
					  {
						  [self _playAmbientSoundAtPageIndex:_lockedPageIndex atFrameIndex:_lockedFrameIndex];
					  }
				  }
				  
				  if (_reviewState == kARCIssueViewReviewStateFullPage)
				  {
					  [self _playFirstSpeechBubbleAtPageIndex:_currentPageIndex atFrameIndex:_currentFrameForBubbleSpeech];
				  }
				  else
				  {
					  [self _playFirstSpeechBubbleAtPageIndex:_lockedPageIndex atFrameIndex:_lockedFrameIndex];
				  }
			  }];
}

- (void)_onHelpButton:(UIButton*)button
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(_hideControls) object:nil];
    [UIView animateWithDuration:0.3 animations:^{
        _helpOverlay.hidden = NO;
    } completion:^(BOOL finished) {
        
    }];
}

- (void)_onBackButton:(UIButton*)button
{
    [self dismissModalViewControllerAnimated:YES];
	
//	[self.navigationController.view removeFromSuperview];
//	[self.navigationController removeFromParentViewController];
}

- (void)_handleHelpOverlayTapGesture:(UIGestureRecognizer*)recognizer
{
    [self _showControls];
    [UIView animateWithDuration:0.3 animations:^{
        _helpOverlay.hidden = YES;
    }];
}

- (void)_lockToPageAtIndex:(NSInteger)pageIndex frameAtIndex:(NSInteger)frameIndex animated:(BOOL)animated
{
    if (pageIndex >= [_issue.pages count]) {
        NSLog(@"ERROR: page index %d is out of bounds %d", pageIndex, [_issue.pages count]);
        return;
    }
    else
    {
        if (frameIndex >= [((PageInfo*)[_issue.pages objectAtIndex:pageIndex]).frames count]) {
            NSLog(@"ERROR: frame index %d is out of bounds %d", pageIndex, [_issue.pages count]);
            return;
        }
        else
        {
            _isAnimating = YES;
			self.currentPageIndex = pageIndex;
			_scrollView.contentOffset = CGPointMake(self.currentPageIndex * _scrollView.bounds.size.width, 0);
			
            _lockedPageIndex    = pageIndex;
            _lockedFrameIndex   = frameIndex;
			_currentFrameForBubbleSpeech = frameIndex;
            FrameInfo* frame = (FrameInfo*)[((PageInfo*)[_issue.pages objectAtIndex:_lockedPageIndex]).frames objectAtIndex:_lockedFrameIndex];
            CGRect rectToZoom = frame.rect;
            
            if (animated)
            {
				[UIView animateWithDuration:0.7
									  delay:0.0
									options:UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionBeginFromCurrentState
								 animations:^{
									 
									 [self _lockToRect:rectToZoom];
								 }
								 completion:^(BOOL finished) {
                                     _isAnimating = NO;
                                     [self _playAmbientSoundAtPageIndex:_lockedPageIndex atFrameIndex:_lockedFrameIndex];
									 [self _playFirstSpeechBubbleAtPageIndex:_lockedPageIndex atFrameIndex:_lockedFrameIndex];
								 }];
            }
            else
			{
                _isAnimating = NO;
                [self _lockToRect:rectToZoom];
                [self _playAmbientSoundAtPageIndex:_lockedPageIndex atFrameIndex:_lockedFrameIndex];
                [self _playFirstSpeechBubbleAtPageIndex:_lockedPageIndex atFrameIndex:_lockedFrameIndex];
            }
        }
    }
}


- (void)_lockToRect:(CGRect)rect
{
	PageView* pageView = _pageViews[self.currentPageIndex];
	_lockingRect = CGRectApplyAffineTransform(rect, CGAffineTransformMakeScale(pageView.imageFrame.size.width, pageView.imageFrame.size.height));
	
	CGFloat lockedRectWidth = rect.size.width * pageView.imageFrame.size.width;
	CGFloat lockedRectHeight = rect.size.height * pageView.imageFrame.size.height;
	
	float lockedRectAspect = lockedRectWidth / lockedRectHeight;
	float viewAspect = self.view.bounds.size.width / self.view.bounds.size.height;
	
	CGFloat lockedZoomScale = 0;
	CGPoint lockedContentOffset = CGPointZero;
	
	if (lockedRectAspect > viewAspect)
	{
		lockedZoomScale = self.view.bounds.size.width / lockedRectWidth;
		lockedContentOffset = CGPointMake(pageView.imageFrame.origin.x + rect.origin.x * pageView.imageFrame.size.width * lockedZoomScale,
										  pageView.imageFrame.origin.y + rect.origin.y * pageView.imageFrame.size.height * lockedZoomScale + (rect.size.height * pageView.imageFrame.size.height * lockedZoomScale / 2 - self.view.bounds.size.height / 2));
	}
	else
	{
		lockedZoomScale = self.view.bounds.size.height / lockedRectHeight;
		lockedContentOffset = CGPointMake(pageView.imageFrame.origin.x + rect.origin.x * pageView.imageFrame.size.width * lockedZoomScale + (rect.size.width * pageView.imageFrame.size.width * lockedZoomScale / 2 - self.view.bounds.size.width / 2),
										  pageView.imageFrame.origin.y + rect.origin.y * pageView.imageFrame.size.height * lockedZoomScale);
	}
	
	pageView.contentScrollView.zoomScale = 1.0;
	pageView.contentScrollView.contentOffset = CGPointZero;
	pageView.shouldIgnoreScrollViewDelegateCalls = YES;
	pageView.contentScrollView.zoomScale = lockedZoomScale;
	pageView.contentScrollView.contentOffset = lockedContentOffset;
	pageView.shouldIgnoreScrollViewDelegateCalls = NO;
}

- (CGRect)_currentLockRect
{
	if (_reviewState != kARCIssueViewReviewStateFrameByFrame)
	{
		return CGRectZero;
	}
	
	CGRect rect = [_scrollView convertRect:_lockingRect fromView:_scrollView];
	
	return rect;
}


- (void)_setCurtainsPositionsAnimated:(BOOL)animated;
{
    if(!CGRectIsEmpty([self _currentLockRect]))
    {
		CGSize focusedRectSize = [self _currentLockRect].size;
		
		float focusedRectRatio = focusedRectSize.width / focusedRectSize.height;
		float selfAspect = self.view.bounds.size.width / self.view.bounds.size.height;
		
		CGRect focusedRect;
		
		if(focusedRectRatio > selfAspect)
		{
			focusedRect.size.width = self.view.bounds.size.width;
			focusedRect.size.height = focusedRect.size.width / focusedRectRatio;
		}
		else
		{
			focusedRect.size.height = self.view.bounds.size.height;
			focusedRect.size.width = focusedRect.size.height * focusedRectRatio;
		}
		
		focusedRect.origin.x = (self.view.bounds.size.width - focusedRect.size.width) / 2.0f;
		focusedRect.origin.y = (self.view.bounds.size.height - focusedRect.size.height) / 2.0f;

        
        [self.view bringSubviewToFront:_leftCurtain];
        [self.view bringSubviewToFront:_rightCurtain];
        [self.view bringSubviewToFront:_topCurtain];
        [self.view bringSubviewToFront:_bottomCurtain];
        [self.view bringSubviewToFront:_backButton];
        [self.view bringSubviewToFront:_settingsButton];
        [self.view bringSubviewToFront:_helpButton];
        [self.view bringSubviewToFront:_switchScreenMode];
        [self.view bringSubviewToFront:_helpOverlay];
        [self.view bringSubviewToFront:_settingsView];
        
		if (CGRectGetHeight(focusedRect) < self.view.bounds.size.height && self.view.bounds.size.width < self.view.bounds.size.height)
		{
			CGRect curtainFrame = _bottomCurtain.frame;
			curtainFrame.size.height = self.view.bounds.size.height - curtainFrame.origin.y;
			_bottomCurtain.frame = curtainFrame;
		}
		
		if (CGRectGetWidth(focusedRect) < self.view.bounds.size.width && self.view.bounds.size.width > self.view.bounds.size.height)
		{
			CGRect curtainFrame = _rightCurtain.frame;
			curtainFrame.size.width = self.view.bounds.size.width - curtainFrame.origin.x;
			_rightCurtain.frame = curtainFrame;
		}
		
		[UIView animateWithDuration: animated ? 0.7 : 0.0
						 animations:^{
							 
							 CGRect curtainRect = CGRectMake(0, 0, CGRectGetMinX(focusedRect), CGRectGetMaxY(focusedRect));
							 _leftCurtain.frame = curtainRect;
							 _leftCurtain.alpha = 1.0;
							 
							 curtainRect = CGRectMake(CGRectGetMaxX(focusedRect), 0, CGRectGetMinX(focusedRect), CGRectGetMaxY(focusedRect));
							 _rightCurtain.frame = curtainRect;
							 _rightCurtain.alpha = 1.0;
							 
							 curtainRect = CGRectMake(0, 0, self.view.bounds.size.width, CGRectGetMinY(focusedRect));
							 _topCurtain.frame = curtainRect;
							 _topCurtain.alpha = 1.0;
							 
							 curtainRect = CGRectMake(0, CGRectGetMaxY(focusedRect), self.view.bounds.size.width, CGRectGetMinY(focusedRect));
							 _bottomCurtain.frame = curtainRect;
							 _bottomCurtain.alpha = 1.0;
							 
						 }];
	}
	else
	{
		[UIView animateWithDuration:animated ? 0.7 : 0.0
						 animations:^{
							 _leftCurtain.frame = CGRectMake(0, 0, 0, self.view.bounds.size.height);
							 _leftCurtain.backgroundColor = [UIColor blackColor];
							 _leftCurtain.alpha = 0.0;
							 
							 _rightCurtain.frame = CGRectMake(self.view.bounds.size.width, 0, 0, self.view.bounds.size.height);
							 _rightCurtain.alpha = 0.0;
							 _rightCurtain.backgroundColor = [UIColor blackColor];
							 
							 _topCurtain.frame = CGRectMake(0, 0, self.view.bounds.size.width, 0);
							 _topCurtain.alpha = 0.0;
							 _topCurtain.backgroundColor = [UIColor blackColor];
							 
							 _bottomCurtain.frame = CGRectMake(0, self.view.bounds.size.height, self.view.bounds.size.width, 0);
							 _bottomCurtain.alpha = 0.0;
							 _bottomCurtain.backgroundColor = [UIColor blackColor];
						 }
						 completion:^(BOOL finished) {
						 }];
	}
}

- (void)curtainsHidden:(BOOL)hidden
{
    if (hidden)
    {
        [UIView animateWithDuration:0.3
						 animations:^{
							 _leftCurtain.frame = CGRectMake(0, 0, 0, self.view.bounds.size.height);
							 _leftCurtain.backgroundColor = [UIColor blackColor];
							 _leftCurtain.alpha = 0.0;
							 
							 _rightCurtain.frame = CGRectMake(self.view.bounds.size.width, 0, 0, self.view.bounds.size.height);
							 _rightCurtain.alpha = 0.0;
							 _rightCurtain.backgroundColor = [UIColor blackColor];
							 
							 _topCurtain.frame = CGRectMake(0, 0, self.view.bounds.size.width, 0);
							 _topCurtain.alpha = 0.0;
							 _topCurtain.backgroundColor = [UIColor blackColor];
							 
							 _bottomCurtain.frame = CGRectMake(0, self.view.bounds.size.height, self.view.bounds.size.width, 0);
							 _bottomCurtain.alpha = 0.0;
							 _bottomCurtain.backgroundColor = [UIColor blackColor];
						 }
						 completion:^(BOOL finished) {
                             _leftCurtain.hidden = YES;
                             _rightCurtain.hidden = YES;
                             _topCurtain.hidden = YES;
                             _bottomCurtain.hidden = YES;
						 }];
    }
    else{
        _leftCurtain.hidden = NO;
        _rightCurtain.hidden = NO;
        _topCurtain.hidden = NO;
        _bottomCurtain.hidden = NO;
    }
   
}

- (void)_onPagesStripViewDoubleTap:(UITapGestureRecognizer*)recognizer
{
    
}

- (void)_scrollContentToPageAtIndex:(NSInteger)index animated:(BOOL)animated
{
	if (index < 0 || index > _pageInfos.count - 1)
	{
        if ((index > _pageInfos.count - 1) && index > 0)
        {
            [self dismissModalViewControllerAnimated:YES];
        }
		return;
	}

//	if (![self _loadWaypointIfNeededAtPageWithIndex:index])
//	{
		if (animated)
		{
			[UIView animateWithDuration:0.3
							 animations:^{
								 
								 [_scrollView setContentOffset:CGPointMake(index * _scrollView.bounds.size.width, 0) animated:NO];
							 }
							 completion:^(BOOL finished) {
								 
								 self.currentPageIndex = index;
							 }];
		}
		else
		{
			[_scrollView setContentOffset:CGPointMake(index * _scrollView.bounds.size.width, 0) animated:NO];
			
			self.currentPageIndex = index;
		}
//	}
}

- (void)_updateContentThumbnails
{
	[_pageInfos enumerateObjectsUsingBlock:^(PageInfo* pageInfo, NSUInteger idx, BOOL *stop) {
		
		id obj = _pageViews[idx];
		
		if (idx == self.currentPageIndex || idx == self.currentPageIndex - 1 || idx == self.currentPageIndex + 1)
		{
			if ([obj isKindOfClass:[NSNull class]])
			{
				PageView* pageView = [PageView pageViewWithPageInfo:pageInfo];
				pageView.delegate = self;
				pageView.frame = CGRectMake(_scrollView.bounds.size.width * idx, 0, _scrollView.bounds.size.width, _scrollView.bounds.size.height);
				pageView.contentScrollView.contentSize = pageView.imageFrame.size;
				pageView.contentScrollView.contentOffset = CGPointZero;
				pageView.contentView.frame = pageView.imageFrame;
				
				[_scrollView addSubview:pageView];
				[_pageViews replaceObjectAtIndex:idx withObject:pageView];
				
				[pageView loadBubbles];
				[pageView recalcBubbleFrames];
				
				if (pageInfo.pageType == kPageInfoTypeEnhancedPanel)
				{
					[_scrollView sendSubviewToBack:pageView];
				}
				
				NSLog(@"%@ %@", NSStringFromCGPoint(pageView.contentScrollView.contentOffset), NSStringFromCGSize(pageView.contentScrollView.contentSize));
			}
		}
		else
		{
			if (![obj isKindOfClass:[NSNull class]])
			{
				PageView* pageView = (PageView*)obj;
				[pageView unloadContent];
				[pageView removeFromSuperview];
				[_pageViews replaceObjectAtIndex:idx withObject:[NSNull null]];
			}
		}
	}];
}

- (void)_loadContent
{
	[_pageViews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
		
		if (idx == self.currentPageIndex && [obj isKindOfClass:[PageView class]])
		{
			PageView* pageView = (PageView*)obj;
			pageView.pageViewMode = PageViewModeContent;
			
			if (!_activeOperations[pageView.pageInfo.fullsizeBackgroundPath] && pageView.needsToLoadContent)
			{
				NSInvocationOperation* operation = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(_loadPageContent:) object:pageView];
				[_imagesLoadingQueue addOperation:operation];
				
				[_activeOperations setObject:operation forKey:pageView.pageInfo.fullsizeBackgroundPath];
			}
		}
	}];
}

- (void)_loadPageContent:(PageView*)pageView
{
	[pageView loadContent];
	[_activeOperations removeObjectForKey:pageView.pageInfo.fullsizeBackgroundPath];
}

- (void)_updateControlsOverlayPosition
{
//	CGPoint testPoint;
//	testPoint.x = 0;
//	testPoint.y = _bottomNavigationPanel.bottomNavigationPanelPositon;
//	float maxOverlayYPosition = [_controlsOverlay.superview convertPoint:testPoint fromView:_bottomNavigationPanel].y;
//	
//	CGRect newOverlaysFrame = _controlsOverlay.frame;
//	newOverlaysFrame.size.height = maxOverlayYPosition - newOverlaysFrame.origin.y;
//	newOverlaysFrame.size.width = _controlsOverlay.superview.bounds.size.width;
//	_controlsOverlay.frame = newOverlaysFrame;
}

- (void)_setTextPanelsVisible:(BOOL)visible animated:(BOOL)animated
{
//	[_pageViews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
//		
//		if ([obj isKindOfClass:[PageView class]])
//		{
//			PageView* pageView = obj;
//			
//			if (animated)
//			{
//				if (visible)
//				{
//					pageView.descriptionTextView.frame = CGRectMake(pageView.descriptionTextView.frame.origin.x,
//																	pageView.bounds.size.height,
//																	pageView.descriptionTextView.frame.size.width,
//																	pageView.descriptionTextView.frame.size.height);
//				}
//				
//				[UIView animateWithDuration:0.2
//								 animations:^{
//									 
//									 pageView.descriptionTextView.alpha = visible ? 1.0 : 0.0;
//									 
//									 if (visible)
//									 {
//										 pageView.descriptionTextView.frame = CGRectOffset(pageView.descriptionTextView.frame, 0, -pageView.descriptionTextView.bounds.size.height);
//									 }
//									 else
//									 {
//										 pageView.descriptionTextView.frame = CGRectOffset(pageView.descriptionTextView.frame, 0, pageView.descriptionTextView.bounds.size.height);
//									 }
//								 }];
//			}
//			else
//			{
//				pageView.descriptionTextView.alpha = visible ? 1.0 : 0.0;
//			}
//		}
//	}];
}

- (void)_openSharingMenuOptionsPopover
{
//	UIKIT_EXTERN NSString *const UIActivityTypePostToFacebook   NS_AVAILABLE_IOS(6_0); // text, images, URLs
//	UIKIT_EXTERN NSString *const UIActivityTypePostToTwitter    NS_AVAILABLE_IOS(6_0); // text, images, URLs
//	UIKIT_EXTERN NSString *const UIActivityTypePostToWeibo      NS_AVAILABLE_IOS(6_0); // text, images, URLs
//	UIKIT_EXTERN NSString *const UIActivityTypeMessage          NS_AVAILABLE_IOS(6_0); // text
//	UIKIT_EXTERN NSString *const UIActivityTypeMail             NS_AVAILABLE_IOS(6_0); // text, image, file:// URLs
//	UIKIT_EXTERN NSString *const UIActivityTypePrint            NS_AVAILABLE_IOS(6_0); // image, NSData, file:// URL, UIPrintPageRenderer, UIPrintFormatter, UIPrintInfo
//	UIKIT_EXTERN NSString *const UIActivityTypeCopyToPasteboard NS_AVAILABLE_IOS(6_0); // text, image, NSURL, UIColor, NSDictionary
//	UIKIT_EXTERN NSString *const UIActivityTypeAssignToContact  NS_AVAILABLE_IOS(6_0); // image
//	UIKIT_EXTERN NSString *const UIActivityTypeSaveToCameraRoll NS_AVAILABLE_IOS(6_0); // image, video
//	
//	[_idleTimer invalidate];
//	_idleTimer = nil;
//	
//	PageInfo* currentPageInfo = _pageInfos[self.currentPageIndex];
//	UIImage* image = [UIImage imageWithContentsOfFile:currentPageInfo.contentPath];
//	image = [ImagesConverter resizedImage:image withLongestSideLength:512];
//	
//	NSArray* activityItems = @[image, [NSURL URLWithString:@"http://www.studiorusselljames.com/"], @"Check out cool app!\n"];
//	
//	UIActivityViewController* activityCtrl = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
//	activityCtrl.excludedActivityTypes = @[UIActivityTypePostToWeibo, UIActivityTypeMessage, UIActivityTypePrint, UIActivityTypeCopyToPasteboard, UIActivityTypeAssignToContact, UIActivityTypeSaveToCameraRoll];
//
//	[activityCtrl setCompletionHandler:^(NSString *activityType, BOOL completed) {
//		
//		[self _enablePanelAndStartIdleTimer];
//	}];
//	
//	self.sharingOptionsPopover = [[UIPopoverController alloc] initWithContentViewController:activityCtrl];
//	[self.sharingOptionsPopover presentPopoverFromRect:_controlsOverlay.sharingMenuButton.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}

//- (void)_scrollToHomePage
//{
//	[_imagesLoadingQueue cancelAllOperations];
//	
//	[self _scrollContentToPageIndex:1 centerPage:NO];
//	[self _loadContent];
//}

#pragma mark - BubbleManagerDelegate Methods

- (void)bubbleManagerShouldPassToNextFrame:(BubblesManager*)sender
{
    if (_lockedFrameIndex == [((PageInfo*)[_issue.pages objectAtIndex:_lockedPageIndex]).frames count] - 1)
    {
        if (_lockedPageIndex == [_issue.pages count] - 1)
        {
            return;
        }
    }

    [self _switchToNextFrame];
}

- (void)bubbleManagerShouldPassToNextPage:(BubblesManager*)sender
{
    [self _switchToNextPage];
}

- (void)bubbleManagerShouldSwitchToNextAmbientSound:(BubblesManager *)sender withFrameIdx:(NSInteger)idx
{
    ReaderSettings* settings = [ReaderSettings sharedManager];
    if (settings.isAudioEnabled) {
        if (_reviewState == kARCIssueViewReviewStateFullPage)
        {
            [self _playAmbientSoundAtPageIndex:_currentPageIndex atFrameIndex:idx];
        }else
        {
            [self _playAmbientSoundAtPageIndex:_lockedPageIndex atFrameIndex:_lockedFrameIndex];
        }
    }
}

@end
