//
//  ArmoryViewController.h
//  Unity-iOS Simulator
//
//  Created by Yuriy Belozerov on 21.05.13.
//
//

#import <UIKit/UIKit.h>

@interface ArmoryViewController : UIViewController

@property(assign)               NSInteger           currentWeaponIndex;

@property(assign, nonatomic)    NSInteger           currentGearIndex;
@property(assign, nonatomic)    NSInteger           currentGearTypeIndex;

@end
