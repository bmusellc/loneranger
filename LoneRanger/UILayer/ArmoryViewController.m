//
//  ArmoryViewController.m
//  Unity-iOS Simulator
//
//  Created by Yuriy Belozerov on 21.05.13.
//
//

#import "ArmoryViewController.h"

#import "WeaponParametersView.h"
#import "CoinsPanelView.h"
#import "UpgradeLevelPanelView.h"
#import "PurchaseMoneyButton.h"
#import "PurchaseCoinButton.h"

#import "WeaponManager.h"
#import "StatisticsManager.h"
#import "PurchasesManager.h"

#import "MenuLabel.h"

@interface ArmoryViewController () <PurchasesManagerDelegate>
{
	UIButton*									_previousButton;
	UIButton*									_nextButton;
    UIImageView*                                _weaponeTabView;
    UIImageView*                                _gearTabView;
	
	CoinsPanelView*								_coinsPanelView;
	
	UIView*										_contentView; //superview for all weapon info subviews
	
	MenuLabel*									_weaponNameLabel;
	UIImageView*								_weaponIconView;
	
	UpgradeLevelPanelView*						_weaponUpgradeLevelPanel;
    UpgradeLevelPanelView*                      _gearUpgradeLevelPanel;
	
	WeaponParametersView*						_weaponParametersView;
    MenuLabel*                                  _gearStatisticsMenuLable;
    MenuLabel*                                  _gearParametersLable;
	
	UIView*										_purchasingPanelView;
//	UIButton*									_purchaseCoinsButton;
    PurchaseMoneyButton*                        _purchaseMoneyButton;
    PurchaseCoinButton*                         _purchaseCoinsButton;
	
	NSArray*									_weapons;
	NSInteger									_currentWeaponIndex;
    
    NSMutableArray*                             _gear;
    NSInteger                                   _currentGearIndex;
    NSInteger                                   _currentGearTypeIndex;
    
    MenuLabel*                                  _gearNameLabel;
    
    UIImageView*								_gearIconView;
    
    UIButton*                                   _toWeaponeTabButton;
    UIButton*                                   _toGearTabButton;
    
	PurchasesManager*							_purchasesManager;
	
	UIActivityIndicatorView*					_spinner;
    
    NSInteger                                   _currentTab;        // 0 - weapons
    // 1 - gear
}

@end

@implementation ArmoryViewController

- (NSInteger)currentWeaponIndex
{
	return _currentWeaponIndex;
}

- (void)setCurrentWeaponIndex:(NSInteger)currentWeaponIndex
{
    WeaponInfo* weaponInfo  = _weapons[currentWeaponIndex];
    
    [UIView animateWithDuration:0.25
                     animations:^{
                         _contentView.alpha = 0.0;
                     }
                     completion:^(BOOL finished) {
                         [self _updateWeaponImageView:weaponInfo];
                         [UIView animateWithDuration:0.25
                                          animations:^{
                                              [self _updateWeaponParameters:weaponInfo];
                                              _contentView.alpha = 1.0;
                                          }];
                     }];
    
    _previousButton.enabled = currentWeaponIndex > 0;
    _nextButton.enabled = currentWeaponIndex < _weapons.count - 1;
    _currentWeaponIndex = currentWeaponIndex;
}

- (void)setCurrentGearIndex:(NSInteger)currentGearIndex withGearInfoArray:(NSArray *)sourceGearInfo
{
    GearInfo* gearInfo      = sourceGearInfo[currentGearIndex];
    
    [self _updateGearParameters:gearInfo];
    _currentGearIndex = currentGearIndex;
}

- (void)setCurrentGearTypeIndex:(NSInteger)currentGearTypeIndex
{
    if (self.currentGearTypeIndex != currentGearTypeIndex && currentGearTypeIndex > -1 && currentGearTypeIndex < _gear.count)
	{
        [self setCurrentGearIndex:0 withGearInfoArray:_gear[currentGearTypeIndex]];
        _currentGearTypeIndex = currentGearTypeIndex;
	}
    
    _previousButton.enabled = currentGearTypeIndex > 0;
    _nextButton.enabled = currentGearTypeIndex < _gear.count - 1;
}


- (id)init
{
    self = [super init];
    
	if (self)
	{
		_weapons = [[WeaponManager sharedManager].weapons copy];
        //		_currentWeaponIndex = -1;
        
        NSArray* gearSource = [[WeaponManager sharedManager].gear copy];
        
        NSMutableArray* _gearVests  = [[NSMutableArray alloc] init];
        NSMutableArray* _gearBadges = [[NSMutableArray alloc] init];
        
        for (GearInfo* obj in gearSource) {
            
            if ([[obj typeStr]      isEqual:@"vest"])
            {
                [_gearVests addObject:obj];
            }
            else if ([[obj typeStr] isEqual:@"badge"])
            {
                [_gearBadges addObject:obj];
            }
        }
        
        _gear = [[NSMutableArray alloc] init];
        [_gear addObject:_gearVests];
        [_gear addObject:_gearBadges];
        _currentGearTypeIndex = 0;
        [_gearVests     release];
        [_gearBadges    release];
        
		_purchasesManager = [PurchasesManager sharedManager];
		_purchasesManager.delegate = self;
        [gearSource         release];
    }
    
	return self;
}

- (void)dealloc
{
	[[NSNotificationCenter defaultCenter] removeObserver:self name:PurchasesManagerReceivedProductsInformationNotification object:nil];
	
    _gearTabView    = nil;
    _weaponeTabView = nil;
    
	[_weapons                       release];
    [_gear                          release];
    
	[_contentView                   release];
	
	[_weaponNameLabel               release];
	[_weaponIconView                release];
	
	[_previousButton                release];
	[_nextButton                    release];
	
	[_coinsPanelView                release];
	
	[_weaponUpgradeLevelPanel       release];
	[_weaponParametersView          release];
	
    [_gearNameLabel                 release];
    [_gearIconView                  release];
    [_gearParametersLable           release];
    [_gearUpgradeLevelPanel         release];
    
    [_toGearTabButton               release];
    [_toWeaponeTabButton            release];
    
    [_gearTabView                   release];
    [_weaponeTabView                release];
    
    [_purchasingPanelView           release];
	[_purchaseCoinsButton           release];
	[_purchaseMoneyButton           release];
    
	[_spinner                       release];
	
	_purchasesManager.delegate = nil;
	_purchasesManager          = nil;
	
    [super                          dealloc];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.view.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.75];
    
    [self initWeaponTab];
	
	_coinsPanelView = [CoinsPanelView new];
	[self.view addSubview:_coinsPanelView];
	_coinsPanelView.frame = CGRectOffset(_coinsPanelView.frame, 1024 - _coinsPanelView.bounds.size.width, 0);
    
    UIImage* btnImg = [UIImage imageNamed:@"menu/LevelSelection/backButton.png"];
	UIButton* backButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[self.view addSubview:backButton];
	backButton.frame = CGRectMake(0, 0, btnImg.size.width, btnImg.size.height);
	[backButton setImage:btnImg forState:UIControlStateNormal];
	[backButton addTarget:self action:@selector(_backButtonDidPressed) forControlEvents:UIControlEventTouchUpInside];
    
    UISwipeGestureRecognizer* swipeLeft = [[[UISwipeGestureRecognizer alloc] initWithTarget:self
                                                                                     action:@selector(_handleSwipeGestureRecognizer:)] autorelease];
	swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipeLeft];
	
	UISwipeGestureRecognizer* swipeRight = [[[UISwipeGestureRecognizer alloc] initWithTarget:self
                                                                                      action:@selector(_handleSwipeGestureRecognizer:)] autorelease];
	swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swipeRight];
	
	_spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
	_spinner.frame = self.view.bounds;
	_spinner.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	_spinner.contentMode = UIViewContentModeCenter;
	_spinner.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.75];
	_spinner.hidesWhenStopped = YES;
    [self.view addSubview:_spinner];
    
    _toWeaponeTabButton = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
    [_toWeaponeTabButton setFrame:CGRectMake(80, 25, 210, 50)];
    [_toWeaponeTabButton addTarget:self action:@selector(changeTab) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_toWeaponeTabButton];
    
    _toGearTabButton = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
    [_toGearTabButton setFrame:CGRectMake(305, 25, 213, 50)];
    [_toGearTabButton addTarget:self action:@selector(changeTab) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_toGearTabButton];
    
    [_toWeaponeTabButton  setEnabled:NO];
}

- (void)changeTab
{
    [_toWeaponeTabButton  setEnabled:NO];
    [_toGearTabButton     setEnabled:NO];
    
    [UIView animateWithDuration:0.25
                     animations:^{
                         _contentView.alpha = 0.0;
                     }
                     completion:^(BOOL finished) {
                         
                         if (_currentTab == 0)
                         {
                             [_weaponeTabView removeFromSuperview];
                             _weaponeTabView = nil;
                             [self.view addSubview:_gearTabView];
                             [self initGearTab];
                             [_toWeaponeTabButton      setEnabled:YES];
                         }
                         else if (_currentTab == 1)
                         {
                             [_gearTabView removeFromSuperview];
                             _gearTabView = nil;
                             [self.view addSubview:_weaponeTabView];
                             [self initWeaponTab];
                             [_toGearTabButton         setEnabled:YES];
                         }
                         
                         [UIView animateWithDuration:0.25
                                          animations:^{
                                              _contentView.alpha = 1.0;
                                          }
                                          completion:nil];
                     }];
    
    _previousButton.enabled = NO;
}

- (void)initGearTab
{
    _gearTabView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"menu/Armory/backgroundTabGear.png"]] autorelease];
    _gearTabView.userInteractionEnabled = YES;
    [self.view addSubview:_gearTabView];
	
    
    _contentView = [[UIView alloc] initWithFrame:_gearTabView.bounds];
    [_gearTabView addSubview:_contentView];
    
    _gearNameLabel = [[MenuLabel menuLabelWithFontType:MenuLabelFontTypeWeswood fontSize:40] retain];
	_gearNameLabel.frame            = CGRectMake(0, 96, 1024, 64);
	_gearNameLabel.textColor        = [UIColor colorWithRed:255.0 / 255.0 green:252.0 /
                                       255.0 blue:232.0 / 255.0 alpha:1.0];
	_gearNameLabel.textAlignment    = NSTextAlignmentCenter;
    [_contentView addSubview:_gearNameLabel];
    
    _gearIconView = [UIImageView new];
    [_contentView addSubview:_gearIconView];
    
    
    _gearStatisticsMenuLable = [[MenuLabel menuLabelWithFontType:MenuLabelFontTypeWeswood fontSize:40] retain];
    [_gearStatisticsMenuLable setTextAlignment:NSTextAlignmentCenter];
    [_gearStatisticsMenuLable setFrame:CGRectMake(425, 170, 425, 170)];
    [_gearStatisticsMenuLable setTextColor:[UIColor colorWithRed:255.0 / 255.0 green:245.0 /
                                            255.0 blue:220.0 / 255.0 alpha:1.0]];
    [_contentView addSubview:_gearStatisticsMenuLable];
    
    _gearParametersLable = [[MenuLabel menuLabelWithFontType:MenuLabelFontTypeSuplexmentary fontSize:18] retain];
    [_gearParametersLable setTextAlignment:NSTextAlignmentCenter];
    [_gearParametersLable setFrame:CGRectMake(425, 175, 425, 325)];
    [_gearParametersLable setTextColor:[UIColor colorWithRed:255.0 / 255.0 green:252.0 /
                                        255.0 blue:232.0 / 255.0 alpha:1.0]];
    [_gearParametersLable attributedText];
    [_gearParametersLable setLineBreakMode:UILineBreakModeWordWrap];
    [_gearParametersLable setNumberOfLines:5];
    [_contentView addSubview:_gearParametersLable];
    
    _gearUpgradeLevelPanel = [UpgradeLevelPanelView new];
	_gearUpgradeLevelPanel.center = CGPointMake(_contentView.center.x + 120, _contentView.center.y + 110);
    [_contentView addSubview:_gearUpgradeLevelPanel];
    
    
    _purchasingPanelView = [[UIView alloc] initWithFrame:CGRectMake(0, 590, 1024, 768 - 590)];
    [_contentView addSubview:_purchasingPanelView];
	
	UIImageView* orCaptionView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"menu/Armory/orCaption.png"]] autorelease];
	orCaptionView.center = CGPointMake(_purchasingPanelView.center.x, 72);
    [_purchasingPanelView addSubview:orCaptionView];
	
    
	UIImage* btnImg         = [UIImage imageNamed:@"menu/Armory/purchaseCoins.png"];
	UIImage* btnHlImg       = [UIImage imageNamed:@"menu/Armory/purchaseCoinsHl.png"];
	_purchaseCoinsButton    = [[PurchaseCoinButton buttonWithType:UIButtonTypeCustom] retain];
    
	[_purchasingPanelView addSubview:_purchaseCoinsButton];
	_purchaseCoinsButton.frame = CGRectMake(194, 24, btnImg.size.width, btnImg.size.height);
	[_purchaseCoinsButton setBackgroundImage:btnImg forState:UIControlStateNormal];
	[_purchaseCoinsButton setBackgroundImage:btnHlImg forState:UIControlStateHighlighted];
	[_purchaseCoinsButton addTarget:self action:@selector(_purchaseCoinsButtonDidPressed:) forControlEvents:UIControlEventTouchUpInside];
	_purchaseCoinsButton.titleLabel.font = [UIFont fontWithName:@"CMWesternWoodblockNo2" size:52];
	[_purchaseCoinsButton setTitleColor:[UIColor colorWithRed:255 / 255.0 green:231 / 255.0 blue:197
                                         / 255.0 alpha:1.0] forState:UIControlStateNormal];
	[_purchaseCoinsButton setTitleColor:[UIColor colorWithRed:255 / 255.0 green:210 / 255.0 blue:90
                                         / 255.0 alpha:1.0] forState:UIControlStateHighlighted];
	[_purchaseCoinsButton setTitleColor:[UIColor colorWithRed:186 / 255.0 green:153 / 255.0 blue:122
                                         / 255.0 alpha:1.0] forState:UIControlStateDisabled];
	[_purchaseCoinsButton setTitleShadowColor:[UIColor blackColor] forState:UIControlStateNormal];
	_purchaseCoinsButton.titleLabel.shadowOffset = CGSizeMake(0, 2);
    [_purchaseCoinsButton setTag:1];
	
    btnImg = [UIImage imageNamed:@"menu/Armory/emptyPurchaseMoney.png"];
	_purchaseMoneyButton = [[PurchaseMoneyButton buttonWithType:UIButtonTypeCustom] retain];
	[_purchasingPanelView addSubview:_purchaseMoneyButton];
	_purchaseMoneyButton.frame = CGRectMake(554, 6, btnImg.size.width, btnImg.size.height);
	[_purchaseMoneyButton setBackgroundImage:btnImg forState:UIControlStateNormal];
	[_purchaseMoneyButton addTarget:self action:@selector(_purchaseMoneyButtonDidPressed) forControlEvents:UIControlEventTouchUpInside];
	
    UIImage* buttonImg = [UIImage imageNamed:@"menu/Armory/previousButton.png"];
	_previousButton = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
	[_previousButton setImage:buttonImg forState:UIControlStateNormal];
	_previousButton.frame = CGRectMake(22, 264, buttonImg.size.width, buttonImg.size.height);
	[_previousButton addTarget:self action:@selector(_previousButtonDidPressed:) forControlEvents:UIControlEventTouchUpInside];
    [_previousButton setTag:1];
    [_contentView addSubview:_previousButton];
	
    buttonImg = [UIImage imageNamed:@"menu/Armory/nextButton.png"];
	_nextButton = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
	[_nextButton setImage:buttonImg forState:UIControlStateNormal];
	_nextButton.frame = CGRectMake(1024 - 22 - buttonImg.size.width, 264, buttonImg.size.width, buttonImg.size.height);
	[_nextButton addTarget:self action:@selector(_nextButtonDidPressed:) forControlEvents:UIControlEventTouchUpInside];
    [_nextButton setTag:1];
    [_contentView addSubview:_nextButton];
    
	
    
    self.currentGearIndex   = 0;
    self.currentGearTypeIndex = 0;
    GearInfo* gearInfo      = _gear[0][0];
    [self _updateGearParameters:gearInfo];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(_productsInformationReceived)
                                                 name:PurchasesManagerReceivedProductsInformationNotification
                                               object:nil];
    _currentTab = 1;
    [self.view sendSubviewToBack:_gearTabView];
}

- (void)initWeaponTab
{
    _weaponeTabView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"menu/Armory/backgroundTabWeapons.png"]] autorelease];
	[self.view addSubview:_weaponeTabView];
	_weaponeTabView.userInteractionEnabled = YES;
	
	_contentView = [[UIView alloc] initWithFrame:_weaponeTabView.bounds];
	[_weaponeTabView addSubview:_contentView];
	
	_weaponNameLabel = [[MenuLabel menuLabelWithFontType:MenuLabelFontTypeWeswood fontSize:40] retain];
	[_contentView addSubview:_weaponNameLabel];
	_weaponNameLabel.frame = CGRectMake(0, 86, 1024, 64);
	_weaponNameLabel.textColor = [UIColor colorWithRed:255.0 / 255.0 green:252.0 / 255.0 blue:232.0 / 255.0 alpha:1.0];
	_weaponNameLabel.textAlignment = NSTextAlignmentCenter;
	
	_weaponIconView = [UIImageView new];
	[_contentView addSubview:_weaponIconView];
	
	_weaponUpgradeLevelPanel = [UpgradeLevelPanelView new];
	[_contentView addSubview:_weaponUpgradeLevelPanel];
	_weaponUpgradeLevelPanel.center = CGPointMake(_contentView.center.x, _contentView.center.y);
	
	_weaponParametersView = [WeaponParametersView new];
	[_contentView addSubview:_weaponParametersView];
	_weaponParametersView.frame = CGRectOffset(_weaponParametersView.frame, 102, 428);
    
	_purchasingPanelView = [[UIView alloc] initWithFrame:CGRectMake(0, 590, 1024, 768 - 590)];
	[_contentView addSubview:_purchasingPanelView];
	
	UIImageView* orCaptionView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"menu/Armory/orCaption.png"]] autorelease];
	[_purchasingPanelView addSubview:orCaptionView];
	orCaptionView.center = CGPointMake(_purchasingPanelView.center.x, 72);
	
	UIImage* btnImg = [UIImage imageNamed:@"menu/Armory/purchaseCoins.png"];
	UIImage* btnHlImg = [UIImage imageNamed:@"menu/Armory/purchaseCoinsHl.png"];
	_purchaseCoinsButton = [[PurchaseCoinButton buttonWithType:UIButtonTypeCustom] retain];
	[_purchasingPanelView addSubview:_purchaseCoinsButton];
	_purchaseCoinsButton.frame = CGRectMake(194, 24, btnImg.size.width, btnImg.size.height);
	[_purchaseCoinsButton setBackgroundImage:btnImg forState:UIControlStateNormal];
	[_purchaseCoinsButton setBackgroundImage:btnHlImg forState:UIControlStateHighlighted];
	[_purchaseCoinsButton addTarget:self action:@selector(_purchaseCoinsButtonDidPressed:) forControlEvents:UIControlEventTouchUpInside];
	_purchaseCoinsButton.titleLabel.font = [UIFont fontWithName:@"CMWesternWoodblockNo2" size:52];
	[_purchaseCoinsButton setTitleColor:[UIColor colorWithRed:255 / 255.0 green:231 / 255.0 blue:197 / 255.0 alpha:1.0] forState:UIControlStateNormal];
	[_purchaseCoinsButton setTitleColor:[UIColor colorWithRed:255 / 255.0 green:210 / 255.0 blue:90 / 255.0 alpha:1.0] forState:UIControlStateHighlighted];
	[_purchaseCoinsButton setTitleColor:[UIColor colorWithRed:186 / 255.0 green:153 / 255.0 blue:122 / 255.0 alpha:1.0] forState:UIControlStateDisabled];
	[_purchaseCoinsButton setTitleShadowColor:[UIColor blackColor] forState:UIControlStateNormal];
	_purchaseCoinsButton.titleLabel.shadowOffset = CGSizeMake(0, 2);
	[_purchaseCoinsButton setTag:0];
    
	btnImg = [UIImage imageNamed:@"menu/Armory/emptyPurchaseMoney.png"];
	_purchaseMoneyButton = [[PurchaseMoneyButton buttonWithType:UIButtonTypeCustom] retain];
	[_purchasingPanelView addSubview:_purchaseMoneyButton];
	_purchaseMoneyButton.frame = CGRectMake(554, 6, btnImg.size.width, btnImg.size.height);
	[_purchaseMoneyButton setBackgroundImage:btnImg forState:UIControlStateNormal];
	[_purchaseMoneyButton addTarget:self action:@selector(_purchaseMoneyButtonDidPressed) forControlEvents:UIControlEventTouchUpInside];
	
    
    UIImage* buttonImg = [UIImage imageNamed:@"menu/Armory/previousButton.png"];
	_previousButton = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
	[_contentView addSubview:_previousButton];
	[_previousButton setImage:buttonImg forState:UIControlStateNormal];
	_previousButton.frame = CGRectMake(22, 264, buttonImg.size.width, buttonImg.size.height);
	[_previousButton addTarget:self action:@selector(_previousButtonDidPressed:) forControlEvents:UIControlEventTouchUpInside];
    [_previousButton setTag:0];
	
	buttonImg = [UIImage imageNamed:@"menu/Armory/nextButton.png"];
	_nextButton = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
	[_contentView addSubview:_nextButton];
	[_nextButton setImage:buttonImg forState:UIControlStateNormal];
	_nextButton.frame = CGRectMake(1024 - 22 - buttonImg.size.width, 264, buttonImg.size.width, buttonImg.size.height);
	[_nextButton addTarget:self action:@selector(_nextButtonDidPressed:) forControlEvents:UIControlEventTouchUpInside];
    [_nextButton setTag:0];
    
	self.currentWeaponIndex = 0;
    WeaponInfo* weaponInfo  = _weapons[0];
    [self _updateWeaponParameters:weaponInfo];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(_productsInformationReceived) name:PurchasesManagerReceivedProductsInformationNotification object:nil];
    _currentTab = 0;
    [self.view sendSubviewToBack:_weaponeTabView];
}

- (void)_updateWeaponParameters:(WeaponInfo*)weaponInfo
{
    _weaponNameLabel.text = [weaponInfo.modelStr uppercaseString];
    _weaponParametersView.weaponInfo = weaponInfo;
    _weaponUpgradeLevelPanel.upgradeLevel = weaponInfo.upgradeLevel;
    //	_purchasingPanelView.hidden = !(weaponInfo.upgradeLevel < weaponInfo.maxUpgradeLevel);
	
    _purchaseCoinsButton.enabled = weaponInfo.upgradeLevel < weaponInfo.maxUpgradeLevel && [StatisticsManager sharedManager].coins >= weaponInfo.price;
    [_purchaseCoinsButton setTitle:[NSString stringWithFormat:@"%d", weaponInfo.price] forState:UIControlStateNormal];
    CGFloat inset = weaponInfo.price < 1000 ? 52 : 60;
    _purchaseCoinsButton.titleEdgeInsets = UIEdgeInsetsMake(6, -inset, 0, inset);
    
    _purchaseMoneyButton.enabled = weaponInfo.upgradeLevel < weaponInfo.maxUpgradeLevel && weaponInfo.appStorePrice.length > 0;
    [_purchaseMoneyButton setTitle:weaponInfo.appStorePrice forState:UIControlStateNormal];
    
    
    _coinsPanelView.numberOfCoins = [StatisticsManager sharedManager].coins;
}

- (void)_updateWeaponImageView:(WeaponInfo*)weaponInfo
{
    UIImage* weaponIcon = [UIImage imageNamed:weaponInfo.iconPath];
    _weaponIconView.image = weaponIcon;
    _weaponIconView.frame = CGRectMake((_contentView.bounds.size.width - weaponIcon.size.width) / 2,
                                       100,
                                       weaponIcon.size.width,
                                       weaponIcon.size.height);
}

- (void)_updateGearParameters:(GearInfo*)gearInfo
{
    [UIView animateWithDuration:0.25
                     animations:^{
                         _contentView.alpha = 0.0;
                     }
                     completion:^(BOOL finished) {
                         
                         _gearNameLabel.text         = [[gearInfo modelStr]      uppercaseString];
                         _gearParametersLable.text   = [[gearInfo description]   uppercaseString];
                         
                         UIImage* gearIcon = [UIImage imageNamed:[gearInfo iconPath]];
                         _gearIconView.image = gearIcon;
                         _gearIconView.frame = CGRectMake(70,
                                                          185,
                                                          gearIcon.size.width,
                                                          gearIcon.size.height);
                         
                         _gearUpgradeLevelPanel.upgradeLevel = gearInfo.level;
                         if ([gearInfo level] < [gearInfo maxLevel])
                         {
                             _gearStatisticsMenuLable.text = @"UPGRADE STATISTICS";
                         }
                         else
                         {
                             _gearStatisticsMenuLable.text = @"CURRENT STATISTICS";
                         }
                         
                         //	_purchasingPanelView.hidden = !(gearInfo.level < gearInfo.maxLevel);
                         
                         _purchaseCoinsButton.enabled = gearInfo.level < gearInfo.maxLevel && [StatisticsManager sharedManager].coins >= gearInfo.price;
                         [_purchaseCoinsButton setTitle:[NSString stringWithFormat:@"%d", gearInfo.price] forState:UIControlStateNormal];
                         CGFloat inset = gearInfo.price < 1000 ? 52 : 60;
                         _purchaseCoinsButton.titleEdgeInsets = UIEdgeInsetsMake(6, -inset, 0, inset);
                         
                         _purchaseMoneyButton.enabled = gearInfo.level < gearInfo.maxLevel && gearInfo.appStorePrice.length > 0;
                         [_purchaseMoneyButton setTitle:gearInfo.appStorePrice forState:UIControlStateNormal];
                         
                         _coinsPanelView.numberOfCoins = [StatisticsManager sharedManager].coins;
                         
                         [UIView animateWithDuration:0.25
                                          animations:^{
                                              
                                              _contentView.alpha = 1.0;
                                          }];
                     }];
}

- (void)_backButtonDidPressed
{
	[UIView animateWithDuration:0.5
					 animations:^{
						 self.view.alpha = 0.0;
					 }
					 completion:^(BOOL finished) {
						 
						 [self.view removeFromSuperview];
						 [self removeFromParentViewController];
					 }];
}

- (void)_previousButtonDidPressed:(UIButton*)sender
{
    switch (sender.tag) {
        case 0:
            self.currentWeaponIndex--;
            break;
            
        case 1:
            self.currentGearTypeIndex--;
            break;
            
        default:
            break;
    }
}

- (void)_nextButtonDidPressed:(UIButton*)sender
{
    switch (sender.tag) {
        case 0:
            self.currentWeaponIndex++;
            break;
            
        case 1:
            self.currentGearTypeIndex++;
            break;
            
        default:
            break;
    }
}

- (void)_purchaseCoinsButtonDidPressed:(UIButton*)sender
{
    WeaponInfo* info = nil;
    GearInfo* gearInfo = nil;
    
    switch (sender.tag) {
        case 0:
            info = _weapons[self.currentWeaponIndex];
            
            [[StatisticsManager sharedManager] decreaseCoins:info.price];
            [[WeaponManager sharedManager] upgradeWeapon:info];
            
            [self _updateWeaponParameters:info];
            break;
            
        case 1:
            gearInfo = _gear[_currentGearTypeIndex][++_currentGearIndex];
            
            [[StatisticsManager sharedManager] decreaseCoins:info.price];
            [self _updateGearParameters:gearInfo];
            break;
            
        default:
            break;
    }
	
    
}

- (void)_purchaseMoneyButtonDidPressed
{
	WeaponInfo* info = _weapons[self.currentWeaponIndex];
	
	[[PurchasesManager sharedManager] purchaseWeaponWithID:info.appStoreID];
	
	[_spinner startAnimating];
}

- (void)_handleSwipeGestureRecognizer:(UISwipeGestureRecognizer*)sender
{
    if (_currentTab == 0)
    {
        switch (sender.direction)
        {
            case UISwipeGestureRecognizerDirectionLeft:
                if (self.currentWeaponIndex < _weapons.count - 1)
                {
                    self.currentWeaponIndex++;
                }
                break;
                
            case UISwipeGestureRecognizerDirectionRight:
                if (self.currentWeaponIndex > 0)
                {
                    self.currentWeaponIndex--;
                }
                break;
                
            default:
                break;
        }
    }
    else if (_currentTab == 1)
    {
        switch (sender.direction)
        {
            case UISwipeGestureRecognizerDirectionLeft:
                if (self.currentGearTypeIndex < _gear.count - 1)
                {
                    self.currentGearTypeIndex++;
                }
                break;
                
            case UISwipeGestureRecognizerDirectionRight:
                if (self.currentGearTypeIndex > 0)
                {
                    self.currentGearTypeIndex--;
                }
                break;
                
            default:
                break;
        }
        
    }
}

- (void)purchasesManager:(PurchasesManager*)manager didSwitchToState:(PurchasesManagerState)newState fromState:(PurchasesManagerState)oldState
{
	switch (oldState)
	{
		case kPurchasesManagerStatePurchasingItem:
		{
			switch (newState)
			{
				case kPurchasesManagerStateInitialized:
				{
					[self _updateWeaponParameters:_weapons[self.currentWeaponIndex]];
					
					[_spinner stopAnimating];
				}
					break;
					
				case kPurchasesManagerStatePurchasingFailed:
                    
					[_spinner stopAnimating];
					
					break;
					
				default:
					break;
			}
		}
			break;
			
		default:
			break;
	}
}

- (void)_productsInformationReceived
{
	[self _updateWeaponParameters   :_weapons[self.currentWeaponIndex]];
    [self _updateGearParameters     :_gear[self.currentGearIndex]];
}

@end
