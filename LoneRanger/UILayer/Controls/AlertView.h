//
//  AlertView.h
//  Unity-iOS Simulator
//
//  Created by Yuriy Belozerov on 07.06.13.
//
//

#import <UIKit/UIKit.h>

@interface AlertView : UIView

- (id)initWithTitle:(NSString*)title fontSize:(CGFloat)fontSize;

- (void)showInView:(UIView*)view;

@end
