//
//  AlertView.m
//  Unity-iOS Simulator
//
//  Created by Yuriy Belozerov on 07.06.13.
//
//

#import "AlertView.h"

@implementation AlertView

- (id)initWithTitle:(NSString*)title fontSize:(CGFloat)fontSize
{
    self = [super init];
    
	if (self)
	{
		self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
		
		UITapGestureRecognizer* tapRecognizer = [[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(_handleTapGestureRecognizer)] autorelease];
		[self addGestureRecognizer:tapRecognizer];
		
		UIImageView* alertView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"menu/LevelSelection/alertBg.png"]] autorelease];
		[self addSubview:alertView];
		alertView.center = CGPointMake(self.bounds.size.width / 2, self.bounds.size.height / 2);
		alertView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
		alertView.contentMode = UIViewContentModeCenter;
		
		UILabel* alertLbl = [[[UILabel alloc] initWithFrame:alertView.bounds] autorelease];
		[alertView addSubview:alertLbl];
		alertLbl.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
		alertLbl.textAlignment = NSTextAlignmentCenter;
		alertLbl.numberOfLines = 0;
		alertLbl.backgroundColor = [UIColor clearColor];
		alertLbl.font = [UIFont fontWithName:@"Suplexmentary Comic NC" size:fontSize];
		alertLbl.text = title;
    }
    
	return self;
}

- (void)showInView:(UIView*)view
{
	self.frame = view.bounds;
	self.alpha = 0.0;
	
	[view addSubview:self];
	
	[UIView animateWithDuration:0.5 animations:^{
		
		self.alpha = 1.0;
	}];
}

- (void)_handleTapGestureRecognizer
{
	[UIView animateWithDuration:0.5
					 animations:^{
						 
						 self.alpha = 0.0;
					 }
					 completion:^(BOOL finished) {
						 
						 [self removeFromSuperview];
					 }];
}

@end
