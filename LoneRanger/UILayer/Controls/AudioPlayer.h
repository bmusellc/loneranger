//
//  AudioPlayer.h
//  RussellJamesPhotobook
//
//  Created by Artem Manzhura on 1/21/13.
//  Copyright (c) 2013 bMuse Ukraine. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

@class AudioPlayer;

@protocol AudioPlayerDelegate <NSObject>

- (void)audioPlayerDidFinishPlaying:(AudioPlayer*)audioPlayer;

@end

@interface AudioPlayer : NSObject
{
}

@property(weak)   id<AudioPlayerDelegate>		delegate;

@property(readonly)	BOOL						shouldAutoplay;

@property(assign)	CGFloat						volume;
@property(assign)	CGFloat						currentTime;
@property(readonly)	CGFloat						duration;
@property(readonly)	CGFloat						remainingTime;
@property(readonly)	BOOL						playing;
@property(readonly)	BOOL						initialized;
@property(readonly) NSString*                   soundPath;

- (void)loadMusicWithFilePath:(NSString*)filePath startTime:(NSTimeInterval)startTime volume:(CGFloat)volume;
- (void)play;
- (void)pause;
- (void)pauseByUser;
- (void)stop;

- (void)fadeInVolumeWithDuration:(NSTimeInterval)duration completionHandler:(void(^)(void))completionHandler;
- (void)fadeOutVolumeWithDuration:(CGFloat)duration completionHandler:(void(^)(void))completionHandler;


@end
