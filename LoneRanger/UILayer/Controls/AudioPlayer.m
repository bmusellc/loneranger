//
//  AudioPlayer.m
//  RussellJamesPhotobook
//
//  Created by Artem Manzhura on 1/21/13.
//  Copyright (c) 2013 bMuse Ukraine. All rights reserved.
//

#import "AudioPlayer.h"


static void* CurrentItemObservationContext = &CurrentItemObservationContext;

@interface AudioPlayer ()
{
	
	NSTimer*									_fadeTimer;
	NSTimeInterval								_fadeDuration;
	NSTimeInterval								_fadeTime;
	
	NSString*									_nextTrackPath;
	NSTimeInterval								_nextTrackStartTime;
	
	CGFloat										_volume;
	CGFloat										_currentTime;
	
	BOOL										_isPlaying;
	
	AVQueuePlayer*								_player;
}

@end

@implementation AudioPlayer

- (CGFloat)volume
{
	return _volume;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)setVolume:(CGFloat)volume
{
	_volume = MIN(MAX(volume, 0.0), 1.0);
	
	NSArray *audioTracks = [_player.currentItem.asset tracksWithMediaType:AVMediaTypeAudio];
	
	NSMutableArray *allAudioParams = [NSMutableArray array];
	
	for (AVAssetTrack *track in audioTracks)
	{
		AVMutableAudioMixInputParameters *audioInputParams = [AVMutableAudioMixInputParameters audioMixInputParameters];
		[audioInputParams setVolume:self.volume atTime:kCMTimeZero];
		[audioInputParams setTrackID:[track trackID]];
		[allAudioParams addObject:audioInputParams];
	}
	
	AVMutableAudioMix *audioMix = [AVMutableAudioMix audioMix];
	[audioMix setInputParameters:allAudioParams];
	
	[_player.currentItem setAudioMix:audioMix];
}

- (CGFloat)duration
{
    return CMTimeGetSeconds(_player.currentItem.duration);
}

- (CGFloat)currentTime
{
	_currentTime = CMTimeGetSeconds(_player.currentTime);
	
	return _currentTime;
}

- (void)setCurrentTime:(CGFloat)time
{
	_currentTime = time;
	
	if (_player.currentItem.status == AVPlayerItemStatusReadyToPlay)
	{
		[_player seekToTime:CMTimeMakeWithSeconds(time, 60) completionHandler:^(BOOL finished) {
			
		}];
	}
	else
	{
		[_player seekToTime:CMTimeMakeWithSeconds(time, 60)];   
	}
}

- (CGFloat)remainingTime
{
	return self.duration - self.currentTime;
}

- (BOOL)playing
{
	return _isPlaying;
}

- (id)init
{
    self = [super init];
    
	if (self)
	{
        
    }
    
	return self;
}


- (void)loadMusicWithFilePath:(NSString*)filePath startTime:(NSTimeInterval)startTime volume:(CGFloat)volume
{
    _soundPath = filePath;
	NSURL* URL = [NSURL fileURLWithPath:filePath];
	AVAsset* asset = [AVAsset assetWithURL:URL];
	AVPlayerItem* playerItem = [AVPlayerItem playerItemWithAsset:asset];
	
	if (!_player)
	{
		_player = [[AVQueuePlayer alloc] initWithItems:@[playerItem]];
		_player.actionAtItemEnd = AVPlayerActionAtItemEndPause;
	}
	else
	{
		if ([_player canInsertItem:playerItem afterItem:_player.currentItem])
		{
			[_player insertItem:playerItem afterItem:_player.currentItem];
			[_player advanceToNextItem];
		}
	}
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(_itemDidFinishPlaying)
												 name:AVPlayerItemDidPlayToEndTimeNotification
											   object:playerItem];
	
	self.currentTime = startTime;
	self.volume = volume;
	
	_initialized = YES;
}

- (void)play
{
	
	_shouldAutoplay = YES;
	_isPlaying = YES;
	
    [_player play];
}

- (void)stop
{
	self.currentTime = 0;
	_soundPath = nil;
	[self pause];
}

- (void)pause
{
	_isPlaying = NO;
	
    [_player pause];
}

- (void)pauseByUser
{
	_shouldAutoplay = NO;
	
	[self pause];
}

- (void)fadeOutVolumeWithDuration:(CGFloat)duration completionHandler:(void(^)(void))completionHandler
{
	void(^handler)(void) = [completionHandler copy];
	
	_fadeDuration = duration;
	_fadeTime = duration;
	[_fadeTimer invalidate];
	_fadeTimer = [NSTimer scheduledTimerWithTimeInterval:0.1
												  target:self
												selector:@selector(_fadeOutTimerFired:)
												userInfo:handler
												 repeats:YES];
	[[NSRunLoop mainRunLoop] addTimer:_fadeTimer forMode:NSRunLoopCommonModes];
}

- (void)_fadeOutTimerFired:(NSTimer*)timer
{
	_fadeTime -= timer.timeInterval;
	
	if (_fadeTime <= 0)
	{
		self.volume = 0.0;
		
		void(^handler)(void) = [timer userInfo];
		
		[_fadeTimer invalidate];
		_fadeTimer = nil;
		
		handler();
	}
	else
	{
		self.volume = _fadeTime / _fadeDuration;
	}
}

- (void)fadeInVolumeWithDuration:(NSTimeInterval)duration completionHandler:(void(^)(void))completionHandler
{
	void(^handler)(void) = [completionHandler copy];
	
	_fadeDuration = duration;
	_fadeTime = duration;
	[_fadeTimer invalidate];
	_fadeTimer = [NSTimer scheduledTimerWithTimeInterval:0.1
												  target:self
												selector:@selector(_fadeInTimerFired:)
												userInfo:handler
												 repeats:YES];
	[[NSRunLoop mainRunLoop] addTimer:_fadeTimer forMode:NSRunLoopCommonModes];
}

- (void)_fadeInTimerFired:(NSTimer*)timer
{	
	_fadeTime -= timer.timeInterval;
	
	if (_fadeTime <= 0)
	{
		self.volume = 1.0;
		
		void(^handler)(void) = [timer userInfo];
		
		[_fadeTimer invalidate];
		_fadeTimer = nil;
		
		handler();
	}
	else
	{
		self.volume = 1.0 - _fadeTime / _fadeDuration;
	}
}


#pragma mark - AVAudioPlayer Delegate Metods

- (void)_itemDidFinishPlaying
{
	[self stop];
	
    if ([self.delegate respondsToSelector:@selector(audioPlayerDidFinishPlaying:)])
    {
        [self.delegate audioPlayerDidFinishPlaying:self];
    }
}

@end
