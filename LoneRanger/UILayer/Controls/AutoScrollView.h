//
//  AutoScrollView.h
//  Unity-iOS Simulator
//
//  Created by Vladislav Bakuta on 9/5/13.
//
//

#import <UIKit/UIKit.h>

@interface AutoScrollView : UIScrollView <UIScrollViewDelegate>

- (void)startAutoScrolling;
- (void)stopAutoScrolling;

@end
