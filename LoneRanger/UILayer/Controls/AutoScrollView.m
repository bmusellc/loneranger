//
//  AutoScrollView.m
//  Unity-iOS Simulator
//
//  Created by Vladislav Bakuta on 9/5/13.
//
//

#import "AutoScrollView.h"

@interface AutoScrollView () <UIGestureRecognizerDelegate>
{
	NSTimer*							_scrollTimer;
}

@end

@implementation AutoScrollView

- (void)dealloc
{
    [self stopAutoScrolling];
    
    [super              dealloc];
}

- (id)init
{
    return [self initWithFrame:CGRectZero];
}

- (id)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
    
	if (self)
	{
		self.panGestureRecognizer.delegate = self;
		[self.panGestureRecognizer addTarget:self action:@selector(_handlePanGestureRecognizer:)];
    }
    
	return self;
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self startAutoScrolling];
}

- (void)startAutoScrolling
{
    _scrollTimer = [NSTimer scheduledTimerWithTimeInterval:0.025
													target:self
                                                  selector:@selector(autoScrollingContent)
                                                  userInfo:nil
												   repeats:YES];
	
	[[NSRunLoop currentRunLoop] addTimer:_scrollTimer forMode:NSRunLoopCommonModes];
}

- (void)stopAutoScrolling
{
	[_scrollTimer invalidate];
	_scrollTimer = nil;
}

- (void)autoScrollingContent
{
    CGFloat newOffset = self.contentOffset.y + 1;
	
    if (newOffset < self.contentSize.height - self.bounds.size.height)
    {
        self.contentOffset = CGPointMake(self.contentOffset.x, newOffset);
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
	if (gestureRecognizer == self.panGestureRecognizer)
	{
		[self stopAutoScrolling];
	}
	
	return YES;
}

- (void)_handlePanGestureRecognizer:(UIPanGestureRecognizer*)sender
{
	if (sender.state == UIGestureRecognizerStateEnded)
	{
		[self startAutoScrolling];
	}
}

@end
