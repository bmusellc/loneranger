//
//  BackgroundVideoView.h
//  VideoOnBackground
//
//  Created by Yuriy Belozerov on 23.04.13.
//  Copyright (c) 2013 bMuse. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import <CoreMedia/CoreMedia.h>
#import <UIKit/UIKit.h>

@class BackgroundVideoView;

@protocol BackgroundVideoViewDelegate <NSObject>
@optional

- (void)backgroundVideoViewIntroVideoItemAlmostFinishPlaying:(BackgroundVideoView*)videoView;

@end

@interface BackgroundVideoView : UIView

@property(assign)	id <BackgroundVideoViewDelegate>	delegate;

- (id)initWithFrame:(CGRect)frame playIntroVideo:(BOOL)playIntro;

- (void)play;
- (void)stop;

@end
