//
//  BackgroundVideoView.m
//  VideoOnBackground
//
//  Created by Yuriy Belozerov on 23.04.13.
//  Copyright (c) 2013 bMuse. All rights reserved.
//

#import "BackgroundVideoView.h"

#import "VideoView.h"

@interface BackgroundVideoView ()
{
	VideoView*						_videoView;
	
	CMTime							_loopStartTime;
	
	UITapGestureRecognizer*			_skipIntroTapRecognizer;
	
	id								_playbackCurrentTimeObserver;
}

@end

@implementation BackgroundVideoView

- (id)init
{
	return [self initWithFrame:CGRectZero];
}

- (id)initWithFrame:(CGRect)frame playIntroVideo:(BOOL)playIntro
{
    self = [super initWithFrame:frame];
	
    if (self)
	{
		self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Default-Landscape.png"]];
		
		//paths
		NSString* introVideoPath = [[NSBundle mainBundle] pathForResource:@"ae_01" ofType:@"mp4" inDirectory:@"Video"];
		NSString* loopVideoPath = [[NSBundle mainBundle] pathForResource:@"ae_02" ofType:@"mp4" inDirectory:@"Video"];
		
		//URLs
		NSURL* introVideoURL = [NSURL fileURLWithPath:introVideoPath];
		NSURL* loopVideoURL = [NSURL fileURLWithPath:loopVideoPath];
		
		//assets
		AVAsset* introAsset = [AVAsset assetWithURL:introVideoURL];
		AVAsset* loopAsset = [AVAsset assetWithURL:loopVideoURL];
		
		//joining video
		AVMutableComposition* mixComposition = [AVMutableComposition composition];
		
		AVMutableCompositionTrack* videoTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo
																			preferredTrackID:kCMPersistentTrackID_Invalid];
		
		CMTime currentTime = kCMTimeZero;
		
		if (playIntro)
		{
			currentTime = introAsset.duration;
			
			[videoTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, introAsset.duration)
								ofTrack:[introAsset tracksWithMediaType:AVMediaTypeVideo][0]
								 atTime:kCMTimeZero error:nil];
		}
		
		_loopStartTime = currentTime;
		
		for (int i = 0; i < 10; i++)
		{
			[videoTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, loopAsset.duration)
								ofTrack:[loopAsset tracksWithMediaType:AVMediaTypeVideo][0]
								 atTime:currentTime error:nil];
			
			currentTime = CMTimeAdd(currentTime, loopAsset.duration);
		}
		
		_videoView = [[[VideoView alloc] initWithFrame:self.bounds] autorelease];
		[self addSubview:_videoView];
		_videoView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
		
		_videoView.player = [AVPlayer playerWithPlayerItem:[AVPlayerItem playerItemWithAsset:mixComposition]];
		_videoView.player.actionAtItemEnd = AVPlayerActionAtItemEndNone;
		
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(_playerItemDidReachEnd:)
													 name:AVPlayerItemDidPlayToEndTimeNotification
												   object:_videoView.player.currentItem];
				
		CMTime interval = CMTimeMakeWithSeconds(0.1, NSEC_PER_SEC);
		_playbackCurrentTimeObserver = [_videoView.player addPeriodicTimeObserverForInterval:interval
																					   queue:NULL
																				  usingBlock:^(CMTime time) {
																					  
																					  if (CMTimeGetSeconds(_loopStartTime) < CMTimeGetSeconds(time))
																					  {
																						  if ([self.delegate respondsToSelector:@selector(backgroundVideoViewIntroVideoItemAlmostFinishPlaying:)])
																						  {
																							  [self.delegate backgroundVideoViewIntroVideoItemAlmostFinishPlaying:self];
																						  }
																						  
																						  [self removeGestureRecognizer:_skipIntroTapRecognizer];
																						  [_videoView.player removeTimeObserver:_playbackCurrentTimeObserver];
																					  }
																				  }];
		
		_skipIntroTapRecognizer = [[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(_skipIntro)] autorelease];
		[self addGestureRecognizer:_skipIntroTapRecognizer];
    }
    
    return self;
}

- (void)_playerItemDidReachEnd:(NSNotification*)notification
{
    AVPlayerItem* playerItem = [notification object];
    [playerItem seekToTime:_loopStartTime];
}

- (void)play
{
	[_videoView.player play];
}

- (void)stop
{
	[_videoView.player pause];
}

- (float)getCurrentTime
{
    return _videoView.player.currentTime.value;
}

- (void)_skipIntro
{
	[self removeGestureRecognizer:_skipIntroTapRecognizer];
	[_videoView.player removeTimeObserver:_playbackCurrentTimeObserver];
	
	UIView* blackView = [[[UIView alloc] initWithFrame:self.bounds] autorelease];
	[self addSubview:blackView];
	blackView.backgroundColor = [UIColor blackColor];
	blackView.alpha = 0.0;
	
	[UIView animateWithDuration:1.0
					 animations:^{
						 
						 blackView.alpha = 1.0;
					 }
	 
					 completion:^(BOOL finished) {
						 
						 [_videoView.player.currentItem seekToTime:_loopStartTime];
						 
						 if ([self.delegate respondsToSelector:@selector(backgroundVideoViewIntroVideoItemAlmostFinishPlaying:)])
						 {
							 [self.delegate backgroundVideoViewIntroVideoItemAlmostFinishPlaying:self];
						 }
						 
						 self.backgroundColor = [UIColor blackColor];
						 
						 [UIView animateWithDuration:1.0
										  animations:^{
											  
											  blackView.alpha = 0.0;
										  }
										  completion:^(BOOL finished) {
											  
											  [blackView removeFromSuperview];
										  }];
					 }];
}

@end
