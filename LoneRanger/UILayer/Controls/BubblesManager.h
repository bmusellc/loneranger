//
//  BubblesManager.h
//  Unity-iOS Simulator
//
//  Created by Artem Manzhura on 6/11/13.
//
//

#import <Foundation/Foundation.h>

#import "PageView.h"
#import "FrameInfo.h"
#import "ARCIssueViewController.h"

@class BubblesManager;

@protocol BubbleManagerDelegate <NSObject>

- (void)bubbleManagerShouldPassToNextFrame:(BubblesManager*)sender;
- (void)bubbleManagerShouldPassToNextPage:(BubblesManager*)sender;
- (void)bubbleManagerShouldSwitchToNextAmbientSound:(BubblesManager *)sender withFrameIdx:(NSInteger)idx;
@end

@interface BubblesManager : NSObject

@property (assign) ARCIssueViewReviewState      currentState;
@property (assign) id<BubbleManagerDelegate>    delegate;

- (id)initWithPages:(NSArray*)pages withState:(ARCIssueViewReviewState)state;
- (void)showBubbleAtPageIndex:(NSInteger)pageIdx atFrameIndex:(NSInteger)frameIdx;
- (void)stopShowing;
- (void)pause;
- (void)revealBubbles;

@end
