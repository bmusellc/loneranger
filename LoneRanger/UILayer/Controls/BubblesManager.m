//
//  BubblesManager.m
//  Unity-iOS Simulator
//
//  Created by Artem Manzhura on 6/11/13.
//
//

#import "BubblesManager.h"
#import "SpeechBubble.h"
#import "PhotoPageView.h"
#import "ReaderSettings.h"
#import "SfxInfo.h"

#define IDLE_ANIMATION_DURATION         2.0

@interface BubblesManager()<SpeechBubbleDelegate, AudioPlayerDelegate>
{
    AudioPlayer*        _sfx;
    SpeechBubble*       _bubble;
    NSArray*            _pages;
    NSInteger           _currentSfxIndex;
    NSInteger           _currentPageIndex;
    NSInteger           _currentFrameIndex;
    NSInteger           _currentBubbleSpeechInFrame;
    BOOL                _isIdle;
    BOOL                _isAllBubblesPlayed;
    BOOL                _isAllSfxPlayed;
    BOOL                _isSfxPlaying;
    NSTimer*            _sfxTimer;
    NSTimeInterval      _startTimeOfSfxTimer;
}   

@end

@implementation BubblesManager

- (void)dealloc
{
    NSLog(@"BubbleManager is deallocated");
    [self _stopSfxTimer];
    [_pages release];
    _sfx.delegate = nil;
    [_sfx release];
    _bubble.delegate = nil;
    [_bubble  release];
    [super dealloc];
}

- (id)initWithPages:(NSArray*)pages withState:(ARCIssueViewReviewState)state
{
    self = [super init];
    if (self) {
        _isIdle = YES;
        _currentState = state;
        NSLog(@"pages %d", [pages count]);
        _pages = [pages retain];
        _currentBubbleSpeechInFrame = 0;
        _currentPageIndex = -1;
        _currentSfxIndex = -1;
        _currentFrameIndex = -1;
        _isAllBubblesPlayed = NO;
        _isAllSfxPlayed = NO;
        [self _startSfxTimer];
    }
    return self;
}

- (void)revealBubbles
{
    PhotoPageView* page = (PhotoPageView*)[_pages objectAtIndex:_currentPageIndex];
    if([self _shouldShowAllBubbles])
    {
        [page showAllBubbles];
    }
    else if ([self _bubblesShouldRemainOnScreen]){
        [page showBubblesUptoFrameIndex:_currentFrameIndex bubbleIndex:_currentBubbleSpeechInFrame];
    }
    if (_isIdle){
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(_switchToNextFrame) object:nil];
        [self performSelector:@selector(_switchToNextFrame) withObject:nil afterDelay:IDLE_ANIMATION_DURATION];
    }
}

- (void)_startSfxTimer
{
    [self _stopSfxTimer];
    _startTimeOfSfxTimer = [[NSDate date] timeIntervalSince1970];
    _sfxTimer = [NSTimer scheduledTimerWithTimeInterval:.01f target:self selector:@selector(_updateCurrentTime:) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:_sfxTimer forMode:NSRunLoopCommonModes];
}

- (void)_stopSfxTimer
{
    if (_sfx != nil)
    {
        [_sfx stop];
        _sfx.delegate = nil;
        [_sfx release];
        _sfx = nil;
    }
    [_sfxTimer invalidate];
    _sfxTimer = nil;
}

- (NSInteger)_currentIdxByCurrentTime:(NSTimeInterval)time withFrameInfo:(FrameInfo*)frameInfo
{
    __block NSInteger currentIdx = -1;
    [frameInfo.sfx enumerateObjectsUsingBlock:^(SfxInfo* nextSfx, NSUInteger idx, BOOL *stop) { 
        if(time >= nextSfx.time)
        {
            currentIdx = idx;
            *stop = YES;
        }
    }];
    return currentIdx;
}

- (void)_updateCurrentTime:(NSTimer*)sender
{
    NSTimeInterval currentTime = [[NSDate date] timeIntervalSince1970];
    CGFloat currentTimeOffset = currentTime - _startTimeOfSfxTimer;
    
    if(_currentPageIndex < 0)
    {
        return;
    }
    
    if(_pages.count == 0)
    {
        return;
    }
    
    PhotoPageView* page = (PhotoPageView*)[_pages objectAtIndex:_currentPageIndex];
    FrameInfo* frame = (FrameInfo*)[page.pageInfo.frames objectAtIndex:_currentFrameIndex];
    
    NSInteger sfxIdx = [self _currentIdxByCurrentTime:currentTimeOffset withFrameInfo:frame];
    
    if ((!_isSfxPlaying && [self _isLastSfxIdx]) || ([frame.sfx count] == 0))
    {
        
        _isAllSfxPlayed = YES;
        [self _stopSfxTimer];
        if ([self _isReadyPassToNextFrame])
        {
            [self _switchToNextFrame];
        }
        return;
    }
    
    if (sfxIdx >= 0)
    {
        [self _sfxWithFrame:frame withIdx:sfxIdx];
    }
}

- (BOOL)_isLastSfxIdx   
{
    BOOL res = NO;
    
    PhotoPageView* page = (PhotoPageView*)[_pages objectAtIndex:_currentPageIndex];
    FrameInfo* frame = (FrameInfo*)[page.pageInfo.frames objectAtIndex:_currentFrameIndex];

    if ((_currentSfxIndex >= [frame.sfx count] - 1) && (_currentSfxIndex != -1))
    {
        res = YES;
    }
    return res;
}

- (void)_playBubbleAtPageIndex:(NSInteger)pageIdx atFrameIndex:(NSInteger)frameIdx
{
    NSLog(@"_playBubbleAtPageIndex %d", pageIdx);
    [self _clearAllBubbles];
    _isAllBubblesPlayed = NO;
    _isAllSfxPlayed = NO;
    _currentSfxIndex = -1;
    _currentBubbleSpeechInFrame = 0;
    _currentPageIndex = pageIdx;
    _currentFrameIndex = frameIdx;
    _isIdle = NO;
    
    [self _startSfxTimer];
    if (![self _isBubbleIndexCorrect:_currentBubbleSpeechInFrame]) // switch to next frame with delay
    {
        if(![self _shouldShowAllBubbles] && ![self _bubblesShouldRemainOnScreen])
        {
            [self _hideBubble];
        }
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(_switchToNextFrame) object:nil];
        [self performSelector:@selector(_switchToNextFrame) withObject:nil afterDelay:IDLE_ANIMATION_DURATION];
    }
    else{
        if ([self _isSoundExistingAtIndex:_currentBubbleSpeechInFrame]) // play
        {
            PhotoPageView* page = (PhotoPageView*)[_pages objectAtIndex:_currentPageIndex];
            FrameInfo* frame = (FrameInfo*)[page.pageInfo.frames objectAtIndex:_currentFrameIndex];
            [self _bubbleWithPage:page withFrameInfo:frame withHidingPreviousAnimated:YES];
        }
        else{  // pass to next bubble with delay
            if(![self _shouldShowAllBubbles] && ![self _bubblesShouldRemainOnScreen])
            {
                [self _hideBubble];
            }

            [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(_switchBubbles) object:nil];
            [self performSelector:@selector(_switchBubbles) withObject:nil afterDelay:IDLE_ANIMATION_DURATION];
        }
    }
}


- (void)showBubbleAtPageIndex:(NSInteger)pageIdx atFrameIndex:(NSInteger)frameIdx
{
    NSLog(@"%d %d", pageIdx, frameIdx);
    
    if ((_currentPageIndex == pageIdx) && (_currentFrameIndex == frameIdx))
    {
        return;
    }
    
    if(pageIdx < 0 || pageIdx >= _pages.count)
    {
        return;
    }
    
    _isIdle = NO;
    if ((_currentPageIndex != pageIdx))
    {
        [self _hideAllBubbles];
    }
    PhotoPageView* page = (PhotoPageView*)[_pages objectAtIndex:pageIdx];
    if([self _shouldShowAllBubbles])
    {
        [page showAllBubbles];
    }
    else if ([self _bubblesShouldRemainOnScreen]){
        [page showBubblesUptoFrameIndex:frameIdx bubbleIndex:0];
    }
    [self _playBubbleAtPageIndex:pageIdx atFrameIndex:frameIdx];
}

- (void)_clearAllBubbles
{
    [self _cancelAllTasks];
    [self _stopSfxTimer];
    if (_bubble != nil)
    {
        [_bubble stop];
        _bubble.delegate = nil;
        [_bubble release];
        _bubble = nil;
    }
    if(![self _shouldShowAllBubbles]  && ![self _bubblesShouldRemainOnScreen])
    {
        [self _hideBubble];
    }
}

- (void)pause
{
    [self _cancelAllTasks];
    [self _stopSfxTimer];
    if (_bubble != nil)
    {
        [_bubble stop];
        _bubble.delegate = nil;
//        [_bubble release];
//        _bubble = nil;
    }
    _currentBubbleSpeechInFrame = 0;
    _currentSfxIndex = -1;
    _currentPageIndex = -1;
    _currentFrameIndex = -1;
    _isAllBubblesPlayed = NO;
    _isAllSfxPlayed = NO;
}

- (void)stopShowing
{
    NSLog(@"stopShowing");
    [self _cancelAllTasks];
    [self _stopSfxTimer];
    if (_bubble != nil)
    {
        [_bubble stop];
        _bubble.delegate = nil;
        [_bubble release];
        _bubble = nil;
    }
    if(![self _shouldShowAllBubbles] && ![self _bubblesShouldRemainOnScreen])
    {
        [self _hideBubble];
    }else
    {
        [self _hideAllBubbles];
    }

    _currentBubbleSpeechInFrame = 0;
    _currentSfxIndex = -1;
    _currentPageIndex = -1;
    _currentFrameIndex = -1;
    _isAllBubblesPlayed = NO;
    _isAllSfxPlayed = NO;
}

#pragma mark - Internal Methods

- (void)_sfxWithFrame:(FrameInfo*)frameInfo withIdx:(NSInteger)idx
{
    if (_currentSfxIndex == idx)
    {
        return;
    }
    
    if (_sfx != nil)
    {
        [_sfx stop];
        _sfx.delegate = nil;
        [_sfx release];
        _sfx = nil;
    }
    
    _currentSfxIndex = idx;
    
    if (![[ReaderSettings sharedManager] isAudioEnabled])
    {
        _isAllSfxPlayed = YES;
        _isSfxPlaying = NO;
        return;
    }
    
    if ((frameInfo.sfx.count < 1) || (idx >= frameInfo.sfx.count))
    {
        NSLog(@"ERROR : incorrect sfx idx");
        return;
    }
    SfxInfo* currentSfx = [frameInfo.sfx objectAtIndex:idx];
    _sfx = [[AudioPlayer alloc] init];
    [_sfx loadMusicWithFilePath:currentSfx.soundPath startTime:0.0 volume:[[ReaderSettings sharedManager] volume] / 100.0];
    _sfx.delegate = self;
    [_sfx play];
    _isSfxPlaying = YES;
}

- (void)_bubbleWithPage:(PhotoPageView*)page withFrameInfo:(FrameInfo*)frameInfo withHidingPreviousAnimated:(BOOL)animated
{
    if (_bubble != nil)
    {
        [_bubble stop];
        _bubble.delegate = nil;
        [_bubble release];
        _bubble = nil;
    }
    
    if (frameInfo.bubbles.count < 1)
    {
        NSLog(@"ERROR : frame doesn't have any BubbleSpeech");
        return;
    }
    
    
    if ([[ReaderSettings sharedManager] isTextBubblesEnabled])
    {
        if (![self _shouldShowAllBubbles])
        {
            if (![self _bubblesShouldRemainOnScreen])
            {
                [page showBubbleAtFrameIndex:_currentFrameIndex atBubbleIndex:_currentBubbleSpeechInFrame hidePrevBubble:YES];
            }
            else{
                [page showBubbleAtFrameIndex:_currentFrameIndex atBubbleIndex:_currentBubbleSpeechInFrame hidePrevBubble:NO];
            }
        
        }
        else{
            [page showAllBubbles];
        }
    }
    
    _bubble = [[SpeechBubble speechBubbleWithBubbleInfo:[frameInfo.bubbles objectAtIndex:_currentBubbleSpeechInFrame]] retain];
    _bubble.delegate = self;
    
    if ([[ReaderSettings sharedManager] isVOEnabled])
    {
        if (![[ReaderSettings sharedManager] isAudioEnabled] && ![self _isBubbleImageExistAtIndex:_currentBubbleSpeechInFrame])
        {
            [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(_switchBubbles) object:nil];
            [self performSelector:@selector(_switchBubbles) withObject:nil afterDelay:IDLE_ANIMATION_DURATION];
        }
        else
        {
            [_bubble play];
        }
    }
    else{
        if ([self _isBubbleImageExistAtIndex:_currentBubbleSpeechInFrame])
        {
            float duration = [_bubble duration];
            [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(_switchBubbles) object:nil];
            [self performSelector:@selector(_switchBubbles) withObject:nil afterDelay:duration];
        }
        else{
            if ([[ReaderSettings sharedManager] isAudioEnabled])
            {
                [_bubble play];
            }else{
                [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(_switchBubbles) object:nil];
                [self performSelector:@selector(_switchBubbles) withObject:nil afterDelay:IDLE_ANIMATION_DURATION];
            }
        }
    }
}

- (BOOL)_bubblesShouldRemainOnScreen
{
    BOOL res = NO;
    if ([[ReaderSettings sharedManager] isTextBubblesEnabled] && (![[ReaderSettings sharedManager]isVOEnabled] || ![[ReaderSettings sharedManager] isAutoplay]))
    {
        res = YES;
    }
    return res;
}

- (BOOL)_isReadyPassToNextFrame
{
    BOOL res = NO;
    if(_isAllBubblesPlayed && _isAllSfxPlayed)
    {
        res = YES;
    }
    return res;
}

- (BOOL)_shouldShowAllBubbles
{
    BOOL res = NO;
    if ([[ReaderSettings sharedManager] isTextBubblesEnabled] &&
        ![[ReaderSettings sharedManager] isVOEnabled] &&
        ![[ReaderSettings sharedManager] isAudioEnabled] &&
        ![[ReaderSettings sharedManager] isAutoplay])
    {
        res = YES;
    }
    return res;
}

- (BOOL)_isFrameIndexCorrect:(NSInteger)index
{
    BOOL res = YES;
    PageView* page = (PageView*)[_pages objectAtIndex:_currentPageIndex];
    int count = page.pageInfo.frames.count;
    if(index >= count)
    {
        res = NO;
    }
    return res;
}

- (BOOL)_isBubbleIndexCorrect:(NSInteger)index
{
    BOOL res = YES;
    PageView* page = (PageView*)[_pages objectAtIndex:_currentPageIndex];
    FrameInfo* frame = (FrameInfo*)[page.pageInfo.frames objectAtIndex:_currentFrameIndex];
    
    int count = frame.bubbles.count;
    if(index >= count)
    {
        res = NO;
    }
    return res;
}

- (BOOL)_isBubbleImageExistAtIndex:(NSInteger)idx
{
    BOOL res = NO;
    if ([self _isBubbleIndexCorrect:idx])
    {
        PageView* page = (PageView*)[_pages objectAtIndex:_currentPageIndex];
        FrameInfo* frame = (FrameInfo*)[page.pageInfo.frames objectAtIndex:_currentFrameIndex];
        BubbleInfo* bubbleInfo = [frame.bubbles objectAtIndex:idx];
        if([bubbleInfo.imagePath length] >= 1)
        {
            return YES;
        }
    }
    return res;
}

- (BOOL)_isSoundExistingAtIndex:(NSInteger)index
{
    BOOL res = NO;
    if ([self _isBubbleIndexCorrect:index])
    {
        PageView* page = (PageView*)[_pages objectAtIndex:_currentPageIndex];
        FrameInfo* frame = (FrameInfo*)[page.pageInfo.frames objectAtIndex:_currentFrameIndex];
        BubbleInfo* bubbleInfo = [frame.bubbles objectAtIndex:index];
        if([bubbleInfo.speeches count] >= 1)
        {
            return YES;
        }
    }
    return res;
}

- (void)_switchToNextFrame
{
    if (!_isAllSfxPlayed)
    {
        _isAllBubblesPlayed = YES;
        return;
    }
    if (_currentState == kARCIssueViewReviewStateFullPage)
    {
        if([self _isFrameIndexCorrect:_currentFrameIndex + 1])
        {
            _currentFrameIndex++;
            _currentBubbleSpeechInFrame = 0;
            [self _playBubbleAtPageIndex:_currentPageIndex atFrameIndex:_currentFrameIndex];
        }
        else{
            if ([[ReaderSettings sharedManager] isAutoplay])
            {
                if([self _shouldShowAllBubbles])
                {
                    [self _hideAllBubbles];
                }
                if ([_delegate respondsToSelector:@selector(bubbleManagerShouldPassToNextPage:)])
                {
                    [_delegate bubbleManagerShouldPassToNextPage:self];
                }
            }else{
                _isIdle = YES;
            }
        }
    }
    else{
        if ([[ReaderSettings sharedManager] isAutoplay])
        {
            if([self _shouldShowAllBubbles])
            {
                [self _hideAllBubbles];
            }
            if ([_delegate respondsToSelector:@selector(bubbleManagerShouldPassToNextFrame:)])
            {
                [_delegate bubbleManagerShouldPassToNextFrame:self];
            }
        }else{
            _isIdle = YES;
        }
    }
    
    if ([_delegate respondsToSelector:@selector(bubbleManagerShouldSwitchToNextAmbientSound:withFrameIdx:)])
    {
        [_delegate bubbleManagerShouldSwitchToNextAmbientSound:self withFrameIdx:_currentFrameIndex];
    }
}

- (void)_hideAllBubbles
{
    if (_currentPageIndex == -1)
    {
        return;
    }
    if ([_pages objectAtIndex:_currentPageIndex] != [NSNull null])
    {
        PhotoPageView* page = (PhotoPageView*)[_pages objectAtIndex:_currentPageIndex];
        [page hideAllBubbles];
    }
}

- (void)_hideBubble
{
    if (_currentPageIndex == -1)
    {
        return;
    }
    if ([_pages objectAtIndex:_currentPageIndex] != [NSNull null])
    {
        PhotoPageView* page = (PhotoPageView*)[_pages objectAtIndex:_currentPageIndex];
        [page hideCurrentBubble];
    }
}

- (void)_cancelAllTasks
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(_switchBubbles) object:nil];
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(_switchToNextFrame) object:nil];
}

- (void)_switchBubbles
{
    if (![self _isBubbleIndexCorrect:_currentBubbleSpeechInFrame + 1]) // switch to next frame with delay
    {
        if(![self _shouldShowAllBubbles] && ![self _bubblesShouldRemainOnScreen])
        {
            [self _hideBubble];
        }
        _isAllBubblesPlayed = YES;
        if ([self _isReadyPassToNextFrame])
        {
            [self _switchToNextFrame];
        }
    }
    else{
        if ([self _isSoundExistingAtIndex:_currentBubbleSpeechInFrame + 1]) // play
        {
            _currentBubbleSpeechInFrame++;
            PhotoPageView* page = (PhotoPageView*)[_pages objectAtIndex:_currentPageIndex];
            FrameInfo* frame = (FrameInfo*)[page.pageInfo.frames objectAtIndex:_currentFrameIndex];
            [self _bubbleWithPage:page withFrameInfo:frame withHidingPreviousAnimated:YES];
        }
        else{  // pass to next bubble with delay
            _currentBubbleSpeechInFrame++;
            if(![self _shouldShowAllBubbles] && ![self _bubblesShouldRemainOnScreen])
            {
                [self _hideBubble];
            }

            [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(_switchBubbles) object:nil];
            [self performSelector:@selector(_switchBubbles) withObject:nil afterDelay:IDLE_ANIMATION_DURATION];
        }
    }
}

#pragma mark - AudioPlayerDelegate Methods
- (void)audioPlayerDidFinishPlaying:(AudioPlayer*)audioPlayer
{
    _isSfxPlaying = NO;
}

#pragma mark - BubbleDelegate Methods
- (void)speechBubbleDidStopPlaying:(SpeechBubble*)sender
{
    [self _switchBubbles];
}




@end
