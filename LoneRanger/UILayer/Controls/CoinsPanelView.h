//
//  CoinsPanelView.h
//  Unity-iOS Simulator
//
//  Created by Yuriy Belozerov on 22.05.13.
//
//

#import <UIKit/UIKit.h>

@interface CoinsPanelView : UIView

@property(assign)	NSInteger			numberOfCoins;

@end
