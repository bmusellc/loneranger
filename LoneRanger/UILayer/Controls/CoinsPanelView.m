//
//  CoinsPanelView.m
//  Unity-iOS Simulator
//
//  Created by Yuriy Belozerov on 22.05.13.
//
//

#import "CoinsPanelView.h"

@interface CoinsPanelView ()
{
	NSInteger					_numberOfCoins;
	
	UILabel*					_coinsLabel;
}

@end

@implementation CoinsPanelView

- (NSInteger)numberOfCoins
{
	return _numberOfCoins;
}

- (void)setNumberOfCoins:(NSInteger)numberOfCoins
{
	_numberOfCoins = numberOfCoins;
	
	_coinsLabel.text = [NSString stringWithFormat:@"%d", self.numberOfCoins];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
	if (self)
	{
		UIImage* coinsIcon = [UIImage imageNamed:@"menu/Armory/coinsIcon.png"];
		
		self.frame = CGRectMake(0, 0, 320, coinsIcon.size.height);
		
		UIImageView* coinsIconView = [[[UIImageView alloc] initWithImage:coinsIcon] autorelease];
		[self addSubview:coinsIconView];
		coinsIconView.frame = CGRectOffset(coinsIconView.frame, self.bounds.size.width - coinsIconView.bounds.size.width, 0);
		
		_coinsLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 15, self.bounds.size.width - coinsIconView.bounds.size.width, self.bounds.size.height - 15)];
		[self addSubview:_coinsLabel];
		_coinsLabel.backgroundColor = [UIColor clearColor];
		_coinsLabel.textColor = [UIColor colorWithRed:250.0 / 255.0 green:228.0 / 255.0 blue:190.0 / 255.0 alpha:1.0];
		_coinsLabel.font = [UIFont fontWithName:@"CMWesternWoodblockNo2" size:44];
		_coinsLabel.textAlignment = NSTextAlignmentRight;
    }
    
	return self;
}

- (id)init
{
    return [self initWithFrame:CGRectZero];
}

@end
