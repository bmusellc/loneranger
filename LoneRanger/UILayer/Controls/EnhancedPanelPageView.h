//
//  EnhancedPanelPageView.h
//  Unity-iPhone
//
//  Created by Yuriy Belozerov on 08.05.13.
//
//

#import "PageView.h"

@interface EnhancedPanelPageView : PageView

- (id)initWithPageInfo:(PageInfo*)pageInfo;

@end
