//
//  EnhancedPanelPageView.m
//  Unity-iPhone
//
//  Created by Yuriy Belozerov on 08.05.13.
//
//

#import "EnhancedPanelPageView.h"
#import "UnityLevelsLoader.h"

extern void UnitySendMessage(const char * className, const char * methodName, const char * param);

@interface EnhancedPanelPageView () <UnityLevelsLoaderDelegate>
{
	BOOL							_isLevelLoaded;
	
	UIView*							_contentView;
	
	UIActivityIndicatorView*		_spinner;
}
@end

@implementation EnhancedPanelPageView

- (CGRect)imageFrame
{
	return self.bounds;
}

- (id)initWithPageInfo:(PageInfo*)pageInfo
{
    self = [super initWithPageInfo:pageInfo];
    
	if (self)
	{
		_isLevelLoaded = NO;
		
		CGRect contentFrame = UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation) ? CGRectMake(-298, 0, 1364, 1024) : CGRectMake(0, 0, 1024, 768);
		
		[self addSubview:self.thumbnailImageView];
		
		_contentView = [[UIView alloc] initWithFrame:contentFrame];
		[self addSubview:_contentView];
		_contentView.clipsToBounds = YES;
		_contentView.alpha = 0.0;
		
		self.thumbnailImageView.frame = _contentView.frame;
		self.thumbnailImageView.autoresizingMask = UIViewAutoresizingNone;
		
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(_applicationWillChangeStatusBarOrientation:)
													 name:UIApplicationWillChangeStatusBarOrientationNotification
												   object:nil];
	}
    
	return self;
}

- (void)dealloc
{
	[UnityLevelsLoader sharedLoader].delegate = nil;
	
	[_contentView release];
	[_spinner release];
	
	[[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillChangeStatusBarOrientationNotification object:nil];
	
    [super dealloc];
}

- (void)onLevelLoaded
{
	NSLog(@"Level loaded");
	
	_isLevelLoaded = YES;
	
//    showUnityInView(_contentView);
//    
//    [UIView animateWithDuration:0.75
//                          delay:1.0
//                        options:0
//                     animations:^{
//                         
//                         _contentView.alpha = 1.0;
//                         _spinner.alpha = 0.0;
//                     }
//                     completion:^(BOOL finished) {
//                         
//                         [_spinner stopAnimating];
//                         [_spinner removeFromSuperview];
//                         
//                         UnitySendMessage("Main Camera", "onDeviceOrientationWillChangedTo", [[NSString stringWithFormat:@"%d", [UIApplication sharedApplication].statusBarOrientation] cStringUsingEncoding:NSUTF8StringEncoding]);
//                     }];
}

- (void)onLevelUnloaded
{
	NSLog(@"Level unloaded");
	
	_isLevelLoaded = NO;
}

- (void)loadContent
{
//#if !TARGET_IPHONE_SIMULATOR
//    [self performSelectorOnMainThread:@selector(_showActivityIndicator) withObject:nil waitUntilDone:YES];
//
//    [[UnityLevelsLoader sharedLoader] loadLevelNamed:self.pageInfo.sceneName forConsumer:self];
//#endif
}

- (void)unloadContent
{
//    [UnityLevelsLoader sharedLoader].delegate = nil;
//    hideUnity();
//    [[UnityLevelsLoader sharedLoader] unloadLevel];
}

- (BOOL)needsToLoadContent
{
	return !_isLevelLoaded;
}

- (void)_applicationWillChangeStatusBarOrientation:(NSNotification*)notification
{
	UIInterfaceOrientation newOrientation = (UIInterfaceOrientation)[notification.userInfo[UIApplicationStatusBarOrientationUserInfoKey] integerValue];
	UIInterfaceOrientation currentOrientation = [UIApplication sharedApplication].statusBarOrientation;
	
//    UnitySendMessage("Main Camera", "onDeviceOrientationWillChangedTo", [[NSString stringWithFormat:@"%d", newOrientation] cStringUsingEncoding:NSUTF8StringEncoding]);
	
	BOOL relayoutNeeded = (UIInterfaceOrientationIsLandscape(newOrientation) && UIInterfaceOrientationIsPortrait(currentOrientation)) ||
	(UIInterfaceOrientationIsPortrait(newOrientation) && UIInterfaceOrientationIsLandscape(currentOrientation));
	
	if (relayoutNeeded)
	{
		CGRect contentViewFrame = UIInterfaceOrientationIsPortrait(newOrientation) ? contentViewFrame = CGRectMake(-298, 0, 1364, 1024) : CGRectMake(0, 0, 1024, 768);
				
		_contentView.frame = contentViewFrame;
		
		self.thumbnailImageView.frame = _contentView.frame;
	}
}

- (void)_showActivityIndicator
{
	_spinner = [[UIActivityIndicatorView alloc] initWithFrame:self.bounds];
	[self insertSubview:_spinner belowSubview:_contentView];
	_spinner.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	_spinner.contentMode = UIViewContentModeCenter;
	_spinner.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
	_spinner.hidesWhenStopped = YES;
	
	[_spinner startAnimating];
}

@end
