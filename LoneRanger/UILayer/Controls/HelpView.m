//
//  HelpView.m
//  Unity-iPhone
//
//  Created by Artem Manzhura on 4/23/13.
//
//

#import "HelpView.h"

@implementation HelpView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        UIImage* helpImage = [UIImage imageNamed:@"reader/HomeHelpImage.png"];
        UIImageView* imageView = [[[UIImageView alloc] initWithImage:helpImage] autorelease];
        imageView.frame = CGRectMake(90, 90, helpImage.size.width, helpImage.size.height);
        [self addSubview:imageView];

        
        helpImage = [UIImage imageNamed:@"reader/SettingsHelpImage.png"];
        imageView = [[[UIImageView alloc] initWithImage:helpImage] autorelease];
        imageView.frame = CGRectMake(frame.size.width - helpImage.size.width - 90, 90, helpImage.size.width, helpImage.size.height);
        [self addSubview:imageView];
        imageView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
        
        
        helpImage = [UIImage imageNamed:@"reader/toggleHelpImage.png"];
        imageView = [[[UIImageView alloc] initWithImage:helpImage] autorelease];
        imageView.frame = CGRectMake(CGRectGetMidX(frame) - helpImage.size.width / 2.0, CGRectGetMidY(frame) - helpImage.size.height / 2.0 - 200, helpImage.size.width, helpImage.size.height);
        [self addSubview:imageView];
        imageView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin;
        
        
        helpImage = [UIImage imageNamed:@"reader/previousHelpImage.png"];
        imageView = [[[UIImageView alloc] initWithImage:helpImage] autorelease];
        imageView.frame = CGRectMake(10, 450, helpImage.size.width, helpImage.size.height);
        [self addSubview:imageView];
        imageView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
        
        
        helpImage = [UIImage imageNamed:@"reader/nextHelpImage.png"];
        imageView = [[[UIImageView alloc] initWithImage:helpImage] autorelease];
        imageView.frame = CGRectMake(frame.size.width - helpImage.size.width - 10, 450, helpImage.size.width, helpImage.size.height);
        [self addSubview:imageView];
        imageView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
        
        
        helpImage = [UIImage imageNamed:@"reader/fullScreenImage.png"];
        imageView = [[[UIImageView alloc] initWithImage:helpImage] autorelease];
        imageView.frame = CGRectMake(45, frame.size.height - helpImage.size.height - 100, helpImage.size.width, helpImage.size.height);
        [self addSubview:imageView];
        imageView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
    }
    return self;
}

@end
