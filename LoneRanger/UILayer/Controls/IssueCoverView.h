//
//  IssueCoverView.h
//  Unity-iOS Simulator
//
//  Created by Yuriy Belozerov on 19.11.13.
//
//

#import "IssuesManager.h"
#import <UIKit/UIKit.h>

@interface IssueCoverView : UIControl

- (id)initWithIssue:(Issue*)issue;

@property(readonly)	Issue*			issue;

- (void)setCoverViewVisible:(BOOL)visible animated:(BOOL)animated;

@end
