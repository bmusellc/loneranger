//
//  IssueCoverView.m
//  Unity-iOS Simulator
//
//  Created by Yuriy Belozerov on 19.11.13.
//
//

#import "IssueCoverView.h"

@interface IssueCoverView ()
{
	UIImageView*				_backCoverImageView;
	UIImageView*				_coverImageView;
	
	UIView*						_highlightView;
}

@end

@implementation IssueCoverView

- (id)initWithIssue:(Issue*)issue
{
	self = [super init];
	
	if (self)
	{
		_issue = [issue retain];
		
		if (self.issue)
		{
			UIImage* backCover = [UIImage imageWithContentsOfFile:self.issue.backCoverPath];
			_backCoverImageView = [[[UIImageView alloc] initWithImage:backCover] autorelease];
			[self addSubview:_backCoverImageView];
			_backCoverImageView.alpha = 0.5;
			
			UIImage* cover = [UIImage imageWithContentsOfFile:self.issue.coverPath];
			_coverImageView = [[[UIImageView alloc] initWithImage:cover] autorelease];
			[self addSubview:_coverImageView];
			
			_highlightView = [[UIView alloc] initWithFrame:self.bounds];
			[self addSubview:_highlightView];
			_highlightView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
			_highlightView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.5];
			_highlightView.hidden = YES;
			
			self.frame = CGRectMake(0, 0, backCover.size.width, backCover.size.height);
			
			self.exclusiveTouch = YES;
		}
	}
	
	return self;
}

- (void)dealloc
{
	[_issue release];
	
	[_backCoverImageView release];
	[_coverImageView release];
	
	[_highlightView release];
    
	[super dealloc];
}

- (void)setHighlighted:(BOOL)highlighted
{
	[super setHighlighted:highlighted];
	
	_highlightView.hidden = !highlighted;
}

- (void)setCoverViewVisible:(BOOL)visible animated:(BOOL)animated
{
	if (animated)
	{
		[UIView animateWithDuration:0.3
						 animations:^{
							 
							 _coverImageView.alpha = visible ? 1.0 : 0.0;
						 }];
	}
	else
	{
		_coverImageView.alpha = visible ? 1.0 : 0.0;
	}
	
	self.userInteractionEnabled = visible;
}

@end
