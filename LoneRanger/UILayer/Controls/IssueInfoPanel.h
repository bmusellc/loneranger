//
//  IssueInfoPanel.h
//  Unity-iPhone
//
//  Created by Artem Manzhura on 4/24/13.
//
//

#import "IssueInfoPanelButton.h"

@interface IssueInfoPanel : UIImageView

@property(readonly)	MenuLabel*					issueNameLabel;

@property(readonly)	IssueInfoPanelButton*		purchaseIssueButton;
@property(readonly)	IssueInfoPanelButton*		restorePurchasesButton;

@end
