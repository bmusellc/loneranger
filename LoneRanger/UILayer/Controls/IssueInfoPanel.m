//
//  IssueInfoPanel.m
//  Unity-iPhone
//
//  Created by Artem Manzhura on 4/24/13.
//
//

#import "IssueInfoPanel.h"

#import "IssuesManager.h"
#import "LevelsManager.h"

@interface IssueInfoPanel()
{
}
@end

@implementation IssueInfoPanel


- (void)dealloc
{
	[[NSNotificationCenter defaultCenter] removeObserver:self name:LevelsManagerChangedLevelsStateNotification object:nil];
	
    [super dealloc];
}

- (id)init
{
	UIImage* backgroundImage = [UIImage imageNamed:@"menu/issueInfoPanel.png"];
	
    self = [super initWithImage:backgroundImage];
	
    if (self)
	{
		self.userInteractionEnabled = YES;
		
		_issueNameLabel = [MenuLabel menuLabelWithFontType:MenuLabelFontTypeWeswood fontSize:52];
		[self addSubview:_issueNameLabel];
		_issueNameLabel.frame = CGRectMake(0, 20, self.bounds.size.width, 74);
		_issueNameLabel.textColor = [UIColor whiteColor];
		_issueNameLabel.textAlignment = NSTextAlignmentCenter;
		
		_purchaseIssueButton = [[IssueInfoPanelButton alloc] initWithFrame:CGRectMake(12, 124, self.bounds.size.width - 24, 60)];
		[self addSubview:_purchaseIssueButton];
		
		_restorePurchasesButton = [[IssueInfoPanelButton alloc] initWithFrame:CGRectOffset(_purchaseIssueButton.frame, 0, 76)];
		[self addSubview:_restorePurchasesButton];
		_restorePurchasesButton.title = @"RESTORE PURCHASES";
		
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(_updateLayout) name:LevelsManagerChangedLevelsStateNotification object:nil];
    }
	
    return self;
}

@end
