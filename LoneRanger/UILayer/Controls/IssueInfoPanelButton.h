//
//  IssueInfoPanelButton.h
//  Unity-iOS Simulator
//
//  Created by Yuriy Belozerov on 20.11.13.
//
//

#import "MenuLabel.h"

@interface IssueInfoPanelButton : UIControl

@property(readonly)	MenuLabel*				titleLabel;
@property(assign)	NSString*				title;

@end
