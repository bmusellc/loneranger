//
//  IssueInfoPanelButton.m
//  Unity-iOS Simulator
//
//  Created by Yuriy Belozerov on 20.11.13.
//
//

#define NORMAL_COLOR		[UIColor colorWithRed:255 / 255.0 green:252 / 255.0 blue:228 / 255.0 alpha:1.0]
#define HIGHLIGHTED_COLOR	[UIColor colorWithRed:255 / 255.0 green:228 / 255.0 blue:72 / 255.0 alpha:1.0]

#import "IssueInfoPanelButton.h"

@implementation IssueInfoPanelButton

- (NSString*)title
{
	return _titleLabel.text;
}

- (void)setTitle:(NSString *)title
{
	_titleLabel.text = title;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
	if (self)
	{		
		_titleLabel = [MenuLabel menuLabelWithFontType:MenuLabelFontTypeWeswood fontSize:34];
		[self addSubview:_titleLabel];
		_titleLabel.frame = self.bounds;
		_titleLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
		_titleLabel.textColor = NORMAL_COLOR;
		_titleLabel.textAlignment = NSTextAlignmentCenter;
    }
    
	return self;
}

- (id)init
{
	return [self initWithFrame:CGRectZero];
}

- (void)setHighlighted:(BOOL)highlighted
{
	[super setHighlighted:highlighted];
	
	_titleLabel.textColor = highlighted ? HIGHLIGHTED_COLOR : NORMAL_COLOR;
}

- (void)setEnabled:(BOOL)enabled
{
	[super setEnabled:enabled];
	
	_titleLabel.alpha = enabled ? 1.0 : 0.5;
}

@end
