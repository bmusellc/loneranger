//
//  IssueStatsPanelView.h
//  Unity-iOS Simulator
//
//  Created by Yuriy Belozerov on 16.05.13.
//
//

#import "IssuePlayStats.h"
#import <UIKit/UIKit.h>

@interface IssueStatsPanelView : UIView

- (void)updateLayout;

@end
