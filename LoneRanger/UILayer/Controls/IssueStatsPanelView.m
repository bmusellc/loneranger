//
//  IssueStatsPanelView.m
//  Unity-iOS Simulator
//
//  Created by Yuriy Belozerov on 16.05.13.
//
//

#import "IssueStatsPanelView.h"
#import "ProgressView.h"
#import "MenuLabel.h"

#import "StatisticsManager.h"

@interface IssueStatsPanelView ()
{
	MenuLabel*							_shotsLabel;
	
	MenuLabel*							_coinsLabel;
	
	ProgressView*						_progressView;
}

@end

@implementation IssueStatsPanelView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
	if (self)
	{
		UIImage* bgImg = [UIImage imageNamed:@"menu/LevelSelection/issueStatsPanelBg.png"];
		self.frame = CGRectMake(0, 0, bgImg.size.width, bgImg.size.height);
		self.backgroundColor = [UIColor colorWithPatternImage:bgImg];
		
		//bullets
		UIImageView* bulletIconView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"menu/LevelSelection/bulletIcon.png"]] autorelease];
		[self addSubview:bulletIconView];
		bulletIconView.frame = CGRectOffset(bulletIconView.frame, 78, 72);
		
		_shotsLabel = [[MenuLabel menuLabelWithFontType:MenuLabelFontTypeSuplexmentary fontSize:23] retain];
		[self addSubview:_shotsLabel];
		_shotsLabel.frame = CGRectMake(134, 100, 180, 26);
		_shotsLabel.textColor = [UIColor colorWithRed:255 / 255.0 green:248 / 255.0 blue:227 / 255.0 alpha:1.0];
		
		//coins
		UIImageView* coinsIconView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"menu/LevelSelection/coinsIcon.png"]] autorelease];
		[self addSubview:coinsIconView];
		coinsIconView.frame = CGRectOffset(coinsIconView.frame, 356, 88);
		
		_coinsLabel = [[MenuLabel menuLabelWithFontType:MenuLabelFontTypeSuplexmentary fontSize:23] retain];
		[self addSubview:_coinsLabel];
		_coinsLabel.frame = CGRectMake(448, 100, 180, 26);
		_coinsLabel.textColor = [UIColor colorWithRed:255 / 255.0 green:248 / 255.0 blue:227 / 255.0 alpha:1.0];
		
		//progress
		MenuLabel* lbl = [MenuLabel menuLabelWithFontType:MenuLabelFontTypeSuplexmentary fontSize:23];
		[self addSubview:lbl];
		lbl.frame = CGRectMake(0, 162, self.bounds.size.width, 26);
		lbl.textAlignment = NSTextAlignmentCenter;
		lbl.text = @"GAME PLAY COMPLETION";
		lbl.textColor = [UIColor colorWithRed:255 / 255.0 green:248 / 255.0 blue:227 / 255.0 alpha:1.0];
		
		_progressView = [ProgressView new];
		[self addSubview:_progressView];
		_progressView.frame = CGRectMake(46, 198, 536, 30);
		
		[self updateLayout];
    }
    
	return self;
}

- (id)init
{
	return [self initWithFrame:CGRectZero];
}

- (void)dealloc
{
	[_shotsLabel release];
	[_coinsLabel release];
	[_progressView release];
	
    [super dealloc];
}

- (void)updateLayout
{
	IssuePlayStats* playStats = [StatisticsManager sharedManager].playStats;
	
	_shotsLabel.text = [NSString stringWithFormat:@"%d / %d", playStats.performedChallenges, playStats.totalChallenges];
	_coinsLabel.text = [NSString stringWithFormat:@"%d", playStats.coinsEarned];
	_progressView.progress = playStats.progress;
}

@end
