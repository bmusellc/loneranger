//
//  IssuesPanel.h
//  Unity-iPhone
//
//  Created by Artem Manzhura on 4/24/13.
//
//

#import "Issue.h"
#import <UIKit/UIKit.h>

typedef enum
{
	IssuesPanelStateHidden,
	IssuesPanelStateVisible,
	IssuesPanelStateFullsizeCoverExpanded,
}
IssuesPanelState;

@class IssuesPanel;

@protocol IssuesPanelDelegate <NSObject>

- (void)issuesPanel:(IssuesPanel*)sender didSelectIssueWithIndex:(NSInteger)index;

@end

@interface IssuesPanel : UIView

@property(assign)	id<IssuesPanelDelegate>         delegate;
@property(readonly)	IssuesPanelState				panelState;
@property(readonly)	Issue*							selectedIssue;

- (void)showPanel;
- (void)hidePanel;

- (void)fadeOutAndCleanupPanelWithCompletionHandler:(void (^)(BOOL finished))handler;

@end
