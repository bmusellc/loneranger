//
//  IssuesPanel.m
//  Unity-iPhone
//
//  Created by Artem Manzhura on 4/24/13.
//
//

#import "IssuesPanel.h"

#import "IssueCoverView.h"
#import "IssueInfoPanel.h"

#import "PurchasesManager.h"

@interface IssuesPanel () <PurchasesManagerDelegate>
{
	UIView*						_backgroundView;
	
	UIImageView*				_coversPanel;
	NSMutableArray*				_covers;
	UIImageView*				_fullsizeCoverView;
	
	IssueInfoPanel*				_issueInfoPanel;
	
    NSInteger					_selectedCoverIndex;
	
	UIActivityIndicatorView*	_purchasingActivityIndicatorView;
}
@end

@implementation IssuesPanel

- (Issue*)selectedIssue
{
	Issue* issue = nil;
	
	if (_selectedCoverIndex >= 0 && _selectedCoverIndex < [IssuesManager sharedManager].issues.count)
	{
		issue = [IssuesManager sharedManager].issues[_selectedCoverIndex];
	}
	
	return issue;
}

- (void)dealloc
{
    [PurchasesManager sharedManager].delegate = nil;
    
	[_backgroundView release];
	
	[_coversPanel release];
	[_covers release];
	
	[_issueInfoPanel release];

	[_purchasingActivityIndicatorView release];
	
    [super dealloc];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
	if (self)
	{
		_backgroundView = [[UIView alloc] initWithFrame:self.bounds];
        [self addSubview:_backgroundView];
		_backgroundView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
		_backgroundView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.5];
		_backgroundView.alpha = 0.0;
		
		_coversPanel = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"menu/issuesPanelBackground.png"]];
		[self addSubview:_coversPanel];
		_coversPanel.frame = CGRectOffset(_coversPanel.frame, 0, self.bounds.size.height);
		_coversPanel.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
		_coversPanel.userInteractionEnabled = YES;
		
		_issueInfoPanel = [IssueInfoPanel new];
		[_issueInfoPanel.purchaseIssueButton addTarget:self action:@selector(_purchaseIssueButtonDidPressed) forControlEvents:UIControlEventTouchUpInside];
		[_issueInfoPanel.restorePurchasesButton addTarget:self action:@selector(_restorePurchasesButtonDidPressed) forControlEvents:UIControlEventTouchUpInside];
		
        _selectedCoverIndex = -1;
		
        _covers = [NSMutableArray new];
        
        [[IssuesManager sharedManager].issues enumerateObjectsUsingBlock:^(Issue* issue, NSUInteger idx, BOOL *stop) {
			
			IssueCoverView* coverView = [[[IssueCoverView alloc] initWithIssue:issue] autorelease];
			[_coversPanel addSubview:coverView];
			[_covers addObject:coverView];
			coverView.frame = CGRectOffset(coverView.frame, 32 + (coverView.frame.size.width + 24) * idx, 32);
			[coverView addTarget:self action:@selector(_coverViewDidPressed:) forControlEvents:UIControlEventTouchUpInside];
			coverView.tag = idx;
			
/*			coverView.layer.shadowColor = [UIColor blackColor].CGColor;
			coverView.layer.shadowOpacity = 1;
			coverView.layer.shadowOffset = CGSizeZero;
			coverView.layer.shadowRadius = 5;
			coverView.layer.masksToBounds = NO;
			coverView.layer.shouldRasterize = YES;*/
			coverView.layer.borderWidth = 1;
			coverView.layer.borderColor = [UIColor blackColor].CGColor;
		}];
        
        [PurchasesManager sharedManager].delegate = self;
		
		_purchasingActivityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
		[self addSubview:_purchasingActivityIndicatorView];
		_purchasingActivityIndicatorView.frame = self.bounds;
		_purchasingActivityIndicatorView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
		_purchasingActivityIndicatorView.contentMode = UIViewContentModeCenter;
		_purchasingActivityIndicatorView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.5];
		_purchasingActivityIndicatorView.hidesWhenStopped = YES;
		
//		_coversPanel.layer.shouldRasterize = YES;
    }
	
    return self;
}

- (id)init
{
	return [self initWithFrame:CGRectZero];
}

- (void)showPanel
{
	self.userInteractionEnabled = NO;
	
	_panelState = IssuesPanelStateVisible;
	
    [self _showIssueCoversPanel];
}

- (void)hidePanel
{
	self.userInteractionEnabled = NO;
	
	if (_panelState == IssuesPanelStateVisible)
	{
		[self _hideIssueCoversPanel];
	}
	else if (_panelState == IssuesPanelStateFullsizeCoverExpanded)
	{
		[self _hideInfoPanelWithCompletionHandler:^(BOOL finished) {
			
			[self _shrinkFullsizeThumbnailToCoverView:_covers[_selectedCoverIndex] withCompletionHandler:^(BOOL finished) {
				
				[self _hideIssueCoversPanel];
			}];
		}];
	}
	
	_panelState = IssuesPanelStateHidden;
}

- (void)fadeOutAndCleanupPanelWithCompletionHandler:(void (^)(BOOL finished))handler
{
	[UIView animateWithDuration:0.3
					 animations:^{
						 
						 self.alpha = 0.0;
					 }
					 completion:^(BOOL finished) {
						 
						 if (self.panelState == IssuesPanelStateFullsizeCoverExpanded)
						 {
							 [_issueInfoPanel removeFromSuperview];
							 
							 [_fullsizeCoverView removeFromSuperview];
							 [_fullsizeCoverView release];
						 }
						 
						 for (IssueCoverView* coverView in _covers)
						 {
							 [coverView setCoverViewVisible:YES animated:NO];
						 }
						 
						 _backgroundView.alpha = 0.0;
						 _coversPanel.center = CGPointMake(_coversPanel.center.x, self.frame.size.height + _coversPanel.frame.size.height / 2);
						 
						 _panelState = IssuesPanelStateHidden;
						 
						 self.alpha = 1.0;
						 
						 handler(finished);
					 }];
}

#pragma mark - Animations

- (void)_showIssueCoversPanel
{
	[UIView animateWithDuration:0.5
					 animations:^{
						 
						 _backgroundView.alpha = 1.0;
						 _coversPanel.center = CGPointMake(_coversPanel.center.x, self.frame.size.height - _coversPanel.frame.size.height / 2);
					 }
					 completion:^(BOOL finished) {
						 
						 self.userInteractionEnabled = YES;
					 }];
}

- (void)_hideIssueCoversPanel
{
	[UIView animateWithDuration:0.5
					 animations:^{
						 
						 _backgroundView.alpha = 0.0;
						 _coversPanel.center = CGPointMake(_coversPanel.center.x, self.frame.size.height + _coversPanel.frame.size.height / 2);
					 }
					 completion:^(BOOL finished) {
						 
						 self.userInteractionEnabled = YES;
					 }];
}

- (void)_coverViewDidPressed:(IssueCoverView*)sender
{
	_selectedCoverIndex = sender.tag;
	
	self.userInteractionEnabled = NO;
	
	if (_panelState == IssuesPanelStateVisible) //initial state, cover isn't expanded
	{
		if ([[PurchasesManager sharedManager] purchaseStateForIssueName:self.selectedIssue.name] == kIssuePurchaseStatePurchased)
		{
			if ([self.delegate respondsToSelector:@selector(issuesPanel:didSelectIssueWithIndex:)])
			{
				[self.delegate issuesPanel:self didSelectIssueWithIndex:_selectedCoverIndex];
			}
			
			self.userInteractionEnabled = YES;
		}
		else
		{
			[sender setCoverViewVisible:NO animated:NO];
			
			[self _expandFullsizeThumbnailForCoverView:sender withCompletionHandler:^(BOOL finished) {
				
				[self _showInfoPanel];
			}];
			
			[self _updateIssueInfoPanel];
		}
	}
	else
	{
		if ([[PurchasesManager sharedManager] purchaseStateForIssueName:self.selectedIssue.name] == kIssuePurchaseStatePurchased)
		{
			IssueCoverView* selectedCoverView = nil;
			
			for (IssueCoverView* coverView in _covers)
			{
				if (!coverView.userInteractionEnabled)
				{
					selectedCoverView = coverView;
					break;
				}
			}
			
			[self _hideInfoPanelWithCompletionHandler:^(BOOL finished) {
				
				[self _shrinkFullsizeThumbnailToCoverView:selectedCoverView withCompletionHandler:^(BOOL finished) {
					
					if ([self.delegate respondsToSelector:@selector(issuesPanel:didSelectIssueWithIndex:)])
					{
						[self.delegate issuesPanel:self didSelectIssueWithIndex:_selectedCoverIndex];
					}
					
					self.userInteractionEnabled = YES;
				}];
			}];
		}
		else
		{
			for (IssueCoverView* coverView in _covers)
			{
				if (!coverView.userInteractionEnabled)
				{
					[coverView setCoverViewVisible:YES animated:YES];
				}
			}
			
			[sender setCoverViewVisible:NO animated:YES];
			
			[self _switchExpandedThumbnailWithOtherForCoverView:sender];
			
			[self _updateIssueInfoPanel];
		}
	}
}

- (void)_expandFullsizeThumbnailForCoverView:(IssueCoverView*)coverView withCompletionHandler:(void (^)(BOOL finished))handler
{
	_panelState = IssuesPanelStateFullsizeCoverExpanded;
	
	UIImage* fullsizeCover = [UIImage imageWithContentsOfFile:coverView.issue.fullsizeCoverPath];
	_fullsizeCoverView = [[UIImageView alloc] initWithImage:fullsizeCover];
	_fullsizeCoverView.contentMode = UIViewContentModeScaleToFill;
	[self addSubview:_fullsizeCoverView];
	_fullsizeCoverView.frame = [self convertRect:coverView.frame fromView:coverView.superview];
	
	_fullsizeCoverView.layer.borderWidth = 1;
	_fullsizeCoverView.layer.borderColor = [UIColor blackColor].CGColor;
	
	CGRect expandedCoverFrame = CGRectZero;
	expandedCoverFrame.size.width = fullsizeCover.size.width;
	expandedCoverFrame.size.height = fullsizeCover.size.height;
	expandedCoverFrame.origin.x = (self.bounds.size.width - expandedCoverFrame.size.width) / 2;
	expandedCoverFrame.origin.y = 16;
	
	[UIView animateWithDuration:0.3
						  delay:0
						options:UIViewAnimationCurveEaseInOut
					 animations:^{
						 
						 _fullsizeCoverView.frame = expandedCoverFrame;
					 }
					 completion:^(BOOL finished) {
						 
						 handler(finished);
					 }];
}

- (void)_shrinkFullsizeThumbnailToCoverView:(IssueCoverView*)coverView withCompletionHandler:(void (^)(BOOL finished))handler
{
	_panelState = IssuesPanelStateVisible;
	
	CGRect shrinkedCoverFrame = [self convertRect:coverView.frame fromView:coverView.superview];
	
	[UIView animateWithDuration:0.3
						  delay:0
						options:UIViewAnimationCurveEaseInOut
					 animations:^{
						 
						 _fullsizeCoverView.frame = shrinkedCoverFrame;
					 }
					 completion:^(BOOL finished) {
						 
						 [coverView setCoverViewVisible:YES animated:NO];
						 
						 [_fullsizeCoverView removeFromSuperview];
						 [_fullsizeCoverView release];
						 
						 handler(finished);
					 }];
}

- (void)_switchExpandedThumbnailWithOtherForCoverView:(IssueCoverView*)coverView
{
	UIImageView* fullsizeCoverViewRef = _fullsizeCoverView;
	
	UIImage* fullsizeCover = [UIImage imageWithContentsOfFile:coverView.issue.fullsizeCoverPath];
	_fullsizeCoverView = [[UIImageView alloc] initWithImage:fullsizeCover];
	_fullsizeCoverView.contentMode = UIViewContentModeScaleToFill;
	[self addSubview:_fullsizeCoverView];
	_fullsizeCoverView.frame = fullsizeCoverViewRef.frame;
	_fullsizeCoverView.alpha = 0.0;
	
	_fullsizeCoverView.layer.borderWidth = 1;
	_fullsizeCoverView.layer.borderColor = [UIColor blackColor].CGColor;
	
	[UIView animateWithDuration:0.3
					 animations:^{
						 
						 _fullsizeCoverView.alpha = 1.0;
					 }
					 completion:^(BOOL finished) {
						 
						 [fullsizeCoverViewRef removeFromSuperview];
						 [fullsizeCoverViewRef release];
						 
						 self.userInteractionEnabled = YES;
					 }];
}

- (void)_showInfoPanel
{
	[self insertSubview:_issueInfoPanel belowSubview:_fullsizeCoverView];
	
	_issueInfoPanel.frame = CGRectMake(CGRectGetMaxX(_fullsizeCoverView.frame) - _issueInfoPanel.bounds.size.width,
									   CGRectGetMinY(_fullsizeCoverView.frame) + (_fullsizeCoverView.bounds.size.height - _issueInfoPanel.bounds.size.height) / 2,
									   _issueInfoPanel.bounds.size.width,
									   _issueInfoPanel.bounds.size.height);
	
	[UIView animateWithDuration:0.3
					 animations:^{
						 
						 _issueInfoPanel.frame = CGRectOffset(_issueInfoPanel.frame, _issueInfoPanel.bounds.size.width, 0);
					 }
					 completion:^(BOOL finished) {
						 
						 self.userInteractionEnabled = YES;
					 }];
}

- (void)_hideInfoPanelWithCompletionHandler:(void (^)(BOOL finished))handler
{
	[UIView animateWithDuration:0.3
					 animations:^{
						 
						 _issueInfoPanel.frame = CGRectOffset(_issueInfoPanel.frame, -_issueInfoPanel.bounds.size.width, 0);
					 }
					 completion:^(BOOL finished) {
						 
						 [_issueInfoPanel removeFromSuperview];
						 
						 handler(finished);
					 }];
}

- (void)_updateIssueInfoPanel
{
	_issueInfoPanel.issueNameLabel.text = [NSString stringWithFormat:@"ISSUE #%d", _selectedCoverIndex + 1];
	
	NSString* price = [[PurchasesManager sharedManager] priceStringForName:self.selectedIssue.name];
	_issueInfoPanel.purchaseIssueButton.title = [NSString stringWithFormat:@"BUY FOR %@", price];
	
	_issueInfoPanel.purchaseIssueButton.enabled = [PurchasesManager sharedManager].state == kPurchasesManagerStateInitialized;
	_issueInfoPanel.restorePurchasesButton.enabled = [PurchasesManager sharedManager].state == kPurchasesManagerStateInitialized;
}

#pragma mark - Buttons Handling

- (void)_purchaseIssueButtonDidPressed
{	
	if ([[PurchasesManager sharedManager] purchaseIssueNamed:self.selectedIssue.name])
	{
#ifndef DEBUG_TEST_PURCHASES
		[self bringSubviewToFront:_purchasingActivityIndicatorView];
		[_purchasingActivityIndicatorView startAnimating];
#endif
	}
}

- (void)_restorePurchasesButtonDidPressed
{
	if ([[PurchasesManager sharedManager] restorePurchases])
	{
		[self bringSubviewToFront:_purchasingActivityIndicatorView];
		[_purchasingActivityIndicatorView startAnimating];
	}
}

#pragma mark - PurshasesManagerDelegate

- (void)purchasesManager:(PurchasesManager*)manager didSwitchToState:(PurchasesManagerState)newState fromState:(PurchasesManagerState)oldState
{
    switch (newState)
    {
        case kPurchasesManagerStateInitialized:
            [self _processSuccesfullPurchaseOperation:oldState];
            break;
            
        case kPurchasesManagerStateInitializingFailed:
            //onPricesFailed
            break;
            
        case kPurchasesManagerStatePurchasingFailed:
            [_purchasingActivityIndicatorView stopAnimating];
            break;
            
        default:
            break;
    }
}

- (void)_processSuccesfullPurchaseOperation:(PurchasesManagerState)oldState
{
    switch (oldState)
    {
        case kPurchasesManagerStateInitializing:
            [self _updateIssueInfoPanel];
            break;
            
        case kPurchasesManagerStatePurchasingItem:
		case kPurchasesManagerStateRestoringPurchases:
            [_purchasingActivityIndicatorView stopAnimating];
			[self _processPurchasedOrRestoredIssues];
            break;
            
        default:
            break;
    }
}

- (void)_processPurchasedOrRestoredIssues
{
	if ([[PurchasesManager sharedManager] purchaseStateForIssueName:self.selectedIssue.name] == kIssuePurchaseStatePurchased)
	{
		if (_panelState == IssuesPanelStateVisible)
		{
			if ([self.delegate respondsToSelector:@selector(issuesPanel:didSelectIssueWithIndex:)])
			{
				[self.delegate issuesPanel:self didSelectIssueWithIndex:_selectedCoverIndex];
			}
		}
		else
		{
			IssueCoverView* selectedCoverView = nil;
			
			for (IssueCoverView* coverView in _covers)
			{
				if (!coverView.userInteractionEnabled)
				{
					selectedCoverView = coverView;
					break;
				}
			}
			
			[self _hideInfoPanelWithCompletionHandler:^(BOOL finished) {
				
				[self _shrinkFullsizeThumbnailToCoverView:selectedCoverView withCompletionHandler:^(BOOL finished) {
					
					if ([self.delegate respondsToSelector:@selector(issuesPanel:didSelectIssueWithIndex:)])
					{
						[self.delegate issuesPanel:self didSelectIssueWithIndex:_selectedCoverIndex];
					}
				}];
			}];
		}
	}
}

@end
