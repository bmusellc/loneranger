//
//  LevelCellView.h
//  Unity-iOS Simulator
//
//  Created by Yuriy Belozerov on 16.05.13.
//
//

#import "LevelStats.h"
#import "GameLevelInfo.h"
#include <UIKit/UIKit.h>

@interface LevelCellView : UIControl

@property(assign)	BOOL							locked;

@property(readonly)	GameLevelInfo*					levelInfo;
@property(retain)	LevelStats*						levelStats;

- (id)initWithLevelInfo:(id)levelInfo; //level info may be instance of GameLevelInfo or WaypointInfo

- (void)updateLayout;

@end
