//
//  LevelCellView.m
//  Unity-iOS Simulator
//
//  Created by Yuriy Belozerov on 16.05.13.
//
//

#import "LevelCellView.h"
#import <QuartzCore/QuartzCore.h>
#import "GameLevelInfo.h"
#import "MenuLabel.h"

#import "StatisticsManager.h"
#import "IssuesManager.h"

@interface LevelCellView ()
{
	BOOL							_locked;
	
	UIView*							_highlightView;
	
	UIImageView*					_thumbnailImageView;
	
	MenuLabel*						_levelNumberLabel;
	
	LevelStats*						_levelStats;
	
	NSMutableArray*					_challengeCompletionIcons;
}

@end

@implementation LevelCellView

- (BOOL)locked
{
	return _locked;
}

- (void)setLocked:(BOOL)locked
{
	_locked = locked;
	
	_thumbnailImageView.alpha = self.locked ? 0.5 : 1.0;
}

- (void)setTag:(NSInteger)tag
{
	[super setTag:tag];
	
	_levelNumberLabel.text = [NSString stringWithFormat:@"%d", tag + 1];
	
#if DEBUG_OPEN_ALL_GAME_LEVELS
	self.locked = NO;
#endif
}

- (id)initWithLevelInfo:(id)levelInfo
{
    self = [super init];
    
	if (self)
	{
		_levelInfo = [levelInfo retain];
		
		//background & thumbnail
		UIImage* bgImg = [UIImage imageNamed:@"menu/LevelSelection/levelCellBg.png"];
		self.frame = CGRectMake(0, 0, bgImg.size.width, bgImg.size.height);
		self.backgroundColor = [UIColor colorWithPatternImage:bgImg];
		
		_thumbnailImageView = [[UIImageView alloc] initWithFrame:CGRectMake(8, 7, 176, 133)];
		[self addSubview:_thumbnailImageView];
		_thumbnailImageView.layer.borderWidth = 1;
		_thumbnailImageView.layer.borderColor = [UIColor blackColor].CGColor;
		
		NSString* levelThumbnailName = [@"menu/LevelSelection/LevelThumbnails" stringByAppendingPathComponent:[self.levelInfo thumbnailImageName]];
		_thumbnailImageView.image = [UIImage imageNamed:levelThumbnailName];
		
		//badge
		UIImage* badgeImg = [UIImage imageNamed:@"menu/LevelSelection/badge.png"];
		UIImageView* levelNumberBadgeView = [[[UIImageView alloc] initWithImage:badgeImg] autorelease];
		[_thumbnailImageView addSubview:levelNumberBadgeView];
		levelNumberBadgeView.frame = CGRectMake(_thumbnailImageView.bounds.size.width - (badgeImg.size.width + 2), 2, badgeImg.size.width, badgeImg.size.height);
		
		CGRect levelNumberLabelFrame = levelNumberBadgeView.frame;
		levelNumberLabelFrame.origin.y += 1;
		
		_levelNumberLabel = [[MenuLabel alloc] initWithFrame:levelNumberLabelFrame];
		[_thumbnailImageView addSubview:_levelNumberLabel];
		_levelNumberLabel.backgroundColor = [UIColor clearColor];
		_levelNumberLabel.font = [UIFont fontWithName:@"Suplexmentary Comic NC" size:18];
		_levelNumberLabel.textColor = [UIColor colorWithRed:255 / 255.0 green:238 / 255.0 blue:207 / 255.0 alpha:1.0];
		_levelNumberLabel.textAlignment = NSTextAlignmentCenter;
		
		//overlay
		_highlightView = [[UIView alloc] initWithFrame:_thumbnailImageView.frame];
		[self addSubview:_highlightView];
		_highlightView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.5];
		_highlightView.hidden = YES;
		
		_challengeCompletionIcons = [NSMutableArray new];
		
		[self updateLayout];
    }
    
	return self;
}

- (void)dealloc
{
	self.levelStats = nil;
	
	[_levelInfo release];
	
	[_levelNumberLabel release];
	
    [_highlightView release];
	
	[_challengeCompletionIcons release];
	
    [super dealloc];
}

- (void)updateLayout
{
	if ([self.levelInfo isKindOfClass:[GameLevelInfo class]])
	{
		IssuePlayStats* issuePlayStats = [StatisticsManager sharedManager].playStats;
		LevelStats* levelStats = [issuePlayStats levelStatsForLevelWithIndex:((GameLevelInfo*)self.levelInfo).levelIndex];
		self.levelStats = levelStats;
	}
	
#if DEBUG_OPEN_ALL_GAME_LEVELS
	self.locked = NO;
#else
	if ([self.levelInfo isKindOfClass:[GameLevelInfo class]])
	{
		self.locked = [self.levelInfo state] < GameLevelInfoStateOpened;
	}
#endif
	
	//challenges
	[_challengeCompletionIcons makeObjectsPerformSelector:@selector(removeFromSuperview)];
	[_challengeCompletionIcons removeAllObjects];
	
	__block NSInteger challengeIconsCount = 0;
	
	[self.levelStats.challenges enumerateObjectsUsingBlock:^(NSNumber* challengeState, NSUInteger idx, BOOL *stop) {
		
		if ([challengeState boolValue])
		{
			UIImageView* imageView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"menu/LevelSelection/challengeProgressBulletComplete.png"]] autorelease];
			[_thumbnailImageView addSubview:imageView];
			[_challengeCompletionIcons addObject:imageView];
			imageView.frame = CGRectMake(2 + (imageView.bounds.size.width - 4) * challengeIconsCount,
										 _thumbnailImageView.bounds.size.height - imageView.bounds.size.height - 4,
										 imageView.bounds.size.width,
										 imageView.bounds.size.height);
			
			challengeIconsCount++;
		}
	}];
}

- (void)setHighlighted:(BOOL)highlighted
{
	[super setHighlighted:highlighted];
	
	_highlightView.hidden = !highlighted;
}

@end
