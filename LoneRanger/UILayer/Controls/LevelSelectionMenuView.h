//
//  LevelSelectionMenuView.h
//  Unity-iOS Simulator
//
//  Created by Yuriy Belozerov on 16.05.13.
//
//

#import <UIKit/UIKit.h>
#import "Issue.h"
#import "LevelSelectionPanelView.h"

@class LevelSelectionMenuView;

@protocol LevelSelectionViewDelegate <NSObject>

- (void)levelSelectionView:(LevelSelectionMenuView*)view levelSelectionCellDidPressed:(LevelCellView*)cellView;

@end

@interface LevelSelectionMenuView : UIView

@property(assign)	id <LevelSelectionViewDelegate>	delegate;

- (void)animateAppearance;
- (void)updateLayout;

@end
