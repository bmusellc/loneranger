//
//  LevelSelectionMenuView.m
//  Unity-iOS Simulator
//
//  Created by Yuriy Belozerov on 16.05.13.
//
//

#import "LevelSelectionMenuView.h"
#import "WeaponSelectionPanelView.h"
#import "IssueStatsPanelView.h"

#import "StatisticsManager.h"
#import "IssuesManager.h"
#import "LevelsManager.h"
#import "WeaponManager.h"

#import "AlertView.h"

@interface LevelSelectionMenuView () <LevelSelectionPanelDelegate>
{
	UIView*								_backgroundView;
	
	WeaponSelectionPanelView*			_weaponSelectionPanel;
	IssueStatsPanelView*				_issueStatsPanel;
	LevelSelectionPanelView*			_levelSelectionPanel;
}

@end

@implementation LevelSelectionMenuView

- (id)init
{
	self = [super init];
	
	if (self)
	{
		_backgroundView = [[UIView alloc] initWithFrame:self.bounds];
		[self addSubview:_backgroundView];
		_backgroundView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
		_backgroundView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.9];
		_backgroundView.alpha = 0.0;
		
		UIImage* btnImg = [UIImage imageNamed:@"menu/LevelSelection/backButton.png"];
		UIButton* backButton = [UIButton buttonWithType:UIButtonTypeCustom];
		[_backgroundView addSubview:backButton];
		backButton.frame = CGRectMake(0, 0, btnImg.size.width, btnImg.size.height);
		[backButton setImage:btnImg forState:UIControlStateNormal];
		[backButton addTarget:self action:@selector(_backButtonDidPressed) forControlEvents:UIControlEventTouchUpInside];
		
		_weaponSelectionPanel = [WeaponSelectionPanelView new];
		[self addSubview:_weaponSelectionPanel];
		
		_issueStatsPanel = [IssueStatsPanelView new];
		[self addSubview:_issueStatsPanel];
		
		_levelSelectionPanel = [[LevelSelectionPanelView alloc] initWithLevels:[LevelsManager sharedManager].gameLevels];
		[self addSubview:_levelSelectionPanel];
		_levelSelectionPanel.delegate = self;
		
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateLayout) name:LevelsManagerChangedLevelsStateNotification object:nil];
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateLayout) name:StatisticsManagerChangedLevelStatisticsNotification object:nil];
	}
	
	return self;
}

- (void)dealloc
{
	[[NSNotificationCenter defaultCenter] removeObserver:self name:LevelsManagerChangedLevelsStateNotification object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:StatisticsManagerChangedLevelStatisticsNotification object:nil];
	
	[_backgroundView release];
    
	[_weaponSelectionPanel release];
	[_issueStatsPanel release];
	[_levelSelectionPanel release];
	
    [super dealloc];
}

- (void)animateAppearance
{
	_weaponSelectionPanel.frame = CGRectMake(self.bounds.size.width - _weaponSelectionPanel.bounds.size.width,
											 self.bounds.size.height,
											 _weaponSelectionPanel.bounds.size.width,
											 _weaponSelectionPanel.bounds.size.height);
	
	_issueStatsPanel.frame = CGRectMake(0,
										self.bounds.size.height,
										_issueStatsPanel.bounds.size.width,
										_issueStatsPanel.bounds.size.height);
	
	_levelSelectionPanel.frame = CGRectMake(-_levelSelectionPanel.bounds.size.width,
										68,
										_levelSelectionPanel.bounds.size.width,
										_levelSelectionPanel.bounds.size.height);
	
	[UIView animateWithDuration:0.7
					 animations:^{
						 
						 _backgroundView.alpha = 1.0;
						 
						 _weaponSelectionPanel.frame = CGRectOffset(_weaponSelectionPanel.frame, 0, -_weaponSelectionPanel.bounds.size.height);
						 _issueStatsPanel.frame = CGRectOffset(_issueStatsPanel.frame, 0, -_issueStatsPanel.bounds.size.height);
						 _levelSelectionPanel.frame = CGRectOffset(_levelSelectionPanel.frame, _levelSelectionPanel.bounds.size.width, 0);
					 }];
}

- (void)updateLayout
{
	[_levelSelectionPanel updateLayout];
	[_issueStatsPanel updateLayout];
}

- (void)_backButtonDidPressed
{
	[UIView animateWithDuration:0.7
					 animations:^{
						 
						 _backgroundView.alpha = 0.0;
						 
						 _weaponSelectionPanel.frame = CGRectOffset(_weaponSelectionPanel.frame, 0, _weaponSelectionPanel.bounds.size.height);
						 _issueStatsPanel.frame = CGRectOffset(_issueStatsPanel.frame, 0, _issueStatsPanel.bounds.size.height);
						 _levelSelectionPanel.frame = CGRectOffset(_levelSelectionPanel.frame, -_levelSelectionPanel.bounds.size.width, 0);
					 }
					 completion:^(BOOL finished) {
						 
						 [self removeFromSuperview];
					 }];
}

- (void)levelSelectionPanel:(LevelSelectionPanelView *)panel didSelectItem:(LevelCellView *)item
{
	[LevelsManager sharedManager].currentGameLevelInfo = item.levelInfo;
	
	if (item.locked)
	{
		AlertView* alert = [[[AlertView alloc] initWithTitle:@"PLEASE PROGRESS IN THE PLAY MODE\nTO UNLOCK THIS LEVEL" fontSize:23] autorelease];
		[alert showInView:self];
	}
	else if ([self.delegate respondsToSelector:@selector(levelSelectionView:levelSelectionCellDidPressed:)])
	{
#if TARGET_IPHONE_SIMULATOR
		if ([item.levelInfo isKindOfClass:[GameLevelInfo class]])
		{
			LevelStats* testStats = [LevelStats levelStatsWithLevel:((GameLevelInfo*)item.levelInfo).levelIndex
														 totalShots:20
															   hits:20
														coinsEarned:1000
														 challenges:@[@(YES), @(NO), @(YES)]];
			
			[[StatisticsManager sharedManager] updateLevelStatistics:testStats];
			[[LevelsManager sharedManager] passLevel:item.levelInfo];
		}
#else
		[self.delegate levelSelectionView:self levelSelectionCellDidPressed:item];
#endif
	}
}

@end
