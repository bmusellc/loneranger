//
//  LevelSelectionPanelView.h
//  Unity-iOS Simulator
//
//  Created by Yuriy Belozerov on 16.05.13.
//
//

#import "LevelCellView.h"
#import <UIKit/UIKit.h>

@class LevelSelectionPanelView;

@protocol LevelSelectionPanelDelegate <NSObject>

- (void)levelSelectionPanel:(LevelSelectionPanelView*)panel didSelectItem:(LevelCellView*)item;

@end

@interface LevelSelectionPanelView : UIView

@property(assign)	id <LevelSelectionPanelDelegate>	delegate;

- (id)initWithLevels:(NSArray*)levels;

- (void)updateLayout;

@end
