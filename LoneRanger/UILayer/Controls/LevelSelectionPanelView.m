//
//  LevelSelectionPanelView.m
//  Unity-iOS Simulator
//
//  Created by Yuriy Belozerov on 16.05.13.
//
//

#import "LevelSelectionPanelView.h"
#import "LevelCellView.h"
#import "PageControl.h"

#import "LevelsManager.h"

@interface LevelSelectionPanelView () <UIScrollViewDelegate>
{
	NSArray*												_levels;
	
	UIScrollView*											_levelsScrollView;
	NSMutableArray*											_levelViews;
	
	PageControl*											_pageControl;
}

@end

@implementation LevelSelectionPanelView

- (id)initWithLevels:(NSArray*)levels
{
    self = [super init];
    
	if (self)
	{
		_levels = [levels copy];
				
		UIImage* bgImg = [UIImage imageNamed:@"menu/LevelSelection/levelSelectionPanelBg.png"];
		self.frame = CGRectMake(0, 0, bgImg.size.width, bgImg.size.height);
		self.backgroundColor = [UIColor colorWithPatternImage:bgImg];

		_levelsScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(52, 70, 920, 300)];
		[self addSubview:_levelsScrollView];
		_levelsScrollView.pagingEnabled = YES;
		_levelsScrollView.bounces = NO;
		_levelsScrollView.delegate = self;
		_levelsScrollView.showsHorizontalScrollIndicator = _levelsScrollView.showsVerticalScrollIndicator = NO;
        
        CAGradientLayer* maskLayer = [CAGradientLayer layer];
        CGColorRef outerColor = [UIColor colorWithWhite:1.0 alpha:0.0].CGColor;
        CGColorRef innerColor = [UIColor colorWithWhite:1.0 alpha:1.0].CGColor;
        
        maskLayer.colors       = [NSArray arrayWithObjects:(id)outerColor, (id)innerColor, (id)innerColor, (id)outerColor, nil];
        maskLayer.locations    = [NSArray arrayWithObjects:[NSNumber numberWithFloat:0.0],
                                   [NSNumber numberWithFloat:0.05],
                                   [NSNumber numberWithFloat:0.95],
                                   [NSNumber numberWithFloat:1.0], nil];
        
        maskLayer.bounds       = CGRectMake(0, 0,
                                             _levelsScrollView.frame.size.width,
                                             _levelsScrollView.frame.size.height);
        maskLayer.startPoint   = CGPointMake(1, 0);
        maskLayer.endPoint     = CGPointMake(0, 0);
        maskLayer.anchorPoint  = CGPointZero;
        _levelsScrollView.layer.mask = maskLayer;
        
		_levelViews = [NSMutableArray new];
		
		[_levels enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
			
			LevelCellView* cellView = [[[LevelCellView alloc] initWithLevelInfo:obj] autorelease];
			[_levelsScrollView addSubview:cellView];
			[_levelViews addObject:cellView];
			cellView.tag = idx;
			
			NSInteger localIdx = idx % 8;
			
			NSInteger row = localIdx % 4;
			NSInteger col = localIdx / 4;
			
			CGFloat globalOffset = idx / 8 * _levelsScrollView.bounds.size.width + 19;
			
			cellView.frame = CGRectOffset(cellView.frame,
										  globalOffset + (cellView.bounds.size.width + 38) * row,
										  (cellView.bounds.size.height + 2) * col);
			
			[cellView addTarget:self action:@selector(_levelCellViewDidPressed:) forControlEvents:UIControlEventTouchUpInside];
		}];
		
		NSInteger pagesCount = ceilf(_levels.count / 6.0);
		
		_levelsScrollView.contentSize = CGSizeMake(pagesCount * _levelsScrollView.bounds.size.width, _levelsScrollView.bounds.size.height);
		
		_pageControl = [PageControl new];
		[self addSubview:_pageControl];
		_pageControl.numberOfPages = pagesCount;
		_pageControl.frame = CGRectMake((self.bounds.size.width - 220) / 2, 364, 220, 38);
		[_pageControl addTarget:self action:@selector(_pageControlCurrentPageChanged:) forControlEvents:UIControlEventValueChanged];
		
        [self updateLayout];
    }
    
	return self;
}

- (void)dealloc
{

	[_levels            release];
	[_levelViews        release];
	[_levelsScrollView  release];
	[_pageControl       release];
            
    [super dealloc];
}

- (void)updateLayout
{
	[_levelViews makeObjectsPerformSelector:@selector(updateLayout)];
}

- (void)_levelCellViewDidPressed:(LevelCellView*)sender
{
	if ([self.delegate respondsToSelector:@selector(levelSelectionPanel:didSelectItem:)])
	{
		[self.delegate levelSelectionPanel:self didSelectItem:sender];
	}	
}

- (void)_pageControlCurrentPageChanged:(PageControl*)sender
{
	[_levelsScrollView setContentOffset:CGPointMake(sender.currentPage * _levelsScrollView.bounds.size.width, 0) animated:YES];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [CATransaction begin];
    [CATransaction setDisableActions:YES];
        _levelsScrollView.layer.mask.position = CGPointMake(scrollView.contentOffset.x, 0);
    [CATransaction commit];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
	_pageControl.currentPage = scrollView.contentOffset.x / scrollView.bounds.size.width;
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
	if (!decelerate)
	{
		_pageControl.currentPage = scrollView.contentOffset.x / scrollView.bounds.size.width;
	}
}

@end
