//
//  MenuLabel.h
//  Unity-iOS Simulator
//
//  Created by Yuriy Belozerov on 30.05.13.
//
//

#import <UIKit/UIKit.h>

typedef enum
{
	MenuLabelFontTypeSuplexmentary,
	MenuLabelFontTypeWeswood,
}
MenuLabelFontType;

@interface MenuLabel : UILabel

+ (MenuLabel*)menuLabelWithFontType:(MenuLabelFontType)fontType fontSize:(CGFloat)fontSize;

@end
