//
//  MenuLabel.m
//  Unity-iOS Simulator
//
//  Created by Yuriy Belozerov on 30.05.13.
//
//

#import "MenuLabel.h"

@implementation MenuLabel

+ (MenuLabel*)menuLabelWithFontType:(MenuLabelFontType)fontType fontSize:(CGFloat)fontSize
{
	MenuLabel* lbl = [[MenuLabel new] autorelease];
	lbl.backgroundColor = [UIColor clearColor];
	lbl.font = fontType == MenuLabelFontTypeSuplexmentary ? [UIFont fontWithName:@"Suplexmentary Comic NC" size:fontSize] : [UIFont fontWithName:@"CMWesternWoodblockNo2" size:fontSize];
	
	return lbl;
}

- (void)drawTextInRect:(CGRect)rect
{
	NSMutableAttributedString* attributedString = nil;
	
	if (self.attributedText)
	{
		attributedString = [self.attributedText mutableCopy];
	}
	else if (self.text)
	{
		attributedString = [[[NSMutableAttributedString alloc] initWithString:self.text] autorelease];
	}
	
	if (attributedString)
	{
		NSRange range = NSMakeRange(0, attributedString.length);
		
		NSShadow* shadow = [[NSShadow new] autorelease];
		[shadow setShadowBlurRadius:2];
		[shadow setShadowColor:[UIColor blackColor]];
		[shadow setShadowOffset:CGSizeMake(0, 2)];
		[attributedString addAttribute:NSShadowAttributeName value:shadow range:range];
		
		[self setAttributedText:attributedString];
		[super drawTextInRect:rect];
		
		[shadow setShadowBlurRadius:1];
		[shadow setShadowColor:[UIColor blackColor]];
		[shadow setShadowOffset:CGSizeMake(1, 1)];
		[attributedString addAttribute:NSShadowAttributeName value:shadow range:range];
		
		[self setAttributedText:attributedString];
		[super drawTextInRect:rect];
		
		[shadow setShadowBlurRadius:1];
		[shadow setShadowColor:[UIColor blackColor]];
		[shadow setShadowOffset:CGSizeMake(-1, 1)];
		[attributedString addAttribute:NSShadowAttributeName value:shadow range:range];
		
		[self setAttributedText:attributedString];
		[super drawTextInRect:rect];
	}
}

@end
