//
//  OptionsMenuView.h
//  Unity-iOS Simulator
//
//  Created by Yuriy Belozerov on 04.06.13.
//
//

#import <UIKit/UIKit.h>

@interface OptionsMenuView : UIView

- (void)showAnimated:(BOOL)animated withCompletionHandler:(void(^)())completionHandler;

@end
