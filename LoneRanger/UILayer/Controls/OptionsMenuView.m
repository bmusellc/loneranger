//
//  OptionsMenuView.m
//  Unity-iOS Simulator
//
//  Created by Yuriy Belozerov on 04.06.13.
//
//

#import "OptionsMenuView.h"

#import "ReaderSettingsView.h"
#import "PlaySettingsView.h"

@interface OptionsMenuView ()
{
	void(^_completionHandler)();
}

@end

@implementation OptionsMenuView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
	if (self)
	{
		UIView* bgView = [[[UIView alloc] initWithFrame:self.bounds] autorelease];
		[self addSubview:bgView];
		bgView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
		bgView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.75];
		
		UITapGestureRecognizer* tap = [[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(_handleTapGestureRecognizer:)] autorelease];
		[bgView addGestureRecognizer:tap];
		
		UIImageView* menuView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"menu/Settings/optionsBg.png"]] autorelease];
		[bgView addSubview:menuView];
		menuView.center = CGPointMake(CGRectGetMidX(bgView.bounds), CGRectGetMidY(bgView.bounds));
		menuView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
		menuView.userInteractionEnabled = YES;
		
		UIImage* btnImg = [UIImage imageNamed:@"menu/LevelSelection/backButton.png"];
		UIButton* backButton = [UIButton buttonWithType:UIButtonTypeCustom];
		[self addSubview:backButton];
		backButton.frame = CGRectMake(10, 10, btnImg.size.width, btnImg.size.height);
		[backButton setImage:btnImg forState:UIControlStateNormal];
		[backButton addTarget:self action:@selector(_backButtonDidPressed) forControlEvents:UIControlEventTouchUpInside];
		
		btnImg = [UIImage imageNamed:@"menu/Settings/optionsReader.png"];
		UIImage* btnHlImg = [UIImage imageNamed:@"menu/Settings/optionsReaderHl.png"];
		UIButton* readerButton = [UIButton buttonWithType:UIButtonTypeCustom];
		[self addSubview:readerButton];
		readerButton.frame = CGRectMake((self.bounds.size.width - btnImg.size.width) / 2, 318, btnImg.size.width, btnImg.size.height);
		readerButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
		[readerButton setImage:btnImg forState:UIControlStateNormal];
		[readerButton setImage:btnHlImg forState:UIControlStateHighlighted];
		[readerButton addTarget:self action:@selector(_readerButtonDidPressed) forControlEvents:UIControlEventTouchUpInside];
		
		btnImg = [UIImage imageNamed:@"menu/Settings/optionsPlay.png"];
		btnHlImg = [UIImage imageNamed:@"menu/Settings/optionsPlayHl.png"];
		UIButton* playButton = [UIButton buttonWithType:UIButtonTypeCustom];
		[self addSubview:playButton];
		playButton.frame = CGRectMake(0, 412, btnImg.size.width, btnImg.size.height);
		playButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
		[playButton setImage:btnImg forState:UIControlStateNormal];
		[playButton setImage:btnHlImg forState:UIControlStateHighlighted];
		[playButton addTarget:self action:@selector(_playButtonDidPressed) forControlEvents:UIControlEventTouchUpInside];
		
		self.alpha = 0.0;
    }
    
	return self;
}

- (id)init
{
	return [self initWithFrame:CGRectZero];
}

- (void)dealloc
{
    
    [super dealloc];
}

- (void)_backButtonDidPressed
{
	[self _hideAnimated:YES];
}

- (void)_handleTapGestureRecognizer:(UITapGestureRecognizer*)sender
{
	CGPoint location = [sender locationInView:sender.view];
	UIView* subview = [sender.view hitTest:location withEvent:nil];
	
	if (sender.view == subview)
	{
		[self _hideAnimated:YES];
	}
}

- (void)_readerButtonDidPressed
{
/*	ReaderSettingsView* settingsView = [[[ReaderSettingsView alloc] initWithFrame:self.bounds] autorelease];
	[self addSubview:settingsView];
	settingsView.alpha = 0.0;
	settingsView.transparentBackground = YES;
	
	[UIView animateWithDuration:0.5
					 animations:^{
						 
						 settingsView.alpha = 1.0;
					 }
					 completion:^(BOOL finished) {
						 
					 }];*/
}

- (void)_playButtonDidPressed
{
	PlaySettingsView* settingsView = [[[PlaySettingsView alloc] initWithFrame:self.bounds] autorelease];
	[self addSubview:settingsView];
	settingsView.alpha = 0.0;
	
	[UIView animateWithDuration:0.5
					 animations:^{
						 
						 settingsView.alpha = 1.0;
					 }
					 completion:^(BOOL finished) {
						 
					 }];
}

- (void)showAnimated:(BOOL)animated withCompletionHandler:(void(^)())completionHandler
{
	_completionHandler = [completionHandler copy];
	
	if (animated)
	{
		[UIView animateWithDuration:0.5
						 animations:^{
							 
							 self.alpha = 1.0;
						 }];
	}
	else
	{
		self.alpha = 1.0;
	}
}

- (void)_hideAnimated:(BOOL)animated
{
	if (animated)
	{
		[UIView animateWithDuration:0.5
						 animations:^{
							 
							 self.alpha = 0.0;
						 }
						 completion:^(BOOL finished) {
							 
							 _completionHandler();
							 [_completionHandler release];
							 
							 [self removeFromSuperview];
						 }];
	}
	else
	{
		_completionHandler();
		[_completionHandler release];
		
		[self removeFromSuperview];
	}
}

@end
