//
//  PageControl.h
//  Unity-iOS Simulator
//
//  Created by Yuriy Belozerov on 17.06.13.
//
//

#import <UIKit/UIKit.h>

@interface PageControl : UIControl

@property(assign)	NSInteger					numberOfPages;
@property(assign)	NSInteger					currentPage;

@end
