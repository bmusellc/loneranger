//
//  PageControl.m
//  Unity-iOS Simulator
//
//  Created by Yuriy Belozerov on 17.06.13.
//
//

#import "PageControl.h"

#define DISTANCE_BETWEEN_DOTS					50

@interface PageControl ()
{
	NSMutableArray*								_dots;
	
	NSInteger									_numberOfPages;
	NSInteger									_currentPage;
}

@end

@implementation PageControl

- (NSInteger)numberOfPages
{
	return _numberOfPages;
}

- (void)setNumberOfPages:(NSInteger)numberOfPages
{
	_numberOfPages = numberOfPages;
	
	[_dots makeObjectsPerformSelector:@selector(removeFromSuperview)];
	[_dots removeAllObjects];
	
	if (self.numberOfPages > 1)
	{
		for (int i = 0; i < self.numberOfPages; i++)
		{
			UIButton* btn = [UIButton buttonWithType:UIButtonTypeCustom];
			[self addSubview:btn];
			[_dots addObject:btn];
			[btn setImage:[UIImage imageNamed:@"menu/LevelSelection/pageControlDot.png"] forState:UIControlStateNormal];
			[btn setImage:[UIImage imageNamed:@"menu/LevelSelection/pageControlActiveDot.png"] forState:UIControlStateSelected];
			btn.tag = i;
			[btn addTarget:self action:@selector(_dotButtonDidPressed:) forControlEvents:UIControlEventTouchUpInside];
		}
	}
	
	self.currentPage = 0;
}

- (NSInteger)currentPage
{
	return _currentPage;
}

- (void)setCurrentPage:(NSInteger)currentPage
{
	if (_currentPage != currentPage)
	{
		_currentPage = currentPage;
		
		[_dots enumerateObjectsUsingBlock:^(UIButton* btn, NSUInteger idx, BOOL *stop) {
			
			btn.selected = idx == self.currentPage;
		}];
		
		[self sendActionsForControlEvents:UIControlEventValueChanged];
	}
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
	if (self)
	{
		_dots = [NSMutableArray new];
		_currentPage = -1;
    }
    
	return self;
}

- (id)init
{
	return [self initWithFrame:CGRectZero];
}

- (void)layoutSubviews
{
	[super layoutSubviews];
	
	CGFloat btnWidth = DISTANCE_BETWEEN_DOTS;
	CGFloat horizontalOffset = roundf((self.bounds.size.width - btnWidth * _dots.count) / 2);
	CGFloat btnHeight = self.bounds.size.height;
	
	[_dots enumerateObjectsUsingBlock:^(UIButton* btn, NSUInteger idx, BOOL *stop) {
		
		btn.frame = CGRectMake(horizontalOffset + btnWidth * idx, 0, btnWidth, btnHeight);
	}];
}

- (void)_dotButtonDidPressed:(UIButton*)sender
{
	self.currentPage = sender.tag;
}

@end
