//
//  PageView.h
//  RussellJamesPhotobook
//
//  Created by Yuriy Bilozerov on 1/17/13.
//  Copyright (c) 2013 bMuse Ukraine. All rights reserved.
//

//#import "ChapterInfo.h"
#import "PageInfo.h"
//#import "TextView.h"
#import <UIKit/UIKit.h>

typedef enum
{
	PageViewModeThumbnail,
	PageViewModeContent,
}
PageViewMode;

@class PageView;

@protocol PageViewDelegate <NSObject>
- (void)pageView:(PageView*)sender didSelectPageWithLocalIndex:(NSUInteger)localIndex;
- (void)pageViewDidRecognizeSwipeUp:(PageView*)sender;
- (void)pageViewDidRecognizeSwipeDown:(PageView*)sender;
- (void)pageView:(PageView*)sender didSelectPageWithAbsoluteIndex:(int)index;
- (void)pageViewDidRecognizeDoubleTap:(PageView*)sender withRecognizer:(UIGestureRecognizer*)recognizer;
- (void)pageViewDidRecognizeTap:(PageView*)sender withRecognizer:(UIGestureRecognizer*)recognizer;
- (void)pageViewDidBeginZooming:(PageView*)sender;
- (void)pageViewDidBecomePrimaryZoom:(PageView*)sender;
//- (void)pageViewDidRecognizeSwipeRight:(PageView*)sender withRecognizer:(UIGestureRecognizer*)recognizer;
//- (void)pageViewDidRecognizeSwipeLeft:(PageView*)sender withRecognizer:(UIGestureRecognizer*)recognizer;
//- (void)mainMenuPageViewDidSelectChapter:(ChapterInfo*)chapterInfo;
//- (void)mainMenuPageWouldLikeToGoFullscreen:(PageView*)sender;
//- (void)mainMenuPageWouldLikeToLeaveFullscreen:(PageView*)sender;
@end

@interface PageView : UIView
{
	UIView*										_shadowView;
}

@property(weak)     id<PageViewDelegate>        delegate;

@property(readonly)	PageInfo*					pageInfo;
@property(assign)	PageViewMode				pageViewMode;

@property(readonly)	UIImageView*				thumbnailImageView;
@property(readonly) BOOL						needsToLoadContent;

@property(readonly)	UIScrollView*				contentScrollView;
@property(readonly)	UIView*						contentView;

@property(readonly)	CGRect						imageFrame;

@property(assign)	BOOL						shouldIgnoreScrollViewDelegateCalls;

+ (id)pageViewWithPageInfo:(PageInfo*)pageInfo;
- (id)initWithPageInfo:(PageInfo*)pageInfo;

- (void)hideBubblesFromFrameIndex:(NSInteger)frameIdx bubbleIndex:(NSInteger)bubbleIdx;
- (void)showBubblesUptoFrameIndex:(NSInteger)frameIdx bubbleIndex:(NSInteger)bubbleIdx;
- (void)showAllBubbles;
- (void)hideAllBubbles;
- (void)showBubbleAtFrameIndex:(NSInteger)frameIndex atBubbleIndex:(NSInteger)bubbleIndex hidePrevBubble:(BOOL)hide;
- (void)hideCurrentBubble;
- (void)recalcBubbleFrames;
- (void)loadBubbles;
- (void)loadThumbnail;
- (void)loadContent;
- (void)unloadContent;
- (BOOL)hasThumbnail;

@end
