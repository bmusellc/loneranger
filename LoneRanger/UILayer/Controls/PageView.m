//
//  PageView.m
//  RussellJamesPhotobook
//
//  Created by Yuriy Bilozerov on 1/17/13.
//  Copyright (c) 2013 bMuse Ukraine. All rights reserved.
//

#import "PageView.h"

#import "PhotoPageView.h"
#import "EnhancedPanelPageView.h"
#import "FrameInfo.h"
#import "BubbleInfo.h"
//#import "VideoPageView.h"
//#import "ChapterCoverView.h"
//#import "IntroPageView.h"
//#import "MainMenuPageView.h"

@interface PageView () <UIScrollViewDelegate>
{
    NSMutableArray*                             _bubbles;
	UIImage*                                    _thumbnailImage;
    UISwipeGestureRecognizer*                   _swipeUpGestureRecognizer;
    UISwipeGestureRecognizer*                   _swipeDownGestureRecognizer;
//    UISwipeGestureRecognizer*                   _swipeRightGestureRecognizer;
//    UISwipeGestureRecognizer*                   _swipeLeftGestureRecognizer;
    UITapGestureRecognizer*                     _doubleTapGestureRecognizer;
    UITapGestureRecognizer*                     _tapGestureRecognizer;
    NSInteger                                   _currentBubbleIdx;
    BOOL                                        _allBubblesShowing;
    id<PageViewDelegate>                        _delegate;
}

@end

@implementation PageView

- (CGRect)imageFrame
{
	CGFloat imageHeight = self.bounds.size.height;
	CGFloat imageWidth = imageHeight * (self.thumbnailImageView.image.size.width / self.thumbnailImageView.image.size.height);
	CGRect imageFrame = CGRectMake((self.bounds.size.width - imageWidth) / 2, 0, imageWidth, imageHeight);
	
	return imageFrame;
}

- (CGFloat)shadowOpacity
{
	return _shadowView.alpha;
}

- (void)setShadowOpacity:(CGFloat)shadowOpacity
{
	_shadowView.alpha = shadowOpacity;
}

+ (id)pageViewWithPageInfo:(PageInfo*)pageInfo
{
	PageView* pageView = nil;
	
	switch (pageInfo.pageType)
	{
		case kPageInfoTypeComic:
		case kPageInfoTypeWaypoint:
			pageView = [[PhotoPageView alloc] initWithPageInfo:pageInfo];
			break;
			
		case kPageInfoTypeEnhancedPanel:
			pageView = [[EnhancedPanelPageView alloc] initWithPageInfo:pageInfo];
			break;
			
		default:
			break;
	}
	
	return pageView;
}

- (id)initWithPageInfo:(PageInfo*)pageInfo
{
	self = [super initWithFrame:pageInfo.contentFrame];
	
	if (self)
	{
		_pageInfo = pageInfo;
        _currentBubbleIdx = 0;
        _allBubblesShowing = NO;
		_contentScrollView = [[UIScrollView alloc] initWithFrame:self.bounds];
		[self addSubview:_contentScrollView];
		_contentScrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
		_contentScrollView.delegate = self;
		_contentScrollView.maximumZoomScale = 10;
		_contentScrollView.scrollEnabled = NO;
        
		_contentView = [UIView new];
		[_contentScrollView addSubview:_contentView];
		_contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
		
		_thumbnailImageView = [[UIImageView alloc] initWithFrame:_contentView.bounds];
		[_contentView addSubview:_thumbnailImageView];
		_thumbnailImageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
		_thumbnailImageView.contentMode = UIViewContentModeScaleToFill;
        
        _bubbles = [[NSMutableArray alloc] init];        
//        _swipeUpGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(_handleSwipeUpRecognizer)];
//        _swipeUpGestureRecognizer.direction = UISwipeGestureRecognizerDirectionUp;
//        [self addGestureRecognizer:_swipeUpGestureRecognizer];
//        
//        _swipeDownGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(_handleSwipeDownRecognizer)];
//        _swipeDownGestureRecognizer.direction = UISwipeGestureRecognizerDirectionDown;
//        [self addGestureRecognizer:_swipeDownGestureRecognizer];
		
        
//        _swipeRightGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(_handleSwipeRight:)];
//        _swipeRightGestureRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
//        [self addGestureRecognizer:_swipeRightGestureRecognizer];
//        
//        _swipeLeftGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(_handleSwipeLeft:)];
//        _swipeLeftGestureRecognizer.direction = UISwipeGestureRecognizerDirectionLeft;
//        [self addGestureRecognizer:_swipeLeftGestureRecognizer];
        
        _doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action: @selector(_handleDoubleTap:)];
        _doubleTapGestureRecognizer.numberOfTapsRequired = 2;
        _doubleTapGestureRecognizer.numberOfTouchesRequired = 1;
        [self.contentView addGestureRecognizer: _doubleTapGestureRecognizer];
        
        _tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action: @selector(_handleTap:)];
        _tapGestureRecognizer.numberOfTapsRequired = 1;
        _tapGestureRecognizer.numberOfTouchesRequired = 1;
        [self addGestureRecognizer: _tapGestureRecognizer];
        _tapGestureRecognizer.delaysTouchesBegan = YES;
        [_tapGestureRecognizer requireGestureRecognizerToFail:_doubleTapGestureRecognizer];
		
		[self loadThumbnail];
	}
	
	return self;
}

- (void)layoutSubviews
{
	[super layoutSubviews];
}

//- (void)_handleSwipeRight:(UIGestureRecognizer*)recognizer
//{
//    if([_delegate respondsToSelector:@selector(pageViewDidRecognizeSwipeRight:withRecognizer:)])
//    {
//        [_delegate pageViewDidRecognizeSwipeRight:self withRecognizer:recognizer];
//    }
//}
//
//- (void)_handleSwipeLeft:(UIGestureRecognizer*)recognizer
//{
//    if([_delegate respondsToSelector:@selector(pageViewDidRecognizeSwipeLeft:withRecognizer:)])
//    {
//        [_delegate pageViewDidRecognizeSwipeLeft:self withRecognizer:recognizer];
//    }
//}

- (void)_handleTap:(UIGestureRecognizer*)recognizer
{
    if([_delegate respondsToSelector:@selector(pageViewDidRecognizeTap:withRecognizer:)])
    {
        [_delegate pageViewDidRecognizeTap:self withRecognizer:recognizer];
    }
}

- (void)_handleDoubleTap:(UIGestureRecognizer*)recognizer
{
//    _contentScrollView.scrollEnabled = NO;
    if([_delegate respondsToSelector:@selector(pageViewDidRecognizeDoubleTap:withRecognizer:)])
    {
        [_delegate pageViewDidRecognizeDoubleTap:self withRecognizer:recognizer];
    }
}

- (void)_handleSwipeUpRecognizer
{
    if([_delegate respondsToSelector:@selector(pageViewDidRecognizeSwipeUp:)])
    {
        [_delegate pageViewDidRecognizeSwipeUp:self];
    }
}

- (void)_handleSwipeDownRecognizer
{
    if([_delegate respondsToSelector:@selector(pageViewDidRecognizeSwipeDown:)])
    {
        [_delegate pageViewDidRecognizeSwipeDown:self];
    }
}

- (void)showBubbleAtIndex:(NSInteger)index
{
    if ([_bubbles count] > 0)
    {
        
    }
}

- (void)recalcBubbleFrames
{
    CGFloat ar = self.bounds.size.width > self.bounds.size.height ? self.bounds.size.height / self.bounds.size.width : 1.0;
	
    int currentIndex = 0;
	
    for (FrameInfo* frameInfo in _pageInfo.frames)
	{
        for (BubbleInfo* bubbleInfo in frameInfo.bubbles)
		{
            if ([bubbleInfo.imagePath length] > 0)
            {
                UIImageView* imageView = [_bubbles objectAtIndex:currentIndex];
                imageView.frame  = CGRectMake(self.imageFrame.size.width * bubbleInfo.origin.x,
                                              self.imageFrame.size.height * bubbleInfo.origin.y,
                                              imageView.image.size.width * ar * 0.5,
                                              imageView.image.size.height * ar * 0.5);
            }
			
            currentIndex++;
        }
    }
}

- (void)hideBubblesFromFrameIndex:(NSInteger)frameIdx bubbleIndex:(NSInteger)bubbleIdx
{
    if (frameIdx < 0 || bubbleIdx < 0)
    {
        return;
    }
    if (frameIdx >= [_pageInfo.frames count])
    {
        return;
    }
    FrameInfo* frameInfo = _pageInfo.frames[frameIdx];
    if (bubbleIdx >= [frameInfo.bubbles count])
    {
        return;
    }
    
    [self recalcBubbleFrames];
    int currentFrameIndex = 0;
    int currentBubbleIndex = 0;
    int bubblesIdx = 0;
    BOOL shouldHideNextBubbles = NO;
    for (FrameInfo* frameInfo in _pageInfo.frames) {
        for (BubbleInfo* bubbleInfo in frameInfo.bubbles) {
            if ([NSNull null] != [_bubbles objectAtIndex:bubblesIdx])
            {
                if (shouldHideNextBubbles)
                {
                    UIImageView* bubble = [_bubbles objectAtIndex:bubblesIdx];
                    [UIView animateWithDuration:0.0 animations:^{
                        bubble.alpha = 0.0;
                    }];
                }
            }
            if (currentFrameIndex == frameIdx && currentBubbleIndex == bubbleIdx)
            {
                shouldHideNextBubbles = YES;
            }
            bubblesIdx++;
            currentBubbleIndex++;
        }
        currentBubbleIndex = 0;
        currentFrameIndex++;
    }

}

- (void)showBubblesUptoFrameIndex:(NSInteger)frameIdx bubbleIndex:(NSInteger)bubbleIdx
{
    if (frameIdx < 0 || bubbleIdx < 0)
    {
        return;
    }
    if (frameIdx >= [_pageInfo.frames count])
    {
        return;
    }
    FrameInfo* frameInfo = _pageInfo.frames[frameIdx];
    if (bubbleIdx >= [frameInfo.bubbles count])
    {
        return;
    }
    
//    if (_allBubblesShowing)
//    {
        [self hideBubblesFromFrameIndex:frameIdx bubbleIndex:bubbleIdx];
//        return;
//    }
    [self recalcBubbleFrames];
    
    int currentFrameIndex = 0;
    int currentBubbleIndex = 0;
    int bubblesIdx = 0;
    for (FrameInfo* frameInfo in _pageInfo.frames) {
        for (BubbleInfo* bubbleInfo in frameInfo.bubbles) {
            if ([NSNull null] != [_bubbles objectAtIndex:bubblesIdx])
            {
                UIImageView* bubble = [_bubbles objectAtIndex:bubblesIdx];
                [UIView animateWithDuration:0.0 animations:^{
                    bubble.alpha = 1.0;
                }];
                
                
            }
            
            if (currentFrameIndex == frameIdx && currentBubbleIndex == bubbleIdx)
            {
                return;
            }
            bubblesIdx++;
            currentBubbleIndex++;
        }
        currentBubbleIndex = 0;
        currentFrameIndex++;
    }
}

- (void)showAllBubbles
{
    if (_allBubblesShowing)
    {
        return;
    }
    [self recalcBubbleFrames];
    [self hideAllBubbles];
    _allBubblesShowing = YES;
    
    int currentIndex = 0;
    for (FrameInfo* frameInfo in _pageInfo.frames) {
        for (BubbleInfo* bubbleInfo in frameInfo.bubbles) {
            _currentBubbleIdx = currentIndex;
            if ([NSNull null] != [_bubbles objectAtIndex:currentIndex])
            {
                UIImageView* bubble = [_bubbles objectAtIndex:currentIndex];
                [UIView animateWithDuration:0.0 animations:^{
                    bubble.alpha = 1.0;
                }];
            }
            currentIndex++;
        }
    }
}

- (void)hideAllBubbles
{
    int currentIndex = 0;
    for (FrameInfo* frameInfo in _pageInfo.frames) {
        for (BubbleInfo* bubbleInfo in frameInfo.bubbles) {
            _currentBubbleIdx = currentIndex;
            if ([NSNull null] != [_bubbles objectAtIndex:currentIndex])
            {
                UIImageView* bubble = [_bubbles objectAtIndex:currentIndex];
                [UIView animateWithDuration:0.0 animations:^{
                    bubble.alpha = 0.0;
                }];
            }
            currentIndex++;
        }
    }
    _allBubblesShowing = NO;
}

- (void)hideCurrentBubble
{
	if (_currentBubbleIdx >= _bubbles.count)
    {
        return;
    }
    if ([NSNull null] != [_bubbles objectAtIndex:_currentBubbleIdx])
    {
        UIImageView* prevBubble = [_bubbles objectAtIndex:_currentBubbleIdx];
        [UIView animateWithDuration:0.3 animations:^{
            prevBubble.alpha = 0.0;
        }];
    }
 
}

- (void)showBubbleAtFrameIndex:(NSInteger)frameIndex atBubbleIndex:(NSInteger)bubbleIndex hidePrevBubble:(BOOL)hide
{
    if ((frameIndex < 0) || (bubbleIndex < 0) || (_pageInfo.frames.count == 0) || ([_pageInfo.frames[frameIndex] bubbles].count == 0))
    {
        return;
    }
    [self recalcBubbleFrames];
    if(hide)
    {
        [self hideCurrentBubble];
    }

    int currentIndex = 0;
    for (FrameInfo* frameInfo in _pageInfo.frames) {
        for (BubbleInfo* bubbleInfo in frameInfo.bubbles) {
            if ((frameInfo == _pageInfo.frames[frameIndex]) && (bubbleInfo == frameInfo.bubbles[bubbleIndex]))
            {
                _currentBubbleIdx = currentIndex;
                if ([NSNull null] != [_bubbles objectAtIndex:currentIndex])
                {
                    UIImageView* bubble = [_bubbles objectAtIndex:currentIndex];
                    [UIView animateWithDuration:0.3 animations:^{
                        bubble.alpha = 1.0;
                    }];
                }
            }
            currentIndex++;
        }
    }
}

- (void)loadBubbles
{
    for (FrameInfo* frameInfo in _pageInfo.frames) {
        for (BubbleInfo* bubbleInfo in frameInfo.bubbles) {
            
            if ([bubbleInfo.imagePath length] > 0)
            {
                UIImageView* imageView = nil;
                
				if ([UIScreen mainScreen].scale > 1)
                {
                    NSString* extention = [bubbleInfo.imagePath pathExtension];
                    NSString* newPath = [bubbleInfo.imagePath stringByDeletingPathExtension];
                    newPath = [[NSString stringWithFormat:@"%@@2x", newPath] stringByAppendingPathExtension:extention];
                    imageView = [[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:newPath]];
                }
                else
				{
                    imageView = [[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:bubbleInfo.imagePath]];
                }
                
				imageView.alpha = 0.0;
								
                [_contentView addSubview:imageView];
                [_bubbles addObject:imageView];
            }
            else
			{
                [_bubbles addObject:[NSNull null]];
            }
            
        }
    }
}

- (void)loadThumbnail
{
	if (![self hasThumbnail])
	{
		return;
	}
	
	if (!_thumbnailImage)
	{
         NSString* path = [self.pageInfo.rootPath stringByAppendingPathComponent:self.pageInfo.previewBackgroundPath];
        
		_thumbnailImage = [UIImage imageWithContentsOfFile:path];
	}
	self.thumbnailImageView.image = _thumbnailImage;    
}

- (void)unloadContent
{
	
}

- (void)loadContent
{
	
}

- (BOOL)needsToLoadContent
{
	return NO;
}

- (BOOL)hasThumbnail
{
	return YES;
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewWillBeginZooming:(UIScrollView *)scrollView withView:(UIView *)view
{
    _contentScrollView.scrollEnabled = YES;
    if([_delegate respondsToSelector:@selector(pageViewDidBeginZooming:)])
    {
        [_delegate pageViewDidBeginZooming:self];
    }
}

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(CGFloat)scale
{
    if (scrollView.zoomScale <= 1.0)
    {
        _contentScrollView.scrollEnabled = NO;
        if([_delegate respondsToSelector:@selector(pageViewDidBecomePrimaryZoom:)])
        {
            [_delegate pageViewDidBecomePrimaryZoom:self];
        }
    }
}

- (UIView*)viewForZoomingInScrollView:(UIScrollView*)scrollView
{
	return _contentView;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView
{
	if (!self.shouldIgnoreScrollViewDelegateCalls)
	{
		CGFloat offsetX = (scrollView.bounds.size.width > scrollView.contentSize.width)?
		(scrollView.bounds.size.width - scrollView.contentSize.width) * 0.5 : 0.0;
		
		CGFloat offsetY = (scrollView.bounds.size.height > scrollView.contentSize.height)?
		(scrollView.bounds.size.height - scrollView.contentSize.height) * 0.5 : 0.0;
		
		self.contentView.center = CGPointMake(scrollView.contentSize.width * 0.5 + offsetX,
											  scrollView.contentSize.height * 0.5 + offsetY);
	}
}

@end
