//
//  PhotoPageView.h
//  RussellJamesPhotobook
//
//  Created by Yuriy Bilozerov on 1/24/13.
//  Copyright (c) 2013 bMuse Ukraine. All rights reserved.
//

#import "PageView.h"
#import <UIKit/UIKit.h>

@interface PhotoPageView : PageView

- (id)initWithPageInfo:(PageInfo*)pageInfo;

@property(readonly)	UIImageView*					imageView;

@end
