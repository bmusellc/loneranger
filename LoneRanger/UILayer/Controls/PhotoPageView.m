//
//  PhotoPageView.m
//  RussellJamesPhotobook
//
//  Created by Yuriy Bilozerov on 1/24/13.
//  Copyright (c) 2013 bMuse Ukraine. All rights reserved.
//

#import "PhotoPageView.h"

@interface PhotoPageView ()
{
}
- (void)_applyImage:(UIImage*)image;
@end

@implementation PhotoPageView

- (id)initWithPageInfo:(PageInfo*)pageInfo
{
    self = [super initWithPageInfo:pageInfo];
    
	if (self)
	{
		self.backgroundColor = [UIColor blackColor];
		
		_imageView = [[UIImageView alloc] initWithFrame:self.contentView.bounds];
		[self.contentView addSubview:_imageView];
		_imageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
		_imageView.contentMode = UIViewContentModeScaleAspectFit;
    }
    
	return self;
}

- (void)loadContent
{
	NSString* path = [self.pageInfo.rootPath stringByAppendingPathComponent:self.pageInfo.fullsizeBackgroundPath];
	UIImage* image = [UIImage imageWithContentsOfFile:path];
	
	CGSize imageSize = image.size;
	imageSize.width *= image.scale;
	imageSize.height *= image.scale;
	
	CGRect imageFrame = CGRectMake(0, 0, imageSize.width, imageSize.height);
	
	CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
	CGContextRef context = CGBitmapContextCreate(NULL,
												 imageSize.width,
												 imageSize.height,
												 8,
												 imageSize.width * 4,
												 colorSpace,
												 kCGBitmapByteOrder32Little | kCGImageAlphaNoneSkipFirst);
	CGContextSetInterpolationQuality(context, kCGInterpolationHigh);
	CGContextSetShouldAntialias(context, YES);
	CGColorSpaceRelease(colorSpace);
	
	CGContextClearRect(context, imageFrame);
	CGContextDrawImage(context, imageFrame, image.CGImage);
	
	CGImageRef imageRef = CGBitmapContextCreateImage(context);
	
	image = [[UIImage alloc] initWithCGImage:imageRef scale:image.scale orientation:image.imageOrientation];
	
	CGImageRelease(imageRef);
	CGContextRelease(context);
    
	if (self.pageViewMode == PageViewModeContent)
	{
		[self performSelectorOnMainThread:@selector(_applyImage:) withObject:image waitUntilDone:NO];
	}
}

- (void)_applyImage:(UIImage *)image
{
	_imageView.image = image;
	_imageView.alpha = 0.0;
	
	[UIView animateWithDuration:0.2
					 animations:^{
						 
						 _imageView.alpha = 1.0;
					 }
					 completion:^(BOOL finished) {
						 
					 }];
}

- (void)unloadContent
{
	_imageView.image = nil;
}

- (BOOL)needsToLoadContent
{
	return _imageView.image == nil;
}

@end
