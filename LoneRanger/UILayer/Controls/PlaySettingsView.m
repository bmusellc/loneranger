//
//  PlaySettingsView.m
//  Unity-iOS Simulator
//
//  Created by Yuriy Belozerov on 07.06.13.
//
//

#import "PlaySettingsView.h"
#import "Slider.h"
#import "Switch.h"

#import "PlaySettings.h"

@interface PlaySettingsView ()
{
	PlaySettings*				_playSettings;
	
	Slider*						_sensitivitySlider;
	
	Switch*						_voSwitch;
	Slider*						_volumeSlider;
	
	UIButton*					_standardShootingPrefsButton;
	UIButton*					_advancedShootingPrefsButton;
	
	UIButton*					_standardAxisButton;
	UIButton*					_reverseAxisButton;
}

@end

@implementation PlaySettingsView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
	if (self)
	{
		_playSettings = [PlaySettings sharedSettings];
		
		UIImageView* bgView = [[[UIImageView alloc] initWithFrame:self.bounds] autorelease];
		[self addSubview:bgView];
		bgView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
		bgView.image = [UIImage imageNamed:@"menu/Settings/playOptionsBg.png"];
		bgView.contentMode = UIViewContentModeCenter;
		
		UIImage* btnImg = [UIImage imageNamed:@"menu/LevelSelection/backButton.png"];
		UIButton* backButton = [UIButton buttonWithType:UIButtonTypeCustom];
		[self addSubview:backButton];
		backButton.frame = CGRectMake(10, 10, btnImg.size.width, btnImg.size.height);
		[backButton setImage:btnImg forState:UIControlStateNormal];
		[backButton addTarget:self action:@selector(_backButtonDidPressed) forControlEvents:UIControlEventTouchUpInside];
		
		UIImage* radioImg = [UIImage imageNamed:@"menu/Settings/radioSwitch.png"];
		UIImage* radioOnImg = [UIImage imageNamed:@"menu/Settings/radioSwitchOn.png"];
		
		_standardShootingPrefsButton = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
		[self addSubview:_standardShootingPrefsButton];
		_standardShootingPrefsButton.frame = CGRectMake(442, 280, radioImg.size.width, radioImg.size.height);
		[_standardShootingPrefsButton setImage:radioImg forState:UIControlStateNormal];
		[_standardShootingPrefsButton setImage:radioOnImg forState:UIControlStateSelected];
		[_standardShootingPrefsButton addTarget:self action:@selector(_shootingPrefsButtonDidPressed:) forControlEvents:UIControlEventTouchUpInside];
		
		_advancedShootingPrefsButton = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
		[self addSubview:_advancedShootingPrefsButton];
		_advancedShootingPrefsButton.frame = CGRectMake(704, 280, radioImg.size.width, radioImg.size.height);
		[_advancedShootingPrefsButton setImage:radioImg forState:UIControlStateNormal];
		[_advancedShootingPrefsButton setImage:radioOnImg forState:UIControlStateSelected];
		[_advancedShootingPrefsButton addTarget:self action:@selector(_shootingPrefsButtonDidPressed:) forControlEvents:UIControlEventTouchUpInside];
		
		if (_playSettings.isAdvancedShooting)
		{
			_advancedShootingPrefsButton.selected = YES;
		}
		else
		{
			_standardShootingPrefsButton.selected = YES;
		}
		
		_sensitivitySlider = [[Slider alloc] init];
        _sensitivitySlider.frame = CGRectMake((self.frame.size.width - _sensitivitySlider.frame.size.width) / 2, 346, _sensitivitySlider.frame.size.width, _sensitivitySlider.frame.size.height);
        [self addSubview:_sensitivitySlider];
		_sensitivitySlider.titleLabel.text = @"AIM SENSITIVITY";
		_sensitivitySlider.minValueLabel.text = @"LOW";
		_sensitivitySlider.maxValueLabel.text = @"HIGH";
		_sensitivitySlider.value =  ((_playSettings.sensitivity - kMinSensitivity) / (kMaxSensitivity - kMinSensitivity)) * 100;
		
		_standardAxisButton = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
		[self addSubview:_standardAxisButton];
		_standardAxisButton.frame = CGRectMake(456, 458, radioImg.size.width, radioImg.size.height);
		[_standardAxisButton setImage:radioImg forState:UIControlStateNormal];
		[_standardAxisButton setImage:radioOnImg forState:UIControlStateSelected];
		[_standardAxisButton addTarget:self action:@selector(_axisButtonDidPressed:) forControlEvents:UIControlEventTouchUpInside];
		
		_reverseAxisButton = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
		[self addSubview:_reverseAxisButton];
		_reverseAxisButton.frame = CGRectMake(680, 458, radioImg.size.width, radioImg.size.height);
		[_reverseAxisButton setImage:radioImg forState:UIControlStateNormal];
		[_reverseAxisButton setImage:radioOnImg forState:UIControlStateSelected];
		[_reverseAxisButton addTarget:self action:@selector(_axisButtonDidPressed:) forControlEvents:UIControlEventTouchUpInside];
		
		if (_playSettings.isAxisReversed)
		{
			_reverseAxisButton.selected = YES;
		}
		else
		{
			_standardAxisButton.selected = YES;
		}
		
		_voSwitch = [[Switch alloc] init];
        [self addSubview:_voSwitch];
        _voSwitch.center = CGPointMake(682, 612);
        _voSwitch.on = _playSettings.isVoiceOverEnabled;
		
		_volumeSlider = [[Slider alloc] init];
        _volumeSlider.frame = CGRectMake((self.frame.size.width - _volumeSlider.frame.size.width) / 2, 654, _volumeSlider.frame.size.width, _volumeSlider.frame.size.height);
        [self addSubview:_volumeSlider];
		_volumeSlider.titleLabel.text = @"VOLUME";
		_volumeSlider.minValueLabel.text = @"MIN";
		_volumeSlider.maxValueLabel.text = @"MAX";
		_volumeSlider.value = _playSettings.volumeLevel * 100;
    }
    
	return self;
}

- (id)init
{
	return [self initWithFrame:CGRectZero];
}

- (void)dealloc
{
	[_standardShootingPrefsButton release];
	[_advancedShootingPrefsButton release];
	
	[_sensitivitySlider release];
	
	[_standardAxisButton release];
	[_reverseAxisButton release];
	
	[_voSwitch release];
	
	[_volumeSlider release];
    
    [super dealloc];
}

- (void)_shootingPrefsButtonDidPressed:(UIButton*)sender
{
	if (sender == _standardShootingPrefsButton)
	{
		_standardShootingPrefsButton.selected = YES;
		_advancedShootingPrefsButton.selected = NO;
	}
	else
	{
		_standardShootingPrefsButton.selected = NO;
		_advancedShootingPrefsButton.selected = YES;
	}
}

- (void)_axisButtonDidPressed:(UIButton*)sender
{
	if (sender == _standardAxisButton)
	{
		_standardAxisButton.selected = YES;
		_reverseAxisButton.selected = NO;
	}
	else
	{
		_standardAxisButton.selected = NO;
		_reverseAxisButton.selected = YES;
	}
}

- (void)_backButtonDidPressed
{
	_playSettings.isAdvancedShooting = _advancedShootingPrefsButton.selected;
	_playSettings.isAxisReversed = _reverseAxisButton.selected;
	
	_playSettings.sensitivity = _sensitivitySlider.value / 100 * (kMaxSensitivity - kMinSensitivity) + kMinSensitivity;
	_playSettings.isVoiceOverEnabled = _voSwitch.on;
	_playSettings.volumeLevel = _volumeSlider.value / 100;
	
	[_playSettings save];
	
	[UIView animateWithDuration:0.5
					 animations:^{
						 
						 self.alpha = 0.0;
					 }
					 completion:^(BOOL finished) {
						 
						 [self removeFromSuperview];
					 }];
}


@end
