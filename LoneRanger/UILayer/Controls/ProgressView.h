//
//  ProgressView.h
//  Unity-iOS Simulator
//
//  Created by Yuriy Belozerov on 30.05.13.
//
//

#import <UIKit/UIKit.h>

@interface ProgressView : UIView

@property(assign)	CGFloat				progress;

@end
