//
//  ProgressView.m
//  Unity-iOS Simulator
//
//  Created by Yuriy Belozerov on 30.05.13.
//
//

#import "ProgressView.h"
#import "MenuLabel.h"

@interface ProgressView ()
{
	CGFloat									_progress;
	
	UIView*									_progressView;
	MenuLabel*								_progressLabel;
}

@end

@implementation ProgressView

- (CGFloat)progress
{
	return _progress;
}

- (void)setProgress:(CGFloat)progress
{
	_progress = progress;
	
	[self _updateLayout];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
	if (self)
	{
		UIImage* bgImg = [UIImage imageNamed:@"menu/ProgressView/background.png"];
		bgImg = [bgImg resizableImageWithCapInsets:UIEdgeInsetsMake(2, 2, 2, 2)];

		UIImageView* bgView = [[[UIImageView alloc] initWithImage:bgImg] autorelease];
		[self addSubview:bgView];
		bgView.frame = self.bounds;
		bgView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
		
		_progressView = [UIView new];
		[self addSubview:_progressView];
		_progressView.backgroundColor = [UIColor colorWithRed:185 / 255.0 green:115 / 255.0 blue:65 / 255.0 alpha:0.9];
		
		_progressLabel = [[MenuLabel alloc] initWithFrame:self.bounds];
		[self addSubview:_progressLabel];
		_progressLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
		_progressLabel.backgroundColor = [UIColor clearColor];
		_progressLabel.textAlignment = NSTextAlignmentCenter;
		_progressLabel.font = [UIFont fontWithName:@"Suplexmentary Comic NC" size:23];
		_progressLabel.text = @"0 %";
		_progressLabel.textColor = [UIColor colorWithRed:255 / 255.0 green:248 / 255.0 blue:227 / 255.0 alpha:1.0];
    }
    
	return self;
}

- (id)init
{
	return [self initWithFrame:CGRectZero];
}

- (void)layoutSubviews
{
	[super layoutSubviews];
	
	[self _updateLayout];
}

- (void)_updateLayout
{
	_progressView.frame = CGRectMake(1,
									 1,
									 (self.bounds.size.width - 2) * self.progress,
									 self.bounds.size.height - 2);
	
	_progressLabel.text = [NSString stringWithFormat:@"%d %%", (NSInteger)roundf(self.progress * 100)];
	_progressLabel.font = [UIFont fontWithName:@"Suplexmentary Comic NC" size:roundf(self.bounds.size.height * 0.75)];
}

@end
