//
//  PurchaseCoinButton.m
//  Unity-iOS Simulator
//
//  Created by Vladislav Bakuta on 9/9/13.
//
//

#import "PurchaseCoinButton.h"
#import "MenuLabel.h"

@implementation PurchaseCoinButton
{
    MenuLabel       *buttonLabel;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    return self;
}

- (void)dealloc
{
    [buttonLabel        release];
    [super  dealloc];
}

- (void)setTitle:(NSString *)title forState:(UIControlState)state
{
    if (!buttonLabel)
    {
        buttonLabel = [[MenuLabel menuLabelWithFontType:MenuLabelFontTypeWeswood fontSize:52] retain];
        [buttonLabel setTextAlignment:NSTextAlignmentCenter];
        [buttonLabel setFrame:CGRectMake(12, -15, 138, 140)];
        [buttonLabel setTextColor:[UIColor colorWithRed:255.0 / 255.0 green:252.0 /
                                   255.0 blue:232.0 / 255.0 alpha:1.0]];
        [buttonLabel setLineBreakMode:UILineBreakModeWordWrap];
        [buttonLabel setNumberOfLines:2];
        [buttonLabel attributedText];
        [buttonLabel setAlpha:0.5];
        
        [self addSubview:buttonLabel];
    }
    
    if (title)
    {
        [buttonLabel setText:title];
    }
    else
    {
        [buttonLabel setText:@"N/A"];
    }
    
}

- (void)setEnabled:(BOOL)enabled
{
    if (enabled)
    {
        for (id obj in self.subviews)
        {
            [obj setAlpha:1.0];
        }
        [self setUserInteractionEnabled:YES];
    }
    else
    {
        for (id obj in self.subviews)
        {
            [obj setAlpha:0.5];
        }
        [self setUserInteractionEnabled:NO];
    }
}

@end
