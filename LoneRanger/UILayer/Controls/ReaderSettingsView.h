//
//  ReaderSettingsView.h
//  Unity-iPhone
//
//  Created by Artem Manzhura on 5/24/13.
//
//

#import <UIKit/UIKit.h>
#import "ReaderSettings.h"

typedef enum
{
	ReaderSettingsViewAppearanceTypeMainMenu,
	ReaderSettingsViewAppearanceTypeReader,
}
ReaderSettingsViewAppearanceType;

@interface ReaderSettingsView : UIView

- (void)showAnimated:(BOOL)animated appearanceType:(ReaderSettingsViewAppearanceType)appearanceType completionHandler:(void(^)())completionHandler;

@end
