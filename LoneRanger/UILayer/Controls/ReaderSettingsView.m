//
//  ReaderSettingsView.m
//  Unity-iPhone
//
//  Created by Artem Manzhura on 5/24/13.
//
//

#import "ReaderSettingsView.h"

#import "Switch.h"
#import "Slider.h"

@interface ReaderSettingsView()<SwitchDelegate, SliderDelegate>
{
	void(^_completionHandler)();
	
	UIImageView*    _settingsPanelView;
	UIImageView*	_backgroundView;
	
    UIButton*       _backButton;
    Switch*         _audioSettingsSwitch;
    Switch*         _voSwitch;
    Switch*         _transitionAnimatedSwitch;
    Switch*         _autoplaySwitch;
    Switch*         _bubbleSwitch;
    Slider*         _volumeSlider;
}
@end

@implementation ReaderSettingsView

- (void)dealloc
{
    [_settingsPanelView release];
	[_backgroundView release];
    [_backButton release];
    [_audioSettingsSwitch release];
    [_voSwitch release];
    [_bubbleSwitch release];
    [_autoplaySwitch release];
    [_transitionAnimatedSwitch release];
    [_volumeSlider release];
	
    [super dealloc];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
	
    if (self)
	{
        self.exclusiveTouch = YES;
        
		_backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"reader/settings/background.png"]];
		[self addSubview:_backgroundView];
		_backgroundView.frame = self.bounds;
		_backgroundView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
		_backgroundView.contentMode = UIViewContentModeScaleToFill;
		
        _settingsPanelView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"reader/settings/backgroundFrame.png"]];
        _settingsPanelView.userInteractionEnabled = YES;
        [self addSubview:_settingsPanelView];
        _settingsPanelView.center = self.center;
        _settingsPanelView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin |
											  UIViewAutoresizingFlexibleRightMargin |
											  UIViewAutoresizingFlexibleTopMargin |
											  UIViewAutoresizingFlexibleBottomMargin;
        
        _transitionAnimatedSwitch = [[Switch alloc] init];
        [_settingsPanelView addSubview:_transitionAnimatedSwitch];
        _transitionAnimatedSwitch.center = CGPointMake(_settingsPanelView.frame.size.width - 120, 312);
        _transitionAnimatedSwitch.on = [[ReaderSettings sharedManager] isTransitionAnimated];
        _transitionAnimatedSwitch.delegate = self;
        
        _autoplaySwitch = [[Switch alloc] init];
        [_settingsPanelView addSubview:_autoplaySwitch];
        _autoplaySwitch.center = CGPointMake(_settingsPanelView.frame.size.width - 120, 250);
        _autoplaySwitch.on = [[ReaderSettings sharedManager] isAutoplay];
        _autoplaySwitch.delegate = self;
        
        
        _bubbleSwitch = [[Switch alloc] init];
        [_settingsPanelView addSubview:_bubbleSwitch];
        _bubbleSwitch.center = CGPointMake(_settingsPanelView.frame.size.width - 120, 312 + 60);
        _bubbleSwitch.on = [[ReaderSettings sharedManager] isTextBubblesEnabled];
        _bubbleSwitch.delegate = self;
        
		CGFloat y = 514;
		
        _audioSettingsSwitch = [[Switch alloc] init];
        [_settingsPanelView addSubview:_audioSettingsSwitch];
        _audioSettingsSwitch.center = CGPointMake(_settingsPanelView.frame.size.width - 120, y);
        _audioSettingsSwitch.on = [[ReaderSettings sharedManager] isAudioEnabled];
        _audioSettingsSwitch.delegate = self;
        
		y += 65;
		
        _voSwitch = [[Switch alloc] init];
        [_settingsPanelView addSubview:_voSwitch];
        _voSwitch.center = CGPointMake(_settingsPanelView.frame.size.width - 120, y);
        _voSwitch.on = [[ReaderSettings sharedManager] isVOEnabled];
        _voSwitch.delegate = self;
        
        _volumeSlider = [[Slider alloc] init];
		[_settingsPanelView addSubview:_volumeSlider];
        _volumeSlider.frame = CGRectMake((_settingsPanelView.frame.size.width - _volumeSlider.frame.size.width) / 2, 634, _volumeSlider.frame.size.width, _volumeSlider.frame.size.height);
        _volumeSlider.value = [[ReaderSettings sharedManager] volume];
        _volumeSlider.delegate = self;
        _volumeSlider.titleLabel.text = @"VOLUME";
		_volumeSlider.minValueLabel.text = @"MIN";
		_volumeSlider.maxValueLabel.text = @"MAX";
        
        
        _backButton = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
		[self addSubview:_backButton];
        [_backButton addTarget:self action:@selector(_onBackButton:) forControlEvents:UIControlEventTouchUpInside];
        UIImage* normalStateImage = [UIImage imageNamed:@"menu/LevelSelection/backButton.png"];
        [_backButton setBackgroundImage:normalStateImage forState:UIControlStateNormal];
        _backButton.frame = CGRectMake(10, 10, normalStateImage.size.width, normalStateImage.size.height);
		
		self.alpha = 0.0;
    }
	
    return self;
}

- (id)init
{
	return [self initWithFrame:CGRectZero];
}

- (void)_onBackButton:(UIButton*)sender
{
    [self _hideAnimated:YES];
}

- (void)showAnimated:(BOOL)animated appearanceType:(ReaderSettingsViewAppearanceType)appearanceType completionHandler:(void(^)())completionHandler;
{
	if (appearanceType == ReaderSettingsViewAppearanceTypeMainMenu)
	{
		_backgroundView.hidden = YES;
		self.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.85];
	}
	
	_completionHandler = [completionHandler copy];
	
	if (animated)
	{
		[UIView animateWithDuration:0.5
						 animations:^{
							 
							 self.alpha = 1.0;
						 }];
	}
	else
	{
		self.alpha = 1.0;
	}
}

- (void)_hideAnimated:(BOOL)animated
{
	if (animated)
	{
		[UIView animateWithDuration:0.5
						 animations:^{
							 
							 self.alpha = 0.0;
						 }
						 completion:^(BOOL finished) {
							 
							 _completionHandler();
							 [_completionHandler release];
						 }];
	}
	else
	{
		_completionHandler();
		[_completionHandler release];
	}
}

#pragma mark - SliderDelegate methods

- (void)sliderDidChangeValue:(Slider*)sender
{
     [[ReaderSettings sharedManager]setVolume:sender.value];
}

#pragma mark - SwitchDelegate methods

-(void)switchStatedidChange:(Switch*)sender
{
    if (sender == _audioSettingsSwitch)
    {
        [[ReaderSettings sharedManager]setAudioEnabled:sender.isOn];
    }else if (sender == _voSwitch)
    {
        if (!_voSwitch.isOn)
        {
            ([_bubbleSwitch setOn:YES]);
            [[ReaderSettings sharedManager] setTextBubblesEnabled:YES];
        }
        [[ReaderSettings sharedManager] setVoEnabled:sender.isOn];
    }else if (sender == _transitionAnimatedSwitch)
    {
        [[ReaderSettings sharedManager] setTransitionAnimated:sender.isOn];
    }
    else if (sender == _autoplaySwitch)
    {
        [[ReaderSettings sharedManager] setAutoplay:sender.isOn];
    }
    else if (sender == _bubbleSwitch)
    {
        if(!_bubbleSwitch.isOn)
        {
            ([_voSwitch setOn:YES]);
            [[ReaderSettings sharedManager] setVoEnabled:YES];
        }
        [[ReaderSettings sharedManager] setTextBubblesEnabled:sender.isOn]; 
    }
}


@end
