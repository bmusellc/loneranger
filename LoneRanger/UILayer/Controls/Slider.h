//
//  Slider.h
//  Unity-iPhone
//
//  Created by Artem Manzhura on 5/27/13.
//
//

#import <UIKit/UIKit.h>

@class Slider;

@protocol SliderDelegate <NSObject>

- (void)sliderDidChangeValue:(Slider*)sender;

@end

@interface Slider : UIView
{
    float _value;
}
@property (assign) id<SliderDelegate>       delegate;
@property (assign, nonatomic) float         value;

@property(readonly)	UILabel*				minValueLabel;
@property(readonly)	UILabel*				maxValueLabel;
@property(readonly)	UILabel*				titleLabel;

@end
