//
//  Slider.m
//  Unity-iPhone
//
//  Created by Artem Manzhura on 5/27/13.
//
//

#import "Slider.h"

@interface Slider()
{
	UIImageView*			_sliderBgView;
    UIView*					_coveringView;
	
    UIImageView*			_thumbView;
}
@end

@implementation Slider

@synthesize value = _value;

- (void)dealloc
{
	[_sliderBgView release];
    [_coveringView release];
    [_thumbView release];
	
	[_minValueLabel release];
	[_maxValueLabel release];
	[_titleLabel release];
	
    [super dealloc];
}

- (void)setValue:(float)value
{
    if (value < 0)
    {
        value = 0;
    }
    else if(value > 100)
    {
        value = 100;
    }
	
    _thumbView.center = CGPointMake(_sliderBgView.frame.size.width * (value / 100) + _sliderBgView.frame.origin.x, _sliderBgView.center.y);
     _coveringView.frame = CGRectMake(_coveringView.frame.origin.x, _coveringView.frame.origin.y, _thumbView.center.x - _sliderBgView.frame.origin.x, _coveringView.frame.size.height);
	
    _value = value;
}

- (float)value
{
    return _value;
}

- (id)init
{
    self = [super init];
    
	if (self)
	{
		self.frame = CGRectMake(0, 0, 500, 62);
		
        _sliderBgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"reader/settings/sliderBackground.png"]];
        [self addSubview:_sliderBgView];
        _sliderBgView.center = CGPointMake(self.bounds.size.width / 2, self.bounds.size.height - _sliderBgView.bounds.size.height / 2);
		
        UIImageView* activeImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"reader/settings/sliderMaxBackground.png"]];
        
        _coveringView = [[UIView alloc] initWithFrame:_sliderBgView.frame];
        [_coveringView addSubview:activeImage];
        _coveringView.clipsToBounds = YES;
        [activeImage autorelease];
        
        [self addSubview:_coveringView];
        
        _thumbView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"reader/settings/sliderThumb.png"]];
        [self addSubview:_thumbView];
		_thumbView.center = CGPointMake(_sliderBgView.frame.origin.x, _sliderBgView.center.y);
        _thumbView.userInteractionEnabled = YES;
        
        UIPanGestureRecognizer*   panRecognizer = [[UIPanGestureRecognizer alloc] init];
        [panRecognizer addTarget:self action:@selector(onPan:)];
        [_thumbView addGestureRecognizer:panRecognizer];
        [panRecognizer autorelease];
        
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, 26)];
        [self addSubview:_titleLabel];
        _titleLabel.textAlignment = UITextAlignmentCenter;
        _titleLabel.backgroundColor = [UIColor clearColor];
        _titleLabel.font = [UIFont fontWithName:@"Suplexmentary Comic NC" size:24];
        
        _minValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 12, 68, 26)];
        [self addSubview:_minValueLabel];
        _minValueLabel.textAlignment = UITextAlignmentCenter;
        _minValueLabel.backgroundColor = [UIColor clearColor];
        _minValueLabel.font = [UIFont fontWithName:@"Suplexmentary Comic NC" size:24];
        
        _maxValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.frame.size.width - 68, 12, 68, 26)];
        [self addSubview:_maxValueLabel];
        _maxValueLabel.textAlignment = UITextAlignmentCenter;
        _maxValueLabel.backgroundColor = [UIColor clearColor];
        _maxValueLabel.font = [UIFont fontWithName:@"Suplexmentary Comic NC" size:24];
    }
	
    return self;
}

- (void)onPan:(UIPanGestureRecognizer*)recognizer
{
    CGPoint offset = [recognizer translationInView:recognizer.view];
	[recognizer setTranslation:CGPointZero inView:recognizer.view];
    
    float newXOffset = _thumbView.center.x;
    newXOffset += offset.x;
    
    if (newXOffset > CGRectGetMaxX(_sliderBgView.frame))
    {
        newXOffset = CGRectGetMaxX(_sliderBgView.frame);
    }
    else if (newXOffset < CGRectGetMinX(_sliderBgView.frame))
    {
        newXOffset = CGRectGetMinX(_sliderBgView.frame);
    }
	
    _thumbView.center = CGPointMake(newXOffset,_sliderBgView.center.y);
    _coveringView.frame = CGRectMake(_coveringView.frame.origin.x, _coveringView.frame.origin.y, _thumbView.center.x - _sliderBgView.frame.origin.x, _coveringView.frame.size.height);
	
    _value = ((_thumbView.center.x - _sliderBgView.frame.origin.x) * 100 / _sliderBgView.frame.size.width);

    if(_value > 100)
    {
        _value = 100;
    }
    else if (_value < 0)
    {
        _value = 0;
    }
	
    if(recognizer.state == UIGestureRecognizerStateEnded || recognizer.state == UIGestureRecognizerStateCancelled)
	{
        if ([_delegate respondsToSelector:@selector(sliderDidChangeValue:)])
        {
            [_delegate sliderDidChangeValue:self];
        }
    }
}

@end
