//
//  Bubble.h
//  Unity-iPhone
//
//  Created by Artem Manzhura on 6/4/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>
#import "BubbleInfo.h"

@class SpeechBubble;

@protocol SpeechBubbleDelegate <NSObject>
- (void)speechBubbleDidStopPlaying:(SpeechBubble*)sender;
@end

@interface SpeechBubble : NSObject
@property (readonly)     BubbleInfo*            info;
@property (assign)      id<SpeechBubbleDelegate>      delegate;

- (void)play;
- (BOOL)playing;
- (void)stop;
- (void)pause;
- (CGFloat)duration;

+ (SpeechBubble*)speechBubbleWithBubbleInfo:(BubbleInfo *)info;

@end
