//
//  Bubble.m
//  Unity-iPhone
//
//  Created by Artem Manzhura on 6/4/13.
//
//

#import "SpeechBubble.h"
#import "AudioPlayer.h"
#import "ReaderSettings.h"

@interface SpeechBubble() <AudioPlayerDelegate>
{
    AudioPlayer*    _speechBubblePlayer;
    NSInteger       _currentSpeechIndex;
    BOOL            _isPaused;
}
@end

@implementation SpeechBubble

- (void)dealloc
{
    [_speechBubblePlayer stop];
    _speechBubblePlayer.delegate = nil;
    [_speechBubblePlayer release];
    [_info release];
    [super dealloc];
}

- (id)initWithBubbleInfo:(BubbleInfo*)info
{
    self = [super init];
    if (self) {
        _info = [info retain];
        _currentSpeechIndex = 0;
        _isPaused = NO;
        _speechBubblePlayer = [[AudioPlayer alloc] init];
        _speechBubblePlayer.delegate = self;
        if (_info.speeches.count != 0)
        {
            [_speechBubblePlayer loadMusicWithFilePath:_info.speeches[_currentSpeechIndex] startTime:0.0 volume:1.0];
        }else{
            NSLog(@"There are no any speeches");
        }
    }
    return self;
}

- (BOOL)playing
{
    return [_speechBubblePlayer playing];
}

- (void)play
{
//    if (_isPaused)
//    {
        _speechBubblePlayer.volume = [[ReaderSettings sharedManager] volume] / 100.0f;
        [_speechBubblePlayer play];
        _isPaused = NO;
//        return;
//    }
    
//        _speechBubblePlayer.volume = [[ReaderSettings sharedManager] volume] / 100.0f;
//        [_speechBubblePlayer play];


}

- (void)pause
{
    _isPaused = YES;
    [_speechBubblePlayer pause];
}

- (CGFloat)duration
{
    return _speechBubblePlayer.duration;
}

- (void)stop
{
    _currentSpeechIndex = 0;
    [_speechBubblePlayer pause];
    _isPaused = NO;
}

+ (SpeechBubble*)speechBubbleWithBubbleInfo:(BubbleInfo *)info
{
    SpeechBubble* bubble = [[[SpeechBubble alloc] initWithBubbleInfo:info] autorelease];
//    bubble->_info = [info retain];
    return bubble;
}

#pragma mark - AudioPlayerDelegate methods

- (void)audioPlayerDidFinishPlaying:(AudioPlayer*)audioPlayer
{
    _currentSpeechIndex++;
    if(_currentSpeechIndex < [_info.speeches count])
    {
        [_speechBubblePlayer loadMusicWithFilePath:_info.speeches[_currentSpeechIndex] startTime:0.0 volume:1.0];
        _speechBubblePlayer.volume = [[ReaderSettings sharedManager] volume] / 100.0f;
        [_speechBubblePlayer play];
    }
    else
    {
        _currentSpeechIndex = 0;
        [self stop];
        if ([_delegate respondsToSelector:@selector(speechBubbleDidStopPlaying:)])
        {
            [_delegate speechBubbleDidStopPlaying:self];
        }
    }
}


@end
