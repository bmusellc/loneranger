//
//  Switch.h
//  Unity-iPhone
//
//  Created by Artem Manzhura on 5/27/13.
//
//

#import <UIKit/UIKit.h>

@class Switch;

@protocol SwitchDelegate <NSObject>

-(void)switchStatedidChange:(Switch*)sender;
@end

@interface Switch : UIView

@property (assign, nonatomic, getter = isOn) BOOL      on;
@property (assign) id<SwitchDelegate>  delegate;
@end
