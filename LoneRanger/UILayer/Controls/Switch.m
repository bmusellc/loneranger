//
//  Switch.m
//  Unity-iPhone
//
//  Created by Artem Manzhura on 5/27/13.
//
//

#import "Switch.h"
#import <QuartzCore/QuartzCore.h>

#define OFFSET_X  10
#define LENGTH_X  60

@interface Switch()
{
    UIView*             _cuttingView;
    UIImageView*        _stateView;
}
@end

@implementation Switch

- (void)dealloc
{
    [_cuttingView release];
    [_stateView release];
    [super dealloc];
}

- (id)init
{
    self = [super init];
    if (self) {
        
        UIImageView* background = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"reader/settings/switchBackground.png"]];
        [self addSubview:background];
        self.frame = CGRectMake(0.0f, 0.0f, background.image.size.width, background.image.size.height);
        [background autorelease];
        
        UIImageView* clippingMask = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"reader/settings/switchClipMask.png"]];
//        [self addSubview:clippingMask];
        
        _cuttingView = [[UIView alloc]init];
        [self addSubview:_cuttingView];
        _cuttingView.frame = CGRectMake(0.0f, 0.0f, background.image.size.width, background.image.size.height);
//        _cuttingView.backgroundColor = [UIColor redColor];
//        _cuttingView.alpha = 0.5f;
        _cuttingView.layer.mask = clippingMask.layer;
        [clippingMask autorelease];
        _cuttingView.clipsToBounds = YES;
        
        _stateView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"reader/settings/switchStates.png"]];
        _stateView.frame = CGRectMake(0, 0, _stateView.image.size.width, _stateView.image.size.height);
        [_cuttingView addSubview:_stateView];
        _stateView.center = CGPointMake(OFFSET_X + _stateView.center.x, _cuttingView.frame.size.height / 2 + 2);
        
//        UISwipeGestureRecognizer* swipeRecognizer = [[UISwipeGestureRecognizer alloc] init];
//        [swipeRecognizer addTarget:self action:@selector(_onSwipe:)];
//        swipeRecognizer.direction = UISwipeGestureRecognizerDirectionLeft;
//        [_cuttingView addGestureRecognizer:swipeRecognizer];
//        [swipeRecognizer autorelease];
//        
//        
//        swipeRecognizer = [[UISwipeGestureRecognizer alloc] init];
//        [swipeRecognizer addTarget:self action:@selector(_onSwipe:)];
//        swipeRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
//        [_cuttingView addGestureRecognizer:swipeRecognizer];
//        [swipeRecognizer autorelease];
        
        UIPanGestureRecognizer* panRecognizer = [[UIPanGestureRecognizer alloc] init];
        [panRecognizer addTarget:self action:@selector(_onPane:)];
        [_cuttingView addGestureRecognizer:panRecognizer];
        [panRecognizer autorelease];
        
        UITapGestureRecognizer* tapRecognizer = [[UITapGestureRecognizer alloc] init];
        [tapRecognizer addTarget:self action:@selector(_onTap:)];
        [_cuttingView addGestureRecognizer:tapRecognizer];
        [tapRecognizer autorelease];
        // Initialization code
    }
    return self;
}

- (void)_onSwipe:(UISwipeGestureRecognizer*)sender
{
//    if(sender.direction == UISwipeGestureRecognizerDirectionRight)
//    {
//        [UIView animateWithDuration:0.3 animations:^{
//            _stateView.frame = CGRectMake(X_OFFSET, _stateView.frame.origin.y, _stateView.frame.size.width, _stateView.frame.size.height);
//        } completion:^(BOOL finished) {
//            
//        }];
//    }
//    else{
//        [UIView animateWithDuration:0.3 animations:^{
//            _stateView.frame = CGRectMake(_cuttingView.frame.size.width - _stateView.frame.origin.y, _stateView.center.y, _stateView.frame.size.width, _stateView.frame.size.height);
//        } completion:^(BOOL finished) {
//            
//        }];
//    }
//
    
}

- (void)_onTap:(UITapGestureRecognizer*)sender
{
    self.on = !self.on;
    if([_delegate respondsToSelector:@selector(switchStatedidChange:)])
    {
        [_delegate switchStatedidChange:self];
    }
}

- (void)_onPane:(UIPanGestureRecognizer*)sender
{
    CGPoint offset = [sender translationInView: self];
	[sender setTranslation:CGPointZero inView: self];
    
    float newXOffset = _stateView.frame.origin.x;
    newXOffset += offset.x;
    
    if(newXOffset > OFFSET_X)
    {
        newXOffset = OFFSET_X;
    }
    else if (newXOffset <  OFFSET_X - LENGTH_X)
    {
        newXOffset  = OFFSET_X - LENGTH_X;
    }
    
    _stateView.frame = CGRectMake(newXOffset, _stateView.frame.origin.y, _stateView.frame.size.width, _stateView.frame.size.height);

	if(sender.state == UIGestureRecognizerStateEnded || sender.state == UIGestureRecognizerStateCancelled)
	{
		if(fabs(OFFSET_X - newXOffset) < 35)
		{
            [UIView animateWithDuration:0.25f
							 animations: ^
			 {
				_stateView.frame = CGRectMake(OFFSET_X, _stateView.frame.origin.y, _stateView.frame.size.width, _stateView.frame.size.height);
			 } completion:^(BOOL finished) {
                 _on = YES;
                 if([_delegate respondsToSelector:@selector(switchStatedidChange:)])
                 {
                     [_delegate switchStatedidChange:self];
                 }
             }];
		}
		else
		{
            [UIView animateWithDuration:0.25f
							 animations: ^
			 {
                 _stateView.frame = CGRectMake(OFFSET_X - LENGTH_X, _stateView.frame.origin.y, _stateView.frame.size.width, _stateView.frame.size.height);
			 }completion:^(BOOL finished) {
                 _on = NO;
                 if([_delegate respondsToSelector:@selector(switchStatedidChange:)])
                 {
                     [_delegate switchStatedidChange:self];
                 }
             }];
		}
	}

}


- (void)setOn:(BOOL)on
{
    _on = on;
    if(_on)
    {
       _stateView.frame = CGRectMake(OFFSET_X, _stateView.frame.origin.y, _stateView.frame.size.width, _stateView.frame.size.height); 
    }
    else
    {
        _stateView.frame = CGRectMake(OFFSET_X - LENGTH_X, _stateView.frame.origin.y, _stateView.frame.size.width, _stateView.frame.size.height);
    }

}



@end
