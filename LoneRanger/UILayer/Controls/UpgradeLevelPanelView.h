//
//  UpgradeLevelPanelView.h
//  Unity-iOS Simulator
//
//  Created by Yuriy Belozerov on 23.05.13.
//
//

#import <UIKit/UIKit.h>

@interface UpgradeLevelPanelView : UIView

@property(assign)	NSInteger					upgradeLevel;

@end
