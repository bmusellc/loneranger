//
//  UpgradeLevelPanelView.m
//  Unity-iOS Simulator
//
//  Created by Yuriy Belozerov on 23.05.13.
//
//

#import "UpgradeLevelPanelView.h"
#import "MenuLabel.h"

@interface UpgradeLevelPanelView ()
{
	NSInteger										_upgradeLevel;
	
	MenuLabel*										_levelLabel;
	
	NSMutableArray*									_starIcons;
}
@end

@implementation UpgradeLevelPanelView

- (NSInteger)upgradeLevel
{
	return _upgradeLevel;
}

- (void)setUpgradeLevel:(NSInteger)upgradeLevel
{
	_upgradeLevel = upgradeLevel;
	
	self.hidden = self.upgradeLevel < 0;
	
	_levelLabel.text = self.upgradeLevel > 0 ? [NSString stringWithFormat:@"LEVEL %d", self.upgradeLevel] : @"STOCK";
	
	[_starIcons enumerateObjectsUsingBlock:^(UIImageView* view, NSUInteger idx, BOOL *stop) {
		
		view.highlighted = self.upgradeLevel > idx;
	}];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
	if (self)
	{
		UIImage* bgImg = [UIImage imageNamed:@"menu/Armory/upgradeLevelBg.png"];
		self.backgroundColor = [UIColor colorWithPatternImage:bgImg];
		
		self.frame = CGRectMake(0, 0, bgImg.size.width, bgImg.size.height);
		
		_levelLabel = [[MenuLabel menuLabelWithFontType:MenuLabelFontTypeWeswood fontSize:34] retain];
		[self addSubview:_levelLabel];
		_levelLabel.frame = CGRectMake(0, 0, self.bounds.size.width, 64);
		_levelLabel.textAlignment = NSTextAlignmentCenter;
		_levelLabel.textColor = [UIColor colorWithRed:255.0 / 255.0 green:180.0 / 255.0 blue:100.0 / 255.0 alpha:1.0];
		
		_starIcons = [NSMutableArray new];
		
		for (int i = 0; i < 3; i++)
		{
			UIImageView* starIconView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"menu/Armory/starIcon.png"]
														   highlightedImage:[UIImage imageNamed:@"menu/Armory/starHlIcon.png"]] autorelease];
			
			[_starIcons addObject:starIconView];
			[self addSubview:starIconView];
			
			starIconView.frame = CGRectMake(90 + 102 * i, 46, starIconView.bounds.size.width, starIconView.bounds.size.height);
		}
    }
    
	return self;
}

- (id)init
{
	return [self initWithFrame:CGRectZero];
}

- (void)dealloc
{
	[_levelLabel    release];
	[_starIcons     release];
    
    [super dealloc];
}

@end
