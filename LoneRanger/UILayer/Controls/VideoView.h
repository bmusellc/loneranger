//
//  VideoView.h
//  DanceWithFriends
//
//  Created by Vitaliy Kolomoets on 2/6/13.
//  Copyright (c) 2013 Vitaliy Kolomoets. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface VideoView : UIView

- (AVPlayerLayer*)playerLayer;
- (AVPlayer*)player;
- (void)setPlayer:(AVPlayer*)player;
- (void)setVideoFillMode:(NSString *)fillMode;

@end
