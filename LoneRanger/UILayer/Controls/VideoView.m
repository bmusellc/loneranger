//
//  VideoView.m
//  DanceWithFriends
//
//  Created by Vitaliy Kolomoets on 2/6/13.
//  Copyright (c) 2013 Vitaliy Kolomoets. All rights reserved.
//

#import "VideoView.h"

@implementation VideoView

- (void)layoutSubviews
{
	[super layoutSubviews];
}

+ (Class)layerClass
{
    return [AVPlayerLayer class];
}

- (AVPlayerLayer*)playerLayer
{
	return (AVPlayerLayer*)[self layer];
}

- (AVPlayer*)player
{
    return [(AVPlayerLayer*)[self layer] player];
}

- (void)setPlayer:(AVPlayer*)player
{
    [(AVPlayerLayer*)[self layer] setPlayer:player];
}

- (void)setVideoFillMode:(NSString *)fillMode
{
    AVPlayerLayer *playerLayer = (AVPlayerLayer*)[self layer];
    playerLayer.videoGravity = fillMode;
}

@end
