//
//  WaypointAnimationView.h
//  Unity-iPhone
//
//  Created by Yuriy Belozerov on 27.05.13.
//
//

#import <UIKit/UIKit.h>

@interface WaypointAnimationView : UIView

- (void)startAnimating;
- (void)removeVideoPlayer;

@end
