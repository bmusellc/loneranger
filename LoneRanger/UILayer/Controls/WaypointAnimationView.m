//
//  WaypointAnimationView.m
//  Unity-iPhone
//
//  Created by Yuriy Belozerov on 27.05.13.
//
//

#import "WaypointAnimationView.h"

#import "VideoView.h"

@interface WaypointAnimationView ()
{
	VideoView*						_videoView;
}

@end

@implementation WaypointAnimationView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
	if (self)
	{
		//paths
		NSString* introVideoPath = [[NSBundle mainBundle] pathForResource:@"Transition_in" ofType:@"mp4" inDirectory:[UIScreen mainScreen].scale > 1 ? @"Video/2048" : @"Video/1024"];
		
		//URLs
		NSURL* introVideoURL = [NSURL fileURLWithPath:introVideoPath];
		
		//assets
		AVAsset* introAsset = [AVAsset assetWithURL:introVideoURL];
				
		_videoView = [[[VideoView alloc] initWithFrame:self.bounds] autorelease];
		[self addSubview:_videoView];
		_videoView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
		
		_videoView.player = [AVPlayer playerWithPlayerItem:[AVPlayerItem playerItemWithAsset:introAsset]];
    }
    
	return self;
}

- (void)startAnimating
{
	[_videoView.player play];
}

- (void)removeVideoPlayer
{
	[_videoView removeFromSuperview];
}

@end
