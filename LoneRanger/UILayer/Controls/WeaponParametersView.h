//
//  WeaponParametersView.h
//  Unity-iOS Simulator
//
//  Created by Yuriy Belozerov on 21.05.13.
//
//

#import "WeaponInfo.h"
#import <UIKit/UIKit.h>

@interface WeaponParametersView : UIView

@property(retain)	WeaponInfo*					weaponInfo;

@end
