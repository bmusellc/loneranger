//
//  WeaponParametersView.m
//  Unity-iOS Simulator
//
//  Created by Yuriy Belozerov on 21.05.13.
//
//

#import "WeaponParametersView.h"
#import "MenuLabel.h"

@interface WeaponParametersView ()
{
	WeaponInfo*							_weaponInfo;
	
	UIImageView*						_weaponStatisticsView;
	UIImageView*						_currentStatisticsView;
	UIImageView*						_upgradeStatisticsView;
	
	NSMutableArray*						_weaponStatsLabels;
	NSMutableArray*						_currentStatsLabels;
	NSMutableArray*						_upgradeStatsLabels;
}

@end

@implementation WeaponParametersView

- (WeaponInfo*)weaponInfo
{
	return _weaponInfo;
}

- (void)setWeaponInfo:(WeaponInfo *)weaponInfo
{
	[_weaponInfo release];
	_weaponInfo = [weaponInfo retain];
	
	[self _updateLayout];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
	if (self)
	{
		self.frame = CGRectMake(0, 0, 830, 158);
		
		UIImage* captionImage = [UIImage imageNamed:@"menu/Armory/weaponStatisticsCaption.png"];
		_weaponStatisticsView = [[[UIImageView alloc] initWithImage:captionImage] autorelease];
		[self addSubview:_weaponStatisticsView];
		_weaponStatisticsView.frame = CGRectMake(0, 0, captionImage.size.width + 40, captionImage.size.height);
		_weaponStatisticsView.contentMode = UIViewContentModeLeft;
		_weaponStatisticsView.center = self.center;
		
		_weaponStatsLabels = [NSMutableArray new];
		
		for (int i = 0; i < 4; i++)
		{
			MenuLabel* lbl = [self _valueLabel];
			lbl.frame = CGRectMake(_weaponStatisticsView.bounds.size.width - 38, 46 + 29 * i, 60, 24);
			
			[_weaponStatsLabels addObject:lbl];
			[_weaponStatisticsView addSubview:lbl];			
		}
		
		captionImage = [UIImage imageNamed:@"menu/Armory/currentStatisticsCaption.png"];
		_currentStatisticsView = [[[UIImageView alloc] initWithImage:captionImage] autorelease];
		[self addSubview:_currentStatisticsView];
		_currentStatisticsView.frame = CGRectMake(0, 0, captionImage.size.width + 40, captionImage.size.height);
		_currentStatisticsView.contentMode = UIViewContentModeLeft;
		
		_currentStatsLabels = [NSMutableArray new];
		
		for (int i = 0; i < 4; i++)
		{
			MenuLabel* lbl = [self _valueLabel];
			lbl.frame = CGRectMake(_currentStatisticsView.bounds.size.width - 38, 46 + 29 * i, 60, 24);
			
			[_currentStatsLabels addObject:lbl];
			[_currentStatisticsView addSubview:lbl];
		}
		
		captionImage = [UIImage imageNamed:@"menu/Armory/upgradeStatisticsCaption.png"];
		_upgradeStatisticsView = [[[UIImageView alloc] initWithImage:captionImage] autorelease];
		[self addSubview:_upgradeStatisticsView];
		_upgradeStatisticsView.frame = CGRectMake(0, 0, captionImage.size.width + 40, captionImage.size.height);
		_upgradeStatisticsView.contentMode = UIViewContentModeLeft;
		_upgradeStatisticsView.frame = CGRectOffset(_upgradeStatisticsView.frame, self.bounds.size.width - _upgradeStatisticsView.bounds.size.width, 0);
		
		_upgradeStatsLabels = [NSMutableArray new];
		
		for (int i = 0; i < 4; i++)
		{
			MenuLabel* lbl = [self _valueLabel];
			lbl.frame = CGRectMake(_upgradeStatisticsView.bounds.size.width - 38, 46 + 29 * i, 60, 24);
			
			[_upgradeStatsLabels addObject:lbl];
			[_upgradeStatisticsView addSubview:lbl];
		}
    }
    
	return self;
}

- (id)init
{
    return [self initWithFrame:CGRectZero];
}

- (void)dealloc
{
    self.weaponInfo = nil;
	
    [super dealloc];
}

- (void)_updateLayout
{
	if (self.weaponInfo.upgradeLevel == WeaponUpgradeLevelNotOwned || self.weaponInfo.upgradeLevel == self.weaponInfo.maxUpgradeLevel)
	{
		_weaponStatisticsView.hidden = NO;
		
		NSArray* values = @[[NSString stringWithFormat:@"%d", self.weaponInfo.clipSize],
							[NSString stringWithFormat:@"%.1f", self.weaponInfo.reloadTime],
							[NSString stringWithFormat:@"%.1f", self.weaponInfo.recoil],
							[NSString stringWithFormat:@"%d", self.weaponInfo.accuracy]];
		
		[_weaponStatsLabels enumerateObjectsUsingBlock:^(UILabel* lbl, NSUInteger idx, BOOL *stop) {
			
			lbl.text = values[idx];
		}];
		
		_currentStatisticsView.hidden = YES;
		_upgradeStatisticsView.hidden = YES;
	}
	else
	{
		_weaponStatisticsView.hidden = YES;
		
		_currentStatisticsView.hidden = NO;
		_upgradeStatisticsView.hidden = NO;
		
		NSArray* values = @[[NSString stringWithFormat:@"%d", self.weaponInfo.clipSize],
							[NSString stringWithFormat:@"%.1f", self.weaponInfo.reloadTime],
							[NSString stringWithFormat:@"%.1f", self.weaponInfo.recoil],
							[NSString stringWithFormat:@"%d", self.weaponInfo.accuracy]];
		
		[_currentStatsLabels enumerateObjectsUsingBlock:^(UILabel* lbl, NSUInteger idx, BOOL *stop) {
			
			lbl.text = values[idx];
		}];
		
		WeaponUpgradeInfo* upgradeInfo = self.weaponInfo.upgrades[self.weaponInfo.upgradeLevel + 1];
		
		values = @[[NSString stringWithFormat:@"%d", upgradeInfo.clipSize],
				   [NSString stringWithFormat:@"%.1f", upgradeInfo.reloadTime],
				   [NSString stringWithFormat:@"%.1f", upgradeInfo.recoil],
				   [NSString stringWithFormat:@"%d", upgradeInfo.accuracy]];
		
		[_upgradeStatsLabels enumerateObjectsUsingBlock:^(UILabel* lbl, NSUInteger idx, BOOL *stop) {
			
			lbl.text = values[idx];
		}];
	}
}

- (MenuLabel*)_valueLabel
{
	MenuLabel* lbl = [MenuLabel menuLabelWithFontType:MenuLabelFontTypeSuplexmentary fontSize:24];
	lbl.textColor = [UIColor colorWithRed:255.0 / 255.0 green:245.0 / 255.0 blue:220.0 / 255.0 alpha:1.0];
	
	return lbl;
}

@end
