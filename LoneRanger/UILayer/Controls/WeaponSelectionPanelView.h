//
//  WeaponSelectionPanelView.h
//  Unity-iOS Simulator
//
//  Created by Yuriy Belozerov on 16.05.13.
//
//

#import <UIKit/UIKit.h>

@interface WeaponSelectionPanelView : UIView

@property(assign)	NSInteger				currentWeaponIndex;

@end
