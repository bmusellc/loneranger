//
//  WeaponSelectionPanelView.m
//  Unity-iOS Simulator
//
//  Created by Yuriy Belozerov on 16.05.13.
//
//

#import "WeaponSelectionPanelView.h"

#import "MenuLabel.h"

#import "WeaponManager.h"

@interface WeaponSelectionPanelView ()
{
	UIButton*									_previousButton;
	UIButton*									_nextButton;
	
	UIView*										_contentView;
	
	MenuLabel*									_weaponNameLabel;
	UIImageView*								_weaponIconView;
	
	NSArray*									_weapons;
	NSInteger									_currentWeaponIndex;
}

@end

@implementation WeaponSelectionPanelView

- (NSInteger)currentWeaponIndex
{
	return _currentWeaponIndex;
}

- (void)setCurrentWeaponIndex:(NSInteger)currentWeaponIndex
{
	if (self.currentWeaponIndex != currentWeaponIndex && currentWeaponIndex > -1 && currentWeaponIndex < _weapons.count)
	{
		WeaponInfo* weaponInfo = _weapons[currentWeaponIndex];
		
		if (_currentWeaponIndex > -1)
		{
			[UIView animateWithDuration:0.25
							 animations:^{
								 
								 _contentView.alpha = 0.0;
							 }
							 completion:^(BOOL finished) {
								 
								 [self _updateWeaponParameters:weaponInfo];
								 
								 [UIView animateWithDuration:0.25
												  animations:^{
													  
													  _contentView.alpha = 1.0;
												  }
												  completion:^(BOOL finished) {
													  
													  
												  }];
							 }];
		}
		else //view will appear
		{
			[self _updateWeaponParameters:weaponInfo];
		}
		
		_previousButton.enabled = currentWeaponIndex > 0;
		_nextButton.enabled = currentWeaponIndex < _weapons.count - 1;
		
		_currentWeaponIndex = currentWeaponIndex;
		
		[WeaponManager sharedManager].currentWeapon = _weapons[self.currentWeaponIndex];
	}
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
	if (self)
	{
		_weapons = [[WeaponManager sharedManager].availableWeapons copy];
		_currentWeaponIndex = -1;
		
		UIImage* bgImg = [UIImage imageNamed:@"menu/LevelSelection/equippedWeaponPanelBg.png"];
		self.frame = CGRectMake(0, 0, bgImg.size.width, bgImg.size.height);
		self.backgroundColor = [UIColor colorWithPatternImage:bgImg];
		
		_contentView = [[UIView alloc] initWithFrame:self.bounds];
		[self addSubview:_contentView];
		
		CGFloat weaponIconViewWidth = 300;
		CGFloat weaponIconViewHeight = weaponIconViewWidth / 2;
		
		_weaponIconView = [UIImageView new];
		[_contentView addSubview:_weaponIconView];
		_weaponIconView.frame = CGRectMake((self.bounds.size.width - weaponIconViewWidth) / 2, 66, weaponIconViewWidth, weaponIconViewHeight);
		
		_weaponNameLabel = [[MenuLabel menuLabelWithFontType:MenuLabelFontTypeSuplexmentary fontSize:23] retain];
		[_contentView addSubview:_weaponNameLabel];
		_weaponNameLabel.frame = CGRectMake(0, 204, self.bounds.size.width, 26);
		_weaponNameLabel.textColor = [UIColor colorWithRed:255.0 / 255.0 green:252.0 / 255.0 blue:232.0 / 255.0 alpha:1.0];
		_weaponNameLabel.textAlignment = NSTextAlignmentCenter;

		UIImage* buttonImg = [UIImage imageNamed:@"menu/LevelSelection/previousButton.png"];
		_previousButton = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
		[self addSubview:_previousButton];
		[_previousButton setImage:buttonImg forState:UIControlStateNormal];
		_previousButton.frame = CGRectMake(20, 102, buttonImg.size.width, buttonImg.size.height);
		[_previousButton addTarget:self action:@selector(_previousButtonDidPressed) forControlEvents:UIControlEventTouchUpInside];
		
		buttonImg = [UIImage imageNamed:@"menu/LevelSelection/nextButton.png"];
		_nextButton = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
		[self addSubview:_nextButton];
		[_nextButton setImage:buttonImg forState:UIControlStateNormal];
		_nextButton.frame = CGRectMake(self.bounds.size.width - 20 - buttonImg.size.width, 102, buttonImg.size.width, buttonImg.size.height);
		[_nextButton addTarget:self action:@selector(_nextButtonDidPressed) forControlEvents:UIControlEventTouchUpInside];
		
		UISwipeGestureRecognizer* swipeLeft = [[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(_handleSwipeGestureRecognizer:)] autorelease];
		swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
		[_contentView addGestureRecognizer:swipeLeft];
		
		UISwipeGestureRecognizer* swipeRight = [[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(_handleSwipeGestureRecognizer:)] autorelease];
		swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
		[_contentView addGestureRecognizer:swipeRight];
		
		self.currentWeaponIndex = [[WeaponManager sharedManager].weapons indexOfObject:[WeaponManager sharedManager].currentWeapon];
    }
    
	return self;
}

- (id)init
{
	return [self initWithFrame:CGRectZero];
}

- (void)dealloc
{
	[_weapons release];
    
    [super dealloc];
}

- (void)_updateWeaponParameters:(WeaponInfo*)weaponInfo
{
	UIImage* weaponIcon = [UIImage imageNamed:weaponInfo.iconPath];
	_weaponIconView.image = weaponIcon;
	_weaponNameLabel.text = [weaponInfo.modelStr uppercaseString];
    
    if ([weaponInfo.typeStr isEqualToString:@"shotgun"])
    {
        _weaponIconView.frame = CGRectMake((self.bounds.size.width - 300) / 2,
                                           90,
                                           weaponIcon.size.width / 3,
                                           weaponIcon.size.height / 3);
    }
    else if ([weaponInfo.typeStr isEqualToString:@"pistol"])
    {
        _weaponIconView.frame = CGRectMake((self.bounds.size.width - 300) / 2, 66, 300, 150);
    }
}

- (void)_previousButtonDidPressed
{
	self.currentWeaponIndex--;
}

- (void)_nextButtonDidPressed
{
	self.currentWeaponIndex++;
}

- (void)_handleSwipeGestureRecognizer:(UISwipeGestureRecognizer*)sender
{
	switch (sender.direction)
	{
		case UISwipeGestureRecognizerDirectionLeft:
			if (self.currentWeaponIndex < _weapons.count - 1)
			{
				self.currentWeaponIndex++;
			}
			break;
			
		case UISwipeGestureRecognizerDirectionRight:
			if (self.currentWeaponIndex > 0)
			{
				self.currentWeaponIndex--;
			}
			break;
			
		default:
			break;
	}
}

@end
