//
//  DebugReaderNavigationController.m
//  Unity-iOS Simulator
//
//  Created by Yuriy Belozerov on 08.06.13.
//
//

#import "DebugReaderNavigationController.h"

@interface DebugReaderNavigationController ()

@end

@implementation DebugReaderNavigationController

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

@end
