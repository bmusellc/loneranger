//
//  DebugReaderViewController.h
//  Unity-iOS Simulator
//
//  Created by Yuriy Belozerov on 08.06.13.
//
//

#import <UIKit/UIKit.h>

@interface DebugReaderViewController : UIViewController

- (id)initWithNumber:(NSInteger)number;
@end
