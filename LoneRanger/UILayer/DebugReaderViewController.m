//
//  DebugReaderViewController.m
//  Unity-iOS Simulator
//
//  Created by Yuriy Belozerov on 08.06.13.
//
//

#import "DebugReaderViewController.h"

#import <QuartzCore/QuartzCore.h>

#import "IssuesManager.h"
#import "Issue.h"
#import "PageInfo.h"
#import "FrameInfo.h"
#import "BubbleInfo.h"


typedef enum{
    kDebugReaderViewControllerTypePage = 1,
    kDebugReaderViewControllerTypeBubble = 2
}ControllersType;

@interface DebugReaderViewController ()
{
    NSInteger                                   _issueNumber;
	UIImageView*								_imageView;
    UIImageView*                                _bubbleImageView;
    ControllersType                             _type;
	//existed data
	Issue*										_issue;
	NSMutableArray*								_pages;
	
	//paths for all images from folder
	NSMutableArray*								_images;
    NSMutableArray*                             _bubbleImages;
	
	NSInteger									_currentPageIdx;
    NSInteger                                   _currentFrameIdx;
	
	//frames
	NSMutableArray*								_frames;
	
	NSMutableArray*								_frameViews;
    NSMutableArray*                             _bubbleViews;
    
}

@end

@implementation DebugReaderViewController

- (id)initWithNumber:(NSInteger)number
{
    self = [super init];

	if (self)
	{
        _currentFrameIdx = 0;
        _issueNumber = number;
        _type = kDebugReaderViewControllerTypePage;
		_issue = [[IssuesManager sharedManager].issues[_issueNumber - 1] retain];
		_pages = [_issue.pages retain];
		
		_images = [NSMutableArray new];
		
        NSString* path = [NSString stringWithFormat:@"issues/Issue%d/pages/fullsize",_issueNumber];
		NSArray* images = [[NSBundle mainBundle] pathsForResourcesOfType:@"jpg" inDirectory:path];
		
		[images enumerateObjectsUsingBlock:^(NSString* path, NSUInteger idx, BOOL *stop) {
			
			if ([path rangeOfString:@"@2x"].location == NSNotFound)
			{
				[_images addObject:path];
			}
		}];
		
        
        _bubbleImages = [NSMutableArray new];
        NSString* directory = [NSString stringWithFormat:@"issues/Issue%d/pages/bubbleImages/raw",_issueNumber];
        NSArray* bubbleImages = [[NSBundle mainBundle] pathsForResourcesOfType:@"png" inDirectory:directory];
        
        [bubbleImages enumerateObjectsUsingBlock:^(NSString* path, NSUInteger idx, BOOL *stop) {
            
            if ([path rangeOfString:@"@2x"].location == NSNotFound)
            {
                [_bubbleImages addObject:path];
            }
        }];
        
        
		_currentPageIdx = 0;
    }
    
	return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

	self.view.backgroundColor = [UIColor grayColor];
	
	_imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 674, 1024)];
	[self.view addSubview:_imageView];
    
    _bubbleImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 674, 1024)];
    [self.view addSubview:_bubbleImageView];
	
	CGFloat btnWidth = 60;
	
	UIButton* nextBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
	[self.view addSubview:nextBtn];
	nextBtn.frame = CGRectMake(768 - btnWidth - 10, 10, btnWidth, btnWidth);
	[nextBtn setTitle:@">" forState:UIControlStateNormal];
	[nextBtn addTarget:self action:@selector(_nextButtonDidPressed) forControlEvents:UIControlEventTouchUpInside];
	
	UIButton* prevBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
	[self.view addSubview:prevBtn];
	prevBtn.frame = CGRectMake(CGRectGetMinX(nextBtn.frame), CGRectGetMaxY(nextBtn.frame) + 10, btnWidth, btnWidth);
	[prevBtn setTitle:@"<" forState:UIControlStateNormal];
	[prevBtn addTarget:self action:@selector(_previousButtonDidPressed) forControlEvents:UIControlEventTouchUpInside];
	
	UIButton* addBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
	[self.view addSubview:addBtn];
	addBtn.frame = CGRectMake(CGRectGetMinX(nextBtn.frame), CGRectGetMaxY(prevBtn.frame) + 40, btnWidth, btnWidth);
	[addBtn setTitle:@"+" forState:UIControlStateNormal];
	[addBtn addTarget:self action:@selector(_addButtonDidPressed) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton* addBtnBubble = [UIButton buttonWithType:UIButtonTypeRoundedRect];
	[self.view addSubview:addBtnBubble];
	addBtnBubble.frame = CGRectMake(CGRectGetMinX(nextBtn.frame), CGRectGetMaxY(addBtn.frame) + 30, btnWidth, btnWidth);
	[addBtnBubble setTitle:@"+B" forState:UIControlStateNormal];
	[addBtnBubble addTarget:self action:@selector(_addButtonBubbleDidPressed) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton* minBubbleBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
	[self.view addSubview:minBubbleBtn];
	minBubbleBtn.frame = CGRectMake(CGRectGetMinX(nextBtn.frame) - 5, CGRectGetMaxY(addBtnBubble.frame) + 10, btnWidth / 2, btnWidth);
	[minBubbleBtn setTitle:@"<" forState:UIControlStateNormal];
	[minBubbleBtn addTarget:self action:@selector(_minButtonBubbleDidPressed) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton* maxBubbleBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
	[self.view addSubview:maxBubbleBtn];
	maxBubbleBtn.frame = CGRectMake(CGRectGetMaxX(minBubbleBtn.frame) + 5, CGRectGetMaxY(addBtnBubble.frame) + 10, btnWidth / 2, btnWidth);
	[maxBubbleBtn setTitle:@">" forState:UIControlStateNormal];
	[maxBubbleBtn addTarget:self action:@selector(_maxButtonBubbleDidPressed) forControlEvents:UIControlEventTouchUpInside];

    [self _updateCurrentPageContents];
}

- (void)_nextButtonDidPressed
{
	[self _save];
	
	if (_currentPageIdx < _images.count - 1)
	{
		_currentPageIdx++;
		_currentFrameIdx = 0;
		[self _updateCurrentPageContents];
	}
}

- (void)_previousButtonDidPressed
{
	[self _save];
	
	if (_currentPageIdx > 0)
	{
		_currentPageIdx--;
		_currentFrameIdx = 0;
		[self _updateCurrentPageContents];
	}
}

- (void)_minButtonBubbleDidPressed
{
    PageInfo* page = _pages[_currentPageIdx];
    if ((_currentFrameIdx - 1 >= page.frames.count) || _currentFrameIdx - 1 < 0)
    {
        return;
    }
    else
    {
        _currentFrameIdx--;
        for (UIView* nextframes in _frameViews) {
            nextframes.backgroundColor = [UIColor colorWithRed:1.0 green:0.0 blue:0.0 alpha:0.35];
        }
        UIView* frame = _frameViews[_currentFrameIdx];
        frame.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:1.0 alpha:0.35];
    }
}

- (void)_maxButtonBubbleDidPressed
{
    PageInfo* page = _pages[_currentPageIdx];
    if (_currentFrameIdx + 1 >= page.frames.count)
    {
        return;
    }
    else
    {
        _currentFrameIdx++;
        for (UIView* nextframes in _frameViews) {
            nextframes.backgroundColor = [UIColor colorWithRed:1.0 green:0.0 blue:0.0 alpha:0.35];
        }
        UIView* frame = _frameViews[_currentFrameIdx];
        frame.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:1.0 alpha:0.35];
    }
}

- (void)_addButtonBubbleDidPressed
{
    BubbleInfo* bubbleInfo = [BubbleInfo new];
	
    [bubbleInfo setValue:[NSMutableArray new] forKey:@"speeches"];
	
    FrameInfo* frame =  _frames[_currentFrameIdx];
	[frame.bubbles addObject:bubbleInfo];
    
	UIView* bubbleView = [self _bubbleView];
	bubbleView.frame = CGRectMake(20, 20, 100, 100);
    
}

- (void)_addButtonDidPressed
{
	FrameInfo* frameInfo = [FrameInfo new];
	
	[frameInfo setValue:[NSMutableArray new] forKey:@"bubbles"];
	[frameInfo setValue:[NSMutableArray new] forKey:@"ambientSounds"];
	
	[_frames addObject:frameInfo];
	UIView* frameView = [self _frameView];
    frameView.clipsToBounds = YES;
	frameView.frame = CGRectMake(284, 412, 200, 200);
}


- (UIView*)_bubbleView
{
	UIView* bubbleView = [[UIView new] autorelease];
    UIView* frameView = _frameViews[_currentFrameIdx];
	[frameView addSubview:bubbleView];
	
    if (_currentFrameIdx >= _bubbleViews.count)
    {
        for (int i = _bubbleViews.count - 1; i < _currentFrameIdx; i++) {
            [_bubbleViews addObject:[NSMutableArray new]];
        }
    }
    
    NSMutableArray* frameBubbles = [_bubbleViews objectAtIndex:_currentFrameIdx];
    
    [frameBubbles addObject:bubbleView];
    
    UILabel* nubber = [[UILabel alloc] init];
    nubber.frame = CGRectMake(5, 5, 100, 25);
    nubber.backgroundColor = [UIColor clearColor];
    nubber.textColor = [UIColor blackColor];
    nubber.text = [NSString stringWithFormat:@"%d",frameBubbles.count];
    [bubbleView addSubview:nubber];
    
	bubbleView.backgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.15];
	bubbleView.layer.borderWidth = 1;
	bubbleView.layer.borderColor = [UIColor whiteColor].CGColor;
	
	UIPanGestureRecognizer* panRecognizer = [[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(_handlePanGestureRecognizer:)] autorelease];
	[bubbleView addGestureRecognizer:panRecognizer];
	
	return bubbleView;
}


- (UIView*)_frameView
{
	UIView* frameView = [[UIView new] autorelease];
	[self.view addSubview:frameView];
	[_frameViews addObject:frameView];
	
    UILabel* nubber = [[UILabel alloc] init];
    nubber.frame = CGRectMake(5, 5, 100, 25);
    nubber.backgroundColor = [UIColor clearColor];
    nubber.textColor = [UIColor whiteColor];
    nubber.text = [NSString stringWithFormat:@"%d",_frameViews.count];
    [frameView addSubview:nubber];

	frameView.backgroundColor = [UIColor colorWithRed:1.0 green:0.0 blue:0.0 alpha:0.35];
	frameView.layer.borderWidth = 1;
	frameView.layer.borderColor = [UIColor redColor].CGColor;
	
	UIPanGestureRecognizer* panRecognizer = [[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(_handlePanGestureRecognizer:)] autorelease];
	[frameView addGestureRecognizer:panRecognizer];
	
	return frameView;
}



- (void)_updateCurrentPageContents
{
	NSString* imagePath = _images[_currentPageIdx];
	
	_imageView.image = [UIImage imageWithContentsOfFile:imagePath];
    
    if (_currentPageIdx < _bubbleImages.count)
    {
        NSString* path = _bubbleImages[_currentPageIdx];
        UIImage* rawImage = [UIImage imageWithContentsOfFile:path];
        _bubbleImageView.image =rawImage;
    }
    else{
        _bubbleImageView.image = nil;
    }

	
	__block PageInfo* targetPageInfo = nil;
	
	[_pages enumerateObjectsUsingBlock:^(PageInfo* pageInfo, NSUInteger idx, BOOL *stop) {
		
		if ([imagePath.lastPathComponent isEqualToString:pageInfo.fullsizeBackgroundPath.lastPathComponent])
		{
			targetPageInfo = pageInfo;
			*stop = YES;
		}
	}];
	
	if (!targetPageInfo)
	{
		targetPageInfo = [PageInfo new];
		
		[targetPageInfo setValue:@(0) forKey:@"pageType"];
		[targetPageInfo setValue:[NSMutableArray new] forKey:@"frames"];
		[targetPageInfo setValue:[@"pages/preview" stringByAppendingPathComponent:imagePath.lastPathComponent] forKey:@"previewBackgroundPath"];
		[targetPageInfo setValue:[@"pages/fullsize" stringByAppendingPathComponent:imagePath.lastPathComponent] forKey:@"fullsizeBackgroundPath"];
		
		[_pages addObject:targetPageInfo];
	}
	
	[self _updateFramesForPageInfo:targetPageInfo];
    
    _currentFrameIdx = 0;
    PageInfo* page = _pages[_currentPageIdx];
    if ((_currentFrameIdx < page.frames.count) && (_currentFrameIdx > 0))
    {
        for (UIView* nextframes in _frameViews) {
            nextframes.backgroundColor = [UIColor colorWithRed:1.0 green:0.0 blue:0.0 alpha:0.35];
        }
        UIView* frame = _frameViews[_currentFrameIdx];
        frame.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:1.0 alpha:0.35];
    }
}

- (void)_updateBubblesForFrameInfo:(FrameInfo*)frameInfo
{
    NSMutableArray* _bubbles = [frameInfo.bubbles retain];

	if (!_bubbles)
	{
		_bubbles = [NSMutableArray new];
	}

	
	[_bubbles enumerateObjectsUsingBlock:^(BubbleInfo* bubbleInfo, NSUInteger idx, BOOL *stop) {
		
		UIView* bubbleView = [self _bubbleView];
        CGSize size = CGSizeMake(40, 40);
		if ([bubbleInfo.imagePath length] > 0)
        {
            NSString* pngPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0] stringByAppendingPathComponent:@"bubbles"];
            NSString* newPath = [bubbleInfo.imagePath lastPathComponent];
            
            pngPath = [pngPath stringByAppendingPathComponent:newPath];
            
            UIImage* image = [UIImage imageWithContentsOfFile:pngPath];
            size = image.size;
            size.width /= _bubbleImageView.image.size.width /_bubbleImageView.frame.size.width;
            size.height /= _bubbleImageView.image.size.height /_bubbleImageView.frame.size.height;
        }
        
        CGPoint pointInFrame = CGPointMake(bubbleInfo.origin.x * _bubbleImageView.frame.size.width, bubbleInfo.origin.y * _bubbleImageView.frame.size.height);
        UIView* _frameView = _frameViews[_currentFrameIdx];
        pointInFrame = [_frameView convertPoint:pointInFrame fromView:_bubbleImageView];
        
		CGRect frame = CGRectMake(pointInFrame.x,
								  pointInFrame.y,
								  size.width,
								  size.height);
		
		bubbleView.frame = frame;
	}];
}

- (void)_updateFramesForPageInfo:(PageInfo*)pageInfo
{
	_frames = [pageInfo.frames retain];
	
	if (!_frames)
	{
		_frames = [NSMutableArray new];
	}
	
    [_bubbleViews enumerateObjectsUsingBlock:^(NSMutableArray* array, NSUInteger idx, BOOL *stop) {
       [array makeObjectsPerformSelector:@selector(removeFromSuperview)];
    }];
    [_bubbleViews release];
    _bubbleViews = [NSMutableArray new];
    
	[_frameViews makeObjectsPerformSelector:@selector(removeFromSuperview)];
	_frameViews = [NSMutableArray new];
    
	[_frames enumerateObjectsUsingBlock:^(FrameInfo* frameInfo, NSUInteger idx, BOOL *stop) {
		_currentFrameIdx = idx;
        
		UIView* frameView = [self _frameView];
		frameView.clipsToBounds = YES;
		CGRect frame = CGRectMake(frameInfo.rect.origin.x * _imageView.frame.size.width,
								  frameInfo.rect.origin.y * _imageView.frame.size.height,
								  frameInfo.rect.size.width * _imageView.frame.size.width,
								  frameInfo.rect.size.height * _imageView.frame.size.height);
		
		frameView.frame = frame;
        [self _updateBubblesForFrameInfo:frameInfo];
	}];
    _currentFrameIdx = 0;
}

- (void)_handlePanGestureRecognizer:(UIPanGestureRecognizer*)sender
{
	CGPoint loc = [sender locationInView:sender.view];
	CGPoint trans = [sender translationInView:sender.view];
	
	CGRect sourceFrame = sender.view.frame;
	
	BOOL isEdge = NO;
	
	if (loc.x < 20)
	{
		isEdge = YES;
		
		sourceFrame.origin.x += trans.x;
		sourceFrame.size.width -= trans.x;
	}
	else if (loc.x > sourceFrame.size.width - 20)
	{
		isEdge = YES;
		
		sourceFrame.size.width += trans.x;
	}
	
	if (loc.y < 20)
	{
		isEdge = YES;
		
		sourceFrame.origin.y += trans.y;
		sourceFrame.size.height -= trans.y;
	}
	else if (loc.y > sourceFrame.size.height - 20)
	{
		isEdge = YES;
		
		sourceFrame.size.height += trans.y;
	}
	
	if (!isEdge)
	{
		sourceFrame.origin.x += trans.x;
		sourceFrame.origin.y += trans.y;
	}
	
	sender.view.frame = sourceFrame;
	
	[sender setTranslation:CGPointZero inView:sender.view];
}

- (void)_save
{
    __block NSInteger bubbleIdx = 0;
    [_frames enumerateObjectsUsingBlock:^(FrameInfo* frameInfo, NSUInteger idx, BOOL *stop) {
		
		UIView* frameView = _frameViews[idx];
		
		CGRect newRect = CGRectMake(frameView.frame.origin.x / _imageView.frame.size.width,
									frameView.frame.origin.y / _imageView.frame.size.height,
									frameView.frame.size.width / _imageView.frame.size.width,
									frameView.frame.size.height / _imageView.frame.size.height);
		[frameInfo setValue:[NSValue valueWithCGRect:newRect] forKey:@"rect"];
        
        if(idx < _bubbleViews.count)
        {
            NSLog(@"%d %d",idx, _bubbleViews.count);
            NSMutableArray* bubbles = [_bubbleViews objectAtIndex:idx];
            [frameInfo.bubbles enumerateObjectsUsingBlock:^(BubbleInfo* bubbleInfo, NSUInteger idx, BOOL *stop) {
                UIView* bubbleView = bubbles[idx];
                               
//                NSLog(@"%f %f %f %f ",  _bubbleImageView.image.size.width, _bubbleImageView.frame.size.width, _bubbleImageView.image.size.height, _bubbleImageView.frame.size.height);
                CGPoint truePoint = [_bubbleImageView convertPoint:bubbleView.frame.origin fromView:frameView];
                CGRect newRect = CGRectMake(truePoint.x * _bubbleImageView.image.size.width /_bubbleImageView.frame.size.width,
                                            truePoint.y * _bubbleImageView.image.size.height /_bubbleImageView.frame.size.height,
                                            bubbleView.frame.size.width * _bubbleImageView.image.size.width /_bubbleImageView.frame.size.width,
                                            bubbleView.frame.size.height * _bubbleImageView.image.size.height /_bubbleImageView.frame.size.height);
                
                CGPoint point = CGPointMake(newRect.origin.x / _bubbleImageView.image.size.width,
                                            newRect.origin.y / _bubbleImageView.image.size.height);
                [bubbleInfo setValue:[NSValue valueWithCGPoint:point] forKey:@"origin"];

                CGImageRef cgImage = CGImageCreateWithImageInRect(_bubbleImageView.image.CGImage, newRect);
                UIImage* newImage = [UIImage imageWithCGImage:cgImage];
                NSString* pngPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0] stringByAppendingPathComponent:@"bubbles"];
                [[NSFileManager defaultManager] createDirectoryAtPath: pngPath
                                          withIntermediateDirectories: YES
                                                           attributes: nil
                                                                error: nil];
                NSString* newPath = [NSString stringWithFormat:@"p%d_%d.png",_currentPageIdx+1,bubbleIdx+1];
                pngPath = [pngPath stringByAppendingPathComponent:newPath];
//                newPath = [@"pages/bubbleImages" stringByAppendingPathComponent:newPath];
                
                [bubbleInfo setValue:newPath forKey:@"imagePath"];
                [UIImagePNGRepresentation(newImage) writeToFile:pngPath atomically:YES];
                bubbleIdx++;
            }];
        }
	}];
	
	NSMutableArray* pages = [NSMutableArray array];
	
	[_pages enumerateObjectsUsingBlock:^(PageInfo* pageInfo, NSUInteger idx, BOOL *stop) {
		
		[pages addObject:[pageInfo dictionaryRepresentation]];
	}];
	
	NSDictionary* issuePages = @{@"pages" : pages};
	
	[issuePages writeToFile:[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0] stringByAppendingPathComponent:@"test.plist"] atomically:YES];
}

@end
