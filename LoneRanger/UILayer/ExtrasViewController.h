//
//  ExtrasViewController.h
//  Unity-iOS Simulator
//
//  Created by Vladislav Bakuta on 9/4/13.
//
//

#import <UIKit/UIKit.h>
#import <Social/Social.h>

@interface ExtrasViewController : UIViewController <UIScrollViewDelegate>

@end
