//
//  ExtrasViewController.m
//  Unity-iOS Simulator
//
//  Created by Vladislav Bakuta on 9/4/13.
//
//

#import "ExtrasViewController.h"
#import "AutoScrollView.h"
#import "SocialManager.h"
#import "MenuLabel.h"

@interface ExtrasViewController ()
{
	AutoScrollView*			_scrollView;
	
    UIButton*               _gameCentreButton;
    UIButton*               _facebookButton;
    UIButton*               _twitterButton;
    
    CAGradientLayer*        _maskLayer;
    
    MenuLabel*              aboutLabel;
    MenuLabel*              versionLabel;
    MenuLabel*              aboutTextLabel;
}

@end

@implementation ExtrasViewController

- (void)dealloc
{	
    [_gameCentreButton          release];
    [_facebookButton            release];
    [_twitterButton             release];
	
	[_scrollView release];
	[_maskLayer release];
    
    [aboutLabel release];
    [aboutTextLabel release];
    [versionLabel release];
    [super                      dealloc];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	self.view.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.75];
    UIImageView* backgroundView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"menu/Extras/backgroundExtras.png"]] autorelease];
    [self.view addSubview:backgroundView];
    
    UIImage* backBtnImg = [UIImage imageNamed:@"menu/LevelSelection/backButton.png"];
	UIButton* backButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[self.view addSubview:backButton];
	backButton.frame = CGRectMake(0, 0, backBtnImg.size.width, backBtnImg.size.height);
	[backButton setImage:backBtnImg forState:UIControlStateNormal];
	[backButton addTarget:self action:@selector(_backButtonDidPressed) forControlEvents:UIControlEventTouchUpInside];
    
//    UIImage* btnImg         = [UIImage imageNamed:@"menu/Extras/gamecenter.png"];
//    UIImage* btnHlImg       = [UIImage imageNamed:@"menu/Extras/gamecenterHighlighted.png"];
//	_gameCentreButton       = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
//	_gameCentreButton.frame = CGRectMake(680, 540, btnImg.size.width, btnImg.size.height);
//	[_gameCentreButton setBackgroundImage:btnImg forState:UIControlStateNormal];
//	[_gameCentreButton setBackgroundImage:btnHlImg forState:UIControlStateHighlighted];
//	[_gameCentreButton addTarget:self action:@selector(_backButtonDidPressed) forControlEvents:UIControlEventTouchUpInside];
//        [self.view addSubview:_gameCentreButton];
//    
//    
//        btnImg         = [UIImage imageNamed:@"menu/Extras/facebook.png"];
//        btnHlImg       = [UIImage imageNamed:@"menu/Extras/facebookHighlighted.png"];
//	_facebookButton       = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
//	_facebookButton.frame = CGRectMake(739, 610, btnImg.size.width, btnImg.size.height);
//	[_facebookButton setBackgroundImage:btnImg forState:UIControlStateNormal];
//	[_facebookButton setBackgroundImage:btnHlImg forState:UIControlStateHighlighted];
//	[_facebookButton addTarget:self action:@selector(postOnFacebook:) forControlEvents:UIControlEventTouchUpInside];
//        [self.view addSubview:_facebookButton];
//    
//        btnImg         = [UIImage imageNamed:@"menu/Extras/twitter.png"];
//        btnHlImg       = [UIImage imageNamed:@"menu/Extras/twitterHighlighted.png"];
//	_twitterButton       = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
//	_twitterButton.frame = CGRectMake(767, 673, btnImg.size.width, btnImg.size.height);
//	[_twitterButton setBackgroundImage:btnImg forState:UIControlStateNormal];
//	[_twitterButton setBackgroundImage:btnHlImg forState:UIControlStateHighlighted];
//	[_twitterButton addTarget:self action:@selector(postOnTwitter:) forControlEvents:UIControlEventTouchUpInside];
//        [self.view addSubview:_twitterButton];
    
    aboutLabel = [[MenuLabel menuLabelWithFontType:MenuLabelFontTypeWeswood fontSize:32] retain];
    [aboutLabel setTextAlignment:NSTextAlignmentCenter];
    [aboutLabel setFrame:CGRectMake(92, 390, 425, 325)];
    
    [aboutLabel setTextColor:[UIColor colorWithRed:255.0 / 255.0 green:252.0 /
                                        255.0 blue:232.0 / 255.0 alpha:1.0]];
    [aboutLabel setText:@"ABOUT"];
    [self.view addSubview:aboutLabel];
    
    versionLabel = [[MenuLabel menuLabelWithFontType:MenuLabelFontTypeSuplexmentary fontSize:20] retain];
    [versionLabel setTextAlignment:NSTextAlignmentCenter];
    [versionLabel setFrame:CGRectMake(90, 415, 425, 325)];
    [versionLabel setTextColor:[UIColor colorWithRed:255 / 255.0f green:233 /
                                        255.0f blue:39 / 255.0f alpha:1.0f]];
    [versionLabel setText:[NSString stringWithFormat:@"THE LONE RANGER V%@", [self appCurrentVersion]]];
    [self.view addSubview:versionLabel];
    
    aboutTextLabel = [[MenuLabel menuLabelWithFontType:MenuLabelFontTypeSuplexmentary fontSize:15] retain];
    [aboutTextLabel setTextAlignment:NSTextAlignmentCenter];
    [aboutTextLabel setFrame:CGRectMake(40, 555, 525, 225)];
    
    [aboutTextLabel setTextColor:[UIColor colorWithRed:255.0 / 255.0 green:252.0 /
                                        255.0 blue:232.0 / 255.0 alpha:1.0]];
    [aboutTextLabel attributedText];
    [aboutTextLabel setLineBreakMode:UILineBreakModeWordWrap];
    [aboutTextLabel setNumberOfLines:8];
	[self.view addSubview:aboutTextLabel];
    
    [aboutTextLabel setText:[[NSString stringWithFormat:
                             @"Copyright 2013 DreamWorks Classics™. Published \n"
                             @"by Dynamite Entertainment®, in partnership with Lm/nl, \n"
                             @"a bMuse company. THE LONE RANGER associated \n"
                             @"character names, images, and other indicia \n"
                             @"are trademarks of and copyrighted \n"
                             @"by DreamWorks Classics™. Digital content \n"
                             @"used by permission from Dynamite Entertainment®. \n"
                             @"All rights reserved."] uppercaseString]];
    
    MenuLabel* creditsLabel = [MenuLabel menuLabelWithFontType:MenuLabelFontTypeWeswood fontSize:32];
    [creditsLabel setTextAlignment:NSTextAlignmentCenter];
    [creditsLabel setFrame:CGRectMake(450, 105, 120, 45)];
    [creditsLabel setTextColor:[UIColor colorWithRed:255.0 / 255.0 green:252.0 /
                              255.0 blue:232.0 / 255.0 alpha:1.0]];
    [creditsLabel setText:@"CREDITS"];
    [self.view addSubview:creditsLabel];
    
    _scrollView = [[AutoScrollView alloc] initWithFrame:self.view.bounds];
    [_scrollView setFrame:CGRectMake(40, 150, 940, 340)];
    [_scrollView setContentSize:CGSizeMake(940, 1760)];
    [_scrollView setDelegate:self];
    [self.view addSubview:_scrollView];
	_scrollView.showsVerticalScrollIndicator = _scrollView.showsHorizontalScrollIndicator = NO;
    
    _maskLayer = [[CAGradientLayer layer] retain];
    
    CGColorRef outerColor = [UIColor colorWithWhite:1.0 alpha:0.0].CGColor;
    CGColorRef innerColor = [UIColor colorWithWhite:1.0 alpha:1.0].CGColor;
    
    _maskLayer.colors = [NSArray arrayWithObjects:(id)outerColor, (id)innerColor, (id)innerColor, (id)outerColor, nil];
    _maskLayer.locations = [NSArray arrayWithObjects:[NSNumber numberWithFloat:0.0],
                            [NSNumber numberWithFloat:0.2],
                            [NSNumber numberWithFloat:0.8],
                            [NSNumber numberWithFloat:1.0], nil];
    
    _maskLayer.bounds = _scrollView.bounds;
    _maskLayer.anchorPoint = CGPointZero;
    _scrollView.layer.mask = _maskLayer;
    
    MenuLabel* creditsTextLabel = [MenuLabel menuLabelWithFontType:MenuLabelFontTypeSuplexmentary fontSize:24];
    [creditsTextLabel setTextAlignment:NSTextAlignmentCenter];
    [creditsTextLabel setFrame:CGRectMake(70, 20, 800, 1760)];
    [creditsTextLabel setLineBreakMode:UILineBreakModeWordWrap];
    [creditsTextLabel setNumberOfLines:0];
    [creditsTextLabel setTextColor:[UIColor colorWithRed:255.0 / 255.0 green:252.0 /
                                255.0 blue:232.0 / 255.0 alpha:1.0]];
    [_scrollView addSubview:creditsTextLabel];
    [creditsTextLabel setText:[NSString stringWithFormat:
                               @"Lm/nl \n \n"
                               @"Josh Shabtai, Co-Founder and Game Director"    @"\n Anthony Vasquez, Lead Producer/Lead QA Engineer \n"
                               @"Davis Allen, Producer/Narrative Design"        @"\n Jeremy Shabtai, Producer/Level Design \n"
                               @"Vitaliy Kolomoets, Development Team Lead"      @"\n Artem Manzhura, Senior Developer \n"
                               @"Yuriy Belozerov, Senior Developer"             @"\n Igor Kosanovsky, Art Director \n"
                               @"Evgeniy Sarychev, 2D Artist"                   @"\n Olga Dubskih, 2D Artist \n"
                               @"Yulia Packhomova, 2D Artist"                   @"\n Yulia Kotenko, 2D Artist \n"
                               @"Valentin Savenko, 2D Artist"                   @"\n Maria Kovtun, 2D Artist \n"
                               @"Andrew Akulov, 3D Artist"                      @"\n Andrew Malik, VFX Artist \n"
                               @"Bill Kates, Sound and Music Designer"          @"\n Eric Larsen, Voice Over Talent \n"
                               @"Leo Vaillancourt-Matsouka, Voice Over Talent"  @"\n Andy Barnett, Voice Over Talent \n"
                               @"Scott Reyns, Voice Over Talent"                @"\n Erin Olivia, Voice Over Talent \n"
                               @"Ken Abernathy, Voice Over Talent \n \n"
                               @"DreamWorks Classics \n \n"
                               @"Eric Ellenbogen, Co-CEO and Founder"           @"\n John Engelman, Co-CEO and Founder \n"
                               @"Nicole Blake, EVP, Global Marketing & Consumer Products"
                                                                                @"\n Colin McLaughlin, Sr. Licensing Manager \n"
                               @"Scott Shillet, Licensing Coordinator \n \n"
                               @"Dynamite Entertainment \n \n"
                               @"Brett Matthews, Writer"                        @"\n Sergio Cariello, Artist \n"
                               @"Dean White, Colorist"                          @"\n John Cassaday, Cover Artist & Art Direction \n"
                               @"Simon Bowland, Lettering \n \n"
                               @"Dynamite - Special Thanks to: \n \n"
                               @"Leslie Levine"                                 @"\n Stephanie Gonzalez \n"
                               @"Lynn Kim"                                      @"\n Kim Anthony Jones \n"
                               @"Tony Knight"                                   @"\n Evan Baily \n"
                               @"Mike Weiss"                                    @"\n John Fraser"]];
}

- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
    
    aboutLabel.center = CGPointMake(self.view.center.x, aboutLabel.center.y);
    aboutTextLabel.center = CGPointMake(self.view.center.x, aboutTextLabel.center.y);
    versionLabel.center = CGPointMake(self.view.center.x, versionLabel.center.y);
	[_scrollView startAutoScrolling];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
	
	[_scrollView stopAutoScrolling];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [CATransaction begin];
    [CATransaction setDisableActions:YES];
    _maskLayer.position = CGPointMake(0, scrollView.contentOffset.y);
    [CATransaction commit];
}

- (void)_backButtonDidPressed
{
	[UIView animateWithDuration:0.5
					 animations:^{
						 self.view.alpha = 0.0;
					 }
					 completion:^(BOOL finished) {
						 
						 [self.view removeFromSuperview];
						 [self removeFromParentViewController];
					 }];
}

- (NSString *)appCurrentVersion
{
    NSString *majorVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
        return majorVersion;
}

-(void)postOnFacebook:(id)sender
{
     SocialManager* socialManager = [SocialManager sharedManager];
    [socialManager postOnFacebook:self];
}

- (void)postOnTwitter:(id)sender
{
     SocialManager* socialManager = [SocialManager sharedManager];
    [socialManager postOnTwitter:self];
}

@end
