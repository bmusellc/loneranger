//
//  MainMenuNavigationController.m
//  Unity-iPhone
//
//  Created by Artem Manzhura on 4/24/13.
//
//

#import "MainMenuNavigationController.h"

@interface MainMenuNavigationController ()

@end

@implementation MainMenuNavigationController

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}

@end
