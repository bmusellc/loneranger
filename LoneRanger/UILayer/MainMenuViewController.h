//
//  MainMenuViewController.h
//  Unity-iPhone
//
//  Created by Artem Manzhura on 4/23/13.
//
//

#import <UIKit/UIKit.h>

typedef enum{
        kMainMenuViewControllerControlHiddenState,
        kMainMenuViewControllerControlRevealedState
}MainMenuViewControllerControlState;

@interface MainMenuViewController : UIViewController

@end
