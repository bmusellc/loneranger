//
//  MainMenuViewController.m
//  Unity-iPhone
//
//  Created by Artem Manzhura on 4/23/13.
//
//


#define BUTTONS_OFFSET_X    20
#define BUTTONS_OFFSET_Y    170

#import "MainMenuViewController.h"
#import "BackgroundVideoView.h"
#import "IssuesPanel.h"
#import "ARCIssueViewController.h"
#import "UnityLevelsLoader.h"
//#include "iPhone_View.h"
#import "ReaderNavigationController.h"

#import "LevelSelectionMenuView.h"
#import "ArmoryViewController.h"
#import "ExtrasViewController.h"
#import "MainMenuNavigationController.h"
#import "IssuesManager.h"
#import "LevelsManager.h"

#import "OptionsMenuView.h"
#import "ReaderSettingsView.h"

#import "AlertView.h"
#import "WaypointAnimationView.h"

#import "PurchasesManager.h"

#import "GameCenterManager.h"

typedef enum
{
	MainMenuButtonTagIssues,
//	MainMenuButtonTagPlay,
//	MainMenuButtonTagArmory,
	MainMenuButtonTagExtras,
	MainMenuButtonTagSettings,
}
MainMenuButtonTag;

@interface MainMenuViewController () <BackgroundVideoViewDelegate, IssuesPanelDelegate, LevelSelectionViewDelegate>
{
	AVAudioPlayer*							_menuBgMusicPlayer;
	
	NSMutableArray*							_menuButtons;
	UIView*									_buttonsView;
	
	IssuesPanel*							_issuesPanel;
	
    BackgroundVideoView*                    _backgroundVideoView;
    MainMenuViewControllerControlState      _controlState;
    NSArray*                                _issues;
	
	WaypointAnimationView*					_waypointAnimationView;
	UIView*									_blackCoverView;
}

@end

@implementation MainMenuViewController

- (BOOL)prefersStatusBarHidden
{
	return YES;
}

- (void)dealloc
{
    [_menuButtons release];
    [_buttonsView release];
    [_issues release];
	[_issuesPanel release];
    [_backgroundVideoView release];
	
    [super dealloc];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	[[PurchasesManager sharedManager] loadPurchasableItems];
    
//    [[GameCenterManager sharedManager] authenticateLocalPlayerWithViewController:self];
	
    self.view.backgroundColor = [UIColor whiteColor];
    _controlState = kMainMenuViewControllerControlHiddenState;
    
    _backgroundVideoView = [[BackgroundVideoView alloc] initWithFrame:self.view.bounds playIntroVideo:YES];
	_backgroundVideoView.delegate = self;
	[self.view addSubview:_backgroundVideoView];
	
	_issuesPanel = [[IssuesPanel alloc] initWithFrame:self.view.bounds];
	[self.view addSubview:_issuesPanel];
	_issuesPanel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	_issuesPanel.delegate = self;
	_issuesPanel.hidden = YES;
	
	_buttonsView = [[UIView alloc] initWithFrame:CGRectMake(50, /*100,*/ 180, 236, 350)];
	[_issuesPanel addSubview:_buttonsView];
	_buttonsView.hidden = YES;
    
	_menuButtons = [NSMutableArray new];
	
	NSArray* buttonImages = @[@"menu/MainMenu/issuesButton.png", /*@"menu/MainMenu/playButton.png", @"menu/MainMenu/armoryButton.png",*/ @"menu/MainMenu/extrasButton.png", @"menu/MainMenu/settingsButton.png"];
	
	[buttonImages enumerateObjectsUsingBlock:^(NSString* imageName, NSUInteger idx, BOOL *stop) {
		
		UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
		[_menuButtons addObject:button];
		[_buttonsView addSubview:button];
		
		UIImage* normalStateImage = [UIImage imageNamed:imageName];
		UIImage* highlightedStateImage = [UIImage imageNamed:[imageName stringByReplacingOccurrencesOfString:@".png" withString:@"Highlighted.png"]];
		UIImage* selectedStateImage = [UIImage imageNamed:[imageName stringByReplacingOccurrencesOfString:@".png" withString:@"Selected.png"]];
		[button setBackgroundImage:normalStateImage forState:UIControlStateNormal];
		[button setBackgroundImage:highlightedStateImage forState:UIControlStateHighlighted];
		[button setBackgroundImage:selectedStateImage forState:UIControlStateSelected];
		
		button.frame = CGRectMake(0, normalStateImage.size.height * idx, normalStateImage.size.width, normalStateImage.size.height);
		[button addTarget:self action:@selector(_menuButtonDidPressed:) forControlEvents:UIControlEventTouchUpInside];
		button.tag = (MainMenuButtonTag)idx;
		button.exclusiveTouch = YES;
	}];
	
	_menuBgMusicPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:[[NSBundle mainBundle] URLForResource:@"menuBgMusic" withExtension:@"mp3"] error:nil];
	_menuBgMusicPlayer.volume = 0.5;
	_menuBgMusicPlayer.numberOfLoops = -1;
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(onBecomeActive)
												 name:UIApplicationDidBecomeActiveNotification
											   object:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
	
#if !TARGET_IPHONE_SIMULATOR
    [_menuBgMusicPlayer play];
#endif
	
    _backgroundVideoView.frame = self.view.bounds;
    [_backgroundVideoView play];
}

- (void)backgroundVideoViewIntroVideoItemAlmostFinishPlaying:(BackgroundVideoView*)videoView
{
	_issuesPanel.hidden = NO;
	_buttonsView.hidden = NO;
	_buttonsView.alpha = 0.0;
	
	[UIView animateWithDuration:0.3
					 animations:^{
						 
						 _buttonsView.alpha = 1.0;
					 }];
}

- (void)onBecomeActive
{
    [_backgroundVideoView play];
}

- (void)onLevelLoaded
{
	//showUnityInView(self.view);
}

- (void)onLevelUnloaded
{
	[self _animateGameLevelUnloading];
	[_menuBgMusicPlayer play];
}

- (void)showArmoryOverUnity
{
	ArmoryViewController* ctrl = [[ArmoryViewController new] autorelease];
	[self addChildViewController:ctrl];
	[self.view addSubview:ctrl.view];
	
	ctrl.view.frame = self.view.bounds;
	ctrl.view.alpha = 0.0;
	
	[UIView animateWithDuration:0.5
					 animations:^{
						 
						 ctrl.view.alpha = 1.0;
					 }];
}

#pragma mark - IssuesPanelDelegate

- (void)issuesPanel:(IssuesPanel *)sender didSelectIssueWithIndex:(NSInteger)index
{
    [_menuBgMusicPlayer pause];
    _menuBgMusicPlayer.currentTime = 0.0;
	
    ARCIssueViewController *ctrl = [[[ARCIssueViewController alloc] initWithIssue:[[IssuesManager sharedManager].issues objectAtIndex:index]] autorelease];
    ReaderNavigationController* navCtrl = [[[ReaderNavigationController alloc] initWithRootViewController:ctrl] autorelease];
	navCtrl.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self.navigationController presentViewController:navCtrl animated:YES completion:^{
		
		[_backgroundVideoView stop];
	}];
}

#pragma mark - LevelSelectionViewDelegate

- (void)levelSelectionView:(LevelSelectionMenuView *)view levelSelectionCellDidPressed:(LevelCellView *)cellView
{
	[self _loadGameLevel:cellView.levelInfo];
	[_menuBgMusicPlayer pause];
}

#pragma mark - Internal implementation

- (void)_menuButtonDidPressed:(UIButton*)sender
{
	switch (sender.tag)
	{
		case MainMenuButtonTagIssues:
			[self _issuesButtonDidPressed:sender];
			break;
			
/*		case MainMenuButtonTagPlay:
			[self _playButtonDidPressed:sender];
			break;

		case MainMenuButtonTagArmory:
			[self _armoryButtonDidPressed:sender];
			break;
 */
		case MainMenuButtonTagExtras:
			[self _extrasButtonDidPressed:sender];
			break;
			
		case MainMenuButtonTagSettings:
			[self _settingsButtonDidPressed:sender];
			break;
			
		default:
			break;
	}
}

- (void)_issuesButtonDidPressed:(UIButton*)sender
{
    if (!sender.selected)
    {
        [_issuesPanel showPanel];
    }
    else
	{
        [_issuesPanel hidePanel];
    }
    
    sender.selected = !sender.selected;
}

- (void)_playButtonDidPressed:(UIButton*)sender
{
	[self _hideIssuesPanelIfNeeded];
	
	LevelSelectionMenuView* view = [[LevelSelectionMenuView new] autorelease];
	[self.view addSubview:view];
	view.frame = self.view.bounds;
	view.delegate = self;
	[view animateAppearance];
}

- (void)_armoryButtonDidPressed:(UIButton*)sender
{
	[self _hideIssuesPanelIfNeeded];
	
	ArmoryViewController* ctrl = [[ArmoryViewController new] autorelease];
	[self addChildViewController:ctrl];
	[self.view addSubview:ctrl.view];
	
	ctrl.view.frame = self.view.bounds;
	ctrl.view.alpha = 0.0;
	
	[UIView animateWithDuration:0.5
					 animations:^{
						 
						 ctrl.view.alpha = 1.0;
					 }];
}

- (void)_extrasButtonDidPressed:(UIButton*)sender
{
	[self _hideIssuesPanelIfNeeded];
    
    ExtrasViewController* ctrl = [[ExtrasViewController new] autorelease];
	[self addChildViewController:ctrl];
	[self.view addSubview:ctrl.view];
	
	ctrl.view.frame = self.view.bounds;
	ctrl.view.alpha = 0.0;
	
	[UIView animateWithDuration:0.5
					 animations:^{
						 
						 ctrl.view.alpha = 1.0;
					 }];
}

- (void)_settingsButtonDidPressed:(UIButton*)sender
{
	[self _hideIssuesPanelIfNeeded];
	
	sender.selected = YES;
	
/*	OptionsMenuView* optionsMenu = [[OptionsMenuView new] autorelease];
	[self.view addSubview:optionsMenu];
	optionsMenu.frame = self.view.bounds;
	optionsMenu.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	
	[optionsMenu showAnimated:YES withCompletionHandler:^{
		
		sender.selected = NO;
	}];*/
	
	ReaderSettingsView* settingsView = [[[ReaderSettingsView alloc] initWithFrame:self.view.bounds] autorelease];
	[self.view addSubview:settingsView];
	settingsView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	
	[settingsView showAnimated:YES
				appearanceType:ReaderSettingsViewAppearanceTypeMainMenu
			 completionHandler:^{
				 
				 [settingsView removeFromSuperview];
				 sender.selected = NO;
			 }];
}

- (void)_hideIssuesPanelIfNeeded
{
	UIButton* issuesBtn = _menuButtons[0];
	
	if (issuesBtn.selected)
	{
		[self.view addSubview:_buttonsView];
		
		[_issuesPanel fadeOutAndCleanupPanelWithCompletionHandler:^(BOOL finished) {
			
			[_issuesPanel addSubview:_buttonsView];
		}];
		
		issuesBtn.selected = NO;
	}
}

#pragma mark - Game Levels

- (void)_loadGameLevel:(GameLevelInfo*)levelInfo
{
	_blackCoverView = [[[UIView alloc] initWithFrame:self.view.bounds] autorelease];
	[self.view addSubview:_blackCoverView];
	_blackCoverView.backgroundColor = [UIColor blackColor];
	_blackCoverView.alpha = 0.0;
	
	[UIView animateWithDuration:0.5
					 animations:^{
						 
						 _blackCoverView.alpha = 1.0;
					 }];
	
	[[UnityLevelsLoader sharedLoader] loadLevelWithIndex:levelInfo.levelIndex
									   forIssueWithIndex:1
											 forConsumer:(NSObject<UnityLevelsLoaderDelegate>*)self];
}

- (void)_animateGameLevelUnloading
{
	[UIView animateWithDuration:0.5
					 animations:^{
						 
						 _blackCoverView.alpha = 0.0;
					 }
					 completion:^(BOOL finished) {
						 
						 [_blackCoverView removeFromSuperview];
					 }];
}

@end
