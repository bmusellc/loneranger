//
//  TrailerPreviewViewController.m
//  Unity-iOS Simulator
//
//  Created by Dmitry Panin on 6/21/13.
//
//

#import "TrailerPreviewViewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import <CoreMotion/CoreMotion.h>
#import <QuartzCore/QuartzCore.h>
#import "TrailerVideoViewController.h"
#import "MainMenuViewController.h"
#import "VideoView.h"

#define VIEW_KEY		@"view"
#define PARALLAX_KEY	@"parallax"
#define CENTER_KEY		@"center"

#define TRAILER_PREVIEW_START_TIME	8.5f

typedef enum
{
	kTrailerPreviewViewControllerActionWatchTrailer,
	kTrailerPreviewViewControllerActionPlayTheGame,
	kTrailerPreviewViewControllerActionFindTickets,
	
} TrailerPreviewViewControllerAction;

@interface TrailerPreviewViewController()
{
	VideoView*					_videoView;
	VideoView*					_trailerPreview;
	NSMutableArray*				_parallaxActiveElements;
	CMMotionManager*			_motionManager;
	NSTimer*					_parallaxUpdateTimer;
	
	CGPoint						_overallRotation;
	
	AVAudioPlayer*				_backgroundMusicPlayer;
	
	UIView*						_blackOverlayView;
}
@end

@implementation TrailerPreviewViewController

- (void)dealloc
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	[_parallaxUpdateTimer invalidate];
	[_motionManager stopGyroUpdates];
	[_motionManager release];
	[_videoView removeFromSuperview];
	[_videoView.player pause];
	[_videoView release];
	[_parallaxActiveElements release];
	[_blackOverlayView release];
	[super dealloc];
}

- (void)viewDidLoad
{
	_parallaxActiveElements = [[NSMutableArray alloc] init];
	_motionManager = [[CMMotionManager alloc] init];
	
	NSURL* backgroundMusicURL = [[NSBundle mainBundle] URLForResource:@"trailerBackground" withExtension:@"mp3" subdirectory:@"menu/IntroScreen/"];
	_backgroundMusicPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL: backgroundMusicURL error: nil];
	_backgroundMusicPlayer.volume = 0.20;
	_backgroundMusicPlayer.numberOfLoops = -1;
	
	//video background
	_videoView = [[VideoView alloc] initWithFrame:self.view.bounds];
	_videoView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	[self.view addSubview:_videoView];
	
	NSURL* videoURL = [[NSBundle mainBundle] URLForResource:@"trailerBackground" withExtension:@"mp4" subdirectory:@"menu/IntroScreen/"];
	_videoView.player = [AVPlayer playerWithPlayerItem:[AVPlayerItem playerItemWithURL: videoURL]];
	_videoView.player.actionAtItemEnd = AVPlayerActionAtItemEndNone;
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(_backgroundPlayerItemDidReachEnd:)
												 name:AVPlayerItemDidPlayToEndTimeNotification
											   object:_videoView.player.currentItem];
	
	[_parallaxActiveElements addObject:[NSDictionary dictionaryWithObjectsAndKeys: _videoView,
										VIEW_KEY,
										[NSNumber numberWithFloat: -1.5],
										PARALLAX_KEY,
										[NSValue valueWithCGPoint: CGPointMake(512, 384)],
										CENTER_KEY,
										nil]];
	
	//tonto
	UIImage* tontoImage = [UIImage imageNamed:@"menu/IntroScreen/tonto"];
	UIImageView* tontoView = [[UIImageView alloc] initWithImage:tontoImage];
	tontoView.center = CGPointMake(486 / 2.0f, 964.0f / 2.0f);
	[self.view addSubview: tontoView];
	
	[_parallaxActiveElements addObject:[NSDictionary dictionaryWithObjectsAndKeys: tontoView,
										VIEW_KEY,
										[NSNumber numberWithFloat: 0.5],
										PARALLAX_KEY,
										[NSValue valueWithCGPoint: tontoView.center],
										CENTER_KEY,
										nil]];
	
	//lone ranger
	UIImage* loneRangerImage = [UIImage imageNamed:@"menu/IntroScreen/loneRanger"];
	UIImageView* loneRangerView = [[UIImageView alloc] initWithImage:loneRangerImage];
	loneRangerView.center = CGPointMake(1564.0f / 2.0f, 900.0f / 2.0f);
	[self.view addSubview: loneRangerView];
	
	[_parallaxActiveElements addObject:[NSDictionary dictionaryWithObjectsAndKeys: loneRangerView,
										VIEW_KEY,
										[NSNumber numberWithFloat: 0.5],
										PARALLAX_KEY,
										[NSValue valueWithCGPoint: loneRangerView.center],
										CENTER_KEY,
										nil]];
	
	//holopad
	UIImage* holopadImage = [UIImage imageNamed:@"menu/IntroScreen/holopad"];
	UIImageView* holopadVuew = [[UIImageView alloc] initWithImage:holopadImage];
	holopadVuew.center = CGPointMake(1992.0f / 2.0f, 42.0f / 2.0f);
	[self.view addSubview: holopadVuew];
	
	[_parallaxActiveElements addObject:[NSDictionary dictionaryWithObjectsAndKeys: holopadVuew,
										VIEW_KEY,
										[NSNumber numberWithFloat: 0.3],
										PARALLAX_KEY,
										[NSValue valueWithCGPoint: holopadVuew.center],
										CENTER_KEY,
										nil]];
	
	//top label
	UIImage* topLabelImage = [UIImage imageNamed:@"menu/IntroScreen/topLabel"];
	UIImageView* topLabelView = [[UIImageView alloc] initWithImage:topLabelImage];
	topLabelView.center = CGPointMake(1026.0f / 2.0f, 102.0f / 2.0f);
	[self.view addSubview: topLabelView];
	
	[_parallaxActiveElements addObject:[NSDictionary dictionaryWithObjectsAndKeys: topLabelView,
										VIEW_KEY,
										[NSNumber numberWithFloat: 0.7],
										PARALLAX_KEY,
										[NSValue valueWithCGPoint: topLabelView.center],
										CENTER_KEY,
										nil]];
	
	//title
	UIImage* titleImage = [UIImage imageNamed:@"menu/IntroScreen/title"];
	UIImageView* titleView = [[UIImageView alloc] initWithImage:titleImage];
	titleView.center = CGPointMake(1126.0f / 2.0f, 812.0f / 2.0f);
	[self.view addSubview: titleView];
	
	[_parallaxActiveElements addObject:[NSDictionary dictionaryWithObjectsAndKeys: titleView,
										VIEW_KEY,
										[NSNumber numberWithFloat: 1.2],
										PARALLAX_KEY,
										[NSValue valueWithCGPoint: titleView.center],
										CENTER_KEY,
										nil]];
	 
	//trailer
	_trailerPreview = [[VideoView alloc] initWithFrame: CGRectMake(0, 0, 814.0f / 2.0f, 370.0f / 2.0f)];
	_trailerPreview.userInteractionEnabled = YES;
	_trailerPreview.backgroundColor = [UIColor blackColor];
	UIImage* trailerMask = [UIImage imageNamed: @"menu/IntroScreen/videoMask"];
	UIImageView* trailerMaskView = [[UIImageView alloc] initWithImage: trailerMask];
	_trailerPreview.layer.mask = trailerMaskView.layer;
	
	UIImage* trailerBorder = [UIImage imageNamed: @"menu/IntroScreen/videoBorder"];
	UIImageView* trailerBorderView = [[UIImageView alloc] initWithImage: trailerBorder];
	[_trailerPreview addSubview: trailerBorderView];
	[trailerBorderView release];
	
	UITapGestureRecognizer* trailerTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTrailerPreviewTap)];
	[_trailerPreview addGestureRecognizer: trailerTapRecognizer];
	[trailerTapRecognizer release];
	
	[self.view addSubview:_trailerPreview];
	
	NSURL* trailerVideoURL = [[NSBundle mainBundle] URLForResource:@"trailer" withExtension:@"mp4" subdirectory:@"menu/IntroScreen/"];
	_trailerPreview.player = [AVPlayer playerWithPlayerItem:[AVPlayerItem playerItemWithURL: trailerVideoURL]];
	_trailerPreview.player.actionAtItemEnd = AVPlayerActionAtItemEndNone;
	_trailerPreview.center = CGPointMake(1052.0f / 2.0f, 1164.0f / 2.0f);
	
	NSArray *audioTracks = [_trailerPreview.player.currentItem.asset tracksWithMediaType:AVMediaTypeAudio];
	
	NSMutableArray *allAudioParams = [NSMutableArray array];
	for (AVAssetTrack *track in audioTracks) {
		AVMutableAudioMixInputParameters *audioInputParams =
		[AVMutableAudioMixInputParameters audioMixInputParameters];
		[audioInputParams setVolume:0.0 atTime:kCMTimeZero];
		[audioInputParams setTrackID:[track trackID]];
		[allAudioParams addObject:audioInputParams];
	}
	AVMutableAudioMix *audioMix = [AVMutableAudioMix audioMix];
	[audioMix setInputParameters:allAudioParams];
	
	[_trailerPreview.player.currentItem  setAudioMix:audioMix];
	
	[_parallaxActiveElements addObject:[NSDictionary dictionaryWithObjectsAndKeys: _trailerPreview,
										VIEW_KEY,
										[NSNumber numberWithFloat: 0.8],
										PARALLAX_KEY,
										[NSValue valueWithCGPoint: _trailerPreview.center],
										CENTER_KEY,
										nil]];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(_trailerPlayerItemDidReachEnd:)
												 name:AVPlayerItemDidPlayToEndTimeNotification
											   object:_trailerPreview.player.currentItem];
	
	//buttons
	NSMutableArray* defaultImages = [NSMutableArray arrayWithCapacity: 0];
	NSMutableArray* pressedImages = [NSMutableArray arrayWithCapacity: 0];
	NSMutableArray* actions = [NSMutableArray arrayWithCapacity: 0];
	NSMutableArray* positions = [NSMutableArray arrayWithCapacity: 0];

	[defaultImages addObject: [UIImage imageNamed: @"menu/IntroScreen/playTheGame"]];
	[pressedImages addObject: [UIImage imageNamed: @"menu/IntroScreen/playTheGamePressed"]];
	[actions addObject: [NSNumber numberWithInt: kTrailerPreviewViewControllerActionPlayTheGame]];
	[positions addObject: [NSValue valueWithCGPoint:CGPointMake(1685.0f / 2.0f, 1336.0f / 2.0f)]];
	
	[defaultImages addObject: [UIImage imageNamed: @"menu/IntroScreen/watchTrailer"]];
	[pressedImages addObject: [UIImage imageNamed: @"menu/IntroScreen/watchTrailerPressed"]];
	[actions addObject: [NSNumber numberWithInt: kTrailerPreviewViewControllerActionWatchTrailer]];
	[positions addObject: [NSValue valueWithCGPoint:CGPointMake(1052.0f / 2.0f, 1332.0f / 2.0f)]];
	
	[defaultImages addObject: [UIImage imageNamed: @"menu/IntroScreen/findTickets"]];
	[pressedImages addObject: [UIImage imageNamed: @"menu/IntroScreen/findTicketsPressed"]];
	[actions addObject: [NSNumber numberWithInt: kTrailerPreviewViewControllerActionFindTickets]];
	[positions addObject: [NSValue valueWithCGPoint:CGPointMake(1052.0f / 2.0f, 1439.0f / 2.0f)]];
	
	for(int i = 0; i < defaultImages.count; i++)
	{
		CGPoint center = [[positions objectAtIndex: i] CGPointValue];
		
		UIButton* button = [UIButton buttonWithType: UIButtonTypeCustom];
		button.center = center;
		button.tag = [[actions objectAtIndex: i] intValue];
		
		button.bounds = CGRectMake(0,
								   0,
								   [[defaultImages objectAtIndex: i] size].width,
								   [[defaultImages objectAtIndex: i] size].height);
		
		[button setImage:[defaultImages objectAtIndex: i] forState:UIControlStateNormal];
		[button setImage:[pressedImages objectAtIndex: i] forState:UIControlStateHighlighted];
		
		[button addTarget:self
				   action:@selector(onButtonPressed:)
		 forControlEvents: UIControlEventTouchUpInside];
		
		[self.view addSubview: button];
		
		[_parallaxActiveElements addObject:[NSDictionary dictionaryWithObjectsAndKeys: button,
											VIEW_KEY,
											[NSNumber numberWithFloat: 1.55],
											PARALLAX_KEY,
											[NSValue valueWithCGPoint: button.center],
											CENTER_KEY,
											nil]];
		
	}
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(onApplicationBecomeActiveNotification)
												 name:UIApplicationDidBecomeActiveNotification
											   object:nil];
	
	//black overlay view
	_blackOverlayView = [[UIView alloc] initWithFrame:self.view.bounds];
	_blackOverlayView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	_blackOverlayView.backgroundColor = [UIColor blackColor];
	_blackOverlayView.userInteractionEnabled = YES;
	[self.view addSubview: _blackOverlayView];
}

- (void)viewWillAppear:(BOOL)animated
{
	[_videoView.player play];
	[_trailerPreview.player play];
	[_trailerPreview.player seekToTime: CMTimeMakeWithSeconds(TRAILER_PREVIEW_START_TIME, 1)];
	[self tweakBackgroundVideoPosition];
	
	[_motionManager startGyroUpdates];
	[_backgroundMusicPlayer play];
	
	if(!_parallaxUpdateTimer)
	{
		_parallaxUpdateTimer = [NSTimer scheduledTimerWithTimeInterval: 1.0f / 50.0f
																target: self
															  selector: @selector(onParallaxUpdateTimer)
															  userInfo: nil
															   repeats: YES];
	}
}

- (void)viewDidDisappear:(BOOL)animated
{
	[_videoView.player pause];
	[_trailerPreview.player pause];
	[_parallaxUpdateTimer invalidate];
	_parallaxUpdateTimer = nil;
	
	[_backgroundMusicPlayer pause];
	[_motionManager stopGyroUpdates];
}

- (void)viewDidAppear:(BOOL)animated
{
	if(!_blackOverlayView.hidden)
	{
		double delayInSeconds = 1.0;
		dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
		dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
			[self hideBlackOverlay];
		});
	}
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationLandscapeLeft | UIInterfaceOrientationLandscapeRight;
}

- (void)hideBlackOverlay
{
	if(!_blackOverlayView.hidden)
	{
		{
			[UIView animateWithDuration: 0.5
							 animations:^
			 {
				 _blackOverlayView.alpha = 0.0f;
			 }
							 completion: ^(BOOL finished)
			 {
				 _blackOverlayView.hidden = YES;
			 }];
		}
	}
}

- (void)onButtonPressed:(UIButton*)sender
{
	int action = sender.tag;
	
	if(action == kTrailerPreviewViewControllerActionFindTickets)
	{
		NSURL* url = [NSURL URLWithString:@"http://www.fandango.com/theloneranger_130723/movieoverview"];
		[[UIApplication sharedApplication] openURL: url];
	}
	else if(action == kTrailerPreviewViewControllerActionPlayTheGame)
	{
		MainMenuViewController* controller = [[MainMenuViewController alloc] init];
		UINavigationController* navController = self.navigationController;
		[navController popViewControllerAnimated:NO];
		[navController pushViewController:controller animated:NO];
		[controller release];
		
		CATransition *animation = [CATransition animation];
		[animation setType:kCATransitionFade];
		[animation setDuration: 0.8f];
		[animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
		[[navController.view layer] addAnimation:animation forKey: @"Revealaniamtion"];
	}
	else if(action == kTrailerPreviewViewControllerActionWatchTrailer)
	{
		[self onTrailerPreviewTap];
	}
}

- (void)onTrailerPreviewTap
{
	TrailerVideoViewController* controller = [[TrailerVideoViewController alloc] init];
	controller.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
	
	[self presentModalViewController:controller animated: YES];
	
	[controller release];
}

- (void)tweakBackgroundVideoPosition
{
	float videoAspect = 1280.0f / 800.0f;
	float frameAspect = self.view.bounds.size.width / self.view.bounds.size.height;
	
	CGSize size = CGSizeZero;
	
	if(videoAspect > frameAspect)
	{
		size.height = self.view.bounds.size.height;
		size.width = videoAspect * size.height;
	}
	else
	{
		size.width = self.view.bounds.size.width;
		size.height = size.width / videoAspect;
	}
	
	size.width *= 1.1;
	size.height *= 1.1;
	
	_videoView.bounds = CGRectMake(0.0f, 0.0f, size.width, size.height);
	_videoView.center = CGPointMake(CGRectGetMidX(self.view.bounds), CGRectGetMidY(self.view.bounds));
}

- (void)onParallaxUpdateTimer
{
	CGPoint rot = [self orientationTweakedRoatation];
	rot.x *= 180 / M_PI / 40;
	rot.y *= 180 / M_PI / 40;
	
	_overallRotation.x += rot.y;
	_overallRotation.y += rot.x;
	
	const float horizontalRotationLimit = 25;
	const float verticalRotationLimit = 15;
	
	if(_overallRotation.x > horizontalRotationLimit)
	{
		_overallRotation.x = horizontalRotationLimit;
	}
	else if(_overallRotation.x < -horizontalRotationLimit)
	{
		_overallRotation.x = -horizontalRotationLimit;
	}
	
	if(_overallRotation.y > verticalRotationLimit)
	{
		_overallRotation.y = verticalRotationLimit;
	}
	else if(_overallRotation.y < -verticalRotationLimit)
	{
		_overallRotation.y = -verticalRotationLimit;
	}
	
	for(NSDictionary* params in _parallaxActiveElements)
	{
		UIView* view = [params objectForKey: VIEW_KEY];
		float parallax = [[params objectForKey: PARALLAX_KEY] floatValue];
		CGPoint center = [[params objectForKey: CENTER_KEY] CGPointValue];
		
		center.x += parallax * _overallRotation.x;
		center.y += parallax * _overallRotation.y;
		
		view.center = center;
	}
}

- (void)onApplicationBecomeActiveNotification
{
	[_videoView.player play];
	[_trailerPreview.player play];
}

- (CGPoint)orientationTweakedRoatation
{
	CGPoint raw;
	
	raw.x = _motionManager.gyroData.rotationRate.x;
	raw.y = _motionManager.gyroData.rotationRate.y;
	
	UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
	
	CGPoint res;
	
	switch (orientation) {
			
		case UIInterfaceOrientationPortraitUpsideDown:
			
			res.x = -raw.x;
			res.y = -raw.y;
			
			break;
			
		case UIInterfaceOrientationLandscapeLeft:
			
			res.x =  raw.y;
			res.y = -raw.x;
			
			break;
			
		case UIInterfaceOrientationLandscapeRight:
			
			res.x = -raw.y;
			res.y =  raw.x;
			
			break;
			
		default:
			
			res.x = raw.x;
			res.y = raw.y;
			
			break;    
	}
	
	return res;
}

- (void)_backgroundPlayerItemDidReachEnd:(NSNotification*)notification
{
	
	[_videoView.player.currentItem seekToTime: kCMTimeZero];
}

- (void)_trailerPlayerItemDidReachEnd:(NSNotification*)notification
{
	[_trailerPreview.player.currentItem seekToTime: CMTimeMakeWithSeconds(TRAILER_PREVIEW_START_TIME, 1)];
}


@end
