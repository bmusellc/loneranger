//
//  TrailerVideoViewController.m
//  Unity-iPhone
//
//  Created by Dmitry Panin on 6/23/13.
//
//

#import "TrailerVideoViewController.h"
#import <MediaPlayer/MediaPlayer.h>

@interface TrailerVideoViewController()
{
	MPMoviePlayerController*	_moviePlayerContoller;
}
@end

@implementation TrailerVideoViewController

- (void)dealloc
{
	[[NSNotificationCenter defaultCenter] removeObserver: self];
	[_moviePlayerContoller release];
	[super dealloc];
}

- (void)viewDidLoad
{
	//video
	NSURL* videoURL = [[NSBundle mainBundle] URLForResource:@"trailer" withExtension:@"mp4" subdirectory:@"menu/IntroScreen/"];
	_moviePlayerContoller = [[MPMoviePlayerController alloc] initWithContentURL: videoURL];
	_moviePlayerContoller.controlStyle = MPMovieControlStyleNone;
	_moviePlayerContoller.view.frame = self.view.bounds;
	_moviePlayerContoller.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	_moviePlayerContoller.scalingMode = MPMovieScalingModeAspectFit;
	_moviePlayerContoller.view.userInteractionEnabled = NO;
	[self.view addSubview: _moviePlayerContoller.view];
	
	UITapGestureRecognizer* tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
																					action:@selector(onTap)];
	[self.view addGestureRecognizer:tapRecognizer];
	[tapRecognizer release];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(onPlaybackFinished)
												 name:MPMoviePlayerPlaybackStateDidChangeNotification
											   object:_moviePlayerContoller];
}

- (void)viewWillAppear:(BOOL)animated
{
	[_moviePlayerContoller play];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[_moviePlayerContoller stop];
}

- (void)onTap
{
	[self hideViewController];
}

- (void)onPlaybackFinished
{
	if(_moviePlayerContoller.playbackState == MPMoviePlaybackStatePaused)
	{
		[self hideViewController];
	}
}

- (void)hideViewController
{
	[[NSNotificationCenter defaultCenter] removeObserver: self];
	[self dismissModalViewControllerAnimated:YES];
}

@end
